Action()
{

	/* Goto Order Tile enter Order and click on search */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_03_Search_Order_In_Order_Tile"));

	web_url("ping.jsp_4", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467027843111", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t7.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=../manh/olm/resources/icons/trigger/search_trigger.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=../manh/olm/ds/model/customertransaction/OrderSearchRequest.js?_dc=1467027853594", "Referer={pURL}/manh/index.html", ENDITEM, 
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"customerOrderSearchCriteria\">"
						"<pair name=\"entityType\" type=\"str\">All</pair>"
						"<pair name=\"orderNumber\" type=\"str\">{pOrder_No}</pair>"
						"<pair name=\"parentOrderNumber\" type=\"str\"></pair>"
						"<pair name=\"createdFromDate\" type=\"str\"></pair>"
						"<pair name=\"createdToDate\" type=\"str\"></pair>"
						"<pair name=\"noOfRecordsPerPage\" type=\"num\">10</pair>"
						"<pair name=\"customerInfo\" type=\"str\"></pair>"
						"<pair name=\"customerBasicInfo\" type=\"str\"></pair>"
						"<object name=\"sortingCriterion\">"
							"<pair name=\"sortField\" type=\"str\">createdDTTM</pair>"
							"<pair name=\"sortDirection\" type=\"str\">DESC</pair>"
						"</object>"
						"<pair name=\"currentPageNumber\" type=\"num\">0</pair>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY", 
		LAST);
	
		/* Extract Customer First Name value from response */
	web_reg_save_param("c_First_Name",
	                   "LB=customerFirstName\":\"",
	                   "RB=\"",
	                   "ORD=1",
	                   "Search=ALL",LAST);
	
	/* Extract Customer Last Name value from response */
	web_reg_save_param("c_Last_Name",
                   "LB=customerLastName\":\"",
                   "RB=\"",
                   "ORD=1",
                   "Search=ALL",LAST);
	
	/* Extract Customer Phone Number value from response */
	web_reg_save_param("c_Phone_Number",
                   "LB=\"customerPhone\":",
                   "RB=,",
                   "ORD=1",
                   "Search=ALL",LAST);
	
	/* Extract Customer Emailid value from response */
	web_reg_save_param("c_Email_Id",
                   "LB=\"customerEmail\":\"",
                   "RB=\"",
                   "ORD=1",
                   "Search=ALL",LAST);
	
	/* Extract Customer Id value from response */
	web_reg_save_param("c_externalCustomerId",
                   "LB=\"externalCustomerId\":",
                   "RB=,",
                   "ORD=1",
                   "Search=ALL",LAST);
	
	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");

	web_custom_request("customerOrderAndTransactionList", 
		"URL={pURL}/services/olm/customerorder/customerOrderAndTransactionList?_dc=1467027854068&page=1&start=0&limit=50&sort=%5B%7B%22property%22%3A%22orderCreatedDate%22%2C%22direction%22%3A%22DESC%22%7D%5D", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t8.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY}", 
		LAST);

	web_url("ping.jsp_5", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467027859805", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t9.inf", 
		"Mode=HTML", 
	/*	EXTRARES, 
		"Url=../manh/olm/ds/screen/CustomerTransactionScreen.js?_dc=1467027868461", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/CustomerTransaction.js?_dc=1467027868854", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/QuickSearchPane.js?_dc=1467027869236", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/utils/plugins/BaselineGrid.js?_dc=1467027869693", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/controller/CustomerTransactionController.js?_dc=1467027870116", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/CustomerPaymentInfoModel.js?_dc=1467027872127", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/CreditCardPaymentInfosModel.js?_dc=1467027872612", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/ECheckPaymentInfosModel.js?_dc=1467027873031", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/GiftCardPaymentInfosModel.js?_dc=1467027873657", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/AccountTypeModel.js?_dc=1467027874075", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/CustomerTypeModel.js?_dc=1467027874490", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/CardTypeModel.js?_dc=1467027874908", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/CustomerDetailsModel.js?_dc=1467027875326", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/CustomerPreferenceModel.js?_dc=1467027875743", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/basedata/ShipViaModel.js?_dc=1467027876163", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/item/ItemUOMModel.js?_dc=1467027876581", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customertransaction/CustomerTransactionModel.js?_dc=1467027876996", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/model/CustomerFullDetailsModel.js?_dc=1467027877414", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/ShipmentModel.js?_dc=1467027878460", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/ShipmentOrderLineModel.js?_dc=1467027878886", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/DODetailModel.js?_dc=1467027879252", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/DOLineDetailModel.js?_dc=1467027879718", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/ShipmentDetailModel.js?_dc=1467027880087", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/CartonModel.js?_dc=1467027880459", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/CartonDetailModel.js?_dc=1467027880861", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/ShipmentDetailsTranslator.js?_dc=1467027881228", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/model/customerorder/invoice/InvoiceDetailsModel.js?_dc=1467027882535", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/model/customerorder/invoice/InvoiceLineDetailsModel.js?_dc=1467027882954", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/model/customerorder/invoice/Invoice.js?_dc=1467027883376", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customertransaction/POSTransactionModel.js?_dc=1467027883787", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customertransaction/POSTransactionLineModel.js?_dc=1467027884204", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/chargedetail/ChargeDetailModel.js?_dc=1467027884573", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/model/asn/ASNModel.js?_dc=1467027884948", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/model/asn/ASNDetailModel.js?_dc=1467027885937", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/model/asn/ASNUnitDetailModel.js?_dc=1467027886398", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/model/customerorder/orderline/SplitLineModel.js?_dc=1467027887389", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/screen/OrderStatusScreenConstants.js?_dc=1467027887868", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/screen/CustomerTransactionScreenConstants.js?_dc=1467027888247", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/CustomerOrderStatusDetails.js?_dc=1467027889109", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/COStatusSummary.js?_dc=1467027889537", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customerorder/override/ReasonCodePopUp.js?_dc=1467027890072", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/components/Popup.js?_dc=1467027890586", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/utils/plugins/CloseOnEscape.js?_dc=1467027890992", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/COStatusHeaderDetails.js?_dc=1467027891419", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/COStatusLineDetails.js?_dc=1467027891841", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/COStatusLineItemDetails.js?_dc=1467027892222", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/components/chart/DonutChart.js?_dc=1467027892672", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/COStatusLineHeader.js?_dc=1467027893096", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/COStatusLine.js?_dc=1467027893509", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/COStatusLineCharge.js?_dc=1467027893928", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/COInvoiceSummary.js?_dc=1467027894346", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/COOtherCharges.js?_dc=1467027894762", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/COPaymentInfo.js?_dc=1467027895181", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/COPaymentOptions.js?_dc=1467027895600", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/COAdditionalCardDetails.js?_dc=1467027896010", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/components/SearchField.js?_dc=1467027896582", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/components/EmbeddedLink.js?_dc=1467027897059", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/components/HyperLink.js?_dc=1467027897473", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/platform/util/plugins/Ellipsis.js?_dc=1467027902688", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/order/OrderList.js?_dc=1467027903643", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/OrderSearchCriteria.js?_dc=1467027904996", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/order/OrderHeader.js?_dc=1467027906165", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/order/OrderDetail.js?_dc=1467027906575", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/order/lineitem/OrderLineHeader.js?_dc=1467027906955", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/order/lineitem/OrderLineDetail.js?_dc=1467027907915", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/order/OrderListAndSearchCriteria.js?_dc=1467027911288", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/order/shipment/ShipmentDetails.js?_dc=1467027911663", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/order/shipment/ShipmentDetailHeader.js?_dc=1467027912049", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/order/shipment/ShipmentLine.js?_dc=1467027912418", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/order/shipment/ShipmentDetail.js?_dc=1467027912833", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/order/lineitem/OrderLine.js?_dc=1467027913225", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/OrderTransaction.js?_dc=1467027913663", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/CustomerTransactionReturnLineTileDetail.js?_dc=1467027914092", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/ReturnTransaction.js?_dc=1467027914505", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/CustomerTransactionDetail.js?_dc=1467027914931", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/view/shipping/ViewAddress.js?_dc=1467027915300", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/ShipmentDetail.js?_dc=1467027915688", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/ShipmentOrderLine.js?_dc=1467027916151", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/PlannedShipmentBasicAttributes.js?_dc=1467027916524", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/ShipmentBasicAttributes.js?_dc=1467027916888", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/COPaymentAndSettlement.js?_dc=1467027917252", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/InvoiceChargeSummary.js?_dc=1467027917625", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/InvoiceHeaderDetails.js?_dc=1467027918050", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/InvoiceLineDetails.js?_dc=1467027918469", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/ReturnStatus.js?_dc=1467027918902", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/StatusHeader.js?_dc=1467027919282", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/customertransaction/return/shipment/ShipmentDetails.js?_dc=1467027919641", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/customertransaction/return/shipment/ShipmentDetailHeader.js?_dc=1467027920045", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/customertransaction/return/shipment/InboundShipment.js?_dc=1467027920445", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/customertransaction/return/shipment/InboundShipmentHeader.js?_dc=1467027920873", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/customertransaction/return/shipment/InboundShipmentLine.js?_dc=1467027921504", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/customertransaction/return/shipment/OutboundShipment.js?_dc=1467027921922", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/screen/ReturnStatusScreenLabels.js?_dc=1467027922290", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/OrderTransactionHeader.js?_dc=1467027922674", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/model/returns/ReturnOrderLineModel.js?_dc=1467027923072", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/StatusDetails.js?_dc=1467027923503", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/StatusSummary.js?_dc=1467027923909", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/lineitem/ReturnLine.js?_dc=1467027925574", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/lineitem/UnExpectedLineDetail.js?_dc=1467027926000", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/ChargeDetailsModel.js?_dc=1467027926420", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/customertransaction/return/ReturnList.js?_dc=1467027926831", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/customertransaction/return/ReturnListAndSearchCriteria.js?_dc=1467027927217", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/customertransaction/return/ReturnDetail.js?_dc=1467027927720", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/customertransaction/return/lineitem/ReturnLineHeader.js?_dc=1467027928187", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/customertransaction/return/lineitem/ReturnLineDetail.js?_dc=1467027928603", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/customertransaction/return/lineitem/ReturnLine.js?_dc=1467027929023", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/customertransaction/return/ReturnShippingInfo.js?_dc=1467027929434", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/customertransaction/return/ReturnTotals.js?_dc=1467027929858", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/customertransaction/return/ReturnOrderHeader.js?_dc=1467027930249", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/lineitem/ReturnLineGroup.js?_dc=1467027930696", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/lineitem/ReturnLineHeader.js?_dc=1467027931108", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/lineitem/LineDetail.js?_dc=1467027931532", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/lineitem/LineTotals.js?_dc=1467027931949", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/lineitem/ExchangeLineHeader.js?_dc=1467027932370", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/utils/plugins/PhoneFormatter.js?_dc=1467027932954", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/lineitem/ExchangeLineDetail.js?_dc=1467027933332", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/OrderTotals.js?_dc=1467027934276", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/postransaction/POSTransactionHeader.js?_dc=1467027934987", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/postransaction/POSTransaction.js?_dc=1467027935394", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/postransaction/lineitem/POSTransactionLine.js?_dc=1467027935811", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/postransaction/lineitem/POSTransactionLineHeader.js?_dc=1467027936221", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/postransaction/lineitem/POSTransactionLineDetail.js?_dc=1467027936647", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/postransaction/POSTransactionTotal.js?_dc=1467027937069", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/shipment/Shipment.js?_dc=1467027937485", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/shipment/InboundShipment.js?_dc=1467027937899", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/shipment/InboundShipmentHeader.js?_dc=1467027938270", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/shipment/OutboundShipment.js?_dc=1467027938951", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/shipment/InboundShipmentLine.js?_dc=1467027939362", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/invoice/Invoice.js?_dc=1467027940713", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/invoice/InvoiceDetails.js?_dc=1467027941091", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/InvoiceTotals.js?_dc=1467027941478", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/components/AmountField.js?_dc=1467027941871", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/InvoiceLineTotals.js?_dc=1467027942251", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customertransaction/PaymentTransactions.js?_dc=1467027942624", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/invoice/InvoiceLineTotals.js?_dc=1467027942994", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/invoice/InvoiceTotals.js?_dc=1467027943445", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/popup/notes/Notes.js?_dc=1467027943858", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/popup/notes/NoteList.js?_dc=1467027944246", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/popup/notes/Note.js?_dc=1467027944798", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/variance/ReceiptVariance.js?_dc=1467027945216", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/variance/ReceiptVarianceDetail.js?_dc=1467027945632", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/variance/VarianceHeader.js?_dc=1467027946040", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/variance/VarianceLine.js?_dc=1467027946470", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/lineitem/ExchangeLine.js?_dc=1467027946889", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/lineitem/ExchangeReturnLine.js?_dc=1467027947269", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returnstatus/ResendLabelPopUp.js?_dc=1467027947723", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/customerorder/HoldAndCancel.js?_dc=1467027948131", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/store/customerorder/OrderStore.js?_dc=1467027948671", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/store/customerorder/CustomerPaymentInfoStore.js?_dc=1467027949072", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/store/customerorder/AccountTypeStore.js?_dc=1467027949488", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/store/customerorder/CustomerTypeStore.js?_dc=1467027950116", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/store/customerorder/CardTypeStore.js?_dc=1467027950480", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/store/customerorder/CustomerPreferenceStore.js?_dc=1467027950943", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/store/customerorder/ReasonCodeStore.js?_dc=1467027951309", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/model/ReasonCodeModel.js?_dc=1467027951788", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/store/basedata/ShipViaStore.js?_dc=1467027952208", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/store/OrderTypesStore.js?_dc=1467027952572", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/OrderTypesModel.js?_dc=1467027952956", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/store/item/ItemUOMStore.js?_dc=1467027953357", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/store/customerorder/DeleteLinesStore.js?_dc=1467027953772", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/store/customerorder/GiftCardStore.js?_dc=1467027955229", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/store/CustomerFullDetailsStore.js?_dc=1467027955653", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/store/returns/GetReturnOrderStore.js?_dc=1467027956069", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/store/customertransaction/POSTransactionStore.js?_dc=1467027956488", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/store/returns/ReturnOrderStore.js?_dc=1467027957076", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/store/returns/ReSendReturnLabelStore.js?_dc=1467027957441", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/utils/tools/Constants.js?_dc=1467027957803", "Referer={pURL}/manh/index.html", ENDITEM, */
		LAST);
	
	lr_end_transaction(lr_eval_string("{sTestCaseName}_03_Search_Order_In_Order_Tile"),LR_AUTO);
	
	lr_think_time(1);
	
	/* Click on Order Searched */

	lr_start_transaction(lr_eval_string("{sTestCaseName}_04_Open_Order_Details"));

	web_custom_request("windows", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14271276&regionId=-1&businessUnitId=-1", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t10.inf", 
		"Mode=HTML", 
		"EncType=", 
	/*	EXTRARES, 
		"Url=../UIConfigService/uiconfig?_dc=1467027958199&screenId=280030&windowId=screen-14271276", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../UIConfigService/uiconfig?_dc=1467027958200&screenId=280032&windowId=screen-14271276", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../UIConfigService/uiconfig?_dc=1467027958200&screenId=280034&windowId=screen-14271276", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../UIConfigService/uiconfig?_dc=1467027958201&screenId=280036&windowId=screen-14271276", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/CountryListStore.js?_dc=1467027960017", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/controller/CommonController.js?_dc=1467027960017", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/StateListStore.js?_dc=1467027960017", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/CompanyParameterListStore.js?_dc=1467027960018", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/CountryListModel.js?_dc=1467027960390", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/StateListModel.js?_dc=1467027960456", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/CompanyParameterModel.js?_dc=1467027960465", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/OrderTotalModel.js?_dc=1467027961057", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/TotalChargeModel.js?_dc=1467027961057", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/PaymentDetailModel.js?_dc=1467027961057", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/PaymentTransactionModel.js?_dc=1467027961057", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/NoteModel.js?_dc=1467027961058", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/AddressModel.js?_dc=1467027961058", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/RefFieldModel.js?_dc=1467027961058", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/PriceDetailsModel.js?_dc=1467027961058", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/ShippingInfoModel.js?_dc=1467027961058", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/OrderLineModel.js?_dc=1467027961058", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/OrderLineTotalsModel.js?_dc=1467027961058", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/AllocationDetailsModel.js?_dc=1467027961058", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/AllocationInfoModel.js?_dc=1467027961059", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/OrderLineQuantityDetailModel.js?_dc=1467027961059", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/TotalDiscountDetailsModel.js?_dc=1467027961059", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/DiscountDetailsListModel.js?_dc=1467027961059", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/DiscountDetailModel.js?_dc=1467027961059", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/AVSResponseModel.js?_dc=1467027961059", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/DiscountModel.js?_dc=1467027961059", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/TriggerWorkflowRequest.js?_dc=1467027961059", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/ContextInfoModel.js?_dc=1467027961060", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/storelocator/ItemAvailabilityModel.js?_dc=1467027961060", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/storelocator/StoreLocatorModel.js?_dc=1467027961060", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/storelocator/ItemFacilityModel.js?_dc=1467027961060", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/storelocator/FacilityInfoModel.js?_dc=1467027961060", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/storelocator/StoreDetailsModel.js?_dc=1467027961060", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/storelocator/FacilityAddressModel.js?_dc=1467027961060", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/storelocator/StoreLocatorRequest.js?_dc=1467027961060", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/item/DeliveryOptionsModel.js?_dc=1467027961061", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/NoteTypeModel.js?_dc=1467027961061", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/model/MessageModel.js?_dc=1467027961061", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/item/promotion/PromotionModel.js?_dc=1467027961061", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/RelatedEntityModel.js?_dc=1467027961061", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/model/SysCodeModel.js?_dc=1467027961061", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/shipping/LineItemShippingInfoModel.js?_dc=1467027961061", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/item/promotion/PromoProdAssocDetailsModel.js?_dc=1467027961062", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/item/promotion/PromotionDetailsModel.js?_dc=1467027961062", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/model/RefTextFieldsModel.js?_dc=1467027961062", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/item/promotion/OfferedProductsModel.js?_dc=1467027961062", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/CustomFieldModel.js?_dc=1467027961062", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/ScreenHeader.js?_dc=1467027961062", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/EditableContainer.js?_dc=1467027961062", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/MSBingMap.js?_dc=1467027961062", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/PaymentException.js?_dc=1467027961063", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/MultiSelectContainer.js?_dc=1467027961063", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/ConfirmationPopup.js?_dc=1467027961062", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/ErrorMessagePopup.js?_dc=1467027961063", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/component/BaseContainer.js?_dc=1467027961063", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/component/BaseToolBar.js?_dc=1467027961063", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/component/BaseGrid.js?_dc=1467027961063", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/component/BaseMenu.js?_dc=1467027961063", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/collapsibles/CollapsibleContainer.js?_dc=1467027961063", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/collapsibles/EditableCollapsibleContainer.js?_dc=1467027961064", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/utils/tools/OrderFormatter.js?_dc=1467027961064", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/AvailableCreditCard.js?_dc=1467027961064", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/AvailableECheck.js?_dc=1467027961064", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/AvailableGiftCard.js?_dc=1467027961064", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/ConfirmTab.js?_dc=1467027961064", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/PaymentTab.js?_dc=1467027961064", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/UsedCreditCard.js?_dc=1467027961065", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/UsedECheck.js?_dc=1467027961065", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/UsedGiftCard.js?_dc=1467027961065", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/AvailablePaymentOptions.js?_dc=1467027961065", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/UsedPaymentOptions.js?_dc=1467027961065", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/NewAddress.js?_dc=1467027961065", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/AddAddress.js?_dc=1467027961065", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/AddCreditCard.js?_dc=1467027961065", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/AddECheck.js?_dc=1467027961065", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/PaymentTitle.js?_dc=1467027961066", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/AddGiftCard.js?_dc=1467027961066", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/EditCreditCard.js?_dc=1467027961066", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/EditECheck.js?_dc=1467027961066", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/PaymentFooter.js?_dc=1467027961066", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/PaymentSummary.js?_dc=1467027961066", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/mixin/PaymentAuthorization.js?_dc=1467027961066", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/ShippingTab.js?_dc=1467027961066", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/ShippingFooter.js?_dc=1467027961066", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/Address.js?_dc=1467027961067", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/AddressSummary.js?_dc=1467027961067", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/UsedAddresses.js?_dc=1467027961067", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/AvailableShippingAddress.js?_dc=1467027961067", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/AvailableAddresses.js?_dc=1467027961067", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/AvailableStoreAddress.js?_dc=1467027961067", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/EditShippingAddress.js?_dc=1467027961067", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/AddShippingAddress.js?_dc=1467027961067", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/DeliveryOptions.js?_dc=1467027961068", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/UsedShippingAddress.js?_dc=1467027961068", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/ShippingMethod.js?_dc=1467027961068", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/LineItemShippingMethods.js?_dc=1467027961068", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/LineItemShippingMethod.js?_dc=1467027961068", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/UsedStoreAddress.js?_dc=1467027961068", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/StoreLocatorItemInfo.js?_dc=1467027961068", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/notes/Note.js?_dc=1467027961069", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/StoreLocator.js?_dc=1467027961068", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/ItemFacility.js?_dc=1467027961068", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/notes/Notes.js?_dc=1467027961069", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/notes/NoteList.js?_dc=1467027961069", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/avs/AVSSuccess.js?_dc=1467027961069", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/avs/AVSFailure.js?_dc=1467027961069", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/avs/AVSRecommendation.js?_dc=1467027961069", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/CustomerFullDetails.js?_dc=1467027961069", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/customer/AddCustomer.js?_dc=1467027961069", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/search/RelatedEntities.js?_dc=1467027961070", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/search/RelatedEntityFlyout.js?_dc=1467027961070", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/util/CustomerOrderRequestManager.js?_dc=1467027961070", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/OrderUpdateStore.js?_dc=1467027961070", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/StoreLocatorStore.js?_dc=1467027961070", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/NoteTypeStore.js?_dc=1467027961070", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/store/SysCodeStore.js?_dc=1467027961070", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/CustomerDetailsStore.js?_dc=1467027961070", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/AVSStore.js?_dc=1467027961070", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/TriggerWorkflowStore.js?_dc=1467027961071", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/RelatedEntityStore.js?_dc=1467027961071", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/SystemPropertyStore.js?_dc=1467027961071", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/component/mixin/BasicMethods.js?_dc=1467027964264", "Referer={pURL}/manh/index.html", ENDITEM, */
		LAST);
	
	/* Extract Order Id value from response */
		web_reg_save_param("c_OrderId",
                   "LB=\"orderId\":",
                   "RB=,\"orderNumber\"",
                   "ORD=1",
                   LAST);
	
	/* "orderType":"USECOM" */
			web_reg_save_param("c_OrderType",
                   "LB=\"orderType\":\"",
                   "RB=\"",
                   "ORD=1",
                   LAST); 
	

	web_url("ping.jsp_6", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467027964917", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t11.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=../manh/olm/common/view/payment/AbstractPaymentView.js?_dc=1467027964979", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../services/olm/systemProperties/getKey?_dc=1467027969007&keyName=BING_MAP_KEY", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/customer/email.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/customer/phone.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/customer/customer_id.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/search/customer.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/search/order.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/search/return.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/search/item.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/collapse/collapseArrow.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=../services/olm/customerorder/customerOrderDetails?_dc=1467027969205&responseType=FullCustomerOrder&customerOrderNumber={pOrder_No}&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/radio/radio_button.png", "Referer={pURL}/manh/olm/resources/css/modern.css", ENDITEM, 
		"Url=../manh/olm/resources/icons/collapse/downArrow.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=../manh/resources/css/gray/images/grid/hd-pop.png", "Referer={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", ENDITEM, 
		"Url=../manh/olm/resources/icons/splitter/drag_divider.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=../manh/olm/common/components/StoreLoadMask.js?_dc=1467027969210", "Referer={pURL}/manh/index.html", ENDITEM, 
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"customerOrderSearchCriteria\">"
						"<pair name=\"entityType\" type=\"str\">All</pair>"
						"<pair name=\"orderNumber\" type=\"num\">{pOrder_No}</pair>"
						"<pair name=\"parentOrderNumber\" type=\"str\"></pair>"
						"<pair name=\"createdFromDate\" type=\"str\"></pair>"
						"<pair name=\"createdToDate\" type=\"str\"></pair>"
						"<pair name=\"noOfRecordsPerPage\" type=\"num\">1000</pair>"
						"<object name=\"customerInfo\">"
							"<pair name=\"customerFirstName\" type=\"str\">{c_First_Name}</pair>"
							"<pair name=\"customerLastName\" type=\"str\">{c_Last_Name}</pair>"
							"<pair name=\"customerFullName\" type=\"str\">{c_First_Name} {c_Last_Name}</pair>"
							"<pair name=\"customerEmail\" type=\"str\">{c_Email_Id}</pair>"
							"<pair name=\"customerPhone\" type=\"str\">{c_Phone_Number}</pair>"
							"<pair name=\"customerId\" type=\"str\"></pair>"
							"<pair name=\"externalCustomerId\" type=\"str\">-76297142</pair>"
							"<pair name=\"customField1\" type=\"str\"></pair>"
							"<pair name=\"customField2\" type=\"str\"></pair>"
							"<pair name=\"customField3\" type=\"str\"></pair>"
							"<pair name=\"customField4\" type=\"str\"></pair>"
							"<pair name=\"customField5\" type=\"str\"></pair>"
							"<pair name=\"customField6\" type=\"str\"></pair>"
							"<pair name=\"customField7\" type=\"str\"></pair>"
							"<pair name=\"customField8\" type=\"str\"></pair>"
							"<pair name=\"customField9\" type=\"str\"></pair>"
							"<pair name=\"customField10\" type=\"str\"></pair>"
						"</object>"
						"<pair name=\"customerBasicInfo\" type=\"str\"></pair>"
						"<object name=\"sortingCriterion\">"
							"<pair name=\"sortField\" type=\"str\">createdDTTM</pair>"
							"<pair name=\"sortDirection\" type=\"str\">DESC</pair>"
						"</object>"
						"<pair name=\"currentPageNumber\" type=\"num\">0</pair>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_1", 
		LAST);

	web_custom_request("customerOrderAndTransactionList_2", 
		"URL={pURL}/services/olm/customerorder/customerOrderAndTransactionList?_dc=1467027969209&page=1&start=0&limit=50&sort=%5B%7B%22property%22%3A%22orderCreatedDate%22%2C%22direction%22%3A%22DESC%22%7D%5D", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t12.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_1}", 
		EXTRARES, 
		"Url=/manh/olm//common/utils/plugins/ToolTip.js?_dc=1467027970645", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=customerOrderDetails?_dc=1467027970779&responseType=FullCustomerOrder&customerOrderNumber={pOrder_No}&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/resources/icons/notes/notes.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=/doms/dom/selling/image/item/ImageNotAvailable.jpg", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/resources/icons/collapse/expandArrowWhite.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=/manh/olm/resources/icons/common/delete.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=/manh/olm/resources/icons/common/StatusClose.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=../location/countrylist?_dc=1467027971912&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"companyParameterIdList\">"
						"<array name=\"parameterList\">"
							"<item type=\"str\">CURRENCY_USED</item>"
							"<item type=\"str\">EXT_CUSTOMER_ID_GENERATION</item>"
							"<item type=\"str\">USE_DELIVERY_ZONE</item>"
							"<item type=\"str\">DEFAULT_SHIP_VIA</item>"
							"<item type=\"str\">DEFAULT_ORDER_TYPE</item>"
							"<item type=\"str\">REQUIRES_VALIDATION_FOR_PAYMENT_AUTHORIZATION</item>"
							"<item type=\"str\">REQUIRES_VALIDATION_FOR_PAYMENT_SETTLEMENT</item>"
							"<item type=\"str\">CHARGE_HANDLING_STRATEGY</item>"
							"<item type=\"str\">AUTOMATIC_PROMOTION_VALUE</item>"
							"<item type=\"str\">ITEM_PRICE_EXPIRY_DATE_EXTENSION_DAYS</item>"
							"<item type=\"str\">ITEM_PRICE_SCHEDULER_INTERVAL_IN_DAYS</item>"
							"<item type=\"str\">DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE</item>"
							"<item type=\"str\">VIEW_STORE_LEVEL_PRICES</item>"
							"<item type=\"str\">NETWORK_LEVEL_VIEW</item>"
							"<item type=\"str\">FACILITY_LEVEL_VIEW</item>"
						"</array>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_2", 
		LAST);

	web_custom_request("companyParameterList", 
		"URL={pURL}/services/olm/basedata/companyParameter/companyParameterList?_dc=1467027971913&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t13.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_2}", 
		EXTRARES, 
		"Url=../noteType/noteTypes?_dc=1467027971913&noteCategory=Instructions&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/services/olm/customerorder/accountTypes?_dc=1467027971914&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/services/atc/cvDef/getSystemProperties?_dc=1467027971914&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/services/olm/customerorder/echeckCustomerTypes?_dc=1467027971914&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/services/olm/cardTypes/cardType?_dc=1467027971915&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/resources/icons/sort/sort_desc_hover.png", "Referer={pURL}/manh/olm/resources/css/modern.css", ENDITEM, 
		"Url=/manh/olm/resources/icons/ds/customer/Hold.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_04_Open_Order_Details"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_7", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467027986311", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t14.inf", 
		"Mode=HTML", 
		LAST);

	web_url("ping.jsp_8", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467028001678", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t15.inf", 
		"Mode=HTML", 
		LAST);

	/* Goto Order search box, enter order and click on search */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_05_Search_Order_from_Order_Search_Pane"));
	
	web_url("ping.jsp_9", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467028018597", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t16.inf", 
		"Mode=HTML", 
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"customerOrderSearchCriteria\">"
						"<pair name=\"entityType\" type=\"str\">All</pair>"
						"<pair name=\"orderNumber\" type=\"str\">{pOrder_No}</pair>"
						"<pair name=\"parentOrderNumber\" type=\"str\"></pair>"
						"<pair name=\"createdFromDate\" type=\"str\"></pair>"
						"<pair name=\"createdToDate\" type=\"str\"></pair>"
						"<pair name=\"noOfRecordsPerPage\" type=\"num\">10</pair>"
						"<pair name=\"customerInfo\" type=\"str\"></pair>"
						"<pair name=\"customerBasicInfo\" type=\"str\"></pair>"
						"<object name=\"sortingCriterion\">"
							"<pair name=\"sortField\" type=\"str\">createdDTTM</pair>"
							"<pair name=\"sortDirection\" type=\"str\">DESC</pair>"
						"</object>"
						"<pair name=\"currentPageNumber\" type=\"num\">0</pair>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_3", 
		LAST);

	web_custom_request("customerOrderAndTransactionList_3", 
		"URL={pURL}/services/olm/customerorder/customerOrderAndTransactionList?_dc=1467028021947&page=1&start=0&limit=50&sort=%5B%7B%22property%22%3A%22orderCreatedDate%22%2C%22direction%22%3A%22DESC%22%7D%5D", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t17.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_3}", 
		EXTRARES, 
		"Url=/manh/olm/resources/icons/transaction/CO_icon_S.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_05_Search_Order_from_Order_Search_Pane"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_10", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467028033970", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t18.inf", 
		"Mode=HTML", 
		LAST);

	/* Click on Searched Order */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_06_Open_Order_Details"));

	web_custom_request("windows_2", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14271276", 
		"Method=DELETE", 
		"TargetFrame=", 
		"Resource=0", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t19.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=/manh/olm/ds/screen/OrderScreen.js?_dc=1467028037805", "Referer={pURL}/manh/index.html", ENDITEM, 
		LAST);

	web_custom_request("windows_3", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14271276", 
		"Method=DELETE", 
		"TargetFrame=", 
		"Resource=0", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t20.inf", 
		"Mode=HTML", 
	/*	EXTRARES, 
		"Url=/manh/olm/ds/view/customerorder/CustomerOrderScreen.js?_dc=1467028038450", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/CustomerOrderDetail.js?_dc=1467028038817", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/controller/CustomerOrderController.js?_dc=1467028039740", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/AvailableAddressesModel.js?_dc=1467028042868", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/supplybalance/SupplyBalanceDetailsModel.js?_dc=1467028043246", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/supplybalance/SupplyBalanceResultModel.js?_dc=1467028043622", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/discounts/promotions/OrderPromotionsModel.js?_dc=1467028044071", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/discounts/promotions/ItemPromotionsModel.js?_dc=1467028044428", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/discounts/promotions/PromotionEntityModel.js?_dc=1467028044784", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/discounts/promotions/PromoDetailModel.js?_dc=1467028045149", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/shipping/ShippingMethodListModel.js?_dc=1467028045508", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/shipping/ItemShippingMethodsModel.js?_dc=1467028045862", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/shipping/ShippingMethodModel.js?_dc=1467028046228", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/override/PriceOverrideHistoryModel.js?_dc=1467028046595", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/override/SnHOverrideHistoryModel.js?_dc=1467028046976", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/item/ItemFullDetailsModel.js?_dc=1467028047349", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/item/ItemDetailsModel.js?_dc=1467028047710", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/item/ItemAdditionalDetailsModel.js?_dc=1467028048153", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/item/SupportedUOMModel.js?_dc=1467028048525", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/item/DeliveryOption.js?_dc=1467028048882", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/item/ShippingMethodsModel.js?_dc=1467028049239", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/inventory/model/viewlist/AvailabilityDetailModel.js?_dc=1467028049603", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/inventory/model/viewlist/AvailabilityModel.js?_dc=1467028050843", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/inventory/model/viewlist/ViewDefinitionModel.js?_dc=1467028051218", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/CustomerOrderHeaderDetails.js?_dc=1467028051591", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/CODiscountAndCommunicationInfo.js?_dc=1467028053685", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/CustomerOrderChargeDetails.js?_dc=1467028054046", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/lineitem/COLineItemHeader.js?_dc=1467028054421", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/orderline/OrderLineGroupHeader.js?_dc=1467028059816", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/lineitem/COLineItem.js?_dc=1467028061150", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/lineitem/COLineItemDetails.js?_dc=1467028062165", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/lineitem/COItemsAndShipping.js?_dc=1467028062603", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/lineitem/CustomerOrderLineHeader.js?_dc=1467028062968", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/lineitem/CustomerOrderLineDetails.js?_dc=1467028063666", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/tabs/CODetailsTabSection.js?_dc=1467028064362", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/tabs/ConfirmOrderPaymentSummary.js?_dc=1467028064736", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/UpdateRegisteredCustomerInfo.js?_dc=1467028065106", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/RegisterCustomer.js?_dc=1467028065466", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/PreferredAddress.js?_dc=1467028065846", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/AddressView.js?_dc=1467028066212", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/DiscountDetails.js?_dc=1467028066650", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/DiscountedLine.js?_dc=1467028067011", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/DiscountedLineHeader.js?_dc=1467028067394", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/DiscountedOrder.js?_dc=1467028067755", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/DiscountsChargeSummary.js?_dc=1467028068123", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/Promotions.js?_dc=1467028068509", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/NoPromotion.js?_dc=1467028068871", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/OrderAndLinePromotions.js?_dc=1467028069253", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/OrderPromotions.js?_dc=1467028069624", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/AvailablePromotions.js?_dc=1467028070989", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/LinePromotions.js?_dc=1467028072014", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/PromotedLine.js?_dc=1467028072375", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/PromotedOrder.js?_dc=1467028072745", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/AvailablePromotionLine.js?_dc=1467028073117", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/PromotionDetail.js?_dc=1467028073816", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/item/details/OfferedItemDescription.js?_dc=1467028074188", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/appeasements/Appeasements.js?_dc=1467028074561", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/appeasements/OrderAndLineAppeasements.js?_dc=1467028075867", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/appeasements/OrderAppeasements.js?_dc=1467028076223", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/appeasements/AppeasementDetails.js?_dc=1467028076595", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/appeasements/AppliedAppeasements.js?_dc=1467028076965", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/appeasements/NewAppeasements.js?_dc=1467028077322", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/appeasements/LineAppeasements.js?_dc=1467028078029", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/override/AbstractOverride.js?_dc=1467028078407", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/override/priceoverride/PriceOverride.js?_dc=1467028078782", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/override/priceoverride/ApplyPriceOverride.js?_dc=1467028079221", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/override/priceoverride/PriceOverrideHistory.js?_dc=1467028080901", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/override/snhoverride/SnHOverride.js?_dc=1467028082188", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/override/snhoverride/ApplySnHOverride.js?_dc=1467028082552", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/override/snhoverride/SnHOverrideHistory.js?_dc=1467028082914", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/coupons/Coupons.js?_dc=1467028083283", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/coupons/CouponList.js?_dc=1467028083991", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/coupons/CouponDetail.js?_dc=1467028084431", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/coupons/CouponOrderLine.js?_dc=1467028084788", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/DeliveryOptionsStore.js?_dc=1467028085155", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/AvailableAddressesStore.js?_dc=1467028085525", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/SupplyBalanceDetailsStore.js?_dc=1467028085880", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/discounts/promotions/OrderPromotionsStore.js?_dc=1467028086241", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/discounts/promotions/ItemPromotionsStore.js?_dc=1467028086605", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/ShippingMethodsStore.js?_dc=1467028087524", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/override/PriceOverrideHistoryStore.js?_dc=1467028087891", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/override/SnHOverrideHistoryStore.js?_dc=1467028088251", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/discounts/promotions/ApplyOrderPromotionStore.js?_dc=1467028088613", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/discounts/promotions/ApplyLinePromotionStore.js?_dc=1467028089035", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/discounts/promotions/RemoveOrderPromotionStore.js?_dc=1467028089392", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/discounts/promotions/RemoveLinePromotionStore.js?_dc=1467028091843", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/item/PromotionDetailsStore.js?_dc=1467028092199", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/discounts/promotions/ApplyBXGYPromotionStore.js?_dc=1467028092560", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/inventory/store/AvailabilityDetailStore.js?_dc=1467028092918", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/inventory/store/CommerceViewDefStore.js?_dc=1467028093288", "Referer={pURL}/manh/index.html", ENDITEM, */
		LAST);

	web_custom_request("windows_4", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14272075&regionId=-1&businessUnitId=-1", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t21.inf", 
		"Mode=HTML", 
		"EncType=", 
		EXTRARES, 
		"Url=../UIConfigService/uiconfig?_dc=1467028093702&screenId=280026&windowId=screen-14272075", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/AppeasementListStore.js?_dc=1467028094228", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/AppeasementModel.js?_dc=1467028094592", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/services/olm/systemProperties/getKey?_dc=1467028094975&keyName=BING_MAP_KEY", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/services/olm/customerorder/customerOrderDetails?_dc=1467028095047&responseType=FullCustomerOrder&customerOrderNumber={pOrder_No}&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/services/olm/basedata/noteType/noteTypes?_dc=1467028095143&noteCategory=Instructions&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/services/atc/cvDef/getSystemProperties?_dc=1467028095145&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/services/olm/customerorder/accountTypes?_dc=1467028095147&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/services/olm/cardTypes/cardType?_dc=1467028095149&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/services/olm/customerorder/echeckCustomerTypes?_dc=1467028095148&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/services/olm/basedata/orderType/orderTypes?_dc=1467028095883&channelType=Customer&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/utils/plugins/InputValidator.js?_dc=1467028096003", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/utils/plugins/ZipCodeFormatter.js?_dc=1467028096965", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/TCHPLoadIsReturnableOrderStore.js?_dc=1467028097539", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/resources/icons/promotion/link_icon2.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<pair name=\"orderId\" type=\"num\">{c_OrderId}</pair>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_4", 
		LAST);

	web_custom_request("loadIsReturnableOrder", 
		"URL={pURL}/services/olm/isReturnableOrderUpdateService/loadIsReturnableOrder?_dc=1467028097925&orderId={c_OrderId}&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t22.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_4}", 
		EXTRARES, 
		"Url=/manh/olm/resources/icons/override/price_override_small.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/resources/icons/checkbox/checkboxes.png", "Referer={pURL}/manh/olm/resources/css/modern.css", ENDITEM, 
		"Url=/manh/olm/resources/icons/sort/arrow.gif", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=/manh/olm/resources/icons/trigger/trigger.png", "Referer={pURL}/manh/olm/resources/css/modern.css", ENDITEM, 
		"Url=../cardTypes/cardType?_dc=1467028098339&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../customerorder/accountTypes?_dc=1467028098340&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<pair name=\"orderId\" type=\"num\">{c_OrderId}</pair>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_5", 
		LAST);

	web_custom_request("loadIsReturnableOrder_2", 
		"URL={pURL}/services/olm/isReturnableOrderUpdateService/loadIsReturnableOrder?_dc=1467028098341&orderId={c_OrderId}&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t23.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_5}", 
		EXTRARES, 
		"Url=../customerorder/echeckCustomerTypes?_dc=1467028098340&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../customer/availableAddresses?_dc=1467028098313&customerOrderNumber={pOrder_No}&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/resources/icons/trigger/search_trigger_white.png", "Referer={pURL}/manh/olm/resources/css/modern.css", ENDITEM, 
		"Url=/manh/olm/resources/icons/common/delete_item.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=/manh/olm/resources/icons/collapse/expandArrow.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=/manh/olm/resources/icons/notes/cust_comm.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=/manh/resources/css/gray/images/tab-bar/default-plain-scroll-left.png", "Referer={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", ENDITEM, 
		"Url=/manh/resources/css/gray/images/tab-bar/default-plain-scroll-right.png", "Referer={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", ENDITEM, 
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"itemList\">"
						"<object name=\"itemNames\">"
							"<array name=\"itemName\"></array>"
						"</object>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_6", 
		LAST);

	web_custom_request("commonDeliveryOptionsForItems", 
		"URL={pURL}/services/olm/item/commonDeliveryOptionsForItems?_dc=1467028099131&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t24.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_6}", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_06_Open_Order_Details"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_11", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467028107534", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t25.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=../services/olm/customerorder/reasonCodes?_dc=1467028118817&reasonCodeTypeValue=CANCEL&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/tool-sprites.gif", "Referer={pURL}/manh/olm/resources/css/modern.css", ENDITEM, 
		LAST);

	/* Goto More and click on Cancel */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_07_Goto_More_and_click_on_cancel_order"));

	web_url("ping.jsp_12", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467028122917", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t26.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_07_Goto_More_and_click_on_cancel_order"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_13", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467028138291", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t27.inf", 
		"Mode=HTML", 
		LAST);

	/* Enter Reason, comment and click on Done */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_08_Enter_reason_comment_and_click_on_Done"));

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"customerOrder\">"
						"<pair name=\"orderNumber\" type=\"num\">{pOrder_No}</pair>"
						"<pair name=\"orderType\" type=\"str\">{c_OrderType}</pair>" /* USECOM */
						"<pair name=\"enteredLocation\" type=\"str\"></pair>"
						"<pair name=\"orderConfirmed\" type=\"bool\">true</pair>"
						"<object name=\"customerOrderNotes\">"
							"<object name=\"customerOrderNote\">"
								"<pair name=\"noteType\" type=\"str\">Cancellation</pair>"
								"<pair name=\"noteSeq\" type=\"str\"></pair>"
								"<pair name=\"noteText\" type=\"str\">{pCancellationComment}</pair>"
								"<pair name=\"visibility\" type=\"str\"></pair>"
							"</object>"
						"</object>"
						"<pair name=\"lastUpdatedDTTM\" type=\"str\">2015-12-11 03:00:35.18</pair>"
						"<pair name=\"orderCancelled\" type=\"str\">true</pair>"
						"<pair name=\"reasonCode\" type=\"str\">{pReasonCode}</pair>"
						"<pair name=\"customerPhone\" type=\"num\">{c_Phone_Number}</pair>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_7", 
		LAST);

	web_custom_request("createOrUpdate", 
		"URL={pURL}/services/olm/customerorder/createOrUpdate?_dc=1467028150676&responseType=FullCustomerOrder", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t28.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_7}", 
		EXTRARES, 
		"Url=../basedata/orderType/orderTypes?_dc=1467028152622&channelType=Customer&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<pair name=\"orderId\" type=\"num\">{c_OrderId}</pair>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_8", 
		LAST);

	web_custom_request("loadIsReturnableOrder_3", 
		"URL={pURL}/services/olm/isReturnableOrderUpdateService/loadIsReturnableOrder?_dc=1467028153059&orderId={c_OrderId}&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t29.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_8}", 
		EXTRARES, 
		"Url=../customerorder/echeckCustomerTypes?_dc=1467028153434&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../customerorder/accountTypes?_dc=1467028153433&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../cardTypes/cardType?_dc=1467028153433&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../customer/availableAddresses?_dc=1467028153408&customerOrderNumber={pOrder_No}&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<pair name=\"orderId\" type=\"num\">{c_OrderId}</pair>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_9", 
		LAST);

	web_custom_request("loadIsReturnableOrder_4", 
		"URL={pURL}/services/olm/isReturnableOrderUpdateService/loadIsReturnableOrder?_dc=1467028153434&orderId={c_OrderId}&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t30.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_9}", 
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"itemList\">"
						"<object name=\"itemNames\">"
							"<array name=\"itemName\"></array>"
						"</object>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_10", 
		LAST);

	web_custom_request("commonDeliveryOptionsForItems_2", 
		"URL={pURL}/services/olm/item/commonDeliveryOptionsForItems?_dc=1467028153894&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t31.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_10}", 
		LAST);

	web_url("ping.jsp_14", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467028153737", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t32.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_08_Enter_reason_comment_and_click_on_Done"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_15", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467028169341", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t33.inf", 
		"Mode=HTML", 
		LAST);

	web_url("ping.jsp_16", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467028184718", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t34.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=../manh/olm/common/model/EventModel.js?_dc=1467028197609", "Referer={pURL}/manh/index.html", ENDITEM, 
		LAST);

	return 0;
}
