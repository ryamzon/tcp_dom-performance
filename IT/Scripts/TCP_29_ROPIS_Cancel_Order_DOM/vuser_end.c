vuser_end()
{
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_08_Logout"));

	/* Logout */

	web_url("logout", 
		"URL={pURL}/saml/logout", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t26.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url={pURL_SSO}/images/loading.gif", "Referer={pURL_SSO}/manh/resources/css/mip.css", ENDITEM, 
		LAST);

	web_url("miplogout", 
		"URL={pURL_SSO}/miplogout", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t27.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_08_Logout"),LR_AUTO);

	return 0;
}