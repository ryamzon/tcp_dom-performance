Action()
{
	/* Goto sales Order screen Search Sales Order */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_03_Search_Order_from_sales_Order_Screen"));
	
	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");

	web_custom_request("windows", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14271272&regionId=-2&businessUnitId=-1", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t8.inf", 
		"Mode=HTML", 
		"EncType=", 
		LAST);
	
	web_reg_save_param("c_MANHCSRFToken",
	                   "LB=\"MANH-CSRFToken\" type=\"hidden\" name=\"MANH-CSRFToken\" value=\"",
	                   "RB=\"/>",
	                   "ORD=1",
	                   "Search=ALL",LAST);
	
	web_reg_save_param("c_dataTable",
                       "LB=dataTable$:$",
                       "RB=\" />",
                       "ORD=1",
                       LAST);

	web_reg_save_param("c_ViewState01",
                       "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
                       "RB=\" autocomplete=\"",
                       "ORD=1",
                       LAST);

	web_url("StoreOrderList.jsflps", 
		"URL={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271272", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t9.inf", 
		"Mode=HTML", 
		LAST);

	web_reg_save_param("c_ViewState02",
                       "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
                       "RB=\" autocomplete=\"",
                       "ORD=1",
                       LAST);
	                   
	web_reg_save_param("c_orderIDPkHiddenParam",
                   "LB=input type=\"hidden\" value=\"",
                   "RB=\" id=\"dataForm:SOList_entityListView:salesOrderData",
                   "ORD=1",
                   "Search=ALL",LAST);
	
	web_reg_save_param("c_External_ID",
                       "LB=dataForm:SOList_entityListView:salesOrderData:0:customerCode1\">",
                       "RB=</span></a><span style",
                       "ORD=1",
                       LAST);
	
	web_reg_save_param("c_inp_hdn_orderCreatedDTTM",
                   "LB=<div id='dataForm:SOList_entityListView:salesOrderData_body_tr0_td12_view' class='dshow'>&#160;",
                   "RB=</div>",
                   "ORD=1",
                   "Search=ALL",LAST);
	
	//<span id="dataForm:SOList_entityListView:salesOrderData:0:domordTyp">USECOM</span>
	
	
	web_reg_save_param("c_Order_Type",
                       "LB=<span id=\"dataForm:SOList_entityListView:salesOrderData:0:domordTyp\">",
                       "RB=</span>",
                       "ORD=1",
                       LAST);
	
	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");
	
	web_custom_request("StoreOrderList.jsflps_2", 
		"URL={pURL}/olm/salesorder/StoreOrderList.jsflps", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271272", 
		"Snapshot=t11.inf", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded; charset=UTF-8", 
		"Body=AJAXREQUEST=_viewRoot&dataForm=dataForm&uniqueToken=1&MANH-CSRFToken={c_MANHCSRFToken}&helpurlEle=%2Flcom%2Fcommon%2Fjsp%2Fhelphelper.jsp%3Fserver%3D58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B%26uri%3D%252Folm%252Fsalesorder%252FStoreOrderList.jsflps&dataForm%3AFacility_popup_Facility_Contact_dataTable_deleteHidden=&dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedRows=&"
		"dataForm%3AFacility_popup_Facility_Contact_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AsortDir=desc&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AcolCount=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableClicked=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableResized=false&Facility_popup_Facility_Contact_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs_pageallrowskey=&"
		"dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedIdList=&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs_allselectedrowskey=Facility_popup_Facility_Contact_dataTable%24%3A%24{c_dataTable}&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_deleteHidden=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedRows=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AsortDir=desc&"
		"dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AcolCount=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AtableClicked=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AtableResized=false&Facility_popup_Credit_Limit_List_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_pageallrowskey=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedIdList=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_allselectedrowskey="
		"Facility_popup_Credit_Limit_List_dataTable%24%3A%24{c_dataTable}&dataForm%3AfltrListFltrId%3AfieldName=&dataForm%3AfltrListFltrId%3AfilterName=FL_{pUserName}&dataForm%3AfltrListFltrId%3Aowner={pUserName}&dataForm%3AfltrListFltrId%3AobjectType=FL_FILTER&dataForm%3AfltrListFltrId%3AfilterObjectType=&dataForm%3AfltrListFltrId%3Afield0value1=&dataForm%3AfltrListFltrId%3Afield0=FILTER.FILTER_NAME&dataForm%3AfltrListFltrId%3Afield0operator=&dataForm%3AfltrListFltrId%3Afield1value1=&"
		"dataForm%3AfltrListFltrId%3Afield1=FILTER.IS_DEFAULT&dataForm%3AfltrListFltrId%3Afield1operator=&dataForm%3AfltrListFltrId%3Afield2value1=&dataForm%3AfltrListFltrId%3Afield2=FILTER.IS_PRIVATE&dataForm%3AfltrListFltrId%3Afield2operator=&dataForm%3AfltrListFltrId%3Afield3value1=&dataForm%3AfltrListFltrId%3Afield3=FILTER.OWNER&dataForm%3AfltrListFltrId%3Afield3operator=&dataForm%3AfltrListFltrId%3Afield4value1=&dataForm%3AfltrListFltrId%3Afield4=FILTER.IS_DELETED&"
		"dataForm%3AfltrListFltrId%3Afield4operator=&dataForm%3AfltrListFltrId%3AfltrCondition=&dataForm%3AfltrListFltrId%3AfltrCrtSel=&windowId=screen-14271272&windowId=screen-14271272&targetLink=&channelTypeHiddenParam=&fromViewPageParam=false&tabToBeSelected=&IsOrderConfirmedHiddenParam=&CustomerOrderTypeHiddenParam=&IsOrderCanceledHiddenParam=&orderNumberHiddenParam=&orderIDPkHiddenParam=&addOrderLineBtnClicked=false&fromOrder=true&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AradioSelect=quick"
		"&dataForm%3ASOList_entityListView%3ASOList_filterId2%3A_fltrExpColTxt=DONE&dataForm%3ASOList_entityListView%3ASOList_filterId2%3A_filtrExpColState=collapsed&dataForm%3ASOList_entityListView%3ASOList_filterId2%3A_filtrExpIconSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2Farrow_expand.gif&dataForm%3ASOList_entityListView%3ASOList_filterId2%3A_filtrColIconSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2Farrow_collapse.gif&dataForm%3ASOList_entityListView%3ASOList_filterId2%3A_filtrdropDownSrc="
		"%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2FarrowDown.gif&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield0=Sales%20Channel&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield0operator=%3D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubObject0=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield0value1=%5B%5D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield3=Order%20no.&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield3operator=%3D&"
		"dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubObject3=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield3value1ecId=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield3value1={pSalesOrder}&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield4=Customer&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield4operator=%3D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubObject4=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield4value1ecId=&"
		"dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield4value1=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield5=Order%20Type&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield5operator=%3D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubObject5=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield5value1=%5B%5D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield7=Status&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield7operator=%3D&"
		"dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubObject7=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield7value1=%5B%5D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield11=Destination%20Facility&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield11operator=%3D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubObject11=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield11value1ecId=&"
		"dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield11value1=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AcurrentAppliedFilterId=-1&dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_quickFilterGroupButton_mainButtonCategory=-1&dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_quickFilterGroupButton_mainButtonIndex=-1&dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_quickFilterGroupButton_changeDefault=false&"
		"dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_savedFilterGroupButton_mainButtonCategory=-1&dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_savedFilterGroupButton_mainButtonIndex=-1&dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_savedFilterGroupButton_changeDefault=false&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AdummyToGetPrefix=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Aowner={pUserName}&customParams%20="
		"windowId%3Dscreen-14271272&queryPersistParameter=%26windowId%3Dscreen-14271272&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AobjectType=DOM%20ORDER&isJSF=true&filterScreenType=ON_SCREEN&dataForm%3ASOList_entityListView%3AsalesOrderData%3Apager%3ApageInput=&dataForm%3ASOList_entityListView%3AsalesOrderData%3ApagerBoxValue=&dataForm%3ASOList_entityListView%3AsalesOrderData%3AisPaginationEvent=&dataForm%3ASOList_entityListView%3AsalesOrderData%3ApagerAction=&"
		"dataForm%3ASOList_entityListView%3AsalesOrderData_deleteHidden=&dataForm%3ASOList_entityListView%3AsalesOrderData_selectedRows=&dataForm%3ASOList_entityListView%3AsalesOrderData%3AisSortButtonClick=po.lastUpdatedDttm&dataForm%3ASOList_entityListView%3AsalesOrderData%3AsortDir=desc&dataForm%3ASOList_entityListView%3AsalesOrderData%3AcolCount=&dataForm%3ASOList_entityListView%3AsalesOrderData%3AtableClicked=&dataForm%3ASOList_entityListView%3AsalesOrderData%3AtableResized=false&"
		"dataForm%3ASOList_entityListView%3AsalesOrderData%3A0%3APK_0=DUMMYROW&orderNbrHidden=&orderIDPkHidden=&IsOrderConfirmedHidden=&CustomerOrderTypeHidden=&IsOrderCanceledHidden=&isEditable=&isEditWarning=&isAddOL=&isConfirm=&isSource=&isSourceWarning=&isAllocate=&isDOCreate=&isDORelease=&isCancel=&isReserve=&isSourceByRate=&isDeAllocate=&isHold=&isUnHold=&isMassUpdate=&isSendUpdate=&markForDeletion=&soFulfillStatus=&dataForm%3ASOList_entityListView%3AsalesOrderData%3A0%3AchannelTypeCode=&"
		"salesOrderData_hdnMaxIndexHldr=0&dataForm%3ASOList_entityListView%3AsalesOrderData_trs_pageallrowskey=&dataForm%3ASOList_entityListView%3AsalesOrderData_selectedIdList=&dataForm%3ASOList_entityListView%3AsalesOrderData_trs_allselectedrowskey=salesOrderData%24%3A%24{c_dataTable}&moreActionTargetLinksoheaderbuttons=&moreActionButtonPressedsoheaderbuttons=&backingBeanName=&javax.faces.ViewState={c_ViewState01}&fltrApplyFromQF=true&"
		"dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2apply=dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2apply&reRenderParent=SOList_rerenderPanel&fltrClientId=dataForm%3ASOList_entityListView%3ASOList_filterId2&", 
		LAST);
	
	lr_end_transaction(lr_eval_string("{sTestCaseName}_03_Search_Order_from_sales_Order_Screen"),LR_AUTO);
	
	lr_think_time(1);

	web_add_cookie("filterExpandState=true; DOMAIN=njitldomapp01.tcphq.tcpcorp.local.com");
	
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_04_Open_Order_Detail"));
	
	// web_reg_find("Text=ECOM Gift Card Article 1",LAST);
	
	
	web_reg_save_param("c_ViewState03",
                       "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
                       "RB=\" autocomplete=\"",
                       "ORD=1",
                       LAST);
	
	/* Allselectedrowskey" value="coNotesTable$:$1488875509101" / */
	
		web_reg_save_param("c_SelectedRow_Keys",
                       "LB=value=\"coNotesTable$:$",
                       "RB=\"",
                       "ORD=1",
                       LAST);
	
	/* >	CMRegPreferredPaymentInfoMenu" size="1"> <option value="7330439"> */
	
			web_reg_save_param("c_PayInfo",
                       "LB=CMRegPreferredPaymentInfoMenu\" size=\"1\">\t<option value=\"",
                       "RB=\">",
                       "ORD=1",
                       LAST); 
	
	/* name="orderVersion" value="1484848589227" /> */
	
				web_reg_save_param("c_Order_Version",
                       "LB=\"orderVersion\" value=\"",
                       "RB=\"",
                       "ORD=1",
                       LAST);
	//16%20Leach%20Hill%20Rd
	
	web_reg_save_param("c_PreferredShippingAddrMenu",
                       "LB=dataForm:CMRegPreferredShippingAddrMenu\" name=\"dataForm:CMRegPreferredShippingAddrMenu\" size=\"1\">	<option value=\"",
                       "RB=\">",
                       "ORD=1",
                       LAST);
	
		web_reg_save_param("c_PreferredBillingAddrMenu",
                       "LB=dataForm:CMRegPreferredBillingAddrMenu\" name=\"dataForm:CMRegPreferredBillingAddrMenu\" size=\"1\">	<option value=\"",
                       "RB=\">",
                       "ORD=1",
                       LAST);

	//400

	web_reg_save_param("c_inp_hdn_for_order_status",
                       "LB=<input type=\"hidden\" id=\"inp_hdn_for_order_status\" name=\"inp_hdn_for_order_status\" value=\"",
                       "RB=\" />",
                       "ORD=1",
                       LAST);
	
	web_reg_save_param("c_hdn_for_discount_code",
                       "LB=<input type=\"hidden\" id=\"hdn_for_discount_code\" name=\"hdn_for_discount_code\" value=\"",
                       "RB=\" />",
                       "ORD=1",
                       LAST);
	
	web_reg_save_param("c_createCO_fn",
                       "LB=<span id=\"dataForm:op_txt_postorder_view_createCO_fn\" class=\"captionData\">",
                       "RB=</span>",
                       "ORD=1",
                       LAST);	
	
	web_reg_save_param("c_createCO_pno",
                       "LB=<span id=\"dataForm:op_txt_postorder_view_createCO_pno\" class=\"captionData\">",
                       "RB=</span>",
                       "ORD=1",
                       LAST);	
	//ALLOCATED
	web_reg_save_param("c_inp_hdn_orderStatus",
                       "LB=<span id=\"dataForm:op_txt_postorder_view_createCO_status\" class=\"captionData\">",
                       "RB=</span>",
                       "ORD=1",
                       LAST);
	
	web_reg_save_param("c_hid_txt_postorder_view_createCO_ln",
                       "LB=<input id=\"dataForm:hid_txt_postorder_view_createCO_ln\" type=\"hidden\" name=\"dataForm:hid_txt_postorder_view_createCO_ln\" value=\"",
                       "RB=\" />",
                       "ORD=1",
                       LAST);
	
	web_reg_save_param("c_hid_txt_postorder_view_createCO_email",
                       "LB=<span id=\"dataForm:op_txt_postorder_view_createCO_email\" class=\"captionData\">",
                       "RB=</span>",
                       "ORD=1",
                       LAST);
	
	web_reg_save_param("c_updateAdditionalDetailsActive",
                       "LB=<input type=\"hidden\" id=\"additional_details_tsk\" name=\"updateAdditionalDetailsActive\" value=\"",
                       "RB=\" />",
                       "ORD=1",
                       LAST);
	
	web_reg_save_param("c_coROILTable_hdnMaxIndexHldr",
                       "LB=<input type=\"hidden\" id=\"coROILTable_hdnMaxIndexHldr\" name=\"coROILTable_hdnMaxIndexHldr\" value=\"",
                       "RB=\" />",
                       "ORD=1",
                       LAST);
	
	web_reg_save_param("c_remainingToPay",
                       "LB=<input type=\"hidden\" name=\"remainingToPay\" value=\"",
                       "RB=\" />",
                       "ORD=1",
                       LAST);
	
web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");
	
	web_submit_data("StoreOrderList.jsflps_3", 
		"Action={pURL}/olm/salesorder/StoreOrderList.jsflps", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271272", 
		"Snapshot=t13.inf", 
		"Mode=HTML", 
		ITEMDATA, 
		"Name=dataForm", "Value=dataForm", ENDITEM, 
		"Name=uniqueToken", "Value=1", ENDITEM, 
		"Name=MANH-CSRFToken", "Value={c_MANHCSRFToken}", ENDITEM, 
		"Name=helpurlEle", "Value=/lcom/common/jsp/helphelper.jsp?server=58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B&uri=%2Folm%2Fsalesorder%2FStoreOrderList.jsflps", ENDITEM, 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_deleteHidden", "Value=", ENDITEM, 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_selectedRows", "Value=", ENDITEM, 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:isSortButtonClick", "Value=", ENDITEM, 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:sortDir", "Value=desc", ENDITEM, 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:colCount", "Value=", ENDITEM, 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:tableClicked", "Value=", ENDITEM, 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:tableResized", "Value=false", ENDITEM, 
		"Name=Facility_popup_Facility_Contact_dataTable_hdnMaxIndexHldr", "Value=0", ENDITEM, 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_trs_pageallrowskey", "Value=", ENDITEM, 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_selectedRows", "Value=", ENDITEM, 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_selectedIdList", "Value=", ENDITEM, 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_trs_allselectedrowskey", "Value=Facility_popup_Facility_Contact_dataTable$:${c_dataTable}", ENDITEM, 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_deleteHidden", "Value=", ENDITEM, 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_selectedRows", "Value=", ENDITEM, 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:isSortButtonClick", "Value=", ENDITEM, 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:sortDir", "Value=desc", ENDITEM, 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:colCount", "Value=", ENDITEM, 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:tableClicked", "Value=", ENDITEM, 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:tableResized", "Value=false", ENDITEM, 
		"Name=Facility_popup_Credit_Limit_List_dataTable_hdnMaxIndexHldr", "Value=0", ENDITEM, 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_trs_pageallrowskey", "Value=", ENDITEM, 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_selectedRows", "Value=", ENDITEM, 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_selectedIdList", "Value=", ENDITEM, 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_trs_allselectedrowskey", "Value=Facility_popup_Credit_Limit_List_dataTable$:${c_dataTable}", ENDITEM, 
		"Name=dataForm:fltrListFltrId:fieldName", "Value=", ENDITEM, 
		"Name=dataForm:fltrListFltrId:filterName", "Value=FL_{pUserName}", ENDITEM, 
		"Name=dataForm:fltrListFltrId:owner", "Value={pUserName}", ENDITEM, 
		"Name=dataForm:fltrListFltrId:objectType", "Value=FL_FILTER", ENDITEM, 
		"Name=dataForm:fltrListFltrId:filterObjectType", "Value=", ENDITEM, 
		"Name=dataForm:fltrListFltrId:field0value1", "Value=", ENDITEM, 
		"Name=dataForm:fltrListFltrId:field0", "Value=FILTER.FILTER_NAME", ENDITEM, 
		"Name=dataForm:fltrListFltrId:field0operator", "Value=", ENDITEM, 
		"Name=dataForm:fltrListFltrId:field1value1", "Value=", ENDITEM, 
		"Name=dataForm:fltrListFltrId:field1", "Value=FILTER.IS_DEFAULT", ENDITEM, 
		"Name=dataForm:fltrListFltrId:field1operator", "Value=", ENDITEM, 
		"Name=dataForm:fltrListFltrId:field2value1", "Value=", ENDITEM, 
		"Name=dataForm:fltrListFltrId:field2", "Value=FILTER.IS_PRIVATE", ENDITEM, 
		"Name=dataForm:fltrListFltrId:field2operator", "Value=", ENDITEM, 
		"Name=dataForm:fltrListFltrId:field3value1", "Value=", ENDITEM, 
		"Name=dataForm:fltrListFltrId:field3", "Value=FILTER.OWNER", ENDITEM, 
		"Name=dataForm:fltrListFltrId:field3operator", "Value=", ENDITEM, 
		"Name=dataForm:fltrListFltrId:field4value1", "Value=", ENDITEM, 
		"Name=dataForm:fltrListFltrId:field4", "Value=FILTER.IS_DELETED", ENDITEM, 
		"Name=dataForm:fltrListFltrId:field4operator", "Value=", ENDITEM, 
		"Name=dataForm:fltrListFltrId:fltrCondition", "Value=", ENDITEM, 
		"Name=dataForm:fltrListFltrId:fltrCrtSel", "Value=", ENDITEM, 
		"Name=windowId", "Value=screen-14271272", ENDITEM, 
		"Name=targetLink", "Value=", ENDITEM, 
		"Name=channelTypeHiddenParam", "Value=", ENDITEM, 
		"Name=fromViewPageParam", "Value=false", ENDITEM, 
		"Name=tabToBeSelected", "Value=", ENDITEM, 
		"Name=IsOrderConfirmedHiddenParam", "Value=", ENDITEM, 
		"Name=CustomerOrderTypeHiddenParam", "Value=", ENDITEM, 
		"Name=IsOrderCanceledHiddenParam", "Value=", ENDITEM, 
		"Name=orderNumberHiddenParam", "Value=", ENDITEM, 
		"Name=orderIDPkHiddenParam", "Value=", ENDITEM, 
		"Name=addOrderLineBtnClicked", "Value=false", ENDITEM, 
		"Name=fromOrder", "Value=true", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:radioSelect", "Value=quick", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:_fltrExpColTxt", "Value=DONE", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:_filtrExpColState", "Value=collapsed", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:_filtrExpIconSrc", "Value=/lps/resources/themes/icons/mablue/arrow_expand.gif", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:_filtrColIconSrc", "Value=/lps/resources/themes/icons/mablue/arrow_collapse.gif", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:_filtrdropDownSrc", "Value=/lps/resources/themes/icons/mablue/arrowDown.gif", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field0", "Value=Sales Channel", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field0operator", "Value==", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject0", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field0value1", "Value=[]", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field3", "Value=Order no.", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field3operator", "Value==", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject3", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field3value1ecId", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field3value1", "Value={pSalesOrder}", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field4", "Value=Customer", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field4operator", "Value==", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject4", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field4value1ecId", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field4value1", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field5", "Value=Order Type", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field5operator", "Value==", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject5", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field5value1", "Value=[]", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field7", "Value=Status", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field7operator", "Value==", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject7", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field7value1", "Value=[]", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field11", "Value=Destination Facility", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field11operator", "Value==", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject11", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field11value1ecId", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field11value1", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:currentAppliedFilterId", "Value=-1", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_quickFilterGroupButton_mainButtonCategory", "Value=-1", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_quickFilterGroupButton_mainButtonIndex", "Value=-1", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_quickFilterGroupButton_changeDefault", "Value=false", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_savedFilterGroupButton_mainButtonCategory", "Value=-1", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_savedFilterGroupButton_mainButtonIndex", "Value=-1", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_savedFilterGroupButton_changeDefault", "Value=false", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:dummyToGetPrefix", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:filterId", "Value=2147483647", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:owner", "Value={pUserName}", ENDITEM, 
		"Name=customParams ", "Value=&&&", ENDITEM, 
		"Name=queryPersistParameter", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:objectType", "Value=DOM ORDER", ENDITEM, 
		"Name=isJSF", "Value=true", ENDITEM, 
		"Name=filterScreenType", "Value=ON_SCREEN", ENDITEM, 
		"Name=dataForm:SOList_entityListView:salesOrderData:pagerBoxValue", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:salesOrderData:isPaginationEvent", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:salesOrderData:pagerAction", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:salesOrderData_deleteHidden", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:salesOrderData_selectedRows", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:salesOrderData:isSortButtonClick", "Value=po.lastUpdatedDttm", ENDITEM, 
		"Name=dataForm:SOList_entityListView:salesOrderData:sortDir", "Value=desc", ENDITEM, 
		"Name=dataForm:SOList_entityListView:salesOrderData:colCount", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:salesOrderData:tableClicked", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:salesOrderData:tableResized", "Value=false", ENDITEM, 
		"Name=salesOrderData_hdnMaxIndexHldr", "Value=1", ENDITEM, 
		"Name=dataForm:SOList_entityListView:salesOrderData:0:PK_0", "Value={c_orderIDPkHiddenParam}", ENDITEM, 
		"Name=orderNbrHidden", "Value={pSalesOrder}", ENDITEM, 
		"Name=orderIDPkHidden", "Value={c_orderIDPkHiddenParam}", ENDITEM, 
		"Name=IsOrderConfirmedHidden", "Value=true", ENDITEM, 
		"Name=CustomerOrderTypeHidden", "Value=4", ENDITEM, 
		"Name=IsOrderCanceledHidden", "Value=false", ENDITEM, 
		"Name=isEditable", "Value=true", ENDITEM, 
		"Name=isEditWarning", "Value=false", ENDITEM, 
		"Name=isAddOL", "Value=true", ENDITEM, 
		"Name=isConfirm", "Value=false", ENDITEM, 
		"Name=isSource", "Value=true", ENDITEM, 
		"Name=isSourceWarning", "Value=true", ENDITEM, 
		"Name=isAllocate", "Value=false", ENDITEM, 
		"Name=isDOCreate", "Value=true", ENDITEM, 
		"Name=isDORelease", "Value=false", ENDITEM, 
		"Name=isCancel", "Value=true", ENDITEM, 
		"Name=isReserve", "Value=false", ENDITEM, 
		"Name=isSourceByRate", "Value=true", ENDITEM, 
		"Name=isDeAllocate", "Value=true", ENDITEM, 
		"Name=isHold", "Value=true", ENDITEM, 
		"Name=isUnHold", "Value=false", ENDITEM, 
		"Name=isMassUpdate", "Value=true", ENDITEM, 
		"Name=isSendUpdate", "Value=true", ENDITEM, 
		"Name=markForDeletion", "Value=false", ENDITEM, 
		"Name=soFulfillStatus", "Value=400", ENDITEM, 
		"Name=dataForm:SOList_entityListView:salesOrderData:0:channelTypeCode", "Value=20", ENDITEM, 
		"Name=dataForm:SOList_entityListView:salesOrderData_trs_pageallrowskey", "Value={c_orderIDPkHiddenParam}#:#", ENDITEM, 
		"Name=dataForm:SOList_entityListView:salesOrderData_selectedRows", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:salesOrderData_selectedIdList", "Value=", ENDITEM, 
		"Name=dataForm:SOList_entityListView:salesOrderData_trs_allselectedrowskey", "Value=salesOrderData$:${c_dataTable}", ENDITEM, 
		"Name=moreActionTargetLinksoheaderbuttons", "Value=", ENDITEM, 
		"Name=moreActionButtonPressedsoheaderbuttons", "Value=", ENDITEM, 
		"Name=backingBeanName", "Value=", ENDITEM, 
		"Name=javax.faces.ViewState", "Value={c_ViewState02}", ENDITEM, 
		"Name=dataForm:SOList_entityListView:salesOrderData:0:defaultactionbutton", "Value=dataForm:SOList_entityListView:salesOrderData:0:defaultactionbutton", ENDITEM, 
		"Name=orderIDPk", "Value={c_orderIDPkHiddenParam}", ENDITEM, 
		"Name=orderNumber", "Value={pSalesOrder}", ENDITEM, 
		"Name=channelType", "Value=20", ENDITEM, 
		"Name=IsOrderConfirmed", "Value=true", ENDITEM, 
		"Name=CustomerOrderType", "Value=4", ENDITEM, 
		"Name=IsOrderCanceled", "Value=false", ENDITEM, 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_04_Open_Order_Detail"),LR_AUTO);

	lr_think_time(1);
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_05_Click_on_Payment_Icon"));
	
	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");
	
	web_custom_request("ViewCustomerOrder.jsflps", 
		"URL={pURL}/doms/dom/selling/co/customerorder/ViewCustomerOrder.jsflps", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/xml", 
		"Referer={pURL}/olm/salesorder/StoreOrderList.jsflps", 
		"Snapshot=t24.inf", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded; charset=UTF-8", 
		"Body=AJAXREQUEST=_viewRoot&dataForm=dataForm&uniqueToken=1&MANH-CSRFToken={c_MANHCSRFToken}&helpurlEle=%2Flcom%2Fcommon%2Fjsp%2Fhelphelper.jsp%3Fserver%3D58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B%26uri%3D%252Fdoms%252Fdom%252Fselling%252Fco%252Fcustomerorder%252FViewCustomerOrder.jsflps%253FpageName%253DviewOrderDetails&dataForm%3AsellingExceptionStkTrcPage_stackTrace_inpArea=&dataForm%3AcoNotesTable_deleteHidden=&"
		"dataForm%3AcoNotesTable_selectedRows=&dataForm%3AcoNotesTable%3AisSortButtonClick=&dataForm%3AcoNotesTable%3AsortDir=desc&dataForm%3AcoNotesTable%3AcolCount=&dataForm%3AcoNotesTable%3AtableClicked=&dataForm%3AcoNotesTable%3AtableResized=false&dataForm%3AcoNotesTable%3A0%3APK_0=DUMMYROW&dataForm%3AcoNotesTable%3A0%3AcoNoteTypeColValue=%20&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteSeq=&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteToBeDeletedFlag=&dataForm%3AcoNotesTable%3A0%3AmarkForDelete=&"
		"dataForm%3AcoNotesTable%3A0%3AcoNotesColValue=&coNotesTable_hdnMaxIndexHldr=0&dataForm%3AcoNotesTable_trs_pageallrowskey=&dataForm%3AcoNotesTable_selectedIdList=&dataForm%3AcoNotesTable_trs_allselectedrowskey=coNotesTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AcoViewNotesTable_deleteHidden=&dataForm%3AcoViewNotesTable_selectedRows=&dataForm%3AcoViewNotesTable%3AisSortButtonClick=&dataForm%3AcoViewNotesTable%3AsortDir=desc&dataForm%3AcoViewNotesTable%3AcolCount=&dataForm%3AcoViewNotesTable%3AtableClicked"
		"=&dataForm%3AcoViewNotesTable%3AtableResized=false&coViewNotesTable_hdnMaxIndexHldr=0&dataForm%3AcoViewNotesTable_trs_pageallrowskey=&dataForm%3AcoViewNotesTable_selectedIdList=&dataForm%3AcoViewNotesTable_trs_allselectedrowskey=coViewNotesTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3Aco_hold_rc_popup_sel_one_menu=525&dataForm%3Aco_hold_rc_popup_inp_txt=&dataForm%3AcocancelListOfIdsToBeCancelledHdn=&dataForm%3Aco_cancel_rc_popup_sel_one_menu=664&dataForm%3Aco_cancel_rc_popup_inp_txt=&"
		"dataForm%3ACMRegPreferredShippingAddrMenu={c_PreferredShippingAddrMenu}&dataForm%3ACMRegPreferredBillingAddrMenu={c_PreferredBillingAddrMenu}&dataForm%3ACMRegPreferredShippingMethodMenu=(none)&dataForm%3ACMRegPreferredPaymentInfoMenu={c_PayInfo}&dataForm%3AcustCommNoteText=&itemPriceOverrideItemIdHdn=&itemPriceOverrideItemDescriptionHdn=&itemPriceOverrideItemOriginalPriceHdn=&itemPriceOverrideItemCurrentPriceHdn=&itemPriceOverrideItemUndergonePriceOverrideHdn=false&itemPriceOverrideorderLineIdHdn=&"
		"itemPriceOverrideCurrencySymbolIdHdn=&itemPriceOverrideOrderLineNumberHdn=&itemPriceOverrideReqQtyHdn=&itemPriceOverrideReqUomHdn=&isEligibleForPriceOverridePrmHdn=&conversionFactorPrmHdn=&showOverridePriceTabInpHdn=true&showOverridePriceTabInpHdn=true&dataForm%3AdisplayingPriceHistoryTabInpHdn=false&ajaxTabClicked=&itemPriceOverridePopupEditable_tabPanel_SubmitOnTabClick=false&itemPriceOverridePopupEditable_tabPanel_selectedTab=itemPriceOverridePopupEditablePriceHistoryTab&dataForm%3Aj_id198="
		"false&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedRows=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AisSortButtonClick=orc.createdDttm&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableClicked=&"
		"dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableResized=false&itemPriceOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=0&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_allselectedrowskey=itemPriceOverridePopupPriceHistoryListTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AdisplayingSnHChargeHistoryTabHdn=true&"
		"dataForm%3AdisplaySnHChargeHistoryTabOnlyHdn=true&dataForm%3AdisplayingSnHChargeHistoryTabInpHdn=true&snhChargeOverridePopupEditable_tabPanel_SubmitOnTabClick=false&snhChargeOverridePopupEditable_tabPanel_selectedTab=snhChargeOverridePopupEditablePriceHistoryTab&dataForm%3Aj_id227=snhChargeOverridePopupEditableTab&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedRows=&"
		"dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AisSortButtonClick=ORC.CREATED_DTTM&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableClicked=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableResized=false&snhChargeOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=0&"
		"dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_allselectedrowskey=snhChargeOverridePopupPriceHistoryListTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AfltrListFltrId%3AfieldName=&dataForm%3AfltrListFltrId%3AfilterName=FL_User001&dataForm%3AfltrListFltrId%3Aowner=User001&dataForm%3AfltrListFltrId%3AobjectType=FL_FILTER&"
		"dataForm%3AfltrListFltrId%3AfilterObjectType=&dataForm%3AfltrListFltrId%3Afield0value1=&dataForm%3AfltrListFltrId%3Afield0=FILTER.FILTER_NAME&dataForm%3AfltrListFltrId%3Afield0operator=&dataForm%3AfltrListFltrId%3Afield1value1=&dataForm%3AfltrListFltrId%3Afield1=FILTER.IS_DEFAULT&dataForm%3AfltrListFltrId%3Afield1operator=&dataForm%3AfltrListFltrId%3Afield2value1=&dataForm%3AfltrListFltrId%3Afield2=FILTER.IS_PRIVATE&dataForm%3AfltrListFltrId%3Afield2operator=&"
		"dataForm%3AfltrListFltrId%3Afield3value1=&dataForm%3AfltrListFltrId%3Afield3=FILTER.OWNER&dataForm%3AfltrListFltrId%3Afield3operator=&dataForm%3AfltrListFltrId%3Afield4value1=&dataForm%3AfltrListFltrId%3Afield4=FILTER.IS_DELETED&dataForm%3AfltrListFltrId%3Afield4operator=&dataForm%3AfltrListFltrId%3AfltrCondition=&dataForm%3AfltrListFltrId%3AfltrCrtSel=&windowId=screen-19311187&domainGroupId=110&dataForm%3Ainp_hdn_for_invoking_cco_bb=&dataForm%3Ainp_hdn_for_invoking_ccnf_bb=&orderNumber="
		"{pSalesOrder}&dataForm%3Apost_COEdit_inp_hdn_orderVersion={c_Order_Version}&orderVersion={c_Order_Version}&CustomerId=&ExternalCustomerId={c_External_ID}&isExchangeOrder=false&hdnRMANo=&inp_hdn_for_order_status={c_inp_hdn_for_order_status}&hdn_for_discount_code={c_inp_hdn_for_order_status}&dataForm%3Ahid_postorder_view_createCO_ordno={pSalesOrder}&dataForm%3Apost_COView_inp_hdn_orderConfirmed=TRUE&dataForm%3Apost_COView_inp_hdn_orderCreatedDTTM=1%2F19%2F17%2005%3A18%20EST&dataForm%3Ahid_txt_postorder_view_createCO_fn={c_createCO_fn}&"
		"dataForm%3Ahid_txt_postorder_view_createCO_pno={c_createCO_pno}&dataForm%3Apost_COView_inp_hdn_orderStatus={c_inp_hdn_orderStatus}&dataForm%3Apost_COEView_inp_hdn_orderStatusId={c_inp_hdn_for_order_status}&dataForm%3Apost_COView_inp_hdn_paymentStatus=AUTHORIZED&dataForm%3Apost_COView_inp_hdn_paymentStatusId=20&dataForm%3Ahid_txt_postorder_view_createCO_ln={c_hid_txt_postorder_view_createCO_ln}&dataForm%3Ainp_hidden_postorder_createCO_customerId_out=&dataForm%3Ainp_hidden_postorder_createCO_extcustomerId_out={c_External_ID}&"
		"dataForm%3Ainp_hidden_postorder_createCO_customerType_out=&dataForm%3Ahid_txt_postorder_view_createCO_ordType={c_Order_Type}&dataForm%3Ahid_txt_postorder_view_createCO_ordTypeId=4&dataForm%3Ahid_txt_postorder_view_createCO_stId=&dataForm%3Ahid_txt_postorder_view_createCO_email={c_hid_txt_postorder_view_createCO_email}&dataForm%3Apost_COView_inp_hdn_onHold=true&dataForm%3Apost_COView_inp_hdn_reasonCodeStr=Fraud%20Hold&dataForm%3Apost_COView_inp_hdn_reasonCodeIdStr=526&dataForm%3Apost_COView_inp_hdn_totalNoOfNotesStr=1&"
		"exchangeOrderIdStr=&loadDefaultNavigationFlow=&dataForm%3Adrop_down_page_ids_som=default&loadNotesFromCategory=10&targetLink=&updateOrderHeaderActive=false&addAppeasementsActive=false&updatePaymentsActive=false&updateShippingInfoActive=false&updateQuantityActive=false&cancelItemActive=false&addItemActive=false&updatePromotionsActive=false&createReturnActive=false&updateAdditionalDetailsActive=true&overridePriceActive=false&overrideSnHChargeActive=false&taskDeterminantLoaded=true&contentPageName="
		"viewOrderDetails&dataForm%3AcoROCustomerOrderIdHidden={c_orderIDPkHiddenParam}&dataForm%3AcoROILTable%3Apager%3ApageInput=&dataForm%3AcoROILTable%3ApagerBoxValue=&dataForm%3AcoROILTable%3AisPaginationEvent=&dataForm%3AcoROILTable%3ApagerAction=&dataForm%3AcoROILTable_deleteHidden=&dataForm%3AcoROILTable_selectedRows=&dataForm%3AcoROILTable%3AisSortButtonClick=col.orderLineNumber&dataForm%3AcoROILTable%3AsortDir=asc&dataForm%3AcoROILTable%3AcolCount=&dataForm%3AcoROILTable%3AtableClicked=&"
		"dataForm%3AcoROILTable%3AtableResized=false&coROILTable_hdnMaxIndexHldr=4&remainingToPay={c_remainingToPay}&eligibleForSnHOverride=false&CustomerOrderId={c_orderIDPkHiddenParam}&orderIDPk={c_orderIDPkHiddenParam}&mainPageName=&moreActionTargetLinkviewCustomerOrderNavFlowFooterNextBtnPanel=&moreActionButtonPressedviewCustomerOrderNavFlowFooterNextBtnPanel=&moreActionTargetLinkviewCustomerOrderNavFlowFooterHoldCancelPanel=&moreActionButtonPressedviewCustomerOrderNavFlowFooterHoldCancelPanel=&backingBeanName=&javax.faces.ViewState="
		"{c_ViewState03}&dataForm%3Anavigation_flow_a4j_repeat%3A3%3Anavigation_commaind_link=dataForm%3Anavigation_flow_a4j_repeat%3A3%3Anavigation_commaind_link&contentPageIdFlowNavFlow=viewPaymentDetails&", 
		LAST);
	
	web_url("ping.jsp_14", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1488875537929", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t25.inf", 
		"Mode=HTML", 
		LAST);
		
	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");
	
	web_custom_request("ViewCustomerOrder.jsflps_2", 
		"URL={pURL}/doms/dom/selling/co/customerorder/ViewCustomerOrder.jsflps", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/xml", 
		"Referer={pURL}/olm/salesorder/StoreOrderList.jsflps", 
		"Snapshot=t26.inf", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded; charset=UTF-8", 
		"Body=AJAXREQUEST=_viewRoot&dataForm=dataForm&uniqueToken=1&MANH-CSRFToken={c_MANHCSRFToken}&helpurlEle=%2Flcom%2Fcommon%2Fjsp%2Fhelphelper.jsp%3Fserver%3D58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B%26uri%3D%252Fdoms%252Fdom%252Fselling%252Fco%252Fcustomerorder%252FViewCustomerOrder.jsflps%253FpageName%253DviewPaymentDetails&dataForm%3AsellingExceptionStkTrcPage_stackTrace_inpArea=&dataForm%3AcoNotesTable_deleteHidden=&"
		"dataForm%3AcoNotesTable_selectedRows=&dataForm%3AcoNotesTable%3AisSortButtonClick=&dataForm%3AcoNotesTable%3AsortDir=desc&dataForm%3AcoNotesTable%3AcolCount=&dataForm%3AcoNotesTable%3AtableClicked=&dataForm%3AcoNotesTable%3AtableResized=false&dataForm%3AcoNotesTable%3A0%3APK_0=DUMMYROW&dataForm%3AcoNotesTable%3A0%3AcoNoteTypeColValue=%20&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteSeq=&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteToBeDeletedFlag=&dataForm%3AcoNotesTable%3A0%3AmarkForDelete=&"
		"dataForm%3AcoNotesTable%3A0%3AcoNotesColValue=&coNotesTable_hdnMaxIndexHldr=0&dataForm%3AcoNotesTable_trs_pageallrowskey=&dataForm%3AcoNotesTable_selectedIdList=&dataForm%3AcoNotesTable_trs_allselectedrowskey=coNotesTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AcoViewNotesTable_deleteHidden=&dataForm%3AcoViewNotesTable_selectedRows=&dataForm%3AcoViewNotesTable%3AisSortButtonClick=&dataForm%3AcoViewNotesTable%3AsortDir=desc&dataForm%3AcoViewNotesTable%3AcolCount=&dataForm%3AcoViewNotesTable%3AtableClicked"
		"=&dataForm%3AcoViewNotesTable%3AtableResized=false&coViewNotesTable_hdnMaxIndexHldr=0&dataForm%3AcoViewNotesTable_trs_pageallrowskey=&dataForm%3AcoViewNotesTable_selectedIdList=&dataForm%3AcoViewNotesTable_trs_allselectedrowskey=coViewNotesTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3Aco_hold_rc_popup_sel_one_menu=525&dataForm%3Aco_hold_rc_popup_inp_txt=&dataForm%3AcocancelListOfIdsToBeCancelledHdn=&dataForm%3Aco_cancel_rc_popup_sel_one_menu=664&dataForm%3Aco_cancel_rc_popup_inp_txt=&"
		"dataForm%3ACMRegPreferredShippingAddrMenu={c_PreferredShippingAddrMenu}&dataForm%3ACMRegPreferredBillingAddrMenu={c_PreferredBillingAddrMenu}&dataForm%3ACMRegPreferredShippingMethodMenu=(none)&dataForm%3ACMRegPreferredPaymentInfoMenu={c_PayInfo}&dataForm%3AcustCommNoteText=&itemPriceOverrideItemIdHdn=&itemPriceOverrideItemDescriptionHdn=&itemPriceOverrideItemOriginalPriceHdn=&itemPriceOverrideItemCurrentPriceHdn=&itemPriceOverrideItemUndergonePriceOverrideHdn=false&itemPriceOverrideorderLineIdHdn=&"
		"itemPriceOverrideCurrencySymbolIdHdn=&itemPriceOverrideOrderLineNumberHdn=&itemPriceOverrideReqQtyHdn=&itemPriceOverrideReqUomHdn=&isEligibleForPriceOverridePrmHdn=&conversionFactorPrmHdn=&showOverridePriceTabInpHdn=true&showOverridePriceTabInpHdn=true&dataForm%3AdisplayingPriceHistoryTabInpHdn=false&ajaxTabClicked=&itemPriceOverridePopupEditable_tabPanel_SubmitOnTabClick=false&itemPriceOverridePopupEditable_tabPanel_selectedTab=itemPriceOverridePopupEditablePriceHistoryTab&dataForm%3Aj_id198="
		"false&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedRows=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AisSortButtonClick=orc.createdDttm&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableClicked=&"
		"dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableResized=false&itemPriceOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=0&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_allselectedrowskey=itemPriceOverridePopupPriceHistoryListTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AdisplayingSnHChargeHistoryTabHdn=true&"
		"dataForm%3AdisplaySnHChargeHistoryTabOnlyHdn=true&dataForm%3AdisplayingSnHChargeHistoryTabInpHdn=true&snhChargeOverridePopupEditable_tabPanel_SubmitOnTabClick=false&snhChargeOverridePopupEditable_tabPanel_selectedTab=snhChargeOverridePopupEditablePriceHistoryTab&dataForm%3Aj_id227=snhChargeOverridePopupEditableTab&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedRows=&"
		"dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AisSortButtonClick=ORC.CREATED_DTTM&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableClicked=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableResized=false&snhChargeOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=1&"
		"dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_allselectedrowskey=snhChargeOverridePopupPriceHistoryListTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AfltrListFltrId%3AfieldName=&dataForm%3AfltrListFltrId%3AfilterName=FL_User001&dataForm%3AfltrListFltrId%3Aowner=User001&dataForm%3AfltrListFltrId%3AobjectType=FL_FILTER&"
		"dataForm%3AfltrListFltrId%3AfilterObjectType=&dataForm%3AfltrListFltrId%3Afield0value1=&dataForm%3AfltrListFltrId%3Afield0=FILTER.FILTER_NAME&dataForm%3AfltrListFltrId%3Afield0operator=&dataForm%3AfltrListFltrId%3Afield1value1=&dataForm%3AfltrListFltrId%3Afield1=FILTER.IS_DEFAULT&dataForm%3AfltrListFltrId%3Afield1operator=&dataForm%3AfltrListFltrId%3Afield2value1=&dataForm%3AfltrListFltrId%3Afield2=FILTER.IS_PRIVATE&dataForm%3AfltrListFltrId%3Afield2operator=&"
		"dataForm%3AfltrListFltrId%3Afield3value1=&dataForm%3AfltrListFltrId%3Afield3=FILTER.OWNER&dataForm%3AfltrListFltrId%3Afield3operator=&dataForm%3AfltrListFltrId%3Afield4value1=&dataForm%3AfltrListFltrId%3Afield4=FILTER.IS_DELETED&dataForm%3AfltrListFltrId%3Afield4operator=&dataForm%3AfltrListFltrId%3AfltrCondition=&dataForm%3AfltrListFltrId%3AfltrCrtSel=&windowId=screen-19311187&domainGroupId=110&dataForm%3Ainp_hdn_for_invoking_cco_bb=&dataForm%3Ainp_hdn_for_invoking_ccnf_bb=&orderNumber="
		"{pSalesOrder}&dataForm%3Apost_COEdit_inp_hdn_orderVersion={c_Order_Version}&orderVersion={c_Order_Version}&CustomerId=&ExternalCustomerId={c_External_ID}&isExchangeOrder=false&hdnRMANo=&inp_hdn_for_order_status={c_inp_hdn_for_order_status}&hdn_for_discount_code={c_inp_hdn_for_order_status}&dataForm%3Ahid_postorder_view_createCO_ordno={pSalesOrder}&dataForm%3Apost_COView_inp_hdn_orderConfirmed=TRUE&dataForm%3Apost_COView_inp_hdn_orderCreatedDTTM=1%2F19%2F17%2005%3A18%20EST&dataForm%3Ahid_txt_postorder_view_createCO_fn={c_createCO_fn}&"
		"dataForm%3Ahid_txt_postorder_view_createCO_pno={c_createCO_pno}&dataForm%3Apost_COView_inp_hdn_orderStatus={c_inp_hdn_orderStatus}&dataForm%3Apost_COEView_inp_hdn_orderStatusId={c_inp_hdn_for_order_status}&dataForm%3Apost_COView_inp_hdn_paymentStatus=AUTHORIZED&dataForm%3Apost_COView_inp_hdn_paymentStatusId=20&dataForm%3Ahid_txt_postorder_view_createCO_ln={c_hid_txt_postorder_view_createCO_ln}&dataForm%3Ainp_hidden_postorder_createCO_customerId_out=&dataForm%3Ainp_hidden_postorder_createCO_extcustomerId_out={c_External_ID}&"
		"dataForm%3Ainp_hidden_postorder_createCO_customerType_out=&dataForm%3Ahid_txt_postorder_view_createCO_ordType={c_Order_Type}&dataForm%3Ahid_txt_postorder_view_createCO_ordTypeId=4&dataForm%3Ahid_txt_postorder_view_createCO_stId=&dataForm%3Ahid_txt_postorder_view_createCO_email={c_hid_txt_postorder_view_createCO_email}&dataForm%3Apost_COView_inp_hdn_onHold=true&dataForm%3Apost_COView_inp_hdn_reasonCodeStr=Fraud%20Hold&dataForm%3Apost_COView_inp_hdn_reasonCodeIdStr=526&dataForm%3Apost_COView_inp_hdn_totalNoOfNotesStr=1&"
		"exchangeOrderIdStr=&loadDefaultNavigationFlow=&dataForm%3Adrop_down_page_ids_som=default&loadNotesFromCategory=10&targetLink=&updateOrderHeaderActive=false&addAppeasementsActive=false&updatePaymentsActive=false&updateShippingInfoActive=false&updateQuantityActive=false&cancelItemActive=false&addItemActive=false&updatePromotionsActive=false&createReturnActive=false&updateAdditionalDetailsActive=true&overridePriceActive=false&overrideSnHChargeActive=false&taskDeterminantLoaded=true&contentPageName="
		"viewPaymentDetails&dataForm%3AviewPaymentSync_comp_id_syncActionHid=syncAction&dataForm%3AcoViewPaymentDetailsListView%3AcoViewPaymentDetailsListTable%3Apager%3ApageInput=&dataForm%3AcoViewPaymentDetailsListView%3AcoViewPaymentDetailsListTable%3ApagerBoxValue=&dataForm%3AcoViewPaymentDetailsListView%3AcoViewPaymentDetailsListTable%3AisPaginationEvent=&dataForm%3AcoViewPaymentDetailsListView%3AcoViewPaymentDetailsListTable%3ApagerAction=&"
		"dataForm%3AcoViewPaymentDetailsListView%3AcoViewPaymentDetailsListTable_deleteHidden=&dataForm%3AcoViewPaymentDetailsListView%3AcoViewPaymentDetailsListTable_selectedRows=&dataForm%3AcoViewPaymentDetailsListView%3AcoViewPaymentDetailsListTable%3AisSortButtonClick=pd.lastModified&dataForm%3AcoViewPaymentDetailsListView%3AcoViewPaymentDetailsListTable%3AsortDir=asc&dataForm%3AcoViewPaymentDetailsListView%3AcoViewPaymentDetailsListTable%3AcolCount=&"
		"dataForm%3AcoViewPaymentDetailsListView%3AcoViewPaymentDetailsListTable%3AtableClicked=&dataForm%3AcoViewPaymentDetailsListView%3AcoViewPaymentDetailsListTable%3AtableResized=false&coViewPaymentDetailsListTable_hdnMaxIndexHldr=1&coViewPaymentDetailsListPopulated=true&coViewPaymentDetailsTabPanel_SubmitOnTabClick=false&coViewPaymentDetailsTabPanel_selectedTab=coViewPaymentDetailsTab&syncClicked=true&CustomerOrderId={c_orderIDPkHiddenParam}&orderIDPk={c_orderIDPkHiddenParam}&mainPageName=&"
		"moreActionTargetLinkviewCustomerOrderNavFlowFooterNextBtnPanel=&moreActionButtonPressedviewCustomerOrderNavFlowFooterNextBtnPanel=&moreActionTargetLinkviewCustomerOrderNavFlowFooterHoldCancelPanel=&moreActionButtonPressedviewCustomerOrderNavFlowFooterHoldCancelPanel=&backingBeanName=&javax.faces.ViewState={c_ViewState03}&dataForm%3AcoViewPaymentDetailsListView%3AcoViewPaymentDetailsListTable%3A0%3AcoViewPaymentDetailsListTablesyncId="
		"dataForm%3AcoViewPaymentDetailsListView%3AcoViewPaymentDetailsListTable%3A0%3AcoViewPaymentDetailsListTablesyncId&PaymentDetailIdFromSyncAction={c_PayInfo}&", 
		LAST);
		
	lr_end_transaction(lr_eval_string("{sTestCaseName}_05_Click_on_Payment_Icon"),LR_AUTO);
	
	lr_think_time(1);

	return 0;
}