Action()
{
	// char *temp,temp1,temp2,temp3,temp4,temp5;
	
	/* Search Item under Item Tile */

	lr_start_transaction(lr_eval_string("{sTestCaseName}_03_Item_Lookup_From_Item_Tile"));

	web_url("ping.jsp_3", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466586311616", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t6.inf", 
		"Mode=HTML", 
		LAST);
	
	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");

	web_custom_request("windows", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14271270&regionId=-1&businessUnitId=-1", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t7.inf", 
		"Mode=HTML", 
		"EncType=", 
		LAST);

	web_url("ping.jsp_4", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466586354792", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t8.inf", 
		"Mode=HTML", 
		LAST);

	web_url("index.html_2", 
		"URL={pURL}/manh/index.html", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t9.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=olm/resources/icons/ds/customer/customer_id.png", ENDITEM, 
		"Url=olm/resources/icons/refresh/refresh.png", ENDITEM, 
		"Url=../services/olm/basedata/noteType/noteTypes?_dc=1466586362539&noteCategory=Instructions&page=1&start=0&limit=25", ENDITEM, 
		"Url=../services/olm/location/countrylist?_dc=1466586362538&page=1&start=0&limit=25", ENDITEM, 
		"Url=olm/resources/icons/ds/search/customer.png", ENDITEM, 
		"Url=olm/resources/icons/ds/search/order.png", ENDITEM, 
		"Url=../services/atc/cvDef/getSystemProperties?_dc=1466586362540&page=1&start=0&limit=25", ENDITEM, 
		"Url=olm/resources/icons/ds/search/return.png", ENDITEM, 
		"Url=olm/resources/icons/ds/search/item.png", ENDITEM, 
		"Url=../services/olm/location/userFacilityDetails?_dc=1466586363766&page=1&start=0&limit=25", ENDITEM, 
		"Url=olm/resources/icons/thumbs/available.png", ENDITEM, 
		LAST);
	
	lr_save_string("{\"companyParameterIdList\":{\"parameterList\":[\"CURRENCY_USED\",\"EXT_CUSTOMER_ID_GENERATION\",\"USE_DELIVERY_ZONE\",\"DEFAULT_SHIP_VIA\",\"DEFAULT_ORDER_TYPE\",\"REQUIRES_VALIDATION_FOR_PAYMENT_AUTHORIZATION\",\"REQUIRES_VALIDATION_FOR_PAYMENT_SETTLEMENT\",\"CHARGE_HANDLING_STRATEGY\",\"AUTOMATIC_PROMOTION_VALUE\",\"ITEM_PRICE_EXPIRY_DATE_EXTENSION_DAYS\",\"ITEM_PRICE_SCHEDULER_INTERVAL_IN_DAYS\",\"DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE\",\"VIEW_STORE_LEVEL_PRICES\",\"NETWORK_LEVEL_VIEW\",\""
	               "FACILITY_LEVEL_VIEW\"]}}","temp");
	
	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");
	
	/* {\"companyParameterIdList\":{\"parameterList\":[\"CURRENCY_USED\",\"EXT_CUSTOMER_ID_GENERATION\",\"USE_DELIVERY_ZONE\",\"DEFAULT_SHIP_VIA\",\"DEFAULT_ORDER_TYPE\",\"REQUIRES_VALIDATION_FOR_PAYMENT_AUTHORIZATION\",\"REQUIRES_VALIDATION_FOR_PAYMENT_SETTLEMENT\",\"CHARGE_HANDLING_STRATEGY\",\"AUTOMATIC_PROMOTION_VALUE\",\"ITEM_PRICE_EXPIRY_DATE_EXTENSION_DAYS\",\"ITEM_PRICE_SCHEDULER_INTERVAL_IN_DAYS\",\"DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE\",\"VIEW_STORE_LEVEL_PRICES\",\"NETWORK_LEVEL_VIEW\",\""
		"FACILITY_LEVEL_VIEW\"]}} */
	web_custom_request("companyParameterList", 
		"URL={pURL}/services/olm/basedata/companyParameter/companyParameterList?_dc=1466586362537&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t10.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={temp}",
		LAST);
	
	/* {\"ItemListRequest\":{\"itemSearchCriteria\":{\"itemSearchStr\":\"{pItem_No}\",\"style\":\"\",\"color\":\"\",\"size\":\"\",\"productClassId\":null,\"customFields\":{}},\"sortingCriterion\":{\"sortField\":\"\",\"sortDirection\":\"\"},\"pageNavigationDetails\":{\"currentPageNumber\":0,\"rowsPerPage\":10}}} */
	
	lr_save_string("{\"ItemListRequest\":{\"itemSearchCriteria\":{\"itemSearchStr\":\"","temp1");
	
	lr_save_string("\",\"style\":\"\",\"color\":\"\",\"size\":\"\",\"productClassId\":null,\"customFields\":{}},\"sortingCriterion\":{\"sortField\":\"\",\"sortDirection\":\"\"},\"pageNavigationDetails\":{\"currentPageNumber\":0,\"rowsPerPage\":10}}}","temp5");
	
	web_reg_find("Text={pItem_No}",LAST);	

	web_reg_save_param("c_ID2","LB=\"ItemInfo\":{\"itemId\":","RB=,\"i",LAST);
	
	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");
	
		web_custom_request("itemlist", 
		"URL={pURL}/services/olm/item/itemlist?_dc=1466586362136&page=1&start=0&limit=10", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t11.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={temp1}{pItem_No}{temp5}", 
		LAST);
	
	/*	{\"ItemListRequest\":{\"messageHeader\":{\"userName\":\"olmteam1\",\"companyId\":\"3070\"},\"itemSearchCriteria\":{\"itemSearchStr\":\"\",\"style\":\"\",\"color\":\"\",\"size\":\"\",\"productClassId\":\"\",\"referenceFields\":{}},\"sortingCriterion\":{\"sortField\":\"\",\"sortDirection\":\"\"},\"pageNavigationDetails\":{\"currentPageNumber\":\"\",\"rowsPerPage\":\"\",\"totalNoOfRows\":\"\"}}} */

	lr_save_string("{\"ItemListRequest\":{\"messageHeader\":{\"userName\":\"olmteam1\",\"companyId\":\"3070\"},\"itemSearchCriteria\":{\"itemSearchStr\":\"\",\"style\":\"\",\"color\":\"\",\"size\":\"\",\"productClassId\":\"\",\"referenceFields\":{}},\"sortingCriterion\":{\"sortField\":\"\",\"sortDirection\":\"\"},\"pageNavigationDetails\":{\"currentPageNumber\":\"\",\"rowsPerPage\":\"\",\"totalNoOfRows\":\"\"}}}","temp2");

	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");
	
	web_custom_request("productClassList",
		"URL={pURL}/services/olm/item/productClassList?_dc=1466586362124&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t12.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={temp2}", 
		LAST);
		
	/* {"transactionId":"83abf93f-3a4b-4a28-9059-0ca73f956289","eventName":*/
	
	web_url("index.html_3", 
		"URL={pURL}/manh/index.html", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t13.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=../services/olm/item/itemdetails?_dc=1466586364385&jsonFileName=ItemDetails&itemId=5493&storeId=&page=1&start=0&limit=25", ENDITEM, 
		"Url=olm/common/model/EventModel.js?_dc=1466586365519", ENDITEM, 
		LAST);
		
		
		/* {\"events\":{\"event\":[{\"transactionId\":\"83abf93f-3a4b-4a28-9059-0ca73f956289\",\"eventName\":\"radio selection\",\"screenId\":280007,\"userName\":\"{pUserName}\",\"logCategory\":\"radio\",\"logLevel\":4,\"eventTimeStamp\":\"6/22/2016, 2:36:05 PM\",\"additionalInfo\":\"Selected Ship to address\"}]}} */
		
		lr_save_string("{\"events\":{\"event\":[{\"transactionId\":\"83abf93f-3a4b-4a28-9059-0ca73f956289\",\"eventName\":\"radio selection\",\"screenId\":280007,\"userName\":\"","temp3");
		lr_save_string("\",\"logCategory\":\"radio\",\"logLevel\":4,\"eventTimeStamp\":\"6/22/2016, 2:36:05 PM\",\"additionalInfo\":\"Selected Ship to address\"}]}}","temp4");

		web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");
	
		web_custom_request("saveUserActivity", 
		"URL={pURL}/services/olm/log/saveUserActivity?_dc=1466586373359&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t14.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={temp3}{pUserName}{temp4}", 
		LAST);

		web_url("ping.jsp_5", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466586377601", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t15.inf", 
		"Mode=HTML", 
		LAST);

		lr_end_transaction(lr_eval_string("{sTestCaseName}_03_Item_Lookup_From_Item_Tile"),LR_AUTO);

		lr_think_time(1);

		return 0;
}
