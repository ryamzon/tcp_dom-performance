Action()
{

	/* Sales Order search */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_03_Open_Sales_Order_Screen"));

	web_url("ping.jsp_2", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467200695783", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t5.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=../manh/olm/resources/icons/trigger/search_trigger.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=../manh/mps/resources/icons/16/default-icon.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		LAST);
	
	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");

	web_custom_request("windows", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14271270&regionId=-1&businessUnitId=-1", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t6.inf", 
		"Mode=HTML", 
		"EncType=", 
		EXTRARES, 
		"Url=/manh/resources/css/gray/images/tools/tool-sprites.png", "Referer={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", ENDITEM, 
		LAST);

	/*Correlation comment - Do not change!  Original value='80bojiTBmJAOvfFzOGbzLJoahp0hlfgbUVG8EiSNtfU=' Name ='MANH-CSRFToken' Type ='ResponseBased'*/
	web_reg_save_param_regexp(
		"ParamName=MANH-CSRFToken",
		"RegExp=name=\"MANH-CSRFToken\"\\ value=\"(.*?)\"/><script\\ type",
		SEARCH_FILTERS,
		"Scope=Body",
		"IgnoreRedirections=No",
		"RequestUrl=*/StoreOrderList.jsflps*",
		LAST);
		

	/* Original Value : 1467200705267 */
	
	web_reg_save_param("c_dataTable",
                       "LB=dataTable$:$",
                       "RB=\" />",
                       "ORD=1",
                       LAST);

	/* 5318721021218658239:-7690085771694200212 */
	
	web_reg_save_param("c_ViewState01",
                       "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
                       "RB=\" autocomplete=\"",
                       "ORD=1",
                       LAST);

	/*Correlation comment - Do not change!  Original value='2147483647' Name ='CorrelationParameter_1' Type ='ResponseBased'*/
	web_reg_save_param_regexp(
		"ParamName=CorrelationParameter_1",
		"RegExp=ontact_dataTable_button'\\),true,1,2,'dataForm:Facility_popup_Facility_Contact_dataTable','view','no','no','1','bottom','view',1,(.*?),",
		SEARCH_FILTERS,
		"Scope=Body",
		"IgnoreRedirections=No",
		"RequestUrl=*/StoreOrderList.jsflps*",
		LAST);

	web_url("StoreOrderList.jsflps", 
		"URL={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t7.inf", 
		"Mode=HTML", 
	/*	EXTRARES, 
		"Url=/lps/resources/rightclickmenu/scripts/rightclickmenu.js", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=/lps/resources/editControl/scripts/idLookup.js", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=/lps/resources/inputmask/scripts/mask.js", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=/lps/resources/editControl/scripts/autocompleteinput.js", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=/javax.faces.resource/jsf.js.jsflps?ln=javax.faces", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=/lps/resources/actionpanel/moreButton/scripts/moreButton.js", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=/lps/resources/menu/images/foPrint.gif", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=/lps/resources/layout/images/backdnew.png", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=/manh/mps/resources/icons/funnel.png", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=/lps/resources/dialogControl/images/close.png", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=/lps/resources/themes/icons/mablue/pinfavapp.png", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=/lps/resources/menu/images/clear.gif", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=/lps/resources/themes/icons/mablue/customize.gif", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=/lps/resources/themes/icons/mablue/foPrint.gif", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=/lcom/common/image/alert.gif", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=/lcom/common/image/hardcheck.gif", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=/lps/resources/actionpanel/moreButton/images/clear.gif", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=/lps/resources/themes/icons/mablue/sortdown.gif", "Referer={pURL}/lps/lpsstyles", ENDITEM, 
		"Url=/lps/resources/common/images/dropdown.png", "Referer={pURL}/lps/lpsstyles", ENDITEM, 
		"Url=/lps/resources/common/images/arrow-black.gif", "Referer={pURL}/lps/lpsstyles", ENDITEM, 
		"Url=/ext/resources/ext-theme-neptune/images/tools/tool-sprites.png", "Referer={pURL}/lps/lpsstyles", ENDITEM, 
		"Url=/lps/resources/panel/images/rule1.gif", "Referer={pURL}/lps/resources/layout/css/LPSLayoutStyles_firefox.css", ENDITEM, */
		LAST);

	web_convert_param("MANH-CSRFToken_URL2",
		"SourceString={MANH-CSRFToken}",
		"SourceEncoding=HTML",
		"TargetEncoding=URL",
		LAST);

	web_url("ping.jsp_3", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467200711113", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t8.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=../lps/resources/themes/icons/mablue/arrow_collapse.gif", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=../lps/resources/themes/icons/mablue/find.gif", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=../lps/resources/themes/icons/mablue/arrow_first_disabled.gif", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=../lps/resources/themes/icons/mablue/arrow_left_disabled.gif", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=../lps/resources/themes/icons/mablue/arrow_last_disabled.gif", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=../lps/resources/themes/icons/mablue/arrow_right_disabled.gif", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		"Url=../lps/resources/themes/icons/mablue/refresh.gif", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM, 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_03_Open_Sales_Order_Screen"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_4", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467200726338", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t9.inf", 
		"Mode=HTML", 
		LAST);
	
	/* 5318721021218658239:1098708854904676922*/
	
	web_reg_save_param("c_ViewState02",
                       "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
                       "RB=\" autocomplete=\"",
                       "ORD=1",
                       LAST);
	/* 185266 */                   
	web_reg_save_param("c_orderIDPkHiddenParam",
                   "LB=input type=\"hidden\" value=\"",
                   "RB=\" id=\"dataForm:SOList_entityListView:salesOrderData",
                   "ORD=1",
                   "Search=ALL",LAST);
	
	/* domordTyp">USECOM</span> */
	
		web_reg_save_param("c_orderType",
                   "LB=domordTyp\">",
                   "RB=</span>",
                   "ORD=1",
                   "Search=ALL",LAST);
	
	/* search specific sales order number */

	lr_start_transaction(lr_eval_string("{sTestCaseName}_04_Search_Sales_Order"));

	web_custom_request("StoreOrderList.jsflps_2",
		"URL={pURL}/olm/salesorder/StoreOrderList.jsflps",
		"Method=POST",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/xml",
		"Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270",
		"Snapshot=t10.inf",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded; charset=UTF-8",
		"Body=AJAXREQUEST=_viewRoot&dataForm=dataForm&uniqueToken=1&MANH-CSRFToken={MANH-CSRFToken_URL2}&helpurlEle=%2Flcom%2Fcommon%2Fjsp%2Fhelphelper.jsp%3Fserver%3D58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B%26uri%3D%252Folm%252Fsalesorder%252FStoreOrderList.jsflps&dataForm%3AFacility_popup_Facility_Contact_dataTable_deleteHidden=&dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedRows=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AsortDir=desc&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AcolCount=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableClicked=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableResized=false&Facility_popup_Facility_Contact_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs_pageallrowskey=&dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedIdList=&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs"
		"_allselectedrowskey=Facility_popup_Facility_Contact_dataTable%24%3A%24{c_dataTable}&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_deleteHidden=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedRows=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AsortDir=desc&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AcolCount=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AtableClicked=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AtableResized=false&Facility_popup_Credit_Limit_List_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_pageallrowskey=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedIdList=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_allselectedrowskey=Facility_popup_Credit_Limit_List_dataTable%24%3A%24{c_dataTable}&dataForm%3AfltrListFltrId%3AfieldName=&dataForm%3AfltrListFltrId%3AfilterName=FL_{pUserName}&dataForm%3Af"
		"ltrListFltrId%3Aowner={pUserName}&dataForm%3AfltrListFltrId%3AobjectType=FL_FILTER&dataForm%3AfltrListFltrId%3AfilterObjectType=&dataForm%3AfltrListFltrId%3Afield0value1=&dataForm%3AfltrListFltrId%3Afield0=FILTER.FILTER_NAME&dataForm%3AfltrListFltrId%3Afield0operator=&dataForm%3AfltrListFltrId%3Afield1value1=&dataForm%3AfltrListFltrId%3Afield1=FILTER.IS_DEFAULT&dataForm%3AfltrListFltrId%3Afield1operator=&dataForm%3AfltrListFltrId%3Afield2value1=&dataForm%3AfltrListFltrId%3Afield2=FILTER.IS_PRIVATE&dataForm%3AfltrListFltrId%3Afield2operator=&dataForm%3AfltrListFltrId%3Afield3value1=&dataForm%3AfltrListFltrId%3Afield3=FILTER.OWNER&dataForm%3AfltrListFltrId%3Afield3operator=&dataForm%3AfltrListFltrId%3Afield4value1=&dataForm%3AfltrListFltrId%3Afield4=FILTER.IS_DELETED&dataForm%3AfltrListFltrId%3Afield4operator=&dataForm%3AfltrListFltrId%3AfltrCondition=&dataForm%3AfltrListFltrId%3AfltrCrtSel=&windowId=screen-14271270&windowId=screen-14271270&targetLink=&channelTypeHiddenParam=&fromViewPageParam=false&tabToBeSelecte"
		"d=&IsOrderConfirmedHiddenParam=&CustomerOrderTypeHiddenParam=&IsOrderCanceledHiddenParam=&orderNumberHiddenParam=&orderIDPkHiddenParam=&addOrderLineBtnClicked=false&fromOrder=true&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AradioSelect=quick&dataForm%3ASOList_entityListView%3ASOList_filterId2%3A_fltrExpColTxt=DONE&dataForm%3ASOList_entityListView%3ASOList_filterId2%3A_filtrExpColState=collapsed&dataForm%3ASOList_entityListView%3ASOList_filterId2%3A_filtrExpIconSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2Farrow_expand.gif&dataForm%3ASOList_entityListView%3ASOList_filterId2%3A_filtrColIconSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2Farrow_collapse.gif&dataForm%3ASOList_entityListView%3ASOList_filterId2%3A_filtrdropDownSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2FarrowDown.gif&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield0=Sales%20Channel&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield0operator=%3D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubOb"
		"ject0=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield0value1=%5B%5D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield3=Order%20no.&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield3operator=%3D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubObject3=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield3value1ecId=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield3value1={pOrder_No}&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield4=Customer&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield4operator=%3D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubObject4=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield4value1ecId=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield4value1=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield5=Order%20Type&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield5operator=%3D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubObject5=&dataForm%3ASOList_"
		"entityListView%3ASOList_filterId2%3Afield5value1=%5B%5D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield7=Status&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield7operator=%3D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubObject7=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield7value1=%5B%5D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield11=Destination%20Facility&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield11operator=%3D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubObject11=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield11value1ecId=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield11value1=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AcurrentAppliedFilterId=-1&dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_quickFilterGroupButton_mainButtonCategory=-1&dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_quickFilterGroupButton_mainButtonIndex=-1&dataForm"
		"%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_quickFilterGroupButton_changeDefault=false&dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_savedFilterGroupButton_mainButtonCategory=-1&dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_savedFilterGroupButton_mainButtonIndex=-1&dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_savedFilterGroupButton_changeDefault=false&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AdummyToGetPrefix=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Aowner={pUserName}&customParams%20=windowId%3Dscreen-14271270&queryPersistParameter=%26windowId%3Dscreen-14271270&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AobjectType=DOM%20ORDER&isJSF=true&filterScreenType=ON_SCREEN&dataForm%3ASOList_entityListView%3AsalesOrderData%3Apager%3ApageInput=&dataForm%3ASOList_entityListView%3AsalesOrderData%3ApagerBoxValue=&dataForm%3ASOList_entityListView%3AsalesOrderData%3AisPaginationEvent=&dataForm%3ASOLi"
		"st_entityListView%3AsalesOrderData%3ApagerAction=&dataForm%3ASOList_entityListView%3AsalesOrderData_deleteHidden=&dataForm%3ASOList_entityListView%3AsalesOrderData_selectedRows=&dataForm%3ASOList_entityListView%3AsalesOrderData%3AisSortButtonClick=po.lastUpdatedDttm&dataForm%3ASOList_entityListView%3AsalesOrderData%3AsortDir=desc&dataForm%3ASOList_entityListView%3AsalesOrderData%3AcolCount=&dataForm%3ASOList_entityListView%3AsalesOrderData%3AtableClicked=&dataForm%3ASOList_entityListView%3AsalesOrderData%3AtableResized=false&dataForm%3ASOList_entityListView%3AsalesOrderData%3A0%3APK_0=DUMMYROW&orderNbrHidden=&orderIDPkHidden=&IsOrderConfirmedHidden=&CustomerOrderTypeHidden=&IsOrderCanceledHidden=&isEditable=&isEditWarning=&isAddOL=&isConfirm=&isSource=&isSourceWarning=&isAllocate=&isDOCreate=&isDORelease=&isCancel=&isReserve=&isSourceByRate=&isDeAllocate=&isHold=&isUnHold=&isMassUpdate=&isSendUpdate=&markForDeletion=&soFulfillStatus=&dataForm%3ASOList_entityListView%3AsalesOrderData%3A0%3AchannelTypeCode=&s"
		"alesOrderData_hdnMaxIndexHldr=0&dataForm%3ASOList_entityListView%3AsalesOrderData_trs_pageallrowskey=&dataForm%3ASOList_entityListView%3AsalesOrderData_selectedIdList=&dataForm%3ASOList_entityListView%3AsalesOrderData_trs_allselectedrowskey=salesOrderData%24%3A%24{c_dataTable}&moreActionTargetLinksoheaderbuttons=&moreActionButtonPressedsoheaderbuttons=&backingBeanName=&javax.faces.ViewState={c_ViewState01}&fltrApplyFromQF=true&dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2apply=dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2apply&reRenderParent=SOList_rerenderPanel&fltrClientId=dataForm%3ASOList_entityListView%3ASOList_filterId2&",
		EXTRARES,
		"URL=/lcom/common/image/checkmark.gif", "Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270", ENDITEM,
		LAST);

	web_url("ping.jsp_5", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467200741651", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t11.inf", 
		"Mode=HTML", 
		LAST);
	
	lr_end_transaction(lr_eval_string("{sTestCaseName}_04_Search_Sales_Order"),LR_AUTO);
	
	lr_think_time(1);

	/* double click on sales Order searched */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_05_Open_Sales_Order_By_Double_Clicking_On_It"));

	web_add_cookie("filterExpandState=true; DOMAIN=njitldomapp01.tcphq.tcpcorp.local.com");
	
	/* 5318721021218658239:3263141450350479985 */
	
	
	web_reg_save_param("c_ViewState03",
                       "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
                       "RB=\" autocomplete=\"",
                       "ORD=1",
                       LAST);
	
	/* Allselectedrowskey" value="coNotesTable$:$1467200753724" / */
	
		web_reg_save_param("c_SelectedRow_Keys",
                       "LB=value=\"coNotesTable$:$",
                       "RB=\"",
                       "ORD=1",
                       LAST);
	
	/* >	CMRegPreferredPaymentInfoMenu" size="1"> <option value="186063"> */
	
			web_reg_save_param("c_PayInfo",
                       "LB=CMRegPreferredPaymentInfoMenu\" size=\"1\">\t<option value=\"",
                       "RB=\">",
                       "ORD=1",
                       LAST); 
	
	/* name="orderVersion" value="1443997204563" /> */
	
				web_reg_save_param("c_Order_Version",
                       "LB=\"orderVersion\" value=\"",
                       "RB=\"",
                       "ORD=1",
                       LAST);
	
	/* id="itemPriceOverrideorderLineId" value="2330925" /> */
	
				web_reg_save_param("c_LineId",
                       "LB=\"itemPriceOverrideorderLineId\" value=\"",
                       "RB=\"",
                       "ORD=1",
                       LAST);
	
	/* id="itemPriceOverrideOrderLineNumber" value="334170839" /> */
	
				web_reg_save_param("c_LineNumber",
                       "LB=\"itemPriceOverrideOrderLineNumber\" value=\"",
                       "RB=\"",
                       "ORD=1",
                       LAST);
	

	web_submit_data("StoreOrderList.jsflps_3",
		"Action={pURL}/olm/salesorder/StoreOrderList.jsflps",
		"Method=POST",
		"TargetFrame=",
		"RecContentType=text/html",
		"Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271270",
		"Snapshot=t12.inf",
		"Mode=HTML",
		ITEMDATA,
		"Name=dataForm", "Value=dataForm", ENDITEM,
		"Name=uniqueToken", "Value=1", ENDITEM,
		"Name=MANH-CSRFToken", "Value={MANH-CSRFToken}", ENDITEM,
		"Name=helpurlEle", "Value=/lcom/common/jsp/helphelper.jsp?server=58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B&uri=%2Folm%2Fsalesorder%2FStoreOrderList.jsflps", ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_deleteHidden", "Value=", ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_selectedRows", "Value=", ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:isSortButtonClick", "Value=", ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:sortDir", "Value=desc", ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:colCount", "Value=", ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:tableClicked", "Value=", ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:tableResized", "Value=false", ENDITEM,
		"Name=Facility_popup_Facility_Contact_dataTable_hdnMaxIndexHldr", "Value=0", ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_trs_pageallrowskey", "Value=", ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_selectedRows", "Value=", ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_selectedIdList", "Value=", ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_trs_allselectedrowskey", "Value=Facility_popup_Facility_Contact_dataTable$:${c_dataTable}", ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_deleteHidden", "Value=", ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_selectedRows", "Value=", ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:isSortButtonClick", "Value=", ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:sortDir", "Value=desc", ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:colCount", "Value=", ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:tableClicked", "Value=", ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:tableResized", "Value=false", ENDITEM,
		"Name=Facility_popup_Credit_Limit_List_dataTable_hdnMaxIndexHldr", "Value=0", ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_trs_pageallrowskey", "Value=", ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_selectedRows", "Value=", ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_selectedIdList", "Value=", ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_trs_allselectedrowskey", "Value=Facility_popup_Credit_Limit_List_dataTable$:${c_dataTable}", ENDITEM,
		"Name=dataForm:fltrListFltrId:fieldName", "Value=", ENDITEM,
		"Name=dataForm:fltrListFltrId:filterName", "Value=FL_{pUserName}", ENDITEM,
		"Name=dataForm:fltrListFltrId:owner", "Value={pUserName}", ENDITEM,
		"Name=dataForm:fltrListFltrId:objectType", "Value=FL_FILTER", ENDITEM,
		"Name=dataForm:fltrListFltrId:filterObjectType", "Value=", ENDITEM,
		"Name=dataForm:fltrListFltrId:field0value1", "Value=", ENDITEM,
		"Name=dataForm:fltrListFltrId:field0", "Value=FILTER.FILTER_NAME", ENDITEM,
		"Name=dataForm:fltrListFltrId:field0operator", "Value=", ENDITEM,
		"Name=dataForm:fltrListFltrId:field1value1", "Value=", ENDITEM,
		"Name=dataForm:fltrListFltrId:field1", "Value=FILTER.IS_DEFAULT", ENDITEM,
		"Name=dataForm:fltrListFltrId:field1operator", "Value=", ENDITEM,
		"Name=dataForm:fltrListFltrId:field2value1", "Value=", ENDITEM,
		"Name=dataForm:fltrListFltrId:field2", "Value=FILTER.IS_PRIVATE", ENDITEM,
		"Name=dataForm:fltrListFltrId:field2operator", "Value=", ENDITEM,
		"Name=dataForm:fltrListFltrId:field3value1", "Value=", ENDITEM,
		"Name=dataForm:fltrListFltrId:field3", "Value=FILTER.OWNER", ENDITEM,
		"Name=dataForm:fltrListFltrId:field3operator", "Value=", ENDITEM,
		"Name=dataForm:fltrListFltrId:field4value1", "Value=", ENDITEM,
		"Name=dataForm:fltrListFltrId:field4", "Value=FILTER.IS_DELETED", ENDITEM,
		"Name=dataForm:fltrListFltrId:field4operator", "Value=", ENDITEM,
		"Name=dataForm:fltrListFltrId:fltrCondition", "Value=", ENDITEM,
		"Name=dataForm:fltrListFltrId:fltrCrtSel", "Value=", ENDITEM,
		"Name=windowId", "Value=screen-14271270", ENDITEM,
		"Name=targetLink", "Value=", ENDITEM,
		"Name=channelTypeHiddenParam", "Value=", ENDITEM,
		"Name=fromViewPageParam", "Value=false", ENDITEM,
		"Name=tabToBeSelected", "Value=", ENDITEM,
		"Name=IsOrderConfirmedHiddenParam", "Value=", ENDITEM,
		"Name=CustomerOrderTypeHiddenParam", "Value=", ENDITEM,
		"Name=IsOrderCanceledHiddenParam", "Value=", ENDITEM,
		"Name=orderNumberHiddenParam", "Value=", ENDITEM,
		"Name=orderIDPkHiddenParam", "Value=", ENDITEM,
		"Name=addOrderLineBtnClicked", "Value=false", ENDITEM,
		"Name=fromOrder", "Value=true", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:radioSelect", "Value=quick", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:_fltrExpColTxt", "Value=DONE", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:_filtrExpColState", "Value=collapsed", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:_filtrExpIconSrc", "Value=/lps/resources/themes/icons/mablue/arrow_expand.gif", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:_filtrColIconSrc", "Value=/lps/resources/themes/icons/mablue/arrow_collapse.gif", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:_filtrdropDownSrc", "Value=/lps/resources/themes/icons/mablue/arrowDown.gif", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field0", "Value=Sales Channel", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field0operator", "Value==", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject0", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field0value1", "Value=[]", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field3", "Value=Order no.", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field3operator", "Value==", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject3", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field3value1ecId", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field3value1", "Value={pOrder_No}", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field4", "Value=Customer", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field4operator", "Value==", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject4", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field4value1ecId", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field4value1", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field5", "Value=Order Type", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field5operator", "Value==", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject5", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field5value1", "Value=[]", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field7", "Value=Status", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field7operator", "Value==", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject7", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field7value1", "Value=[]", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field11", "Value=Destination Facility", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field11operator", "Value==", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject11", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field11value1ecId", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field11value1", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:currentAppliedFilterId", "Value=-1", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_quickFilterGroupButton_mainButtonCategory", "Value=-1", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_quickFilterGroupButton_mainButtonIndex", "Value=-1", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_quickFilterGroupButton_changeDefault", "Value=false", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_savedFilterGroupButton_mainButtonCategory", "Value=-1", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_savedFilterGroupButton_mainButtonIndex", "Value=-1", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_savedFilterGroupButton_changeDefault", "Value=false", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:dummyToGetPrefix", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:filterId", "Value={CorrelationParameter_1}", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:owner", "Value={pUserName}", ENDITEM,
		"Name=customParams ", "Value=&&&", ENDITEM,
		"Name=queryPersistParameter", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:SOList_filterId2:objectType", "Value=DOM ORDER", ENDITEM,
		"Name=isJSF", "Value=true", ENDITEM,
		"Name=filterScreenType", "Value=ON_SCREEN", ENDITEM,
		"Name=dataForm:SOList_entityListView:salesOrderData:pagerBoxValue", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:salesOrderData:isPaginationEvent", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:salesOrderData:pagerAction", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:salesOrderData_deleteHidden", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:salesOrderData_selectedRows", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:salesOrderData:isSortButtonClick", "Value=po.lastUpdatedDttm", ENDITEM,
		"Name=dataForm:SOList_entityListView:salesOrderData:sortDir", "Value=desc", ENDITEM,
		"Name=dataForm:SOList_entityListView:salesOrderData:colCount", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:salesOrderData:tableClicked", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:salesOrderData:tableResized", "Value=false", ENDITEM,
		"Name=salesOrderData_hdnMaxIndexHldr", "Value=1", ENDITEM,
		"Name=dataForm:SOList_entityListView:salesOrderData:0:PK_0", "Value={c_orderIDPkHiddenParam}", ENDITEM,
		"Name=orderNbrHidden", "Value={pOrder_No}", ENDITEM,
		"Name=orderIDPkHidden", "Value={c_orderIDPkHiddenParam}", ENDITEM,
		"Name=IsOrderConfirmedHidden", "Value=true", ENDITEM,
		"Name=CustomerOrderTypeHidden", "Value=4", ENDITEM,
		"Name=IsOrderCanceledHidden", "Value=false", ENDITEM,
		"Name=isEditable", "Value=false", ENDITEM,
		"Name=isEditWarning", "Value=false", ENDITEM,
		"Name=isAddOL", "Value=false", ENDITEM,
		"Name=isConfirm", "Value=false", ENDITEM,
		"Name=isSource", "Value=false", ENDITEM,
		"Name=isSourceWarning", "Value=false", ENDITEM,
		"Name=isAllocate", "Value=false", ENDITEM,
		"Name=isDOCreate", "Value=false", ENDITEM,
		"Name=isDORelease", "Value=false", ENDITEM,
		"Name=isCancel", "Value=false", ENDITEM,
		"Name=isReserve", "Value=false", ENDITEM,
		"Name=isSourceByRate", "Value=false", ENDITEM,
		"Name=isDeAllocate", "Value=false", ENDITEM,
		"Name=isHold", "Value=false", ENDITEM,
		"Name=isUnHold", "Value=false", ENDITEM,
		"Name=isMassUpdate", "Value=false", ENDITEM,
		"Name=isSendUpdate", "Value=true", ENDITEM,
		"Name=markForDeletion", "Value=true", ENDITEM,
		"Name=soFulfillStatus", "Value=850", ENDITEM,
		"Name=dataForm:SOList_entityListView:salesOrderData:0:channelTypeCode", "Value=20", ENDITEM,
		"Name=dataForm:SOList_entityListView:salesOrderData_trs_pageallrowskey", "Value={c_orderIDPkHiddenParam}#:#", ENDITEM,
		"Name=dataForm:SOList_entityListView:salesOrderData_selectedRows", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:salesOrderData_selectedIdList", "Value=", ENDITEM,
		"Name=dataForm:SOList_entityListView:salesOrderData_trs_allselectedrowskey", "Value=salesOrderData$:${c_dataTable}", ENDITEM,
		"Name=moreActionTargetLinksoheaderbuttons", "Value=", ENDITEM,
		"Name=moreActionButtonPressedsoheaderbuttons", "Value=", ENDITEM,
		"Name=backingBeanName", "Value=", ENDITEM,
		"Name=javax.faces.ViewState", "Value={c_ViewState02}", ENDITEM,
		"Name=dataForm:SOList_entityListView:salesOrderData:0:defaultactionbutton", "Value=dataForm:SOList_entityListView:salesOrderData:0:defaultactionbutton", ENDITEM,
		"Name=orderIDPk", "Value={c_orderIDPkHiddenParam}", ENDITEM,
		"Name=orderNumber", "Value={pOrder_No}", ENDITEM,
		"Name=channelType", "Value=20", ENDITEM,
		"Name=IsOrderConfirmed", "Value=true", ENDITEM,
		"Name=CustomerOrderType", "Value=4", ENDITEM,
		"Name=IsOrderCanceled", "Value=false", ENDITEM,
		EXTRARES,
		"URL=/lps/resources/layout/images/backnew.png", ENDITEM,
		"URL=/doms/dom/scripts/sellingCustomerOrder.js", ENDITEM,
		"URL=/doms/dom/scripts/domcommon.js", ENDITEM,
		"URL=/doms/dom/styles/DSStyles.css", ENDITEM,
		"URL=/doms/dom/selling/image/customerorder/add_notes.png", ENDITEM,
		"URL=/doms/dom/selling/image/customerorder/add_communication_note.png", ENDITEM,
		"URL=/doms/dom/selling/image/navigation/cartgreen.gif", ENDITEM,
		"URL=/doms/dom/selling/image/navigation/promoblue.gif", ENDITEM,
		"URL=/doms/dom/selling/image/navigation/packblue.gif", ENDITEM,
		"URL=/doms/dom/selling/image/navigation/payblue.gif", ENDITEM,
		"URL=/lps/resources/panel/images/expand.gif", ENDITEM,
		"URL=/doms/dom/selling/image/customerorder/pencilgrey.png", ENDITEM,
		"URL=/doms/dom/selling/image/customerorder/notes_icon.gif", ENDITEM,
		"URL=/lps/resources/themes/icons/mablue/sortup.gif", "Referer={pURL}/lps/lpsstyles", ENDITEM,
		"URL=/lps/resources/filter/images/default.gif", ENDITEM,
		"URL=/manh/mps/resources/icons/navigate_up.png", "Referer={pURL}/lps/lpsstyles", ENDITEM,
		"URL=/manh/mps/resources/icons/navigate_down.png", "Referer={pURL}/lps/lpsstyles", ENDITEM,
		LAST);

	web_url("ping.jsp_6", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467200757043", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t13.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_05_Open_Sales_Order_By_Double_Clicking_On_It"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_7", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467200772490", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t14.inf", 
		"Mode=HTML", 
		LAST);
	
	/* "sourcingDetailListTable$:$1467200779406" /><script  */
	
	/* "coLineViewAdditionalListTable$:$1467200779406" */
	
	 	/*			web_reg_save_param("c_SourcingTable",
                       "LB=value=\"sourcingDetailListTable",
                       "RB=\"\t/><script type=\"text/javascript\"",
                       "ORD=1",
                       LAST); */
	
	/* "5318721021218658239:-1650991263828701896 */
	
		web_reg_save_param("c_ViewState04",
                       "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
                       "RB=\" autocomplete=\"",
                       "ORD=1",
                       LAST);
	
	/*  Goto More and Navigate 1st Option */ 
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_06_Goto_More_Dropdown_And_Navigate_To_Additional_Details"));

	web_custom_request("ViewCustomerOrder.jsflps",
		"URL={pURL}/doms/dom/selling/co/customerorder/ViewCustomerOrder.jsflps",
		"Method=POST",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/xml",
		"Referer={pURL}/olm/salesorder/StoreOrderList.jsflps",
		"Snapshot=t15.inf",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded; charset=UTF-8",
		"Body=AJAXREQUEST=_viewRoot&dataForm=dataForm&uniqueToken=1&MANH-CSRFToken={MANH-CSRFToken_URL2}&helpurlEle=%2Flcom%2Fcommon%2Fjsp%2Fhelphelper.jsp%3Fserver%3D58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B%26uri%3D%252Fdoms%252Fdom%252Fselling%252Fco%252Fcustomerorder%252FViewCustomerOrder.jsflps%253FpageName%253DviewOrderDetails&dataForm%3AsellingExceptionStkTrcPage_stackTrace_inpArea=&dataForm%3AcoNotesTable_deleteHidden=&dataForm%3AcoNotesTable_selectedRows=&dataForm%3AcoNotesTable%3AisSortButtonClick=&dataForm%3AcoNotesTable%3AsortDir=desc&dataForm%3AcoNotesTable%3AcolCount=&dataForm%3AcoNotesTable%3AtableClicked=&dataForm%3AcoNotesTable%3AtableResized=false&dataForm%3AcoNotesTable%3A0%3APK_0=DUMMYROW&dataForm%3AcoNotesTable%3A0%3AcoNoteTypeColValue=%20&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteSeq=&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteToBeDeletedFlag=&dataForm%3AcoNotesTable%3A0%3AmarkForDelete=&dataForm%3AcoNotesTable%3A0%3AcoNotesColValue=&coNotesTable_hdnMaxIndexHldr=0&d"
		"ataForm%3AcoNotesTable_trs_pageallrowskey=&dataForm%3AcoNotesTable_selectedIdList=&dataForm%3AcoNotesTable_trs_allselectedrowskey=coNotesTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AcoViewNotesTable_deleteHidden=&dataForm%3AcoViewNotesTable_selectedRows=&dataForm%3AcoViewNotesTable%3AisSortButtonClick=&dataForm%3AcoViewNotesTable%3AsortDir=desc&dataForm%3AcoViewNotesTable%3AcolCount=&dataForm%3AcoViewNotesTable%3AtableClicked=&dataForm%3AcoViewNotesTable%3AtableResized=false&coViewNotesTable_hdnMaxIndexHldr=0&dataForm%3AcoViewNotesTable_trs_pageallrowskey=&dataForm%3AcoViewNotesTable_selectedIdList=&dataForm%3AcoViewNotesTable_trs_allselectedrowskey=coViewNotesTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3Aco_hold_rc_popup_sel_one_menu=525&dataForm%3Aco_hold_rc_popup_inp_txt=&dataForm%3AcocancelListOfIdsToBeCancelledHdn=&dataForm%3Aco_cancel_rc_popup_sel_one_menu=664&dataForm%3Aco_cancel_rc_popup_inp_txt=&dataForm%3ACMRegPreferredShippingAddrMenu=110%20Viewpoint%20Dr&dataForm%3ACMRegPreferredBillingAddrMenu=110%20Viewpoi"
		"nt%20Dr&dataForm%3ACMRegPreferredShippingMethodMenu=(none)&dataForm%3ACMRegPreferredPaymentInfoMenu={c_PayInfo}&dataForm%3AcustCommNoteText=&itemPriceOverrideItemIdHdn=&itemPriceOverrideItemDescriptionHdn=&itemPriceOverrideItemOriginalPriceHdn=&itemPriceOverrideItemCurrentPriceHdn=&itemPriceOverrideItemUndergonePriceOverrideHdn=false&itemPriceOverrideorderLineIdHdn=&itemPriceOverrideCurrencySymbolIdHdn=&itemPriceOverrideOrderLineNumberHdn=&itemPriceOverrideReqQtyHdn=&itemPriceOverrideReqUomHdn=&isEligibleForPriceOverridePrmHdn=&conversionFactorPrmHdn=&showOverridePriceTabInpHdn=true&showOverridePriceTabInpHdn=true&dataForm%3AdisplayingPriceHistoryTabInpHdn=false&ajaxTabClicked=&itemPriceOverridePopupEditable_tabPanel_SubmitOnTabClick=false&itemPriceOverridePopupEditable_tabPanel_selectedTab=itemPriceOverridePopupEditablePriceHistoryTab&dataForm%3Aj_id198=false&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedRows=&dataForm%3AitemP"
		"riceOverridePopupPriceHistoryListTable%3AisSortButtonClick=orc.createdDttm&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableClicked=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableResized=false&itemPriceOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=0&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_allselectedrowskey=itemPriceOverridePopupPriceHistoryListTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AdisplayingSnHChargeHistoryTabHdn=true&dataForm%3AdisplaySnHChargeHistoryTabOnlyHdn=true&dataForm%3AdisplayingSnHChargeHistoryTabInpHdn=true&snhChargeOverridePopupEditable_tabPanel_SubmitOnTabClick=false&snhChargeOverridePopupEditable_tabPanel_selectedTab=snhChargeOverridePopupEditablePriceHistoryTab"
		"&dataForm%3Aj_id227=snhChargeOverridePopupEditableTab&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedRows=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AisSortButtonClick=ORC.CREATED_DTTM&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableClicked=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableResized=false&snhChargeOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=0&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_allselectedrowskey=snhChargeOverridePopupPriceHistoryListTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AfltrListFltrId%3AfieldName=&dataForm%3AfltrListFltrId%3AfilterName=FL_{pUserName}&dataForm%3Aflt"
		"rListFltrId%3Aowner={pUserName}&dataForm%3AfltrListFltrId%3AobjectType=FL_FILTER&dataForm%3AfltrListFltrId%3AfilterObjectType=&dataForm%3AfltrListFltrId%3Afield0value1=&dataForm%3AfltrListFltrId%3Afield0=FILTER.FILTER_NAME&dataForm%3AfltrListFltrId%3Afield0operator=&dataForm%3AfltrListFltrId%3Afield1value1=&dataForm%3AfltrListFltrId%3Afield1=FILTER.IS_DEFAULT&dataForm%3AfltrListFltrId%3Afield1operator=&dataForm%3AfltrListFltrId%3Afield2value1=&dataForm%3AfltrListFltrId%3Afield2=FILTER.IS_PRIVATE&dataForm%3AfltrListFltrId%3Afield2operator=&dataForm%3AfltrListFltrId%3Afield3value1=&dataForm%3AfltrListFltrId%3Afield3=FILTER.OWNER&dataForm%3AfltrListFltrId%3Afield3operator=&dataForm%3AfltrListFltrId%3Afield4value1=&dataForm%3AfltrListFltrId%3Afield4=FILTER.IS_DELETED&dataForm%3AfltrListFltrId%3Afield4operator=&dataForm%3AfltrListFltrId%3AfltrCondition=&dataForm%3AfltrListFltrId%3AfltrCrtSel=&windowId=screen-14271270&domainGroupId=110&dataForm%3Ainp_hdn_for_invoking_cco_bb=&dataForm%3Ainp_hdn_for_invoking_ccnf_bb=&or"
		"derNumber={pOrder_No}&dataForm%3Apost_COEdit_inp_hdn_orderVersion={c_Order_Version}&orderVersion={c_Order_Version}&CustomerId=&ExternalCustomerId=&isExchangeOrder=false&hdnRMANo=&inp_hdn_for_order_status=850&hdn_for_discount_code=850&dataForm%3Ahid_postorder_view_createCO_ordno={pOrder_No}&dataForm%3Apost_COView_inp_hdn_orderConfirmed=TRUE&dataForm%3Apost_COView_inp_hdn_orderCreatedDTTM=10%2F4%2F15%2014%3A40%20EDT&dataForm%3Ahid_txt_postorder_view_createCO_fn=Alison&dataForm%3Ahid_txt_postorder_view_createCO_pno=6103636787&dataForm%3Apost_COView_inp_hdn_orderStatus=SHIPPED&dataForm%3Apost_COEView_inp_hdn_orderStatusId=850&dataForm%3Apost_COView_inp_hdn_paymentStatus=PAID&dataForm%3Apost_COView_inp_hdn_paymentStatusId=30&dataForm%3Ahid_txt_postorder_view_createCO_ln=Cruice&dataForm%3Ainp_hidden_postorder_createCO_customerId_out=&dataForm%3Ainp_hidden_postorder_createCO_extcustomerId_out=&dataForm%3Ainp_hidden_postorder_createCO_customerType_out=&dataForm%3Ahid_txt_postorder_view_createCO_ordType={c_orderType}&dataForm%3Ahid_tx"
		"t_postorder_view_createCO_ordTypeId=4&dataForm%3Ahid_txt_postorder_view_createCO_stId=&dataForm%3Ahid_txt_postorder_view_createCO_email=huttonalison%40hotmail.com&dataForm%3Apost_COView_inp_hdn_totalNoOfNotesStr=0&exchangeOrderIdStr=&loadDefaultNavigationFlow=&dataForm%3Adrop_down_page_ids_som=viewAdditionalDetails&loadNotesFromCategory=10&targetLink=&updateOrderHeaderActive=false&addAppeasementsActive=false&updatePaymentsActive=false&updateShippingInfoActive=false&updateQuantityActive=false&cancelItemActive=false&addItemActive=false&updatePromotionsActive=false&createReturnActive=false&updateAdditionalDetailsActive=false&overridePriceActive=false&overrideSnHChargeActive=false&taskDeterminantLoaded=true&contentPageName=viewOrderDetails&dataForm%3AcoROCustomerOrderIdHidden={c_orderIDPkHiddenParam}&dataForm%3AcoROILTable%3Apager%3ApageInput=&dataForm%3AcoROILTable%3ApagerBoxValue=&dataForm%3AcoROILTable%3AisPaginationEvent=&dataForm%3AcoROILTable%3ApagerAction=&dataForm%3AcoROILTable_deleteHidden=&dataForm%3AcoROILTable_select"
		"edRows=&dataForm%3AcoROILTable%3AisSortButtonClick=col.orderLineNumber&dataForm%3AcoROILTable%3AsortDir=asc&dataForm%3AcoROILTable%3AcolCount=&dataForm%3AcoROILTable%3AtableClicked=&dataForm%3AcoROILTable%3AtableResized=false&coROILTable_hdnMaxIndexHldr=1&remainingToPay=0.00&eligibleForSnHOverride=false&CustomerOrderId={c_orderIDPkHiddenParam}&orderIDPk={c_orderIDPkHiddenParam}&mainPageName=&moreActionTargetLinkviewCustomerOrderNavFlowFooterNextBtnPanel=&moreActionButtonPressedviewCustomerOrderNavFlowFooterNextBtnPanel=&moreActionTargetLinkviewCustomerOrderNavFlowFooterHoldCancelPanel=&moreActionButtonPressedviewCustomerOrderNavFlowFooterHoldCancelPanel=&backingBeanName=&javax.faces.ViewState={c_ViewState03}&dataForm%3Aj_id333=dataForm%3Aj_id333&",
		LAST);
	
		/* "5318721021218658239:1200329710367481899*/
	
		web_reg_save_param("c_ViewState05",
                       "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
                       "RB=\" autocomplete=\"",
                       "ORD=1",
                       LAST);

	web_custom_request("ViewCustomerOrder.jsflps_2",
		"URL={pURL}/doms/dom/selling/co/customerorder/ViewCustomerOrder.jsflps",
		"Method=POST",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/xml",
		"Referer={pURL}/olm/salesorder/StoreOrderList.jsflps",
		"Snapshot=t16.inf",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded; charset=UTF-8",
		"Body=AJAXREQUEST=_viewRoot&dataForm=dataForm&uniqueToken=1&MANH-CSRFToken={MANH-CSRFToken_URL2}&helpurlEle=%2Flcom%2Fcommon%2Fjsp%2Fhelphelper.jsp%3Fserver%3D58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B%26uri%3D%252Fdoms%252Fdom%252Fselling%252Fco%252Fcustomerorder%252FViewCustomerOrder.jsflps%253FpageName%253DviewAdditionalDetails&dataForm%3AsellingExceptionStkTrcPage_stackTrace_inpArea=&dataForm%3AcoNotesTable_deleteHidden=&dataForm%3AcoNotesTable_selectedRows=&dataForm%3AcoNotesTable%3AisSortButtonClick=&dataForm%3AcoNotesTable%3AsortDir=desc&dataForm%3AcoNotesTable%3AcolCount=&dataForm%3AcoNotesTable%3AtableClicked=&dataForm%3AcoNotesTable%3AtableResized=false&dataForm%3AcoNotesTable%3A0%3APK_0=DUMMYROW&dataForm%3AcoNotesTable%3A0%3AcoNoteTypeColValue=%20&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteSeq=&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteToBeDeletedFlag=&dataForm%3AcoNotesTable%3A0%3AmarkForDelete=&dataForm%3AcoNotesTable%3A0%3AcoNotesColValue=&coNotesTable_hdnMaxIndexHld"
		"r=0&dataForm%3AcoNotesTable_trs_pageallrowskey=&dataForm%3AcoNotesTable_selectedIdList=&dataForm%3AcoNotesTable_trs_allselectedrowskey=coNotesTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AcoViewNotesTable_deleteHidden=&dataForm%3AcoViewNotesTable_selectedRows=&dataForm%3AcoViewNotesTable%3AisSortButtonClick=&dataForm%3AcoViewNotesTable%3AsortDir=desc&dataForm%3AcoViewNotesTable%3AcolCount=&dataForm%3AcoViewNotesTable%3AtableClicked=&dataForm%3AcoViewNotesTable%3AtableResized=false&coViewNotesTable_hdnMaxIndexHldr=0&dataForm%3AcoViewNotesTable_trs_pageallrowskey=&dataForm%3AcoViewNotesTable_selectedIdList=&dataForm%3AcoViewNotesTable_trs_allselectedrowskey=coViewNotesTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3Aco_hold_rc_popup_sel_one_menu=525&dataForm%3Aco_hold_rc_popup_inp_txt=&dataForm%3AcocancelListOfIdsToBeCancelledHdn=&dataForm%3Aco_cancel_rc_popup_sel_one_menu=664&dataForm%3Aco_cancel_rc_popup_inp_txt=&dataForm%3ACMRegPreferredShippingAddrMenu=110%20Viewpoint%20Dr&dataForm%3ACMRegPreferredBillingAddrMenu=110%20Vi"
		"ewpoint%20Dr&dataForm%3ACMRegPreferredShippingMethodMenu=(none)&dataForm%3ACMRegPreferredPaymentInfoMenu={c_PayInfo}&dataForm%3AcustCommNoteText=&itemPriceOverrideItemIdHdn=&itemPriceOverrideItemDescriptionHdn=&itemPriceOverrideItemOriginalPriceHdn=&itemPriceOverrideItemCurrentPriceHdn=&itemPriceOverrideItemUndergonePriceOverrideHdn=false&itemPriceOverrideorderLineIdHdn=&itemPriceOverrideCurrencySymbolIdHdn=&itemPriceOverrideOrderLineNumberHdn=&itemPriceOverrideReqQtyHdn=&itemPriceOverrideReqUomHdn=&isEligibleForPriceOverridePrmHdn=&conversionFactorPrmHdn=&showOverridePriceTabInpHdn=true&showOverridePriceTabInpHdn=true&dataForm%3AdisplayingPriceHistoryTabInpHdn=false&ajaxTabClicked=&itemPriceOverridePopupEditable_tabPanel_SubmitOnTabClick=false&itemPriceOverridePopupEditable_tabPanel_selectedTab=itemPriceOverridePopupEditablePriceHistoryTab&dataForm%3Aj_id198=false&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedRows=&dataForm%3A"
		"itemPriceOverridePopupPriceHistoryListTable%3AisSortButtonClick=orc.createdDttm&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableClicked=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableResized=false&itemPriceOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=0&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_allselectedrowskey=itemPriceOverridePopupPriceHistoryListTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AdisplayingSnHChargeHistoryTabHdn=true&dataForm%3AdisplaySnHChargeHistoryTabOnlyHdn=true&dataForm%3AdisplayingSnHChargeHistoryTabInpHdn=true&snhChargeOverridePopupEditable_tabPanel_SubmitOnTabClick=false&snhChargeOverridePopupEditable_tabPanel_selectedTab=snhChargeOverridePopupEditablePriceHisto"
		"ryTab&dataForm%3Aj_id227=snhChargeOverridePopupEditableTab&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedRows=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AisSortButtonClick=ORC.CREATED_DTTM&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableClicked=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableResized=false&snhChargeOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=1&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_allselectedrowskey=snhChargeOverridePopupPriceHistoryListTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AfltrListFltrId%3AfieldName=&dataForm%3AfltrListFltrId%3AfilterName=FL_{pUserName}&dataForm%"
		"3AfltrListFltrId%3Aowner={pUserName}&dataForm%3AfltrListFltrId%3AobjectType=FL_FILTER&dataForm%3AfltrListFltrId%3AfilterObjectType=&dataForm%3AfltrListFltrId%3Afield0value1=&dataForm%3AfltrListFltrId%3Afield0=FILTER.FILTER_NAME&dataForm%3AfltrListFltrId%3Afield0operator=&dataForm%3AfltrListFltrId%3Afield1value1=&dataForm%3AfltrListFltrId%3Afield1=FILTER.IS_DEFAULT&dataForm%3AfltrListFltrId%3Afield1operator=&dataForm%3AfltrListFltrId%3Afield2value1=&dataForm%3AfltrListFltrId%3Afield2=FILTER.IS_PRIVATE&dataForm%3AfltrListFltrId%3Afield2operator=&dataForm%3AfltrListFltrId%3Afield3value1=&dataForm%3AfltrListFltrId%3Afield3=FILTER.OWNER&dataForm%3AfltrListFltrId%3Afield3operator=&dataForm%3AfltrListFltrId%3Afield4value1=&dataForm%3AfltrListFltrId%3Afield4=FILTER.IS_DELETED&dataForm%3AfltrListFltrId%3Afield4operator=&dataForm%3AfltrListFltrId%3AfltrCondition=&dataForm%3AfltrListFltrId%3AfltrCrtSel=&windowId=screen-14271270&domainGroupId=110&dataForm%3Ainp_hdn_for_invoking_cco_bb=&dataForm%3Ainp_hdn_for_invoking_ccnf_b"
		"b=&orderNumber={pOrder_No}&dataForm%3Apost_COEdit_inp_hdn_orderVersion={c_Order_Version}&orderVersion={c_Order_Version}&CustomerId=&ExternalCustomerId=&isExchangeOrder=false&hdnRMANo=&inp_hdn_for_order_status=850&hdn_for_discount_code=850&dataForm%3Ahid_postorder_view_createCO_ordno={pOrder_No}&dataForm%3Apost_COView_inp_hdn_orderConfirmed=TRUE&dataForm%3Apost_COView_inp_hdn_orderCreatedDTTM=10%2F4%2F15%2014%3A40%20EDT&dataForm%3Ahid_txt_postorder_view_createCO_fn=Alison&dataForm%3Ahid_txt_postorder_view_createCO_pno=6103636787&dataForm%3Apost_COView_inp_hdn_orderStatus=SHIPPED&dataForm%3Apost_COEView_inp_hdn_orderStatusId=850&dataForm%3Apost_COView_inp_hdn_paymentStatus=PAID&dataForm%3Apost_COView_inp_hdn_paymentStatusId=30&dataForm%3Ahid_txt_postorder_view_createCO_ln=Cruice&dataForm%3Ainp_hidden_postorder_createCO_customerId_out=&dataForm%3Ainp_hidden_postorder_createCO_extcustomerId_out=&dataForm%3Ainp_hidden_postorder_createCO_customerType_out=&dataForm%3Ahid_txt_postorder_view_createCO_ordType={c_orderType}&dataForm%3Ah"
		"id_txt_postorder_view_createCO_ordTypeId=4&dataForm%3Ahid_txt_postorder_view_createCO_stId=&dataForm%3Ahid_txt_postorder_view_createCO_email=huttonalison%40hotmail.com&dataForm%3Apost_COView_inp_hdn_totalNoOfNotesStr=0&exchangeOrderIdStr=&loadDefaultNavigationFlow=&dataForm%3Adrop_down_page_ids_som=viewAdditionalDetails&loadNotesFromCategory=10&targetLink=&updateOrderHeaderActive=false&addAppeasementsActive=false&updatePaymentsActive=false&updateShippingInfoActive=false&updateQuantityActive=false&cancelItemActive=false&addItemActive=false&updatePromotionsActive=false&createReturnActive=false&updateAdditionalDetailsActive=false&overridePriceActive=false&overrideSnHChargeActive=false&taskDeterminantLoaded=true&contentPageName=viewAdditionalDetails&dataForm%3AsourcingDetailListTable_deleteHidden=&dataForm%3AsourcingDetailListTable_selectedRows=&dataForm%3AsourcingDetailListTable%3AisSortButtonClick=&dataForm%3AsourcingDetailListTable%3AsortDir=desc&dataForm%3AsourcingDetailListTable%3AcolCount=&dataForm%3Asour"
		"cingDetailListTable%3AtableClicked=&dataForm%3AsourcingDetailListTable%3AtableResized=false&sourcingDetailListTable_hdnMaxIndexHldr=0&dataForm%3AsourcingDetailListTable_trs_pageallrowskey=&dataForm%3AsourcingDetailListTable_selectedIdList=&dataForm%3AsourcingDetailListTable_trs_allselectedrowskey=sourcingDetailListTable%24%3A%241467200779406&dataForm%3AcoViewAI_lastSelectedTabId=customerOrderAdditionalDetailTab&viewAdditionalDetailsTabPanel_SubmitOnTabClick=false&viewAdditionalDetailsTabPanel_selectedTab=customerOrderAdditionalDetailTab&dataForm%3AcoLineViewAdditionalInfoSyncComp_syncActionHid=syncAction&dataForm%3AcoLineViewAdditionalListTable%3Apager%3ApageInput=&dataForm%3AcoLineViewAdditionalListTable%3ApagerBoxValue=&dataForm%3AcoLineViewAdditionalListTable%3AisPaginationEvent=&dataForm%3AcoLineViewAdditionalListTable%3ApagerAction=&dataForm%3AcoLineViewAdditionalListTable_deleteHidden=&dataForm%3AcoLineViewAdditionalListTable_selectedRows=&dataForm%3AcoLineViewAdditionalListTable%3AisSortButtonClick=c"
		"ol.orderLineNumber&dataForm%3AcoLineViewAdditionalListTable%3AsortDir=asc&dataForm%3AcoLineViewAdditionalListTable%3AcolCount=&dataForm%3AcoLineViewAdditionalListTable%3AtableClicked=&dataForm%3AcoLineViewAdditionalListTable%3AtableResized=false&coLineViewAdditionalListTable_hdnMaxIndexHldr=1&dataForm%3AcoLineViewAdditionalListTable%3A0%3APK_0={c_LineId}&isSource=false&isAllocate=false&isDeAllocate=false&isSourceWarning=false&isViewSourcingResults=true&isCreateDO=false&isReleased=true&dataForm%3AcoLineViewAdditionalListTable_trs_pageallrowskey={c_LineId}%23%3A%23&dataForm%3AcoLineViewAdditionalListTable_selectedIdList=&dataForm%3AcoLineViewAdditionalListTable_trs_allselectedrowskey=coLineViewAdditionalListTable%24%3A%241467200779406&moreActionTargetLinkcoLViewAI_listActionPanel=&moreActionButtonPressedcoLViewAI_listActionPanel=&coLinesListPopulated=true&dataForm%3AcoViewAddInfoCstOrderLineIdHidden=&coLineAdditionalDetailsTabPanel_SubmitOnTabClick=false&coLineAdditionalDetailsTabPanel_selectedTab=viewLineDetailsTa"
		"b&syncClicked=true&dataForm%3AcoLVAI_Hidden_COLNbr=&coLineCanceledStatusFromSyncDetail=false&CustomerOrderId={c_orderIDPkHiddenParam}&orderIDPk={c_orderIDPkHiddenParam}&mainPageName=viewOrderDetails&moreActionTargetLinkviewCustomerOrderDropDownFooterNextBtnPanel=&moreActionButtonPressedviewCustomerOrderDropDownFooterNextBtnPanel=&moreActionTargetLinkcoViewAdditionalInfoButtonsPanel=&moreActionButtonPressedcoViewAdditionalInfoButtonsPanel=&moreActionTargetLinkviewCustomerOrderNavFlowFooterHoldCancelPanel=&moreActionButtonPressedviewCustomerOrderNavFlowFooterHoldCancelPanel=&backingBeanName=&javax.faces.ViewState={c_ViewState04}&coAdditionalLineFulFillmentStatusIdnm=850&syncRecordIdForCOLinesAdditionalInfo={c_LineId}&dataForm%3AcoLineViewAdditionalListTable%3A0%3Acoaiv_td4d=dataForm%3AcoLineViewAdditionalListTable%3A0%3Acoaiv_td4d&",
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_06_Goto_More_Dropdown_And_Navigate_To_Additional_Details"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_8", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467200788054", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t17.inf", 
		"Mode=HTML", 
		LAST);
	
	/* Navigate 2nd Option from More */

	lr_start_transaction(lr_eval_string("{sTestCaseName}_07_Goto_More_Dropdown_And_Navigate_To_Payment_Transactions"));
	
	/* "5318721021218658239:-8179324099670671363*/
	
		web_reg_save_param("c_ViewState06",
                       "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
                       "RB=\" autocomplete=\"",
                       "ORD=1",
                       LAST);
	
	
	web_custom_request("ViewCustomerOrder.jsflps_3",
		"URL={pURL}/doms/dom/selling/co/customerorder/ViewCustomerOrder.jsflps",
		"Method=POST",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/xml",
		"Referer={pURL}/olm/salesorder/StoreOrderList.jsflps",
		"Snapshot=t18.inf",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded; charset=UTF-8",
		"Body=AJAXREQUEST=_viewRoot&dataForm=dataForm&uniqueToken=1&MANH-CSRFToken={MANH-CSRFToken_URL2}&helpurlEle=%2Flcom%2Fcommon%2Fjsp%2Fhelphelper.jsp%3Fserver%3D58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B%26uri%3D%252Fdoms%252Fdom%252Fselling%252Fco%252Fcustomerorder%252FViewCustomerOrder.jsflps%253FpageName%253DviewAdditionalDetails&dataForm%3AsellingExceptionStkTrcPage_stackTrace_inpArea=&dataForm%3AcoNotesTable_deleteHidden=&dataForm%3AcoNotesTable_selectedRows=&dataForm%3AcoNotesTable%3AisSortButtonClick=&dataForm%3AcoNotesTable%3AsortDir=desc&dataForm%3AcoNotesTable%3AcolCount=&dataForm%3AcoNotesTable%3AtableClicked=&dataForm%3AcoNotesTable%3AtableResized=false&dataForm%3AcoNotesTable%3A0%3APK_0=DUMMYROW&dataForm%3AcoNotesTable%3A0%3AcoNoteTypeColValue=%20&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteSeq=&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteToBeDeletedFlag=&dataForm%3AcoNotesTable%3A0%3AmarkForDelete=&dataForm%3AcoNotesTable%3A0%3AcoNotesColValue=&coNotesTable_hdnMaxIndexHld"
		"r=0&dataForm%3AcoNotesTable_trs_pageallrowskey=&dataForm%3AcoNotesTable_selectedIdList=&dataForm%3AcoNotesTable_trs_allselectedrowskey=coNotesTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AcoViewNotesTable_deleteHidden=&dataForm%3AcoViewNotesTable_selectedRows=&dataForm%3AcoViewNotesTable%3AisSortButtonClick=&dataForm%3AcoViewNotesTable%3AsortDir=desc&dataForm%3AcoViewNotesTable%3AcolCount=&dataForm%3AcoViewNotesTable%3AtableClicked=&dataForm%3AcoViewNotesTable%3AtableResized=false&coViewNotesTable_hdnMaxIndexHldr=0&dataForm%3AcoViewNotesTable_trs_pageallrowskey=&dataForm%3AcoViewNotesTable_selectedIdList=&dataForm%3AcoViewNotesTable_trs_allselectedrowskey=coViewNotesTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3Aco_hold_rc_popup_sel_one_menu=525&dataForm%3Aco_hold_rc_popup_inp_txt=&dataForm%3AcocancelListOfIdsToBeCancelledHdn=&dataForm%3Aco_cancel_rc_popup_sel_one_menu=664&dataForm%3Aco_cancel_rc_popup_inp_txt=&dataForm%3ACMRegPreferredShippingAddrMenu=110%20Viewpoint%20Dr&dataForm%3ACMRegPreferredBillingAddrMenu=110%20Vi"
		"ewpoint%20Dr&dataForm%3ACMRegPreferredShippingMethodMenu=(none)&dataForm%3ACMRegPreferredPaymentInfoMenu={c_PayInfo}&dataForm%3AcustCommNoteText=&itemPriceOverrideItemIdHdn=&itemPriceOverrideItemDescriptionHdn=&itemPriceOverrideItemOriginalPriceHdn=&itemPriceOverrideItemCurrentPriceHdn=&itemPriceOverrideItemUndergonePriceOverrideHdn=false&itemPriceOverrideorderLineIdHdn=&itemPriceOverrideCurrencySymbolIdHdn=&itemPriceOverrideOrderLineNumberHdn=&itemPriceOverrideReqQtyHdn=&itemPriceOverrideReqUomHdn=&isEligibleForPriceOverridePrmHdn=&conversionFactorPrmHdn=&showOverridePriceTabInpHdn=true&showOverridePriceTabInpHdn=true&dataForm%3AdisplayingPriceHistoryTabInpHdn=false&ajaxTabClicked=&itemPriceOverridePopupEditable_tabPanel_SubmitOnTabClick=false&itemPriceOverridePopupEditable_tabPanel_selectedTab=itemPriceOverridePopupEditablePriceHistoryTab&dataForm%3Aj_id198=false&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedRows=&dataForm%3A"
		"itemPriceOverridePopupPriceHistoryListTable%3AisSortButtonClick=orc.createdDttm&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableClicked=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableResized=false&itemPriceOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=0&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_allselectedrowskey=itemPriceOverridePopupPriceHistoryListTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AdisplayingSnHChargeHistoryTabHdn=true&dataForm%3AdisplaySnHChargeHistoryTabOnlyHdn=true&dataForm%3AdisplayingSnHChargeHistoryTabInpHdn=true&snhChargeOverridePopupEditable_tabPanel_SubmitOnTabClick=false&snhChargeOverridePopupEditable_tabPanel_selectedTab=snhChargeOverridePopupEditablePriceHisto"
		"ryTab&dataForm%3Aj_id227=snhChargeOverridePopupEditableTab&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedRows=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AisSortButtonClick=ORC.CREATED_DTTM&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableClicked=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableResized=false&snhChargeOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=1&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_allselectedrowskey=snhChargeOverridePopupPriceHistoryListTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AfltrListFltrId%3AfieldName=&dataForm%3AfltrListFltrId%3AfilterName=FL_{pUserName}&dataForm%"
		"3AfltrListFltrId%3Aowner={pUserName}&dataForm%3AfltrListFltrId%3AobjectType=FL_FILTER&dataForm%3AfltrListFltrId%3AfilterObjectType=&dataForm%3AfltrListFltrId%3Afield0value1=&dataForm%3AfltrListFltrId%3Afield0=FILTER.FILTER_NAME&dataForm%3AfltrListFltrId%3Afield0operator=&dataForm%3AfltrListFltrId%3Afield1value1=&dataForm%3AfltrListFltrId%3Afield1=FILTER.IS_DEFAULT&dataForm%3AfltrListFltrId%3Afield1operator=&dataForm%3AfltrListFltrId%3Afield2value1=&dataForm%3AfltrListFltrId%3Afield2=FILTER.IS_PRIVATE&dataForm%3AfltrListFltrId%3Afield2operator=&dataForm%3AfltrListFltrId%3Afield3value1=&dataForm%3AfltrListFltrId%3Afield3=FILTER.OWNER&dataForm%3AfltrListFltrId%3Afield3operator=&dataForm%3AfltrListFltrId%3Afield4value1=&dataForm%3AfltrListFltrId%3Afield4=FILTER.IS_DELETED&dataForm%3AfltrListFltrId%3Afield4operator=&dataForm%3AfltrListFltrId%3AfltrCondition=&dataForm%3AfltrListFltrId%3AfltrCrtSel=&windowId=screen-14271270&domainGroupId=110&dataForm%3Ainp_hdn_for_invoking_cco_bb=&dataForm%3Ainp_hdn_for_invoking_ccnf_b"
		"b=&orderNumber={pOrder_No}&dataForm%3Apost_COEdit_inp_hdn_orderVersion={c_Order_Version}&orderVersion={c_Order_Version}&CustomerId=&ExternalCustomerId=&isExchangeOrder=false&hdnRMANo=&inp_hdn_for_order_status=850&hdn_for_discount_code=850&dataForm%3Ahid_postorder_view_createCO_ordno={pOrder_No}&dataForm%3Apost_COView_inp_hdn_orderConfirmed=TRUE&dataForm%3Apost_COView_inp_hdn_orderCreatedDTTM=10%2F4%2F15%2014%3A40%20EDT&dataForm%3Ahid_txt_postorder_view_createCO_fn=Alison&dataForm%3Ahid_txt_postorder_view_createCO_pno=6103636787&dataForm%3Apost_COView_inp_hdn_orderStatus=SHIPPED&dataForm%3Apost_COEView_inp_hdn_orderStatusId=850&dataForm%3Apost_COView_inp_hdn_paymentStatus=PAID&dataForm%3Apost_COView_inp_hdn_paymentStatusId=30&dataForm%3Ahid_txt_postorder_view_createCO_ln=Cruice&dataForm%3Ainp_hidden_postorder_createCO_customerId_out=&dataForm%3Ainp_hidden_postorder_createCO_extcustomerId_out=&dataForm%3Ainp_hidden_postorder_createCO_customerType_out=&dataForm%3Ahid_txt_postorder_view_createCO_ordType={c_orderType}&dataForm%3Ah"
		"id_txt_postorder_view_createCO_ordTypeId=4&dataForm%3Ahid_txt_postorder_view_createCO_stId=&dataForm%3Ahid_txt_postorder_view_createCO_email=huttonalison%40hotmail.com&dataForm%3Apost_COView_inp_hdn_totalNoOfNotesStr=0&exchangeOrderIdStr=&loadDefaultNavigationFlow=&dataForm%3Adrop_down_page_ids_som=viewPaymentTransactions&loadNotesFromCategory=10&targetLink=&updateOrderHeaderActive=false&addAppeasementsActive=false&updatePaymentsActive=false&updateShippingInfoActive=false&updateQuantityActive=false&cancelItemActive=false&addItemActive=false&updatePromotionsActive=false&createReturnActive=false&updateAdditionalDetailsActive=false&overridePriceActive=false&overrideSnHChargeActive=false&taskDeterminantLoaded=true&contentPageName=viewAdditionalDetails&dataForm%3AsourcingDetailListTable_deleteHidden=&dataForm%3AsourcingDetailListTable_selectedRows=&dataForm%3AsourcingDetailListTable%3AisSortButtonClick=&dataForm%3AsourcingDetailListTable%3AsortDir=desc&dataForm%3AsourcingDetailListTable%3AcolCount=&dataForm%3Aso"
		"urcingDetailListTable%3AtableClicked=&dataForm%3AsourcingDetailListTable%3AtableResized=false&sourcingDetailListTable_hdnMaxIndexHldr=0&dataForm%3AsourcingDetailListTable_trs_pageallrowskey=&dataForm%3AsourcingDetailListTable_selectedIdList=&dataForm%3AsourcingDetailListTable_trs_allselectedrowskey=sourcingDetailListTable%24%3A%241467200779406&dataForm%3AcoViewAI_lastSelectedTabId=customerOrderAdditionalDetailTab&viewAdditionalDetailsTabPanel_SubmitOnTabClick=false&viewAdditionalDetailsTabPanel_selectedTab=customerOrderAdditionalDetailTab&dataForm%3AcoLineViewAdditionalInfoSyncComp_syncActionHid=&dataForm%3AcoLineViewAdditionalListTable%3Apager%3ApageInput=&dataForm%3AcoLineViewAdditionalListTable%3ApagerBoxValue=&dataForm%3AcoLineViewAdditionalListTable%3AisPaginationEvent=&dataForm%3AcoLineViewAdditionalListTable%3ApagerAction=&dataForm%3AcoLineViewAdditionalListTable_deleteHidden=&dataForm%3AcoLineViewAdditionalListTable_selectedRows={c_LineId}%23%3A%23&dataForm%3AcoLineViewAdditionalListTable_selectedRows="
		"&dataForm%3AcoLineViewAdditionalListTable%3AisSortButtonClick=col.orderLineNumber&dataForm%3AcoLineViewAdditionalListTable%3AsortDir=asc&dataForm%3AcoLineViewAdditionalListTable%3AcolCount=&dataForm%3AcoLineViewAdditionalListTable%3AtableClicked=&dataForm%3AcoLineViewAdditionalListTable%3AtableResized=false&coLineViewAdditionalListTable_hdnMaxIndexHldr=1&checkAll_c0_dataForm%3AcoLineViewAdditionalListTable=0&dataForm%3AcoLineViewAdditionalListTable%3A0%3APK_0={c_LineId}&isSource=false&isAllocate=false&isDeAllocate=false&isSourceWarning=false&isViewSourcingResults=true&isCreateDO=false&isReleased=true&dataForm%3AcoLineViewAdditionalListTable_trs_pageallrowskey={c_LineId}%23%3A%23&dataForm%3AcoLineViewAdditionalListTable_selectedIdList=&dataForm%3AcoLineViewAdditionalListTable_trs_allselectedrowskey=coLineViewAdditionalListTable%24%3A%241467200779406&moreActionTargetLinkcoLViewAI_listActionPanel=&moreActionButtonPressedcoLViewAI_listActionPanel=&coLinesListPopulated=true&dataForm%3AcoViewAddInfoCstOrderLineIdHidden"
		"={c_LineId}&coLineAdditionalDetailsTabPanel_SubmitOnTabClick=false&coLineAdditionalDetailsTabPanel_selectedTab=viewLineDetailsTab&syncClicked=false&dataForm%3AcoLVAI_Hidden_COLNbr={c_LineNumber}&coLineCanceledStatusFromSyncDetail=true&CustomerOrderId={c_orderIDPkHiddenParam}&orderIDPk={c_orderIDPkHiddenParam}&mainPageName=viewOrderDetails&moreActionTargetLinkviewCustomerOrderDropDownFooterNextBtnPanel=&moreActionButtonPressedviewCustomerOrderDropDownFooterNextBtnPanel=&moreActionTargetLinkcoViewAdditionalInfoButtonsPanel=&moreActionButtonPressedcoViewAdditionalInfoButtonsPanel=&moreActionTargetLinkviewCustomerOrderNavFlowFooterHoldCancelPanel=&moreActionButtonPressedviewCustomerOrderNavFlowFooterHoldCancelPanel=&backingBeanName=&javax.faces.ViewState={c_ViewState05}&dataForm%3Aj_id333=dataForm%3Aj_id333&",
		LAST);
	
	/* 7519001242079730298 */
	
	web_reg_save_param("c_ViewState07",
                       "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
                       "RB=\" autocomplete=\"",
                       "ORD=1",
                       LAST);

	web_custom_request("ViewCustomerOrder.jsflps_4",
		"URL={pURL}/doms/dom/selling/co/customerorder/ViewCustomerOrder.jsflps",
		"Method=POST",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/xml",
		"Referer={pURL}/olm/salesorder/StoreOrderList.jsflps",
		"Snapshot=t19.inf",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded; charset=UTF-8",
		"Body=AJAXREQUEST=_viewRoot&dataForm=dataForm&uniqueToken=1&MANH-CSRFToken={MANH-CSRFToken_URL2}&helpurlEle=%2Flcom%2Fcommon%2Fjsp%2Fhelphelper.jsp%3Fserver%3D58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B%26uri%3D%252Fdoms%252Fdom%252Fselling%252Fco%252Fcustomerorder%252FViewCustomerOrder.jsflps%253FpageName%253DviewPaymentTransactions&dataForm%3AsellingExceptionStkTrcPage_stackTrace_inpArea=&dataForm%3AcoNotesTable_deleteHidden=&dataForm%3AcoNotesTable_selectedRows=&dataForm%3AcoNotesTable%3AisSortButtonClick=&dataForm%3AcoNotesTable%3AsortDir=desc&dataForm%3AcoNotesTable%3AcolCount=&dataForm%3AcoNotesTable%3AtableClicked=&dataForm%3AcoNotesTable%3AtableResized=false&dataForm%3AcoNotesTable%3A0%3APK_0=DUMMYROW&dataForm%3AcoNotesTable%3A0%3AcoNoteTypeColValue=%20&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteSeq=&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteToBeDeletedFlag=&dataForm%3AcoNotesTable%3A0%3AmarkForDelete=&dataForm%3AcoNotesTable%3A0%3AcoNotesColValue=&coNotesTable_hdnMaxIndexH"
		"ldr=0&dataForm%3AcoNotesTable_trs_pageallrowskey=&dataForm%3AcoNotesTable_selectedIdList=&dataForm%3AcoNotesTable_trs_allselectedrowskey=coNotesTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AcoViewNotesTable_deleteHidden=&dataForm%3AcoViewNotesTable_selectedRows=&dataForm%3AcoViewNotesTable%3AisSortButtonClick=&dataForm%3AcoViewNotesTable%3AsortDir=desc&dataForm%3AcoViewNotesTable%3AcolCount=&dataForm%3AcoViewNotesTable%3AtableClicked=&dataForm%3AcoViewNotesTable%3AtableResized=false&coViewNotesTable_hdnMaxIndexHldr=0&dataForm%3AcoViewNotesTable_trs_pageallrowskey=&dataForm%3AcoViewNotesTable_selectedIdList=&dataForm%3AcoViewNotesTable_trs_allselectedrowskey=coViewNotesTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3Aco_hold_rc_popup_sel_one_menu=525&dataForm%3Aco_hold_rc_popup_inp_txt=&dataForm%3AcocancelListOfIdsToBeCancelledHdn=&dataForm%3Aco_cancel_rc_popup_sel_one_menu=664&dataForm%3Aco_cancel_rc_popup_inp_txt=&dataForm%3ACMRegPreferredShippingAddrMenu=110%20Viewpoint%20Dr&dataForm%3ACMRegPreferredBillingAddrMenu=110%20"
		"Viewpoint%20Dr&dataForm%3ACMRegPreferredShippingMethodMenu=(none)&dataForm%3ACMRegPreferredPaymentInfoMenu={c_PayInfo}&dataForm%3AcustCommNoteText=&itemPriceOverrideItemIdHdn=&itemPriceOverrideItemDescriptionHdn=&itemPriceOverrideItemOriginalPriceHdn=&itemPriceOverrideItemCurrentPriceHdn=&itemPriceOverrideItemUndergonePriceOverrideHdn=false&itemPriceOverrideorderLineIdHdn=&itemPriceOverrideCurrencySymbolIdHdn=&itemPriceOverrideOrderLineNumberHdn=&itemPriceOverrideReqQtyHdn=&itemPriceOverrideReqUomHdn=&isEligibleForPriceOverridePrmHdn=&conversionFactorPrmHdn=&showOverridePriceTabInpHdn=true&showOverridePriceTabInpHdn=true&dataForm%3AdisplayingPriceHistoryTabInpHdn=false&ajaxTabClicked=&itemPriceOverridePopupEditable_tabPanel_SubmitOnTabClick=false&itemPriceOverridePopupEditable_tabPanel_selectedTab=itemPriceOverridePopupEditablePriceHistoryTab&dataForm%3Aj_id198=false&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedRows=&dataForm%"
		"3AitemPriceOverridePopupPriceHistoryListTable%3AisSortButtonClick=orc.createdDttm&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableClicked=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableResized=false&itemPriceOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=0&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_allselectedrowskey=itemPriceOverridePopupPriceHistoryListTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AdisplayingSnHChargeHistoryTabHdn=true&dataForm%3AdisplaySnHChargeHistoryTabOnlyHdn=true&dataForm%3AdisplayingSnHChargeHistoryTabInpHdn=true&snhChargeOverridePopupEditable_tabPanel_SubmitOnTabClick=false&snhChargeOverridePopupEditable_tabPanel_selectedTab=snhChargeOverridePopupEditablePriceHis"
		"toryTab&dataForm%3Aj_id227=snhChargeOverridePopupEditableTab&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedRows=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AisSortButtonClick=ORC.CREATED_DTTM&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableClicked=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableResized=false&snhChargeOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=1&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_allselectedrowskey=snhChargeOverridePopupPriceHistoryListTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AfltrListFltrId%3AfieldName=&dataForm%3AfltrListFltrId%3AfilterName=FL_{pUserName}&dataFor"
		"m%3AfltrListFltrId%3Aowner={pUserName}&dataForm%3AfltrListFltrId%3AobjectType=FL_FILTER&dataForm%3AfltrListFltrId%3AfilterObjectType=&dataForm%3AfltrListFltrId%3Afield0value1=&dataForm%3AfltrListFltrId%3Afield0=FILTER.FILTER_NAME&dataForm%3AfltrListFltrId%3Afield0operator=&dataForm%3AfltrListFltrId%3Afield1value1=&dataForm%3AfltrListFltrId%3Afield1=FILTER.IS_DEFAULT&dataForm%3AfltrListFltrId%3Afield1operator=&dataForm%3AfltrListFltrId%3Afield2value1=&dataForm%3AfltrListFltrId%3Afield2=FILTER.IS_PRIVATE&dataForm%3AfltrListFltrId%3Afield2operator=&dataForm%3AfltrListFltrId%3Afield3value1=&dataForm%3AfltrListFltrId%3Afield3=FILTER.OWNER&dataForm%3AfltrListFltrId%3Afield3operator=&dataForm%3AfltrListFltrId%3Afield4value1=&dataForm%3AfltrListFltrId%3Afield4=FILTER.IS_DELETED&dataForm%3AfltrListFltrId%3Afield4operator=&dataForm%3AfltrListFltrId%3AfltrCondition=&dataForm%3AfltrListFltrId%3AfltrCrtSel=&windowId=screen-14271270&domainGroupId=110&dataForm%3Ainp_hdn_for_invoking_cco_bb=&dataForm%3Ainp_hdn_for_invoking_ccnf"
		"_bb=&orderNumber={pOrder_No}&dataForm%3Apost_COEdit_inp_hdn_orderVersion={c_Order_Version}&orderVersion={c_Order_Version}&CustomerId=&ExternalCustomerId=&isExchangeOrder=false&hdnRMANo=&inp_hdn_for_order_status=850&hdn_for_discount_code=850&dataForm%3Ahid_postorder_view_createCO_ordno={pOrder_No}&dataForm%3Apost_COView_inp_hdn_orderConfirmed=TRUE&dataForm%3Apost_COView_inp_hdn_orderCreatedDTTM=10%2F4%2F15%2014%3A40%20EDT&dataForm%3Ahid_txt_postorder_view_createCO_fn=Alison&dataForm%3Ahid_txt_postorder_view_createCO_pno=6103636787&dataForm%3Apost_COView_inp_hdn_orderStatus=SHIPPED&dataForm%3Apost_COEView_inp_hdn_orderStatusId=850&dataForm%3Apost_COView_inp_hdn_paymentStatus=PAID&dataForm%3Apost_COView_inp_hdn_paymentStatusId=30&dataForm%3Ahid_txt_postorder_view_createCO_ln=Cruice&dataForm%3Ainp_hidden_postorder_createCO_customerId_out=&dataForm%3Ainp_hidden_postorder_createCO_extcustomerId_out=&dataForm%3Ainp_hidden_postorder_createCO_customerType_out=&dataForm%3Ahid_txt_postorder_view_createCO_ordType={c_orderType}&dataForm%3"
		"Ahid_txt_postorder_view_createCO_ordTypeId=4&dataForm%3Ahid_txt_postorder_view_createCO_stId=&dataForm%3Ahid_txt_postorder_view_createCO_email=huttonalison%40hotmail.com&dataForm%3Apost_COView_inp_hdn_totalNoOfNotesStr=0&exchangeOrderIdStr=&loadDefaultNavigationFlow=&dataForm%3Adrop_down_page_ids_som=viewPaymentTransactions&loadNotesFromCategory=10&targetLink=&updateOrderHeaderActive=false&addAppeasementsActive=false&updatePaymentsActive=false&updateShippingInfoActive=false&updateQuantityActive=false&cancelItemActive=false&addItemActive=false&updatePromotionsActive=false&createReturnActive=false&updateAdditionalDetailsActive=false&overridePriceActive=false&overrideSnHChargeActive=false&taskDeterminantLoaded=true&contentPageName=viewPaymentTransactions&dataForm%3AcustomerOrderId={c_orderIDPkHiddenParam}&dataForm%3Async_comp_id_syncActionHid=syncAction&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3Apager%3ApageInput=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3ApagerBoxValu"
		"e=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3AisPaginationEvent=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3ApagerAction=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable_deleteHidden=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable_selectedRows=%23%3A%23&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable_selectedRows=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3AisSortButtonClick=pt.transactionDTTM%2C%20pt.identity&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3AsortDir=asc&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3AcolCount=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3AtableClicked=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3AtableResized=false&paymentTransactionListTable_hdnMaxIndexHldr=2&dataForm%3ApaymentTransactionListView_id%3ApaymentTransac"
		"tionListTable%3A0%3APK_0=248828&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3A1%3APK_1=250211&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable_trs_pageallrowskey=248828%23%3A%23250211%23%3A%23&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable_selectedIdList=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable_trs_allselectedrowskey=paymentTransactionListTable%24%3A%241467200794265&isPaymentTransactionListPopulated=true&moreActionTargetLinkpaymentTransactionListTableButtonActionPanel=&moreActionButtonPressedpaymentTransactionListTableButtonActionPanel=&paymentTransactionDetail_SubmitOnTabClick=false&paymentTransactionDetail_selectedTab=TAB1&syncClicked=true&CustomerOrderId={c_orderIDPkHiddenParam}&orderIDPk={c_orderIDPkHiddenParam}&mainPageName=viewOrderDetails&moreActionTargetLinkviewCustomerOrderDropDownFooterNextBtnPanel=&moreActionButtonPressedviewCustomerOrderDropDownFooterNextBtnPanel=&moreActionTargetLinkviewCustomerOrderNavFlowFooterHoldCancelPanel"
		"=&moreActionButtonPressedviewCustomerOrderNavFlowFooterHoldCancelPanel=&backingBeanName=&javax.faces.ViewState={c_ViewState06}dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3A0%3Atd4d=dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3A0%3Atd4d&syncRecordId=248828&",
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_07_Goto_More_Dropdown_And_Navigate_To_Payment_Transactions"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_9", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467200803352", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t20.inf", 
		"Mode=HTML", 
		LAST);

	/* Navigate 3rd Option */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_08_Goto_More_Dropdown_And_Navigate_To_Allocation_Details"));
	
	/* 6534549934617496407 */
	
	web_reg_save_param("c_ViewState08",
                       "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
                       "RB=\" autocomplete=\"",
                       "ORD=1",
                       LAST);

	web_custom_request("ViewCustomerOrder.jsflps_5",
		"URL={pURL}/doms/dom/selling/co/customerorder/ViewCustomerOrder.jsflps",
		"Method=POST",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/xml",
		"Referer={pURL}/olm/salesorder/StoreOrderList.jsflps",
		"Snapshot=t21.inf",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded; charset=UTF-8",
		"Body=AJAXREQUEST=_viewRoot&dataForm=dataForm&uniqueToken=1&MANH-CSRFToken={MANH-CSRFToken_URL2}&helpurlEle=%2Flcom%2Fcommon%2Fjsp%2Fhelphelper.jsp%3Fserver%3D58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B%26uri%3D%252Fdoms%252Fdom%252Fselling%252Fco%252Fcustomerorder%252FViewCustomerOrder.jsflps%253FpageName%253DviewPaymentTransactions&dataForm%3AsellingExceptionStkTrcPage_stackTrace_inpArea=&dataForm%3AcoNotesTable_deleteHidden=&dataForm%3AcoNotesTable_selectedRows=&dataForm%3AcoNotesTable%3AisSortButtonClick=&dataForm%3AcoNotesTable%3AsortDir=desc&dataForm%3AcoNotesTable%3AcolCount=&dataForm%3AcoNotesTable%3AtableClicked=&dataForm%3AcoNotesTable%3AtableResized=false&dataForm%3AcoNotesTable%3A0%3APK_0=DUMMYROW&dataForm%3AcoNotesTable%3A0%3AcoNoteTypeColValue=%20&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteSeq=&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteToBeDeletedFlag=&dataForm%3AcoNotesTable%3A0%3AmarkForDelete=&dataForm%3AcoNotesTable%3A0%3AcoNotesColValue=&coNotesTable_hdnMaxIndexH"
		"ldr=0&dataForm%3AcoNotesTable_trs_pageallrowskey=&dataForm%3AcoNotesTable_selectedIdList=&dataForm%3AcoNotesTable_trs_allselectedrowskey=coNotesTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AcoViewNotesTable_deleteHidden=&dataForm%3AcoViewNotesTable_selectedRows=&dataForm%3AcoViewNotesTable%3AisSortButtonClick=&dataForm%3AcoViewNotesTable%3AsortDir=desc&dataForm%3AcoViewNotesTable%3AcolCount=&dataForm%3AcoViewNotesTable%3AtableClicked=&dataForm%3AcoViewNotesTable%3AtableResized=false&coViewNotesTable_hdnMaxIndexHldr=0&dataForm%3AcoViewNotesTable_trs_pageallrowskey=&dataForm%3AcoViewNotesTable_selectedIdList=&dataForm%3AcoViewNotesTable_trs_allselectedrowskey=coViewNotesTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3Aco_hold_rc_popup_sel_one_menu=525&dataForm%3Aco_hold_rc_popup_inp_txt=&dataForm%3AcocancelListOfIdsToBeCancelledHdn=&dataForm%3Aco_cancel_rc_popup_sel_one_menu=664&dataForm%3Aco_cancel_rc_popup_inp_txt=&dataForm%3ACMRegPreferredShippingAddrMenu=110%20Viewpoint%20Dr&dataForm%3ACMRegPreferredBillingAddrMenu=110%20"
		"Viewpoint%20Dr&dataForm%3ACMRegPreferredShippingMethodMenu=(none)&dataForm%3ACMRegPreferredPaymentInfoMenu={c_PayInfo}&dataForm%3AcustCommNoteText=&itemPriceOverrideItemIdHdn=&itemPriceOverrideItemDescriptionHdn=&itemPriceOverrideItemOriginalPriceHdn=&itemPriceOverrideItemCurrentPriceHdn=&itemPriceOverrideItemUndergonePriceOverrideHdn=false&itemPriceOverrideorderLineIdHdn=&itemPriceOverrideCurrencySymbolIdHdn=&itemPriceOverrideOrderLineNumberHdn=&itemPriceOverrideReqQtyHdn=&itemPriceOverrideReqUomHdn=&isEligibleForPriceOverridePrmHdn=&conversionFactorPrmHdn=&showOverridePriceTabInpHdn=true&showOverridePriceTabInpHdn=true&dataForm%3AdisplayingPriceHistoryTabInpHdn=false&ajaxTabClicked=&itemPriceOverridePopupEditable_tabPanel_SubmitOnTabClick=false&itemPriceOverridePopupEditable_tabPanel_selectedTab=itemPriceOverridePopupEditablePriceHistoryTab&dataForm%3Aj_id198=false&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedRows=&dataForm%"
		"3AitemPriceOverridePopupPriceHistoryListTable%3AisSortButtonClick=orc.createdDttm&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableClicked=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableResized=false&itemPriceOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=0&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_allselectedrowskey=itemPriceOverridePopupPriceHistoryListTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AdisplayingSnHChargeHistoryTabHdn=true&dataForm%3AdisplaySnHChargeHistoryTabOnlyHdn=true&dataForm%3AdisplayingSnHChargeHistoryTabInpHdn=true&snhChargeOverridePopupEditable_tabPanel_SubmitOnTabClick=false&snhChargeOverridePopupEditable_tabPanel_selectedTab=snhChargeOverridePopupEditablePriceHis"
		"toryTab&dataForm%3Aj_id227=snhChargeOverridePopupEditableTab&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedRows=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AisSortButtonClick=ORC.CREATED_DTTM&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableClicked=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableResized=false&snhChargeOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=1&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_allselectedrowskey=snhChargeOverridePopupPriceHistoryListTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AfltrListFltrId%3AfieldName=&dataForm%3AfltrListFltrId%3AfilterName=FL_{pUserName}&dataFor"
		"m%3AfltrListFltrId%3Aowner={pUserName}&dataForm%3AfltrListFltrId%3AobjectType=FL_FILTER&dataForm%3AfltrListFltrId%3AfilterObjectType=&dataForm%3AfltrListFltrId%3Afield0value1=&dataForm%3AfltrListFltrId%3Afield0=FILTER.FILTER_NAME&dataForm%3AfltrListFltrId%3Afield0operator=&dataForm%3AfltrListFltrId%3Afield1value1=&dataForm%3AfltrListFltrId%3Afield1=FILTER.IS_DEFAULT&dataForm%3AfltrListFltrId%3Afield1operator=&dataForm%3AfltrListFltrId%3Afield2value1=&dataForm%3AfltrListFltrId%3Afield2=FILTER.IS_PRIVATE&dataForm%3AfltrListFltrId%3Afield2operator=&dataForm%3AfltrListFltrId%3Afield3value1=&dataForm%3AfltrListFltrId%3Afield3=FILTER.OWNER&dataForm%3AfltrListFltrId%3Afield3operator=&dataForm%3AfltrListFltrId%3Afield4value1=&dataForm%3AfltrListFltrId%3Afield4=FILTER.IS_DELETED&dataForm%3AfltrListFltrId%3Afield4operator=&dataForm%3AfltrListFltrId%3AfltrCondition=&dataForm%3AfltrListFltrId%3AfltrCrtSel=&windowId=screen-14271270&domainGroupId=110&dataForm%3Ainp_hdn_for_invoking_cco_bb=&dataForm%3Ainp_hdn_for_invoking_ccnf"
		"_bb=&orderNumber={pOrder_No}&dataForm%3Apost_COEdit_inp_hdn_orderVersion={c_Order_Version}&orderVersion={c_Order_Version}&CustomerId=&ExternalCustomerId=&isExchangeOrder=false&hdnRMANo=&inp_hdn_for_order_status=850&hdn_for_discount_code=850&dataForm%3Ahid_postorder_view_createCO_ordno={pOrder_No}&dataForm%3Apost_COView_inp_hdn_orderConfirmed=TRUE&dataForm%3Apost_COView_inp_hdn_orderCreatedDTTM=10%2F4%2F15%2014%3A40%20EDT&dataForm%3Ahid_txt_postorder_view_createCO_fn=Alison&dataForm%3Ahid_txt_postorder_view_createCO_pno=6103636787&dataForm%3Apost_COView_inp_hdn_orderStatus=SHIPPED&dataForm%3Apost_COEView_inp_hdn_orderStatusId=850&dataForm%3Apost_COView_inp_hdn_paymentStatus=PAID&dataForm%3Apost_COView_inp_hdn_paymentStatusId=30&dataForm%3Ahid_txt_postorder_view_createCO_ln=Cruice&dataForm%3Ainp_hidden_postorder_createCO_customerId_out=&dataForm%3Ainp_hidden_postorder_createCO_extcustomerId_out=&dataForm%3Ainp_hidden_postorder_createCO_customerType_out=&dataForm%3Ahid_txt_postorder_view_createCO_ordType={c_orderType}&dataForm%3"
		"Ahid_txt_postorder_view_createCO_ordTypeId=4&dataForm%3Ahid_txt_postorder_view_createCO_stId=&dataForm%3Ahid_txt_postorder_view_createCO_email=huttonalison%40hotmail.com&dataForm%3Apost_COView_inp_hdn_totalNoOfNotesStr=0&exchangeOrderIdStr=&loadDefaultNavigationFlow=&dataForm%3Adrop_down_page_ids_som=viewAllocationDetails&loadNotesFromCategory=10&targetLink=&updateOrderHeaderActive=false&addAppeasementsActive=false&updatePaymentsActive=false&updateShippingInfoActive=false&updateQuantityActive=false&cancelItemActive=false&addItemActive=false&updatePromotionsActive=false&createReturnActive=false&updateAdditionalDetailsActive=false&overridePriceActive=false&overrideSnHChargeActive=false&taskDeterminantLoaded=true&contentPageName=viewPaymentTransactions&dataForm%3AcustomerOrderId={c_orderIDPkHiddenParam}&dataForm%3Async_comp_id_syncActionHid=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3Apager%3ApageInput=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3ApagerBoxValue=&dataForm%"
		"3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3AisPaginationEvent=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3ApagerAction=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable_deleteHidden=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable_selectedRows=%23%3A%23&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable_selectedRows=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3AisSortButtonClick=pt.transactionDTTM%2C%20pt.identity&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3AsortDir=asc&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3AcolCount=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3AtableClicked=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3AtableResized=false&paymentTransactionListTable_hdnMaxIndexHldr=2&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTabl"
		"e%3A0%3APK_0=248828&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable%3A1%3APK_1=250211&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable_trs_pageallrowskey=248828%23%3A%23250211%23%3A%23&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable_selectedIdList=&dataForm%3ApaymentTransactionListView_id%3ApaymentTransactionListTable_trs_allselectedrowskey=paymentTransactionListTable%24%3A%241467200794265&isPaymentTransactionListPopulated=true&moreActionTargetLinkpaymentTransactionListTableButtonActionPanel=&moreActionButtonPressedpaymentTransactionListTableButtonActionPanel=&paymentTransactionDetail_SubmitOnTabClick=false&paymentTransactionDetail_selectedTab=TAB1&syncClicked=false&CustomerOrderId={c_orderIDPkHiddenParam}&orderIDPk={c_orderIDPkHiddenParam}&mainPageName=viewOrderDetails&moreActionTargetLinkviewCustomerOrderDropDownFooterNextBtnPanel=&moreActionButtonPressedviewCustomerOrderDropDownFooterNextBtnPanel=&moreActionTargetLinkviewCustomerOrderNavFlowFooterHoldCancelPanel=&moreActio"
		"nButtonPressedviewCustomerOrderNavFlowFooterHoldCancelPanel=&backingBeanName=&javax.faces.ViewState={c_ViewState07}&dataForm%3Aj_id333=dataForm%3Aj_id333&",
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_08_Goto_More_Dropdown_And_Navigate_To_Allocation_Details"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_10", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467200818643", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t22.inf", 
		"Mode=HTML", 
		LAST);

	/* Navigate 4th Option */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_09_Goto_More_Dropdown_And_Navigate_To_Audit_Details"));

	web_custom_request("ViewCustomerOrder.jsflps_6",
		"URL={pURL}/doms/dom/selling/co/customerorder/ViewCustomerOrder.jsflps",
		"Method=POST",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/xml",
		"Referer={pURL}/olm/salesorder/StoreOrderList.jsflps",
		"Snapshot=t23.inf",
		"Mode=HTML",
		"EncType=application/x-www-form-urlencoded; charset=UTF-8",
		"Body=AJAXREQUEST=_viewRoot&dataForm=dataForm&uniqueToken=1&MANH-CSRFToken={MANH-CSRFToken_URL2}&helpurlEle=%2Flcom%2Fcommon%2Fjsp%2Fhelphelper.jsp%3Fserver%3D58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B%26uri%3D%252Fdoms%252Fdom%252Fselling%252Fco%252Fcustomerorder%252FViewCustomerOrder.jsflps%253FpageName%253DviewAllocationDetails&dataForm%3AsellingExceptionStkTrcPage_stackTrace_inpArea=&dataForm%3AcoNotesTable_deleteHidden=&dataForm%3AcoNotesTable_selectedRows=&dataForm%3AcoNotesTable%3AisSortButtonClick=&dataForm%3AcoNotesTable%3AsortDir=desc&dataForm%3AcoNotesTable%3AcolCount=&dataForm%3AcoNotesTable%3AtableClicked=&dataForm%3AcoNotesTable%3AtableResized=false&dataForm%3AcoNotesTable%3A0%3APK_0=DUMMYROW&dataForm%3AcoNotesTable%3A0%3AcoNoteTypeColValue=%20&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteSeq=&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteToBeDeletedFlag=&dataForm%3AcoNotesTable%3A0%3AmarkForDelete=&dataForm%3AcoNotesTable%3A0%3AcoNotesColValue=&coNotesTable_hdnMaxIndexHld"
		"r=0&dataForm%3AcoNotesTable_trs_pageallrowskey=&dataForm%3AcoNotesTable_selectedIdList=&dataForm%3AcoNotesTable_trs_allselectedrowskey=coNotesTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AcoViewNotesTable_deleteHidden=&dataForm%3AcoViewNotesTable_selectedRows=&dataForm%3AcoViewNotesTable%3AisSortButtonClick=&dataForm%3AcoViewNotesTable%3AsortDir=desc&dataForm%3AcoViewNotesTable%3AcolCount=&dataForm%3AcoViewNotesTable%3AtableClicked=&dataForm%3AcoViewNotesTable%3AtableResized=false&coViewNotesTable_hdnMaxIndexHldr=0&dataForm%3AcoViewNotesTable_trs_pageallrowskey=&dataForm%3AcoViewNotesTable_selectedIdList=&dataForm%3AcoViewNotesTable_trs_allselectedrowskey=coViewNotesTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3Aco_hold_rc_popup_sel_one_menu=525&dataForm%3Aco_hold_rc_popup_inp_txt=&dataForm%3AcocancelListOfIdsToBeCancelledHdn=&dataForm%3Aco_cancel_rc_popup_sel_one_menu=664&dataForm%3Aco_cancel_rc_popup_inp_txt=&dataForm%3ACMRegPreferredShippingAddrMenu=110%20Viewpoint%20Dr&dataForm%3ACMRegPreferredBillingAddrMenu=110%20Vi"
		"ewpoint%20Dr&dataForm%3ACMRegPreferredShippingMethodMenu=(none)&dataForm%3ACMRegPreferredPaymentInfoMenu={c_PayInfo}&dataForm%3AcustCommNoteText=&itemPriceOverrideItemIdHdn=&itemPriceOverrideItemDescriptionHdn=&itemPriceOverrideItemOriginalPriceHdn=&itemPriceOverrideItemCurrentPriceHdn=&itemPriceOverrideItemUndergonePriceOverrideHdn=false&itemPriceOverrideorderLineIdHdn=&itemPriceOverrideCurrencySymbolIdHdn=&itemPriceOverrideOrderLineNumberHdn=&itemPriceOverrideReqQtyHdn=&itemPriceOverrideReqUomHdn=&isEligibleForPriceOverridePrmHdn=&conversionFactorPrmHdn=&showOverridePriceTabInpHdn=true&showOverridePriceTabInpHdn=true&dataForm%3AdisplayingPriceHistoryTabInpHdn=false&ajaxTabClicked=&itemPriceOverridePopupEditable_tabPanel_SubmitOnTabClick=false&itemPriceOverridePopupEditable_tabPanel_selectedTab=itemPriceOverridePopupEditablePriceHistoryTab&dataForm%3Aj_id198=false&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedRows=&dataForm%3A"
		"itemPriceOverridePopupPriceHistoryListTable%3AisSortButtonClick=orc.createdDttm&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableClicked=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableResized=false&itemPriceOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=0&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_allselectedrowskey=itemPriceOverridePopupPriceHistoryListTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AdisplayingSnHChargeHistoryTabHdn=true&dataForm%3AdisplaySnHChargeHistoryTabOnlyHdn=true&dataForm%3AdisplayingSnHChargeHistoryTabInpHdn=true&snhChargeOverridePopupEditable_tabPanel_SubmitOnTabClick=false&snhChargeOverridePopupEditable_tabPanel_selectedTab=snhChargeOverridePopupEditablePriceHisto"
		"ryTab&dataForm%3Aj_id227=snhChargeOverridePopupEditableTab&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedRows=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AisSortButtonClick=ORC.CREATED_DTTM&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableClicked=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableResized=false&snhChargeOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=1&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_allselectedrowskey=snhChargeOverridePopupPriceHistoryListTable%24%3A%24{c_SelectedRow_Keys}&dataForm%3AfltrListFltrId%3AfieldName=&dataForm%3AfltrListFltrId%3AfilterName=FL_{pUserName}&dataForm%"
		"3AfltrListFltrId%3Aowner={pUserName}&dataForm%3AfltrListFltrId%3AobjectType=FL_FILTER&dataForm%3AfltrListFltrId%3AfilterObjectType=&dataForm%3AfltrListFltrId%3Afield0value1=&dataForm%3AfltrListFltrId%3Afield0=FILTER.FILTER_NAME&dataForm%3AfltrListFltrId%3Afield0operator=&dataForm%3AfltrListFltrId%3Afield1value1=&dataForm%3AfltrListFltrId%3Afield1=FILTER.IS_DEFAULT&dataForm%3AfltrListFltrId%3Afield1operator=&dataForm%3AfltrListFltrId%3Afield2value1=&dataForm%3AfltrListFltrId%3Afield2=FILTER.IS_PRIVATE&dataForm%3AfltrListFltrId%3Afield2operator=&dataForm%3AfltrListFltrId%3Afield3value1=&dataForm%3AfltrListFltrId%3Afield3=FILTER.OWNER&dataForm%3AfltrListFltrId%3Afield3operator=&dataForm%3AfltrListFltrId%3Afield4value1=&dataForm%3AfltrListFltrId%3Afield4=FILTER.IS_DELETED&dataForm%3AfltrListFltrId%3Afield4operator=&dataForm%3AfltrListFltrId%3AfltrCondition=&dataForm%3AfltrListFltrId%3AfltrCrtSel=&windowId=screen-14271270&domainGroupId=110&dataForm%3Ainp_hdn_for_invoking_cco_bb=&dataForm%3Ainp_hdn_for_invoking_ccnf_b"
		"b=&orderNumber={pOrder_No}&dataForm%3Apost_COEdit_inp_hdn_orderVersion={c_Order_Version}&orderVersion={c_Order_Version}&CustomerId=&ExternalCustomerId=&isExchangeOrder=false&hdnRMANo=&inp_hdn_for_order_status=850&hdn_for_discount_code=850&dataForm%3Ahid_postorder_view_createCO_ordno={pOrder_No}&dataForm%3Apost_COView_inp_hdn_orderConfirmed=TRUE&dataForm%3Apost_COView_inp_hdn_orderCreatedDTTM=10%2F4%2F15%2014%3A40%20EDT&dataForm%3Ahid_txt_postorder_view_createCO_fn=Alison&dataForm%3Ahid_txt_postorder_view_createCO_pno=6103636787&dataForm%3Apost_COView_inp_hdn_orderStatus=SHIPPED&dataForm%3Apost_COEView_inp_hdn_orderStatusId=850&dataForm%3Apost_COView_inp_hdn_paymentStatus=PAID&dataForm%3Apost_COView_inp_hdn_paymentStatusId=30&dataForm%3Ahid_txt_postorder_view_createCO_ln=Cruice&dataForm%3Ainp_hidden_postorder_createCO_customerId_out=&dataForm%3Ainp_hidden_postorder_createCO_extcustomerId_out=&dataForm%3Ainp_hidden_postorder_createCO_customerType_out=&dataForm%3Ahid_txt_postorder_view_createCO_ordType={c_orderType}&dataForm%3Ah"
		"id_txt_postorder_view_createCO_ordTypeId=4&dataForm%3Ahid_txt_postorder_view_createCO_stId=&dataForm%3Ahid_txt_postorder_view_createCO_email=huttonalison%40hotmail.com&dataForm%3Apost_COView_inp_hdn_totalNoOfNotesStr=0&exchangeOrderIdStr=&loadDefaultNavigationFlow=&dataForm%3Adrop_down_page_ids_som=viewAuditDetails&loadNotesFromCategory=10&targetLink=&updateOrderHeaderActive=false&addAppeasementsActive=false&updatePaymentsActive=false&updateShippingInfoActive=false&updateQuantityActive=false&cancelItemActive=false&addItemActive=false&updatePromotionsActive=false&createReturnActive=false&updateAdditionalDetailsActive=false&overridePriceActive=false&overrideSnHChargeActive=false&taskDeterminantLoaded=true&contentPageName=viewAllocationDetails&dataForm%3AFacility_popup_Facility_Contact_dataTable_deleteHidden=&dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedRows=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AsortDir="
		"desc&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AcolCount=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableClicked=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableResized=false&Facility_popup_Facility_Contact_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs_pageallrowskey=&dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedIdList=&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs_allselectedrowskey=Facility_popup_Facility_Contact_dataTable%24%3A%241467200810040&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_deleteHidden=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedRows=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AsortDir=desc&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AcolCount=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AtableClicked=&dataForm%3AFacility_popup_Credit_Limit_List_dataTab"
		"le%3AtableResized=false&Facility_popup_Credit_Limit_List_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_pageallrowskey=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedIdList=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_allselectedrowskey=Facility_popup_Credit_Limit_List_dataTable%24%3A%241467200810040&dataForm%3Aalloclistview_id%3AallocationDetails_id%3Apager%3ApageInput=&dataForm%3Aalloclistview_id%3AallocationDetails_id%3ApagerBoxValue=&dataForm%3Aalloclistview_id%3AallocationDetails_id%3AisPaginationEvent=&dataForm%3Aalloclistview_id%3AallocationDetails_id%3ApagerAction=&dataForm%3Aalloclistview_id%3AallocationDetails_id_deleteHidden=&dataForm%3Aalloclistview_id%3AallocationDetails_id_selectedRows=&dataForm%3Aalloclistview_id%3AallocationDetails_id%3AisSortButtonClick=obj.orderLineNumber&dataForm%3Aalloclistview_id%3AallocationDetails_id%3AsortDir=asc&dataForm%3Aalloclistview_id%3AallocationDetails_id%3AcolCount=&dataForm%3Aalloclistview"
		"_id%3AallocationDetails_id%3AtableClicked=&dataForm%3Aalloclistview_id%3AallocationDetails_id%3AtableResized=false&allocationDetails_id_hdnMaxIndexHldr=1&CustomerOrderId={c_orderIDPkHiddenParam}&orderIDPk={c_orderIDPkHiddenParam}&mainPageName=viewOrderDetails&moreActionTargetLinkviewCustomerOrderDropDownFooterNextBtnPanel=&moreActionButtonPressedviewCustomerOrderDropDownFooterNextBtnPanel=&moreActionTargetLinkviewCustomerOrderNavFlowFooterHoldCancelPanel=&moreActionButtonPressedviewCustomerOrderNavFlowFooterHoldCancelPanel=&backingBeanName=&javax.faces.ViewState={c_ViewState08}&dataForm%3Aj_id333=dataForm%3Aj_id333&",
		LAST);
	
		web_url("ping.jsp_11", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467200833877", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t24.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=../manh/mps/resources/icons/64/user.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/resources/css/gray/images/button/default-medium-arrow.png", "Referer={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", ENDITEM, 
		"Url=../manh/mps/resources/icons/mouse.png", "Referer={pURL}/manh/resources/css/gray/mps-gray.css", ENDITEM, 
		"Url=../manh/resources/css/gray/images/form/checkbox.png", "Referer={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", ENDITEM, 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_09_Goto_More_Dropdown_And_Navigate_To_Audit_Details"),LR_AUTO);

	lr_think_time(1);

	return 0;
}
