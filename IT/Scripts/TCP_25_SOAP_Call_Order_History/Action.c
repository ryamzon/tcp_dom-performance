Action()
{
	web_set_sockets_option("CLOSE_KEEPALIVE_CONNECTIONS", "1");
	
	 web_set_sockets_option("IGNORE_PREMATURE_SHUTDOWN", "1");
	
	web_set_user("TESTSUPERUSER2", "Place2017!","10.18.3.185:30000");
	
	//10.18.3.171:30000
		
	web_url("soapui-updates-os.xml", 
		"URL=http://dl.eviware.com/version-update/soapui-updates-os.xml", 
		"Resource=0", 
		"RecContentType=text/xml", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTML", 
		LAST);

//	web_add_header("Authorization","Basic VEVTVFNVUEVSVVNFUjI6UGxhY2UyMDE3IQ=="); /* c2NvcGVhZG06c2NvcGVhZG0=  VEVTVFNVUEVSVVNFUjI6UGxhY2UyMDE3IQ== TESTSUPERUSER2*/
	
	lr_think_time(1);
			
	lr_start_transaction("TCP_25_SOAP_Call_Order_History");

	web_add_header("Content-Type","application/xml; charset=utf-8");
	
	web_reg_find("Text=getCustomerOrderListResponse",LAST);

	web_custom_request("web_custom_request_1",
 	"URL=http://eom-batch-perf.tcphq.tcpcorp.local.com:30000/services/CustomerOrderWebService",
  	"Method=POST", 
	"TargetFrame=", 
	"Resource=0", 
	"RecContentType=application/xml", 
  	"Referer=", 
	"Mode=HTML",
	"Body="
 	"<x:Envelope xmlns:x=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:imp=\"http://impl.ejb.customerordermanager.sellingservice.services.scope.manh.com/\">"
   	"<x:Header />"
   	"<x:Body>"
     "<imp:getCustomerOrderList>"
       "<arg0>B5AC788452A901ACEBB0578FAE7B9F9835EFFC7A6FD03FAE86D4FA3331162DDE45D3B8D435C970B83B72EB666B23D0F7339A281ACE90EE6BC094C448B708B4E3EAEE2213BE341F7308268BEF08AE8829331032744A7AEBDEE222AC7AE88E1C129F6C3A8AC61690881E7C540E633094EF689161283DBF9D6107338CD8CE254AE20A7BC57C1198151A7D68DB13E047D39F30E2E00F95C37BEBF093ADC9F3A66448</arg0>"
      "<arg1>"
         "<![CDATA[<tXML><Header><Source>DOM</Source><Action_Type>Update</Action_Type><Message_Type>GetOrderList</Message_Type><Company_ID>1</Company_ID></Header><Message><EntityType>Customer Order</EntityType><GetOrderList><OrderType>USECOM,USROPIS,USBOPIS</OrderType><CustomerInfo><CustomerId>{pCustomerID}</CustomerId></CustomerInfo></GetOrderList></Message></tXML>]]>"
 	"</arg1>"
     "</imp:getCustomerOrderList>"
   	"</x:Body>"
 	"</x:Envelope>", 
	LAST);
	
lr_end_transaction("TCP_25_SOAP_Call_Order_History", LR_AUTO);


return 0;
}