/*
 * Example WebSphere MQ LoadRunner script (written in Java)
 * 
 * Script Description: 
 *     This script puts a message on a queue, then gets a response message from 
 *     another queue.
 *
 * You will probably need to add the following jar files to your classpath
 *   - com.ibm.mq.jar
 *   - connector.jar
 *   - com.ibm.mq.jmqi.jar
 *   - com.ibm.mq.headers.jar
 *   - com.ibm.mq.commonservices.jar
 */
 
import lrapi.lr;
import com.ibm.mq.*;
import java.io.*;
 
public class Actions
{

   // Variables used by more than one method
    String queueMgrName = "PERF.TCP.QMGR";
    
    String putQueueName = "STORE.TO.MIF.PIX";
    
    String getQueueName = "STORE.TO.MIF.PIX";
    
    StringBuffer idBuffer = null;
    FileWriter idFile = null;
	BufferedWriter idWriter = null;
 
    MQQueueManager queueMgr = null;
    MQQueue getQueue = null;
    MQQueue putQueue = null;
    MQPutMessageOptions pmo = new MQPutMessageOptions();
    MQGetMessageOptions gmo = new MQGetMessageOptions();
    MQMessage requestMsg = new MQMessage();
    MQMessage responseMsg = new MQMessage();
    String msgBody = null;
 
    public int init() throws Throwable {
        // Open a connection to the queue manager and the put/get queues
        try {
            // As values set in the MQEnvironment class take effect when the 
            // MQQueueManager constructor is called, you must set the values 
            // in the MQEnvironment class before you construct an MQQueueManager object.
            MQEnvironment.hostname="10.18.1.21";
            
            MQEnvironment.port=1414;
            
            MQEnvironment.channel = "TCP.SVRCONN.CHANNEL"; 
            
            queueMgr = new MQQueueManager(queueMgrName);
            
            idFile = new FileWriter("C:\\ROPIS_Trickle_US.txt");
			idWriter = new BufferedWriter(idFile);
			idBuffer = new StringBuffer();
 
            // Access the put/get queues. Note the open options used.
            putQueue = queueMgr.accessQueue(putQueueName, MQC.MQOO_BIND_NOT_FIXED | MQC.MQOO_OUTPUT);
            getQueue= queueMgr.accessQueue(getQueueName, MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_OUTPUT);
            
        } catch(Exception e) {
            lr.error_message("Error connecting to queue manager or accessing queues.");
            lr.exit(lr.EXIT_VUSER, lr.FAIL);
        }
        //lr_output_message("%s",getQueue);
 
        return 0;
    }//end of init
  
    public int action() throws Throwable {
    	
        // This is an XML message that will be put on the queue. Could do some fancy 
        // things with XML classes here if necessary.
        // The message string can contain {parameters} if lr.eval_string() is used.
       MQMessage textMessage = new MQMessage(); // creating a object textMessage
	     textMessage.format = "MQSTR";
	     MQPutMessageOptions pmo = new MQPutMessageOptions();
	     StringBuffer strBuf = new StringBuffer(); // StringBuffer is also string but varying
	    strBuf.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><tXML><Header><Source>SAP</Source><Action_Type>Create</Action_Type><Message_Type>Inventory_Event</Message_Type><Company_ID>1</Company_ID></Header><Message><InventoryEventList><InventoryEvent><TransactionNumber>"+lr.eval_string("{pTransactionNumber}")+"</TransactionNumber><TransactionType>Adjustment</TransactionType><TransactionDateTime>"+lr.eval_string("{pTransactionDate}")+" 00:01</TransactionDateTime><InventoryDefinition><ItemName>"+lr.eval_string("{pItemID}")+"</ItemName><FacilityName>"+lr.eval_string("{pFacilityId}")+"</FacilityName><PONumber/><POLineNumber/><ASNNumber/><InventoryData><Quantity>"+lr.eval_string("{pQty}")+"</Quantity><QuantityUOM>eaches</QuantityUOM><QuantityAdjType>"+lr.eval_string("{pQtyAdjType}")+"</QuantityAdjType><TransferFromLockCode/></InventoryData></InventoryDefinition></InventoryEvent><InventoryEvent><TransactionNumber>"+lr.eval_string("{pTransactionNumber}")+"</TransactionNumber><TransactionType>Adjustment</TransactionType><TransactionDateTime>"+lr.eval_string("{pTransactionDate}")+" 00:01</TransactionDateTime><InventoryDefinition><ItemName>"+lr.eval_string("{pItemID}")+"</ItemName><FacilityName>"+lr.eval_string("{pFacilityId}")+"</FacilityName><PONumber/><POLineNumber/><ASNNumber/><InventoryData><Quantity>"+lr.eval_string("{pQty}")+"</Quantity><QuantityUOM>eaches</QuantityUOM><QuantityAdjType>"+lr.eval_string("{pQtyAdjType}")+"</QuantityAdjType><TransferFromLockCode/></InventoryData></InventoryDefinition></InventoryEvent><InventoryEvent><TransactionNumber>"+lr.eval_string("{pTransactionNumber}")+"</TransactionNumber><TransactionType>Adjustment</TransactionType><TransactionDateTime>"+lr.eval_string("{pTransactionDate}")+" 00:01</TransactionDateTime><InventoryDefinition><ItemName>"+lr.eval_string("{pItemID}")+"</ItemName><FacilityName>"+lr.eval_string("{pFacilityId}")+"</FacilityName><PONumber/><POLineNumber/><ASNNumber/><InventoryData><Quantity>"+lr.eval_string("{pQty}")+"</Quantity><QuantityUOM>eaches</QuantityUOM><QuantityAdjType>"+lr.eval_string("{pQtyAdjType}")+"</QuantityAdjType><TransferFromLockCode/></InventoryData></InventoryDefinition></InventoryEvent><InventoryEvent><TransactionNumber>"+lr.eval_string("{pTransactionNumber}")+"</TransactionNumber><TransactionType>Adjustment</TransactionType><TransactionDateTime>"+lr.eval_string("{pTransactionDate}")+" 00:01</TransactionDateTime><InventoryDefinition><ItemName>"+lr.eval_string("{pItemID}")+"</ItemName><FacilityName>"+lr.eval_string("{pFacilityId}")+"</FacilityName><PONumber/><POLineNumber/><ASNNumber/><InventoryData><Quantity>"+lr.eval_string("{pQty}")+"</Quantity><QuantityUOM>eaches</QuantityUOM><QuantityAdjType>"+lr.eval_string("{pQtyAdjType}")+"</QuantityAdjType><TransferFromLockCode/></InventoryData></InventoryDefinition></InventoryEvent><InventoryEvent><TransactionNumber>"+lr.eval_string("{pTransactionNumber}")+"</TransactionNumber><TransactionType>Adjustment</TransactionType><TransactionDateTime>"+lr.eval_string("{pTransactionDate}")+" 00:01</TransactionDateTime><InventoryDefinition><ItemName>"+lr.eval_string("{pItemID}")+"</ItemName><FacilityName>"+lr.eval_string("{pFacilityId}")+"</FacilityName><PONumber/><POLineNumber/><ASNNumber/><InventoryData><Quantity>"+lr.eval_string("{pQty}")+"</Quantity><QuantityUOM>eaches</QuantityUOM><QuantityAdjType>"+lr.eval_string("{pQtyAdjType}")+"</QuantityAdjType><TransferFromLockCode/></InventoryData></InventoryDefinition></InventoryEvent><InventoryEvent><TransactionNumber>"+lr.eval_string("{pTransactionNumber}")+"</TransactionNumber><TransactionType>Adjustment</TransactionType><TransactionDateTime>"+lr.eval_string("{pTransactionDate}")+" 00:01</TransactionDateTime><InventoryDefinition><ItemName>"+lr.eval_string("{pItemID}")+"</ItemName><FacilityName>"+lr.eval_string("{pFacilityId}")+"</FacilityName><PONumber/><POLineNumber/><ASNNumber/><InventoryData><Quantity>"+lr.eval_string("{pQty}")+"</Quantity><QuantityUOM>eaches</QuantityUOM><QuantityAdjType>"+lr.eval_string("{pQtyAdjType}")+"</QuantityAdjType><TransferFromLockCode/></InventoryData></InventoryDefinition></InventoryEvent><InventoryEvent><TransactionNumber>"+lr.eval_string("{pTransactionNumber}")+"</TransactionNumber><TransactionType>Adjustment</TransactionType><TransactionDateTime>"+lr.eval_string("{pTransactionDate}")+" 00:01</TransactionDateTime><InventoryDefinition><ItemName>"+lr.eval_string("{pItemID}")+"</ItemName><FacilityName>"+lr.eval_string("{pFacilityId}")+"</FacilityName><PONumber/><POLineNumber/><ASNNumber/><InventoryData><Quantity>"+lr.eval_string("{pQty}")+"</Quantity><QuantityUOM>eaches</QuantityUOM><QuantityAdjType>"+lr.eval_string("{pQtyAdjType}")+"</QuantityAdjType><TransferFromLockCode/></InventoryData></InventoryDefinition></InventoryEvent></InventoryEventList></Message></tXML>");
 
         System.out.println(strBuf);
       //	String data = lr.eval_string(""+lr.eval_string(""+lr.eval_string("000117999")+"")+"");
		//String data1= "700";
		//System.out.println(data1+data);
		//idWriter.write(data1+data);
		//idWriter.write(",");
//         lr.output_message("%s"
         //textMessage.writeString(strBuf.toString());
//        msgBody=strBuf;
        // Clear the message objects on each iteration.
        requestMsg.clearMessage();
        responseMsg.clearMessage();
 
        // Create a message object and put it on the request queue
        lr.start_transaction("TCP_1_ROPIS_Post_Order_XML");
        
        try {
            pmo.options = MQC.MQPMO_NEW_MSG_ID; // The queue manager replaces the contents of the MsgId field in MQMD with a new message identifier.
            requestMsg.replyToQueueName = getQueueName; // the response should be put on this queue
            requestMsg.report=MQC.MQRO_PASS_MSG_ID; //If a report or reply is generated as a result of this message, the MsgId of this message is copied to the MsgId of the report or reply message.
            requestMsg.format = MQC.MQFMT_STRING; // Set message format. The application message data can be either an SBCS string (single-byte character set), or a DBCS string (double-byte character set). 
            requestMsg.messageType=MQC.MQMT_REQUEST; // The message is one that requires a reply.
            requestMsg.writeString(strBuf.toString()); // message payload msgBody
            putQueue.put(requestMsg, pmo);
            
            String data2 = lr.eval_string("gdh");
            idWriter.write(data2);
            idWriter.write("\n");
           } 
        
        catch(Exception e) 
        {
        	lr.error_message("Error sending message.");
        	lr.exit(lr.EXIT_VUSER, lr.FAIL);
        }
        
        lr.end_transaction("TCP_1_ROPIS_Post_Order_XML", lr.AUTO);
        
        lr.think_time(1);
        
 
        return 0;
    
    }//end of action
    
    public int end() throws Throwable 
    {
        // 	Close all the connections
        try 
        {
            putQueue.close();
            getQueue.close();
            queueMgr.close();
            idWriter.close();
        } 
        
        catch(Exception e)
              {
            lr.error_message("Exception in closing the connections");
            lr.exit(lr.EXIT_VUSER, lr.FAIL);
              }
 
        return 0;
    }//end of end

}