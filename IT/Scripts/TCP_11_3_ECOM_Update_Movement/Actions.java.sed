/*
 * Example WebSphere MQ LoadRunner script (written in Java)
 * 
 * Script Description: 
 *     This script puts a message on a queue, then gets a response message from 
 *     another queue.
 *
 * You will probably need to add the following jar files to your classpath
 *   - com.ibm.mq.jar
 *   - connector.jar
 *   - com.ibm.mq.jmqi.jar
 *   - com.ibm.mq.headers.jar
 *   - com.ibm.mq.commonservices.jar
 */
 
import lrapi.lr;
import com.ibm.mq.*;
import java.io.*;
 
public class Actions
{
    // Variables used by more than one method
    String queueMgrName = "PERF.WM.QMGR";//QA.WM.QMGR";
    String putQueueName = "WM.TO.MIF.PIX";//"TCPMQSI3.QMTCPMQSI3T.QL.INTDOM.WCS.DOM.ORD";
    String getQueueName = "WM.TO.MIF.PIX";//"TCPMQSI3.QMTCPMQSI3T.QL.INTDOM.WCS.DOM.ORD";
    
    StringBuffer idBuffer = null;
    FileWriter idFile = null;
	BufferedWriter idWriter = null;
 
    MQQueueManager queueMgr = null;
    MQQueue getQueue = null;
    MQQueue putQueue = null;
    MQPutMessageOptions pmo = new MQPutMessageOptions();
    MQGetMessageOptions gmo = new MQGetMessageOptions();
    MQMessage requestMsg = new MQMessage();
    MQMessage responseMsg = new MQMessage();
    String msgBody = null;
 
    public int init() throws Throwable {
        // Open a connection to the queue manager and the put/get queues
        try {
            // As values set in the MQEnvironment class take effect when the 
            // MQQueueManager constructor is called, you must set the values 
            // in the MQEnvironment class before you construct an MQQueueManager object.
            MQEnvironment.hostname="10.18.1.21";
            MQEnvironment.port=1416;
            MQEnvironment.channel = "SYSTEM.ADMIN.SVRCONN";
            queueMgr = new MQQueueManager(queueMgrName);
            
             idFile = new FileWriter("C:\\correction_Receipts.txt");
			idWriter = new BufferedWriter(idFile);
			idBuffer = new StringBuffer();
 
            // Access the put/get queues. Note the open options used.
            putQueue = queueMgr.accessQueue(putQueueName, MQC.MQOO_BIND_NOT_FIXED | MQC.MQOO_OUTPUT);
            getQueue= queueMgr.accessQueue(getQueueName, MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_OUTPUT);
            
        } catch(Exception e) {
            lr.error_message("Error connecting to queue manager or accessing queues.");
            lr.exit(lr.EXIT_VUSER, lr.FAIL);
        }
        //lr_output_message("%s",getQueue);
 
        return 0;
    }//end of init
 
    public int action() throws Throwable {
        // This is an XML message that will be put on the queue. Could do some fancy 
        // things with XML classes here if necessary.
        // The message string can contain {parameters} if lr.eval_string() is used.
       MQMessage textMessage = new MQMessage(); // creating a object textMessage
	     textMessage.format = "MQSTR";
	     MQPutMessageOptions pmo = new MQPutMessageOptions();
	     StringBuffer strBuf = new StringBuffer(); // StringBuffer is also string but varying
	     strBuf.append("<?xml version=\"1.0\" ?>"+lr.eval_string("<tXML>")+""+lr.eval_string("<Header>")+""+lr.eval_string("<Source>")+"WMIS</Source><Source_Type/><Action_Type/><Sequence_Number/><Batch_ID/>"+lr.eval_string("<Reference_ID>")+""+lr.eval_string("{pTxnNumber}")+"11</Reference_ID><User_ID/><Password/>"+lr.eval_string("<Message_Type>")+"Update_MOVEMENT</Message_Type>"+lr.eval_string("<Company_ID>")+"00000001</Company_ID><Msg_Locale/></Header>"+lr.eval_string("<Message>")+"<PIXBridge id=\"PIXBridge_"+lr.eval_string("{pDate_1}")+"\" version=\"2.2\" timestamp=\""+lr.eval_string("{pCreatedDate}")+"\">"+lr.eval_string("<PIX>")+""+lr.eval_string("<TransactionType>")+"606</TransactionType>"+lr.eval_string("<TransactionCode>")+"02</TransactionCode>"+lr.eval_string("<TransactionNumber>")+""+lr.eval_string("{pTxnNumber}")+"1</TransactionNumber>"+lr.eval_string("<SequenceNumber>")+"1</SequenceNumber>"+lr.eval_string("<SKUDefinition>")+""+lr.eval_string("<Company>")+"01</Company>"+lr.eval_string("<Division>")+"01</Division>"+lr.eval_string("<Style>")+""+lr.eval_string("{pStyle}")+"</Style>"+lr.eval_string("<StyleSuffix>")+""+lr.eval_string("{pSuffix}")+"</StyleSuffix></SKUDefinition>"+lr.eval_string("<SubSKUFields>")+""+lr.eval_string("<InventoryType>")+"F</InventoryType></SubSKUFields>"+lr.eval_string("<LocnDefinition>")+""+lr.eval_string("<LocationFileID>")+"R</LocationFileID></LocnDefinition>"+lr.eval_string("<PIXFields>")+""+lr.eval_string("<DateCreated>")+""+lr.eval_string("{pCreatedDate}")+"</DateCreated>"+lr.eval_string("<CaseNumber>")+""+lr.eval_string("{pCaseNumber}")+"</CaseNumber>"+lr.eval_string("<PackageBarcode>")+""+lr.eval_string("{pPackageBarcode}")+"</PackageBarcode>"+lr.eval_string("<InvAdjustmentQty>")+""+lr.eval_string("{pInvAdjustmentQty}")+"</InvAdjustmentQty>"+lr.eval_string("<UnitOfMeasure>")+"1</UnitOfMeasure>"+lr.eval_string("<InvAdjustmentType>")+"S</InvAdjustmentType>"+lr.eval_string("<ReceivedFrom>")+"05</ReceivedFrom>"+lr.eval_string("<Warehouse>")+"03</Warehouse>"+lr.eval_string("<ReferenceCode2>")+"07</ReferenceCode2>"+lr.eval_string("<ReferenceCode4>")+"25</ReferenceCode4>"+lr.eval_string("<Reference4>")+"PP</Reference4>"+lr.eval_string("<ActionCode>")+"06</ActionCode>"+lr.eval_string("<ProgramID>")+"SLG1G1RP</ProgramID>"+lr.eval_string("<JobName>")+"QPADEV0010</JobName>"+lr.eval_string("<JobNumber>")+""+lr.eval_string("{pJobNumber}")+"</JobNumber>"+lr.eval_string("<UserID>")+"TEMP63</UserID>"+lr.eval_string("<As400UserID>")+"TEMP63</As400UserID>"+lr.eval_string("<EntryNumber>")+"M000000852</EntryNumber></PIXFields></PIX></PIXBridge></Message></tXML>");
	          
        System.out.println(strBuf);
        requestMsg.clearMessage();
        responseMsg.clearMessage();
 
        // Create a message object and put it on the request queue
        lr.start_transaction("TCP_8_ECOM_Post_XML_Order_MQ_01_Post");
        
        try {
            pmo.options = MQC.MQPMO_NEW_MSG_ID; // The queue manager replaces the contents of the MsgId field in MQMD with a new message identifier.
            requestMsg.replyToQueueName = getQueueName; // the response should be put on this queue
            requestMsg.report=MQC.MQRO_PASS_MSG_ID; //If a report or reply is generated as a result of this message, the MsgId of this message is copied to the MsgId of the report or reply message.
            requestMsg.format = MQC.MQFMT_STRING; // Set message format. The application message data can be either an SBCS string (single-byte character set), or a DBCS string (double-byte character set). 
            requestMsg.messageType=MQC.MQMT_REQUEST; // The message is one that requires a reply.
            requestMsg.writeString(strBuf.toString()); // message payload msgBody
            putQueue.put(requestMsg, pmo);
            } 
        
        catch(Exception e) 
        {
       	lr.error_message("Error sending message.");
		lr.exit(lr.EXIT_VUSER, lr.FAIL);
        }
        
        lr.end_transaction("TCP_8_ECOM_Post_XML_Order_MQ_01_Post", lr.AUTO);
 
        return 0;
    
    }//end of action
    
    public int end() throws Throwable 
    {
        // 	Close all the connections
        try 
        {
            putQueue.close();
            getQueue.close();
            queueMgr.close();
            idWriter.close();
        }
        
        catch(Exception e)
              {
            lr.error_message("Exception in closing the connections");
            lr.exit(lr.EXIT_VUSER, lr.FAIL);
              }
 
        return 0;
    }//end of end
}