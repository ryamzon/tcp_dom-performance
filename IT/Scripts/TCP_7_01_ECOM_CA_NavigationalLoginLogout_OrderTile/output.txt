Virtual User Script started at : 9/29/2017 9:07:19 AM
Starting action vuser_init.
Web Turbo Replay of LoadRunner 12.0.0 for Windows 2008 R2; build 2079 (Jun 17 2014 10:56:12)  	[MsgId: MMSG-27143]
Run mode: HTML  	[MsgId: MMSG-26993]
Run-Time Settings file: "C:\IT\Scripts\TCP_7_01_ECOM_CA_NavigationalLoginLogout_OrderTile\\default.cfg"  	[MsgId: MMSG-27141]
vuser_init.c(12): web_cache_cleanup started  	[MsgId: MMSG-26355]
vuser_init.c(12): web_cache_cleanup was successful  	[MsgId: MMSG-26392]
vuser_init.c(14): web_cleanup_cookies started  	[MsgId: MMSG-26355]
vuser_init.c(14): web_cleanup_cookies was successful  	[MsgId: MMSG-26392]
vuser_init.c(16): web_set_max_html_param_len started  	[MsgId: MMSG-26355]
vuser_init.c(16): web_set_max_html_param_len was successful  	[MsgId: MMSG-26392]
vuser_init.c(18): web_set_sockets_option started  	[MsgId: MMSG-26355]
vuser_init.c(18): web_set_sockets_option was successful  	[MsgId: MMSG-26392]
vuser_init.c(20): web_set_sockets_option started  	[MsgId: MMSG-26355]
vuser_init.c(20): web_set_sockets_option was successful  	[MsgId: MMSG-26392]
vuser_init.c(22): web_set_sockets_option started  	[MsgId: MMSG-26355]
vuser_init.c(22): web_set_sockets_option was successful  	[MsgId: MMSG-26392]
vuser_init.c(24): web_reg_save_param started  	[MsgId: MMSG-26355]
vuser_init.c(24): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
vuser_init.c(30): Notify: Saving Parameter "sTestCaseName = TCP_07_1_ECOM_CA_NavigationalLoginLogout_OrderTile".
vuser_init.c(32): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_1_ECOM_CA_NavigationalLoginLogout_OrderTile"
vuser_init.c(32): Notify: Transaction "TCP_07_1_ECOM_CA_NavigationalLoginLogout_OrderTile_01_Launch" started.
vuser_init.c(36): web_url("index.html") started  	[MsgId: MMSG-26355]
vuser_init.c(36): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(36): Notify: Saving Parameter "c_SAML_Request = PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c2FtbDJwOkF1dGhuUmVxdWVzdCB4bWxuczpzYW1sMnA9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpwcm90b2NvbCIgQXNzZXJ0aW9uQ29uc3VtZXJTZXJ2aWNlVVJMPSJodHRwOi8vZW9tLWNhLXBlcmYudGNwaHEudGNwY29ycC5sb2NhbC5jb206MzAwMDAvc2FtbC9TU08vYWxpYXMvY2EiIERlc3RpbmF0aW9uPSJodHRwczovL0VPTS1NSVAtUGVyZi5jaGlsZHJlbnNwbGFjZS5jb206MTUwMDEvcHJvZmlsZS9TQU1MMi9QT1NUL1NTTyIgRm9yY2VBdXRobj0iZmFsc2UiIElEPSJhNTgyNmNlNmRoN2plMjg0ajM1MmNoYWpnYzczM2kiIElzUGFzc2l2ZT0iZmFsc2UiIElzc3VlSW5zdGFudD0iMjAxNy0wOS0yOVQxMzowNzoxOS40NTRaIiBQcm90b2NvbEJpbmRpbmc9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpiaW5kaW5nczpIVFRQLUFydGlmYWN0IiBWZXJzaW9uPSIyLjAiPjxzYW1sMjpJc3N1ZXIgeG1sbnM6c2FtbDI9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iPmNhPC9zYW1sMjpJc3N1ZXI+PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+PGRzOlNpZ25lZEluZm8+PGRzOkNhbm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48ZHM6U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3JzYS1zaGExIi8+PGRzOlJlZmVyZW5jZSBVUkk9IiNhNTgyNmNlNmRoN2plMjg0ajM1MmNoYWpnYzczM2kiPjxkczpUcmFuc2Zvcm1zPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48L2RzOlRyYW5zZm9ybXM+PGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNzaGExIi8+PGRzOkRpZ2VzdFZhbHVlPk5peEZqM0xZZWJTSVJRMGtONUNBWlhmMmVIYz08L2RzOkRpZ2VzdFZhbHVlPjwvZHM6UmVmZXJlbmNlPjwvZHM6U2lnbmVkSW5mbz48ZHM6U2lnbmF0dXJlVmFsdWU+S0xnQXVtelZsbEx3OGVXQUNJWS95NzhLN1hhcDhoQ1dPR1JZcEQ2MmxuYWZsRHNIdUZWWXV2bFNDRDBGa1BNcEk4Yi9MbUJGRFd1M2Y4STVZcXJQbUUrRmJqUWtDc2NkbURPQ3dKdkY2cEU0M1ZURkdPTFBLU3IvZTZPbjIzeklLVzFxNHVQREN0MUdDSnFoaTlDVFFzdUdndlExSlpmNitidGdvZFhSa2R5Q0ZYK3dIN2h3NmVuM0xoQ1FVQnhpYkhzMmNGMHdQV1ZjdU5XK3R3cTNNUloxLzViem5UTGF0S1BTT1llTHhETkRjU1RBNWdiL21tMmdUMUZBb0VuK09sYlVBZldqZ3doeDFtUDJwUUtjVUJqZmdJNXhiQ3JLS3BhVFVtcHJJYnZXQnA1SklGZm9IVjk3RDh0Zi9sL1dVWmVadTI5OFo3ZktzWDRHeEJ0MVdBPT08L2RzOlNpZ25hdHVyZVZhbHVlPjxkczpLZXlJbmZvPjxkczpYNTA5RGF0YT48ZHM6WDUwOUNlcnRpZmljYXRlPk1JSURCVENDQWUwQ0JGS0RuRkV3RFFZSktvWklodmNOQVFFTkJRQXdSekVMTUFrR0ExVUVCaE1DVlZNeElqQWdCZ05WQkFvTUdVMWgKYm1oaGRIUmhiaUJCYzNOdlkybGhkR1Z6TENCSmJtTXhGREFTQmdOVkJBTU1DM053TG0xaGJtZ3VZMjl0TUI0WERURXpNVEV4TXpFMQpNelUwTlZvWERUTXpNVEV3T0RFMU16VTBOVm93UnpFTE1Ba0dBMVVFQmhNQ1ZWTXhJakFnQmdOVkJBb01HVTFoYm1oaGRIUmhiaUJCCmMzTnZZMmxoZEdWekxDQkpibU14RkRBU0JnTlZCQU1NQzNOd0xtMWhibWd1WTI5dE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0MKQVE4QU1JSUJDZ0tDQVFFQWpSeXA0QXRacHV1TnZORFExakhTc1o2REdHUGlPQ0xaWlBvV3d4NHowNXFjRzQ4MTFMVVhabEZvL05aawpGeHY1bG1kamx2SWI0bzgzbGx1ajFkSWNFSnFCUEpxcEtZOS95SStwdUVtMUFFSjlqT0NIZitjeWREQjM2dmU3M1VtdndaWjBETFNvCmxaVjl5NW1yYUhaaDBLV1paZ2lianRZcCtybEFTa0hiTUI2ZWNZVzZPWlJPWXNoR05ES2tpSSt3ZHFsTXY4SjdNTldRaXlMTlFLc0EKQlFJbXd5dy9lQ05rdHFRWDMvclpNdXpqSVRVMG0xbWZtTUNTR3JybEJEZzJqR2hJNndxYlVPQXQwUk96cysvK3ZpOTIyMzdadnNVQwo5cDRXWko0S0pJK0lwaTROL3hGckZReU1RKzdXNTZQaFVpTmk0eUUvc3FNb3pMZDJrSStKTlFJREFRQUJNQTBHQ1NxR1NJYjNEUUVCCkRRVUFBNElCQVFDRjZpeHFwVXo5ZUY4aU5LQTIrczZLdlFrNUh2enF0OTNYaFlCcURPWTlwNEMzL3NIVHNsdCtxM3oxcG9MRmdqQm8KYW9oNWl5aklkTytudi83SUR1Z2hVdHJLWVFpaUZPbGlQNlFaL2RSNnNqT0VvL29IVVRFNXNNejBaOGVwcVJBYlVwTlUyQzFNRXE5agpUdmpGUXJQcm01T01JazlmaHdPOE1DdmoydmdlZmQ2dHdza3F5Ylp5TDlMTFdBTXdIb0FTY3VTOFBtUVU0eE1aT1RNM3FkVDlFQkVJCjBIY0N2dmxSc0dDTTFQZFFTWTBtZ2ZSUThNRjVCMGhEOVlaanF5ZWRIREFpaEExeW9IRW5sd2JFbnR5Y0lQc3dZcUQ3Zm4wNXM4cU8KdFU0ZFRnaUxqUmpSMmVHcXhDZlZ6cWd2V3VpdnhmczRVWjIyem5MSTRaZ2RUY3FDPC9kczpYNTA5Q2VydGlmaWNhdGU+PC9kczpYNTA5RGF0YT48L2RzOktleUluZm8+PC9kczpTaWduYXR1cmU+PHNhbWwycDpOYW1lSURQb2xpY3kgRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6bmFtZWlkLWZvcm1hdDp1bnNwZWNpZmllZCIvPjwvc2FtbDJwOkF1dGhuUmVxdWVzdD4=".
vuser_init.c(36): web_url("index.html") was successful, 4660 body bytes, 357 header bytes  	[MsgId: MMSG-26386]
vuser_init.c(46): web_set_sockets_option started  	[MsgId: MMSG-26355]
vuser_init.c(46): web_set_sockets_option was successful  	[MsgId: MMSG-26392]
vuser_init.c(48): web_submit_data("SSO") started  	[MsgId: MMSG-26355]
vuser_init.c(48): Notify: Next row for parameter pURL_SSO = 1 [table  = pURL_SSO].
vuser_init.c(48): Notify: Parameter Substitution: parameter "pURL_SSO" =  "https://eom-mip-perf.childrensplace.com:15001"
vuser_init.c(48): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(48): Notify: Parameter Substitution: parameter "c_SAML_Request" =  "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c2FtbDJwOkF1dGhuUmVxdWVzdCB4bWxuczpzYW1sMnA9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpwcm90b2NvbCIgQXNzZXJ0aW9uQ29uc3VtZXJTZXJ2aWNlVVJMPSJodHRwOi8vZW9tLWNhLXBlcmYudGNwaHEudGNwY29ycC5sb2NhbC5jb206MzAwMDAvc2FtbC9TU08vYWxpYXMvY2EiIERlc3RpbmF0aW9uPSJodHRwczovL0VPTS1NSVAtUGVyZi5jaGlsZHJlbnNwbGFjZS5jb206MTUwMDEvcHJvZmlsZS9TQU1MMi9QT1NUL1NTTyIgRm9yY2VBdXRobj0iZmFsc2UiIElEPSJhNTgyNmNlNmRoN2plMjg0ajM1MmNoYWpnYzczM2kiIElzUGFzc2l2ZT0iZmFsc2UiIElzc3VlSW5zdGFudD0iMjAxNy0wOS0yOVQxMzowNzoxOS40NTRaIiBQcm90b2NvbEJpbmRpbmc9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpiaW5kaW5nczpIVFRQLUFydGlmYWN0IiBWZXJzaW9uPSIyLjAiPjxzYW1sMjpJc3N1ZXIgeG1sbnM6c2FtbDI9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iPmNhPC9zYW1sMjpJc3N1ZXI+PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+PGRzOlNpZ25lZEluZm8+PGRzOkNhbm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48ZHM6U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3JzYS1zaGExIi8+PGRzOlJlZmVyZW5jZSBVUkk9IiNhNTgyNmNlNmRoN2plMjg0ajM1MmNoYWpnYzczM2kiPjxkczpUcmFuc2Zvcm1zPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48L2RzOlRyYW5zZm9ybXM+PGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNzaGExIi8+PGRzOkRpZ2VzdFZhbHVlPk5peEZqM0xZZWJTSVJRMGtONUNBWlhmMmVIYz08L2RzOkRpZ2VzdFZhbHVlPjwvZHM6UmVmZXJlbmNlPjwvZHM6U2lnbmVkSW5mbz48ZHM6U2lnbmF0dXJlVmFsdWU+S0xnQXVtelZsbEx3OGVXQUNJWS95NzhLN1hhcDhoQ1dPR1JZcEQ2MmxuYWZsRHNIdUZWWXV2bFNDRDBGa1BNcEk4Yi9MbUJGRFd1M2Y4STVZcXJQbUUrRmJqUWtDc2NkbURPQ3dKdkY2cEU0M1ZURkdPTFBLU3IvZTZPbjIzeklLVzFxNHVQREN0MUdDSnFoaTlDVFFzdUdndlExSlpmNitidGdvZFhSa2R5Q0ZYK3dIN2h3NmVuM0xoQ1FVQnhpYkhzMmNGMHdQV1ZjdU5XK3R3cTNNUloxLzViem5UTGF0S1BTT1llTHhETkRjU1RBNWdiL21tMmdUMUZBb0VuK09sYlVBZldqZ3doeDFtUDJwUUtjVUJqZmdJNXhiQ3JLS3BhVFVtcHJJYnZXQnA1SklGZm9IVjk3RDh0Zi9sL1dVWmVadTI5OFo3ZktzWDRHeEJ0MVdBPT08L2RzOlNpZ25hdHVyZVZhbHVlPjxkczpLZXlJbmZvPjxkczpYNTA5RGF0YT48ZHM6WDUwOUNlcnRpZmljYXRlPk1JSURCVENDQWUwQ0JGS0RuRkV3RFFZSktvWklodmNOQVFFTkJRQXdSekVMTUFrR0ExVUVCaE1DVlZNeElqQWdCZ05WQkFvTUdVMWgKYm1oaGRIUmhiaUJCYzNOdlkybGhkR1Z6TENCSmJtTXhGREFTQmdOVkJBTU1DM053TG0xaGJtZ3VZMjl0TUI0WERURXpNVEV4TXpFMQpNelUwTlZvWERUTXpNVEV3T0RFMU16VTBOVm93UnpFTE1Ba0dBMVVFQmhNQ1ZWTXhJakFnQmdOVkJBb01HVTFoYm1oaGRIUmhiaUJCCmMzTnZZMmxoZEdWekxDQkpibU14RkRBU0JnTlZCQU1NQzNOd0xtMWhibWd1WTI5dE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0MKQVE4QU1JSUJDZ0tDQVFFQWpSeXA0QXRacHV1TnZORFExakhTc1o2REdHUGlPQ0xaWlBvV3d4NHowNXFjRzQ4MTFMVVhabEZvL05aawpGeHY1bG1kamx2SWI0bzgzbGx1ajFkSWNFSnFCUEpxcEtZOS95SStwdUVtMUFFSjlqT0NIZitjeWREQjM2dmU3M1VtdndaWjBETFNvCmxaVjl5NW1yYUhaaDBLV1paZ2lianRZcCtybEFTa0hiTUI2ZWNZVzZPWlJPWXNoR05ES2tpSSt3ZHFsTXY4SjdNTldRaXlMTlFLc0EKQlFJbXd5dy9lQ05rdHFRWDMvclpNdXpqSVRVMG0xbWZtTUNTR3JybEJEZzJqR2hJNndxYlVPQXQwUk96cysvK3ZpOTIyMzdadnNVQwo5cDRXWko0S0pJK0lwaTROL3hGckZReU1RKzdXNTZQaFVpTmk0eUUvc3FNb3pMZDJrSStKTlFJREFRQUJNQTBHQ1NxR1NJYjNEUUVCCkRRVUFBNElCQVFDRjZpeHFwVXo5ZUY4aU5LQTIrczZLdlFrNUh2enF0OTNYaFlCcURPWTlwNEMzL3NIVHNsdCtxM3oxcG9MRmdqQm8KYW9oNWl5aklkTytudi83SUR1Z2hVdHJLWVFpaUZPbGlQNlFaL2RSNnNqT0VvL29IVVRFNXNNejBaOGVwcVJBYlVwTlUyQzFNRXE5agpUdmpGUXJQcm01T01JazlmaHdPOE1DdmoydmdlZmQ2dHdza3F5Ylp5TDlMTFdBTXdIb0FTY3VTOFBtUVU0eE1aT1RNM3FkVDlFQkVJCjBIY0N2dmxSc0dDTTFQZFFTWTBtZ2ZSUThNRjVCMGhEOVlaanF5ZWRIREFpaEExeW9IRW5sd2JFbnR5Y0lQc3dZcUQ3Zm4wNXM4cU8KdFU0ZFRnaUxqUmpSMmVHcXhDZlZ6cWd2V3VpdnhmczRVWjIyem5MSTRaZ2RUY3FDPC9kczpYNTA5Q2VydGlmaWNhdGU+PC9kczpYNTA5RGF0YT48L2RzOktleUluZm8+PC9kczpTaWduYXR1cmU+PHNhbWwycDpOYW1lSURQb2xpY3kgRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6bmFtZWlkLWZvcm1hdDp1bnNwZWNpZmllZCIvPjwvc2FtbDJwOkF1dGhuUmVxdWVzdD4="
vuser_init.c(48): Redirecting "https://eom-mip-perf.childrensplace.com:15001/profile/SAML2/POST/SSO" (redirection depth is 0)  	[MsgId: MMSG-26694]
vuser_init.c(48): To location "https://eom-mip-perf.childrensplace.com:15001/AuthnEngine"  	[MsgId: MMSG-26693]
vuser_init.c(48): Redirecting "https://eom-mip-perf.childrensplace.com:15001/AuthnEngine" (redirection depth is 1)  	[MsgId: MMSG-26694]
vuser_init.c(48): To location "https://eom-mip-perf.childrensplace.com:15001/Authn/RemoteUser?loginContextKey=1a9530a5-fcec-43c7-9742-0035e4aceb34"  	[MsgId: MMSG-26693]
vuser_init.c(48): Redirecting "https://eom-mip-perf.childrensplace.com:15001/Authn/RemoteUser?loginContextKey=1a9530a5-fcec-43c7-9742-0035e4aceb34" (redirection depth is 2)  	[MsgId: MMSG-26694]
vuser_init.c(48): To location "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26693]
vuser_init.c(48): Found resource "https://eom-mip-perf.childrensplace.com:15001/ext/resources/css/ext-all.css" in HTML "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26659]
vuser_init.c(48): Found resource "https://eom-mip-perf.childrensplace.com:15001/manh/resources/css/mip.css" in HTML "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26659]
vuser_init.c(48): Found resource "https://eom-mip-perf.childrensplace.com:15001/ext/ext-all.js" in HTML "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26659]
vuser_init.c(48): Found resource "https://eom-mip-perf.childrensplace.com:15001/app.js" in HTML "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26659]
vuser_init.c(48): web_submit_data("SSO") was successful, 462766 body bytes, 2796 header bytes, 6426 chunking overhead bytes  	[MsgId: MMSG-26385]
vuser_init.c(60): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_1_ECOM_CA_NavigationalLoginLogout_OrderTile"
vuser_init.c(60): Notify: Transaction "TCP_07_1_ECOM_CA_NavigationalLoginLogout_OrderTile_01_Launch" ended with "Pass" status (Duration: 1.5903 Wasted Time: 1.2553).
vuser_init.c(66): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_1_ECOM_CA_NavigationalLoginLogout_OrderTile"
vuser_init.c(66): Notify: Transaction "TCP_07_1_ECOM_CA_NavigationalLoginLogout_OrderTile_02_Login" started.
vuser_init.c(68): web_custom_request("j_spring_security_check") started  	[MsgId: MMSG-26355]
vuser_init.c(68): Notify: Next row for parameter pURL_SSO = 1 [table  = pURL_SSO].
vuser_init.c(68): Notify: Parameter Substitution: parameter "pURL_SSO" =  "https://eom-mip-perf.childrensplace.com:15001"
vuser_init.c(68): Notify: Parameter Substitution: parameter "pUserName" =  "User204"
vuser_init.c(68): Notify: Next row for parameter pPassword = 1 [table  = pPassword].
vuser_init.c(68): Notify: Parameter Substitution: parameter "pPassword" =  "password"
vuser_init.c(68): Redirecting "https://eom-mip-perf.childrensplace.com:15001/j_spring_security_check" (redirection depth is 0)  	[MsgId: MMSG-26694]
vuser_init.c(68): To location "https://eom-mip-perf.childrensplace.com:15001/Authn/RemoteUser?loginContextKey=1a9530a5-fcec-43c7-9742-0035e4aceb34"  	[MsgId: MMSG-26693]
vuser_init.c(68): Redirecting "https://eom-mip-perf.childrensplace.com:15001/Authn/RemoteUser?loginContextKey=1a9530a5-fcec-43c7-9742-0035e4aceb34" (redirection depth is 1)  	[MsgId: MMSG-26694]
vuser_init.c(68): To location "https://eom-mip-perf.childrensplace.com:15001/profile/SAML2/POST/SSO"  	[MsgId: MMSG-26693]
vuser_init.c(68): Redirecting "https://eom-mip-perf.childrensplace.com:15001/profile/SAML2/POST/SSO" (redirection depth is 2)  	[MsgId: MMSG-26694]
vuser_init.c(68): To location "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/saml/SSO/alias/ca?SAMLart=AAQAAf5QSaNfZd4JBmYl5vT6TLHqylbwLLRlcsrVr2UAvvNaF0fOTBmaw3I%3D"  	[MsgId: MMSG-26693]
vuser_init.c(68): Redirecting "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/saml/SSO/alias/ca?SAMLart=AAQAAf5QSaNfZd4JBmYl5vT6TLHqylbwLLRlcsrVr2UAvvNaF0fOTBmaw3I%3D" (redirection depth is 3)  	[MsgId: MMSG-26694]
vuser_init.c(68): To location "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26693]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/extstyles" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/ext/ext-all.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/ext/ext-theme-neptune.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/hammer.min.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/overrides.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/webtop.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/bootstrap.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/webtop-overrides.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/mps.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/html2canvas.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/notifications/portal.min.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/notifications/atmosphere-min.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/lps/resources/common/scripts/3rdparty/jquery/jquery.min.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/highcharts/js/highcharts.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/highcharts/js/highcharts-more.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): web_custom_request("j_spring_security_check") was successful, 2034925 body bytes, 6520 header bytes  	[MsgId: MMSG-26386]
vuser_init.c(80): web_url("ping.jsp") started  	[MsgId: MMSG-26355]
vuser_init.c(80): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(80): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(80): web_url("ping.jsp") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
vuser_init.c(92): web_url("ping.jsp_2") started  	[MsgId: MMSG-26355]
vuser_init.c(92): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(92): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(92): web_url("ping.jsp_2") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
vuser_init.c(102): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_1_ECOM_CA_NavigationalLoginLogout_OrderTile"
vuser_init.c(102): Notify: Transaction "TCP_07_1_ECOM_CA_NavigationalLoginLogout_OrderTile_02_Login" ended with "Pass" status (Duration: 0.5998 Wasted Time: 0.1900).
Ending action vuser_init.
Running Vuser...
Starting iteration 1.
Starting action Action.
Action.c(5): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_1_ECOM_CA_NavigationalLoginLogout_OrderTile"
Action.c(5): Notify: Transaction "TCP_07_1_ECOM_CA_NavigationalLoginLogout_OrderTile_03_Order_Lookup_from_Order_tile" started.
Action.c(19): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(19): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(25): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(25): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(31): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(31): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(37): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(37): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(43): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(43): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(49): Notify: Saving Parameter "p1 = {"customerOrderSearchCriteria":{"entityType":"All","orderNumber":"".
Action.c(51): Notify: Parameter Substitution: parameter "p1" =  "{"customerOrderSearchCriteria":{"entityType":"All","orderNumber":""
Action.c(51): 110761984
Action.c(53): Notify: Saving Parameter "p2 = ","parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":10,"customerInfo":"","customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}".
Action.c(55): Notify: Parameter Substitution: parameter "p2" =  "","parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":10,"customerInfo":"","customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}"
Action.c(55): 112320800
Action.c(57): web_set_user started  	[MsgId: MMSG-26355]
Action.c(57): Notify: Parameter Substitution: parameter "pUserName" =  "User204"
Action.c(57): Notify: Next row for parameter pPassword = 1 [table  = pPassword].
Action.c(57): Notify: Parameter Substitution: parameter "pPassword" =  "password"
Action.c(57): web_set_user was successful  	[MsgId: MMSG-26392]
Action.c(59): web_custom_request("customerOrderAndTransactionList") started  	[MsgId: MMSG-26355]
Action.c(59): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(59): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(59): Notify: Parameter Substitution: parameter "p1" =  "{"customerOrderSearchCriteria":{"entityType":"All","orderNumber":""
Action.c(59): Notify: Parameter Substitution: parameter "pOrderNumber" =  "80360000160"
Action.c(59): Notify: Parameter Substitution: parameter "p2" =  "","parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":10,"customerInfo":"","customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}"
Action.c(59): Notify: Saving Parameter "c_externalCustomerId = 202493282".
Action.c(59): Notify: Saving Parameter "c_First_Name = Green".
Action.c(59): Notify: Saving Parameter "c_Last_Name = Lantern".
Action.c(59): Notify: Saving Parameter "c_Phone_Number = 9884098840".
Action.c(59): Notify: Saving Parameter "c_Email_Id = SIRIUSLUME@GMAIL.COM".
Action.c(59): web_custom_request("customerOrderAndTransactionList") was successful, 554 body bytes, 186 header bytes, 12 chunking overhead bytes  	[MsgId: MMSG-26385]
Action.c(73): web_custom_request("windows") started  	[MsgId: MMSG-26355]
Action.c(73): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(73): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(73): web_custom_request("windows") was successful, 24 body bytes, 186 header bytes, 11 chunking overhead bytes  	[MsgId: MMSG-26385]
Action.c(85): web_url("ping.jsp_3") started  	[MsgId: MMSG-26355]
Action.c(85): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(85): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(85): web_url("ping.jsp_3") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Action.c(95): Notify: Saving Parameter "p3 = {"companyParameterIdList":{"parameterList":["CURRENCY_USED","EXT_CUSTOMER_ID_GENERATION","USE_DELIVERY_ZONE","DEFAULT_SHIP_VIA","DEFAULT_ORDER_TYPE","REQUIRES_VALIDATION_FOR_PAYMENT_AUTHORIZATION","REQUIRES_VALIDATION_FOR_PAYMENT_SETTLEMENT","CHARGE_HANDLING_STRATEGY","AUTOMATIC_PROMOTION_VALUE","ITEM_PRICE_EXPIRY_DATE_EXTENSION_DAYS","ITEM_PRICE_SCHEDULER_INTERVAL_IN_DAYS","DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE","VIEW_STORE_LEVEL_PRICES","NETWORK_LEVEL_VIEW","FACILITY_LEVEL_VIEW"]}}".
Action.c(98): Notify: Parameter Substitution: parameter "p3" =  "{"companyParameterIdList":{"parameterList":["CURRENCY_USED","EXT_CUSTOMER_ID_GENERATION","USE_DELIVERY_ZONE","DEFAULT_SHIP_VIA","DEFAULT_ORDER_TYPE","REQUIRES_VALIDATION_FOR_PAYMENT_AUTHORIZATION","REQUIRES_VALIDATION_FOR_PAYMENT_SETTLEMENT","CHARGE_HANDLING_STRATEGY","AUTOMATIC_PROMOTION_VALUE","ITEM_PRICE_EXPIRY_DATE_EXTENSION_DAYS","ITEM_PRICE_SCHEDULER_INTERVAL_IN_DAYS","DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE","VIEW_STORE_LEVEL_PRICES","NETWORK_LEVEL_VIEW","FACILITY_LEVEL_VIEW"]}}"
Action.c(98): 111581488
Action.c(99): web_set_user started  	[MsgId: MMSG-26355]
Action.c(99): Notify: Parameter Substitution: parameter "pUserName" =  "User204"
Action.c(99): Notify: Next row for parameter pPassword = 1 [table  = pPassword].
Action.c(99): Notify: Parameter Substitution: parameter "pPassword" =  "password"
Action.c(99): web_set_user was successful  	[MsgId: MMSG-26392]
Action.c(101): web_custom_request("companyParameterList") started  	[MsgId: MMSG-26355]
Action.c(101): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(101): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(101): Notify: Parameter Substitution: parameter "p3" =  "{"companyParameterIdList":{"parameterList":["CURRENCY_USED","EXT_CUSTOMER_ID_GENERATION","USE_DELIVERY_ZONE","DEFAULT_SHIP_VIA","DEFAULT_ORDER_TYPE","REQUIRES_VALIDATION_FOR_PAYMENT_AUTHORIZATION","REQUIRES_VALIDATION_FOR_PAYMENT_SETTLEMENT","CHARGE_HANDLING_STRATEGY","AUTOMATIC_PROMOTION_VALUE","ITEM_PRICE_EXPIRY_DATE_EXTENSION_DAYS","ITEM_PRICE_SCHEDULER_INTERVAL_IN_DAYS","DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE","VIEW_STORE_LEVEL_PRICES","NETWORK_LEVEL_VIEW","FACILITY_LEVEL_VIEW"]}}"
Action.c(101): web_custom_request("companyParameterList") was successful, 377 body bytes, 233 header bytes, 12 chunking overhead bytes  	[MsgId: MMSG-26385]
Action.c(115): Notify: Saving Parameter "p4 = {"customerOrderSearchCriteria":{"entityType":"All","orderNumber":".
Action.c(117): Notify: Parameter Substitution: parameter "p4" =  "{"customerOrderSearchCriteria":{"entityType":"All","orderNumber":"
Action.c(117): 110761776
Action.c(119): Notify: Saving Parameter "p5 = ,"parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":1000,"customerInfo":{"customerFirstName":"".
Action.c(121): Notify: Parameter Substitution: parameter "p5" =  ","parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":1000,"customerInfo":{"customerFirstName":""
Action.c(121): 6215368
Action.c(123): Notify: Saving Parameter "p6 = ","customerLastName":"".
Action.c(125): Notify: Parameter Substitution: parameter "p6" =  "","customerLastName":""
Action.c(125): 111444608
Action.c(127): Notify: Saving Parameter "p7 = ","customField1":"","customField2":"","customField3":"","customField4":"","customField5":"","customField6":"","customField7":"","customField8":"","customField9":"","customField10":""},"customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}".
Action.c(130): Notify: Parameter Substitution: parameter "p7" =  "","customField1":"","customField2":"","customField3":"","customField4":"","customField5":"","customField6":"","customField7":"","customField8":"","customField9":"","customField10":""},"customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}"
Action.c(130): 111463912
Action.c(134): web_set_user started  	[MsgId: MMSG-26355]
Action.c(134): Notify: Parameter Substitution: parameter "pUserName" =  "User204"
Action.c(134): Notify: Next row for parameter pPassword = 1 [table  = pPassword].
Action.c(134): Notify: Parameter Substitution: parameter "pPassword" =  "password"
Action.c(134): web_set_user was successful  	[MsgId: MMSG-26392]
Action.c(136): web_custom_request("customerOrderAndTransactionList_2") started  	[MsgId: MMSG-26355]
Action.c(136): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(136): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(136): Notify: Parameter Substitution: parameter "p4" =  "{"customerOrderSearchCriteria":{"entityType":"All","orderNumber":"
Action.c(136): Notify: Parameter Substitution: parameter "pOrderNumber" =  "80360000160"
Action.c(136): Notify: Parameter Substitution: parameter "p5" =  ","parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":1000,"customerInfo":{"customerFirstName":""
Action.c(136): Notify: Parameter Substitution: parameter "c_First_Name" =  "Green"
Action.c(136): Notify: Parameter Substitution: parameter "p6" =  "","customerLastName":""
Action.c(136): Notify: Parameter Substitution: parameter "c_Last_Name" =  "Lantern"
Action.c(136): Notify: Parameter Substitution: parameter "c_First_Name" =  "Green"
Action.c(136): Notify: Parameter Substitution: parameter "c_Last_Name" =  "Lantern"
Action.c(136): Notify: Parameter Substitution: parameter "c_Email_Id" =  "SIRIUSLUME@GMAIL.COM"
Action.c(136): Notify: Parameter Substitution: parameter "c_Phone_Number" =  "9884098840"
Action.c(136): Notify: Parameter Substitution: parameter "c_externalCustomerId" =  "202493282"
Action.c(136): Notify: Parameter Substitution: parameter "p7" =  "","customField1":"","customField2":"","customField3":"","customField4":"","customField5":"","customField6":"","customField7":"","customField8":"","customField9":"","customField10":""},"customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}"
Action.c(136): web_custom_request("customerOrderAndTransactionList_2") was successful, 554 body bytes, 186 header bytes, 12 chunking overhead bytes  	[MsgId: MMSG-26385]
Action.c(149): web_url("ping.jsp_4") started  	[MsgId: MMSG-26355]
Action.c(149): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(149): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(149): web_url("ping.jsp_4") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Action.c(159): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_1_ECOM_CA_NavigationalLoginLogout_OrderTile"
Action.c(159): Notify: Transaction "TCP_07_1_ECOM_CA_NavigationalLoginLogout_OrderTile_03_Order_Lookup_from_Order_tile" ended with "Pass" status (Duration: 2.1891 Wasted Time: 0.0071).
Action.c(163): web_url("ping.jsp_5") started  	[MsgId: MMSG-26355]
Action.c(163): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(163): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(163): web_url("ping.jsp_5") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Action.c(173): web_url("ping.jsp_6") started  	[MsgId: MMSG-26355]
Action.c(173): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(173): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(173): web_url("ping.jsp_6") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Action.c(183): web_url("ping.jsp_7") started  	[MsgId: MMSG-26355]
Action.c(183): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(183): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(183): web_url("ping.jsp_7") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Action.c(193): web_url("ping.jsp_9") started  	[MsgId: MMSG-26355]
Action.c(193): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(193): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(193): web_url("ping.jsp_9") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Ending action Action.
Ending iteration 1.
Ending Vuser...
Starting action vuser_end.
vuser_end.c(6): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_1_ECOM_CA_NavigationalLoginLogout_OrderTile"
vuser_end.c(6): Notify: Transaction "TCP_07_1_ECOM_CA_NavigationalLoginLogout_OrderTile_04_Logout" started.
vuser_end.c(8): web_url("logout") started  	[MsgId: MMSG-26355]
vuser_end.c(8): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_end.c(8): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_end.c(8): Redirecting "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/saml/logout" (redirection depth is 0)  	[MsgId: MMSG-26694]
vuser_end.c(8): To location "https://EOM-MIP-Perf.childrensplace.com:15001/profile/SAML2/Redirect/SLO?SAMLRequest=nVLBbqMwFPwV5DtgbCDGClSVspUiJdts0%2B6hN2OewRLYLDarfv5C2lRtDz3s0eM3M29Gb3vzMvTBX5ictqZESYRRAEbaRpu2RE%2BPdyFDN9XWiaEnIz%2FY1s7%2BAf7M4HywMI3jr18lmifDrXDacSMGcNxLfr49HjiJMB8n6620PQp2C1Eb4S9unfej43H84%2F4YHven8ASTimSn%2B2YC48ZeSIikHXiSYZzEi4jSPcSrKokfoNETSB%2BfD%2Fco2O9KJFLSkYx1tE4Aq7YjOU1ZojWV0LYMliHnZtgb54XxJSI42YS4CEnxmFCON5zQiOXZMwp%2BX9tYVkdv2fmFPH3M%2FH1k4RxMa0xUSbGNP4pcJX8upP3ufySDOzsNwn8%2FviK6CdVllM9LoSC10tCgYHX%2BNYt%2BfU4lGvSIgvPpCyoFqp4WR4LT6%2F6vG7%2BfwxncWtTeNPBSQU0ZiCxtoE4Iy3OW00KxmkqMa5wyYEkGacbqnMpCUZUJLBQrhIBsI5Wo1ZvHF9V39NPpVf8A&SigAlg=http%3A%2F%2Fwww.w3.org%2F2000%2F09%2Fxmldsig%23rsa-sha1&Signature=P6IVuoGK1oYdPMyGmr3%2B3HMhDg8JvbPwDjX7cbR%2BdCQW%2B6YWsr1K8rjJzeapi4fbAiAwnOjP9adH3U6elVoa96XM7fFRLWvcyP6%2Fq%2BiYLiKJBuKzGrJpDgdGNHQxeAnkcPEu5DBYfAaX1BqUg3fFpHoPXPA0nL225lvSa4TuQJ6kmhehAEuShsiUnM%2BqwBPuFXD%2FVwVGmoO%2FMmkk4wRP9HEt5bFewuawXEG28lnTqG%2F5gay2gwCAErvEQFg4h4r53RfQarLiHmg3geZCzR%2BGul%2FtjlEIPMKmqu9%2FGG%2BVreWbzl1fmob3z7ScE0n3OB%2FkKoBozCFvanRk%2FotkakcPbg%3D%3D"  	[MsgId: MMSG-26693]
vuser_end.c(8): Found resource "https://EOM-MIP-Perf.childrensplace.com:15001/manh/resources/css/mip.css" in HTML "https://EOM-MIP-Perf.childrensplace.com:15001/profile/SAML2/Redirect/SLO?SAMLRequest=nVLBbqMwFPwV5DtgbCDGClSVspUiJdts0%2B6hN2OewRLYLDarfv5C2lRtDz3s0eM3M29Gb3vzMvTBX5ictqZESYRRAEbaRpu2RE%2BPdyFDN9XWiaEnIz%2FY1s7%2BAf7M4HywMI3jr18lmifDrXDacSMGcNxLfr49HjiJMB8n6620PQp2C1Eb4S9unfej43H84%2F4YHven8ASTimSn%2B2YC48ZeSIikHXiSYZzEi4jSPcSrKokfoNETSB%2BfD%2Fco2O9KJFLSkYx1tE4Aq7YjOU1ZojWV0LYMliHnZtgb54XxJSI42YS4CEnxmFCON5zQiOXZMwp%2BX9tYVkdv2fmFPH3M%2FH1k4RxMa0xUSbGNP4pcJX8upP3ufySDOzsNwn8%2FviK6CdVllM9LoSC10tCgYHX%2BNYt%2BfU4lGvSIgvPpCyoFqp4WR4LT6%2F6vG7%2BfwxncWtTeNPBSQU0ZiCxtoE4Iy3OW00KxmkqMa5wyYEkGacbqnMpCUZUJLBQrhIBsI5Wo1ZvHF9V39NPpVf8A&SigAlg=http%3A%2F%2Fwww.w3.org%2F2000%2F09%2Fxmldsig%23rsa-sha1&Signature=P6IVuoGK1oYdPMyGmr3%2B3HMhDg8JvbPwDjX7cbR%2BdCQW%2B6YWsr1K8rjJzeapi4fbAiAwnOjP9adH3U6elVoa96XM7fFRLWvcyP6%2Fq%2BiYLiKJBuKzGrJpDgdGNHQxeAnkcPEu5DBYfAaX1BqUg3fFpHoPXPA0nL225lvSa4TuQJ6kmhehAEuShsiUnM%2BqwBPuFXD%2FVwVGmoO%2FMmkk4wRP9HEt5bFewuawXEG28lnTqG%2F5gay2gwCAErvEQFg4h4r53RfQarLiHmg3geZCzR%2BGul%2FtjlEIPMKmqu9%2FGG%2BVreWbzl1fmob3z7ScE0n3OB%2FkKoBozCFvanRk%2FotkakcPbg%3D%3D"  	[MsgId: MMSG-26659]
vuser_end.c(8): Found resource "https://EOM-MIP-Perf.childrensplace.com:15001/ext/ext-all.js" in HTML "https://EOM-MIP-Perf.childrensplace.com:15001/profile/SAML2/Redirect/SLO?SAMLRequest=nVLBbqMwFPwV5DtgbCDGClSVspUiJdts0%2B6hN2OewRLYLDarfv5C2lRtDz3s0eM3M29Gb3vzMvTBX5ictqZESYRRAEbaRpu2RE%2BPdyFDN9XWiaEnIz%2FY1s7%2BAf7M4HywMI3jr18lmifDrXDacSMGcNxLfr49HjiJMB8n6620PQp2C1Eb4S9unfej43H84%2F4YHven8ASTimSn%2B2YC48ZeSIikHXiSYZzEi4jSPcSrKokfoNETSB%2BfD%2Fco2O9KJFLSkYx1tE4Aq7YjOU1ZojWV0LYMliHnZtgb54XxJSI42YS4CEnxmFCON5zQiOXZMwp%2BX9tYVkdv2fmFPH3M%2FH1k4RxMa0xUSbGNP4pcJX8upP3ufySDOzsNwn8%2FviK6CdVllM9LoSC10tCgYHX%2BNYt%2BfU4lGvSIgvPpCyoFqp4WR4LT6%2F6vG7%2BfwxncWtTeNPBSQU0ZiCxtoE4Iy3OW00KxmkqMa5wyYEkGacbqnMpCUZUJLBQrhIBsI5Wo1ZvHF9V39NPpVf8A&SigAlg=http%3A%2F%2Fwww.w3.org%2F2000%2F09%2Fxmldsig%23rsa-sha1&Signature=P6IVuoGK1oYdPMyGmr3%2B3HMhDg8JvbPwDjX7cbR%2BdCQW%2B6YWsr1K8rjJzeapi4fbAiAwnOjP9adH3U6elVoa96XM7fFRLWvcyP6%2Fq%2BiYLiKJBuKzGrJpDgdGNHQxeAnkcPEu5DBYfAaX1BqUg3fFpHoPXPA0nL225lvSa4TuQJ6kmhehAEuShsiUnM%2BqwBPuFXD%2FVwVGmoO%2FMmkk4wRP9HEt5bFewuawXEG28lnTqG%2F5gay2gwCAErvEQFg4h4r53RfQarLiHmg3geZCzR%2BGul%2FtjlEIPMKmqu9%2FGG%2BVreWbzl1fmob3z7ScE0n3OB%2FkKoBozCFvanRk%2FotkakcPbg%3D%3D"  	[MsgId: MMSG-26659]
vuser_end.c(8): web_url("logout") was successful, 461203 body bytes, 2500 header bytes, 6390 chunking overhead bytes  	[MsgId: MMSG-26385]
vuser_end.c(18): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_1_ECOM_CA_NavigationalLoginLogout_OrderTile"
vuser_end.c(18): Notify: Transaction "TCP_07_1_ECOM_CA_NavigationalLoginLogout_OrderTile_04_Logout" ended with "Pass" status (Duration: 0.2254 Wasted Time: 0.0921).
Ending action vuser_end.
Vuser Terminated.
