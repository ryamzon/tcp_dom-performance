/*
 * Example WebSphere MQ LoadRunner script (written in Java)
 * 
 * Script Description: 
 *     This script puts a message on a queue, then gets a response message from 
 *     another queue.
 *
 * You will probably need to add the following jar files to your classpath
 *   - com.ibm.mq.jar
 *   - connector.jar
 *   - com.ibm.mq.jmqi.jar
 *   - com.ibm.mq.headers.jar
 *   - com.ibm.mq.commonservices.jar
 */
 
import lrapi.lr;
import com.ibm.mq.*;
import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.*;

public class Actions
{
    // Variables used by more than one method
    String queueMgrName = "PERF.TCP.QMGR";
    
    String putQueueName = "TCP.TO.MIF.CUSTOMER.ORDER";
    
    String getQueueName = "TCP.TO.MIF.CUSTOMER.ORDER";
    
    StringBuffer idBuffer = null;
    FileWriter idFile = null;
	BufferedWriter idWriter = null;
 
    MQQueueManager queueMgr = null;
    
    MQQueue getQueue = null;
    
    MQQueue putQueue = null;
    
    MQPutMessageOptions pmo = new MQPutMessageOptions();
    
    MQGetMessageOptions gmo = new MQGetMessageOptions();
    
    MQMessage requestMsg = new MQMessage();
    
    MQMessage responseMsg = new MQMessage();
    
    String msgBody = null;
    
    public int init() throws Throwable
    {
        
    	// Open a connection to the queue manager and the put/get queues
        
        try
        {
            // As values set in the MQEnvironment class take effect when the 
            // MQQueueManager constructor is called, you must set the values 
            // in the MQEnvironment class before you construct an MQQueueManager object.
            
            MQEnvironment.hostname="10.18.1.21";
            
            MQEnvironment.port=1414;
            
            MQEnvironment.channel = "TCP.SVRCONN.CHANNEL";
            
            queueMgr = new MQQueueManager(queueMgrName);
            
            idFile = new FileWriter("C:\\PostXML_CA.txt");
			idWriter = new BufferedWriter(idFile);
			idBuffer = new StringBuffer();
 
            // Access the put/get queues. Note the open options used.
            putQueue = queueMgr.accessQueue(putQueueName, MQC.MQOO_BIND_NOT_FIXED | MQC.MQOO_OUTPUT);
            
            getQueue= queueMgr.accessQueue(getQueueName, MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_OUTPUT);
            
        } 
        
        catch(Exception e)
        {
            lr.error_message("Error connecting to queue manager or accessing queues.");
        
            lr.exit(lr.EXIT_VUSER, lr.FAIL);
        }
 
        return 0;
    }//end of init
 
    public int action() throws Throwable 
    {
        // This is an XML message that will be put on the queue. Could do some fancy 
        // things with XML classes here if necessary.
        // The message string can contain {parameters} if lr.eval_string() is used.
       MQMessage textMessage = new MQMessage(); // creating a object textMessage
	     
       textMessage.format = "MQSTR";
	   
       MQPutMessageOptions pmo = new MQPutMessageOptions();
	   
       StringBuffer strBuf = new StringBuffer(); // StringBuffer is also string but varying
	  
      strBuf.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><tXML xsi:noNamespaceSchemaLocation=\"dom-order.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><Header><Source>WCS</Source><Action_Type>Update</Action_Type><Reference_ID>803"+lr.eval_string("{pOrderID}")+"</Reference_ID><Message_Type>Customer Order</Message_Type><Company_ID>2</Company_ID></Header><Message><Order><OrderNumber>803"+lr.eval_string("{pOrderID}")+"</OrderNumber><OrderCaptureDate>"+lr.eval_string("{pOrderDate}")+" EDT</OrderCaptureDate><ExternalOrderNumber>803"+lr.eval_string("{pOrderID}")+"</ExternalOrderNumber><OrderType>CAECOM</OrderType><OrderCurrency>CAD</OrderCurrency><EnteredBy>WCS</EnteredBy><EntryType>Web</EntryType><Confirmed>true</Confirmed><Canceled>false</Canceled><OnHold>false</OnHold><PaymentStatus>AUTHORIZED</PaymentStatus><CustomerInfo><CustomerId>202493282</CustomerId></CustomerInfo><PaymentDetails><PaymentDetail><ExternalPaymentDetailId>"+lr.eval_string("{pExtPaymentID}")+"</ExternalPaymentDetailId><PaymentMethod>Credit Card</PaymentMethod><CardType>Amex</CardType><AccountNumber>818369675311007</AccountNumber><AccountDisplayNumber>***********1007</AccountDisplayNumber><NameAsOnCard>Green Lantern</NameAsOnCard><CardExpiryMonth>07</CardExpiryMonth><CardExpiryYear>2018</CardExpiryYear><ReqAuthorizationAmount>67.49</ReqAuthorizationAmount><CurrencyCode>CAD</CurrencyCode><ChargeSequence>1</ChargeSequence><Encrypted>true</Encrypted><ReferenceFields><ReferenceField1>I1</ReferenceField1></ReferenceFields><BillToDetail><BillToFirstName>Green</BillToFirstName><BillToLastName>Lantern</BillToLastName><BillToAddressLine1>1010 EASY ST</BillToAddressLine1><BillToAddressLine2></BillToAddressLine2><BillToCity>OTTAWA</BillToCity><BillToState>ON</BillToState><BillToPostalCode>K1A0B1</BillToPostalCode><BillToCountry>CA</BillToCountry><BillToPhone>9884098840</BillToPhone><BillToEmail>SIRIUSLUME@GMAIL.COM</BillToEmail></BillToDetail><PaymentTransactionDetails><PaymentTransactionDetail><ExternalPaymentTransactionId>"+lr.eval_string("{pExtPaymentID}")+"</ExternalPaymentTransactionId><TransactionType>Authorization</TransactionType><RequestedAmount>67.49</RequestedAmount><RequestId>"+lr.eval_string("{pRequestID}")+"</RequestId><FollowOnToken></FollowOnToken><RequestedDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</RequestedDTTM><ProcessedAmount>67.49</ProcessedAmount><TransactionDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TransactionDTTM><TransactionExpiryDate>"+lr.eval_string("{pExpiryDate}")+" EDT</TransactionExpiryDate><TransactionDecision>Success</TransactionDecision><TransactionDecisionDescription>Success</TransactionDecisionDescription></PaymentTransactionDetail></PaymentTransactionDetails></PaymentDetail></PaymentDetails><ChargeDetails><ChargeDetail><ExtChargeDetailId>1</ExtChargeDetailId><ChargeCategory>Shipping</ChargeCategory><ChargeAmount>8.00</ChargeAmount><OriginalChargeAmount>8.00</OriginalChargeAmount><IsOverridden>TRUE</IsOverridden><OverrideReason><ReasonCode>OT</ReasonCode><Comments>SHIPPING_DISCOUNT_COUPON</Comments></OverrideReason></ChargeDetail></ChargeDetails><TaxDetails><TaxDetail><ExtTaxDetailId>1</ExtTaxDetailId><TaxCategory>30</TaxCategory><ChargeCategory>Shipping</ChargeCategory><TaxLocationName>CA</TaxLocationName><TaxName>Shipping</TaxName><TaxAmount>0.40</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><ReferenceFields><ReferenceField1>English</ReferenceField1><ReferenceField2></ReferenceField2><ReferenceField4></ReferenceField4><ReferenceField5></ReferenceField5><ReferenceField7>172.17.182.92</ReferenceField7><ReferenceNumber1>3</ReferenceNumber1><ReferenceNumber2>15.00000</ReferenceNumber2><ReferenceNumber3></ReferenceNumber3><ReferenceNumber4>487</ReferenceNumber4></ReferenceFields><OrderLines><OrderLine><LineNumber>1</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>2.99</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>PGND</ShipVia><ShippingAddress><ShipToFirstName>jeya</ShipToFirstName><ShipToLastName>ram</ShipToLastName><ShipToAddressLine1>120 Broadway Street</ShipToAddressLine1><ShipToAddressLine2></ShipToAddressLine2><ShipToCity>Ottawa</ShipToCity><ShipToState>ON</ShipToState><ShipToPostalCode>K1S2V6</ShipToPostalCode><ShipToCounty></ShipToCounty><ShipToCountry>CA</ShipToCountry><ShipToPhone>9098765432</ShipToPhone><ShipToEmail>qaqaqa@yopmail.com</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails><DiscountDetail><ExtDiscountDetailId>1</ExtDiscountDetailId><ExtDiscountId>DEAL_007_20160629154907</ExtDiscountId><DiscountType>Others</DiscountType><DiscountAmount>4.50000</DiscountAmount><DiscountStatus>Applied</DiscountStatus><ReasonCode>DD</ReasonCode><Comments>DEAL</Comments></DiscountDetail></DiscountDetails><TaxDetails><TaxDetail><ExtTaxDetailId>2</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>CA</TaxLocationName><TaxName>COUNTRY</TaxName><TaxAmount>0.40</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00889705247985</ReferenceField1><ReferenceNumber1>7.50</ReferenceNumber1><ReferenceNumber2>0.00</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>2</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>3.50</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>PGND</ShipVia><ShippingAddress><ShipToFirstName>jeya</ShipToFirstName><ShipToLastName>ram</ShipToLastName><ShipToAddressLine1>120 Broadway Street</ShipToAddressLine1><ShipToAddressLine2></ShipToAddressLine2><ShipToCity>Ottawa</ShipToCity><ShipToState>ON</ShipToState><ShipToPostalCode>K1S2V6</ShipToPostalCode><ShipToCounty></ShipToCounty><ShipToCountry>CA</ShipToCountry><ShipToPhone>9098765432</ShipToPhone><ShipToEmail>qaqaqa@yopmail.com</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>2</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>CA</TaxLocationName><TaxName>COUNTRY</TaxName><TaxAmount>0.40</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00889705271478</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>0.00</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>3</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>3.50</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>PGND</ShipVia><ShippingAddress><ShipToFirstName>jeya</ShipToFirstName><ShipToLastName>ram</ShipToLastName><ShipToAddressLine1>120 Broadway Street</ShipToAddressLine1><ShipToAddressLine2></ShipToAddressLine2><ShipToCity>Ottawa</ShipToCity><ShipToState>ON</ShipToState><ShipToPostalCode>K1S2V6</ShipToPostalCode><ShipToCounty></ShipToCounty><ShipToCountry>CA</ShipToCountry><ShipToPhone>9098765432</ShipToPhone><ShipToEmail>qaqaqa@yopmail.com</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>2</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>CA</TaxLocationName><TaxName>COUNTRY</TaxName><TaxAmount>0.40</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00889705271478</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>0.00</ReferenceNumber2></LineReferenceFields></OrderLine>");
     
      strBuf.append("<OrderLine><LineNumber>4</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>10.95</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>PGND</ShipVia><ShippingAddress><ShipToFirstName>jeya</ShipToFirstName><ShipToLastName>ram</ShipToLastName><ShipToAddressLine1>120 Broadway Street</ShipToAddressLine1><ShipToAddressLine2></ShipToAddressLine2><ShipToCity>Ottawa</ShipToCity><ShipToState>ON</ShipToState><ShipToPostalCode>K1S2V6</ShipToPostalCode><ShipToCounty></ShipToCounty><ShipToCountry>CA</ShipToCountry><ShipToPhone>9098765432</ShipToPhone><ShipToEmail>qaqaqa@yopmail.com</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>2</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>CA</TaxLocationName><TaxName>COUNTRY</TaxName><TaxAmount>0.40</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00889705271478</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>0.00</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>5</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>10.95</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>PGND</ShipVia><ShippingAddress><ShipToFirstName>jeya</ShipToFirstName><ShipToLastName>ram</ShipToLastName><ShipToAddressLine1>120 Broadway Street</ShipToAddressLine1><ShipToAddressLine2></ShipToAddressLine2><ShipToCity>Ottawa</ShipToCity><ShipToState>ON</ShipToState><ShipToPostalCode>K1S2V6</ShipToPostalCode><ShipToCounty></ShipToCounty><ShipToCountry>CA</ShipToCountry><ShipToPhone>9098765432</ShipToPhone><ShipToEmail>qaqaqa@yopmail.com</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>2</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>CA</TaxLocationName><TaxName>COUNTRY</TaxName><TaxAmount>0.40</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00889705271478</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>0.00</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>6</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>0.95</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>PGND</ShipVia><ShippingAddress><ShipToFirstName>jeya</ShipToFirstName><ShipToLastName>ram</ShipToLastName><ShipToAddressLine1>120 Broadway Street</ShipToAddressLine1><ShipToAddressLine2></ShipToAddressLine2><ShipToCity>Ottawa</ShipToCity><ShipToState>ON</ShipToState><ShipToPostalCode>K1S2V6</ShipToPostalCode><ShipToCounty></ShipToCounty><ShipToCountry>CA</ShipToCountry><ShipToPhone>9098765432</ShipToPhone><ShipToEmail>qaqaqa@yopmail.com</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>2</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>CA</TaxLocationName><TaxName>COUNTRY</TaxName><TaxAmount>0.40</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00889705271478</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>0.00</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>7</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>12.95</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>PGND</ShipVia><ShippingAddress><ShipToFirstName>jeya</ShipToFirstName><ShipToLastName>ram</ShipToLastName><ShipToAddressLine1>120 Broadway Street</ShipToAddressLine1><ShipToAddressLine2></ShipToAddressLine2><ShipToCity>Ottawa</ShipToCity><ShipToState>ON</ShipToState><ShipToPostalCode>K1S2V6</ShipToPostalCode><ShipToCounty></ShipToCounty><ShipToCountry>CA</ShipToCountry><ShipToPhone>9098765432</ShipToPhone><ShipToEmail>qaqaqa@yopmail.com</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>2</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>CA</TaxLocationName><TaxName>COUNTRY</TaxName><TaxAmount>0.40</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00889705271478</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>0.00</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>8</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>1.25</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>PGND</ShipVia><ShippingAddress><ShipToFirstName>jeya</ShipToFirstName><ShipToLastName>ram</ShipToLastName><ShipToAddressLine1>120 Broadway Street</ShipToAddressLine1><ShipToAddressLine2></ShipToAddressLine2><ShipToCity>Ottawa</ShipToCity><ShipToState>ON</ShipToState><ShipToPostalCode>K1S2V6</ShipToPostalCode><ShipToCounty></ShipToCounty><ShipToCountry>CA</ShipToCountry><ShipToPhone>9098765432</ShipToPhone><ShipToEmail>qaqaqa@yopmail.com</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>2</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>CA</TaxLocationName><TaxName>COUNTRY</TaxName><TaxAmount>0.40</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00889705271478</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>0.00</ReferenceNumber2></LineReferenceFields></OrderLine>");

      strBuf.append("<OrderLine><LineNumber>9</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>1.25</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>PGND</ShipVia><ShippingAddress><ShipToFirstName>jeya</ShipToFirstName><ShipToLastName>ram</ShipToLastName><ShipToAddressLine1>120 Broadway Street</ShipToAddressLine1><ShipToAddressLine2></ShipToAddressLine2><ShipToCity>Ottawa</ShipToCity><ShipToState>ON</ShipToState><ShipToPostalCode>K1S2V6</ShipToPostalCode><ShipToCounty></ShipToCounty><ShipToCountry>CA</ShipToCountry><ShipToPhone>9098765432</ShipToPhone><ShipToEmail>qaqaqa@yopmail.com</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>2</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>CA</TaxLocationName><TaxName>COUNTRY</TaxName><TaxAmount>0.40</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00889705271478</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>0.00</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>10</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>3.50</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>PGND</ShipVia><ShippingAddress><ShipToFirstName>jeya</ShipToFirstName><ShipToLastName>ram</ShipToLastName><ShipToAddressLine1>120 Broadway Street</ShipToAddressLine1><ShipToAddressLine2></ShipToAddressLine2><ShipToCity>Ottawa</ShipToCity><ShipToState>ON</ShipToState><ShipToPostalCode>K1S2V6</ShipToPostalCode><ShipToCounty></ShipToCounty><ShipToCountry>CA</ShipToCountry><ShipToPhone>9098765432</ShipToPhone><ShipToEmail>qaqaqa@yopmail.com</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>2</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>CA</TaxLocationName><TaxName>COUNTRY</TaxName><TaxAmount>0.40</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00889705271478</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>0.00</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>11</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>3.50</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>PGND</ShipVia><ShippingAddress><ShipToFirstName>jeya</ShipToFirstName><ShipToLastName>ram</ShipToLastName><ShipToAddressLine1>120 Broadway Street</ShipToAddressLine1><ShipToAddressLine2></ShipToAddressLine2><ShipToCity>Ottawa</ShipToCity><ShipToState>ON</ShipToState><ShipToPostalCode>K1S2V6</ShipToPostalCode><ShipToCounty></ShipToCounty><ShipToCountry>CA</ShipToCountry><ShipToPhone>9098765432</ShipToPhone><ShipToEmail>qaqaqa@yopmail.com</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>2</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>CA</TaxLocationName><TaxName>COUNTRY</TaxName><TaxAmount>0.40</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00889705271478</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>0.00</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>12</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>3.50</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>PGND</ShipVia><ShippingAddress><ShipToFirstName>jeya</ShipToFirstName><ShipToLastName>ram</ShipToLastName><ShipToAddressLine1>120 Broadway Street</ShipToAddressLine1><ShipToAddressLine2></ShipToAddressLine2><ShipToCity>Ottawa</ShipToCity><ShipToState>ON</ShipToState><ShipToPostalCode>K1S2V6</ShipToPostalCode><ShipToCounty></ShipToCounty><ShipToCountry>CA</ShipToCountry><ShipToPhone>9098765432</ShipToPhone><ShipToEmail>qaqaqa@yopmail.com</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>2</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>CA</TaxLocationName><TaxName>COUNTRY</TaxName><TaxAmount>0.40</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00889705271478</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>0.00</ReferenceNumber2></LineReferenceFields></OrderLine></OrderLines></Order></Message></tXML>");
      
      System.out.println(strBuf);
      
        String data = lr.eval_string("{pOrderID}");
		String data1= "803";
		System.out.println(data1+data);
		idWriter.write(data1+data);
		idWriter.write(",");

       // Clear the message objects on each iteration.
       
       requestMsg.clearMessage();
       
       responseMsg.clearMessage();
 
       // Create a message object and put it on the request queue
        
       lr.start_transaction("TCP_1_2_ECOM_CA_Post_XML_Order_01_Post");
        
       try 
       {
            pmo.options = MQC.MQPMO_NEW_MSG_ID; // The queue manager replaces the contents of the MsgId field in MQMD with a new message identifier.
       
            requestMsg.replyToQueueName = getQueueName; // the response should be put on this queue
       
            requestMsg.report=MQC.MQRO_PASS_MSG_ID; //If a report or reply is generated as a result of this message, the MsgId of this message is copied to the MsgId of the report or reply message.
       
            requestMsg.format = MQC.MQFMT_STRING; // Set message format. The application message data can be either an SBCS string (single-byte character set), or a DBCS string (double-byte character set).
       
            requestMsg.messageType=MQC.MQMT_REQUEST; // The message is one that requires a reply.
       
            requestMsg.writeString(strBuf.toString()); // message payload msgBody
       
            putQueue.put(requestMsg, pmo);
            
            String data2 = lr.eval_string("{pTimeStamp}");
            idWriter.write(data2);
            idWriter.write("\n");
       }
        
        catch(Exception e) 
        {
        	lr.error_message("Error sending message.");
        	
        	lr.exit(lr.EXIT_VUSER, lr.FAIL);
        }
        
        lr.end_transaction("TCP_1_2_ECOM_CA_Post_XML_Order_01_Post",lr.AUTO); 
        
        lr.think_time(1);
 
        return 0;
    
    }//end of action
   
    public int end() throws Throwable  
    {
        // 	Close all the connections
        try 
        {
            putQueue.close();
            
            getQueue.close();
            
            queueMgr.close();
            idWriter.close();
        }
        
        catch(Exception e)
        {
            lr.error_message("Exception in closing the connections");
            
            lr.exit(lr.EXIT_VUSER, lr.FAIL);
        }
 
        return 0;
    }//end of end
} 