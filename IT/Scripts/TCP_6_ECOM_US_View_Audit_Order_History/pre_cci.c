# 1 "c:\\it\\scripts\\tcp_6_ecom_us_view_audit_order_history\\\\combined_TCP_6_ECOM_US_View_Audit_Order_History.c"
# 1 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h" 1
 
 












 











# 103 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"








































































	

 



















 
 
 
 
 


 
 
 
 
 
 














int     lr_start_transaction   (char * transaction_name);
int lr_start_sub_transaction          (char * transaction_name, char * trans_parent);
long lr_start_transaction_instance    (char * transaction_name, long parent_handle);
int   lr_start_cross_vuser_transaction		(char * transaction_name, char * trans_id_param); 



int     lr_end_transaction     (char * transaction_name, int status);
int lr_end_sub_transaction            (char * transaction_name, int status);
int lr_end_transaction_instance       (long transaction, int status);
int   lr_end_cross_vuser_transaction	(char * transaction_name, char * trans_id_param, int status);


 
typedef char* lr_uuid_t;
 



lr_uuid_t lr_generate_uuid();

 


int lr_generate_uuid_free(lr_uuid_t uuid);

 



int lr_generate_uuid_on_buf(lr_uuid_t buf);

   
# 266 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
int lr_start_distributed_transaction  (char * transaction_name, lr_uuid_t correlator, long timeout  );

   







int lr_end_distributed_transaction  (lr_uuid_t correlator, int status);


double lr_stop_transaction            (char * transaction_name);
double lr_stop_transaction_instance   (long parent_handle);


void lr_resume_transaction           (char * trans_name);
void lr_resume_transaction_instance  (long trans_handle);


int lr_update_transaction            (const char *trans_name);


 
void lr_wasted_time(long time);


 
int lr_set_transaction(const char *name, double duration, int status);
 
long lr_set_transaction_instance(const char *name, double duration, int status, long parent_handle);


int   lr_user_data_point                      (char *, double);
long lr_user_data_point_instance                   (char *, double, long);
 



int lr_user_data_point_ex(const char *dp_name, double value, int log_flag);
long lr_user_data_point_instance_ex(const char *dp_name, double value, long parent_handle, int log_flag);


int lr_transaction_add_info      (const char *trans_name, char *info);
int lr_transaction_instance_add_info   (long trans_handle, char *info);
int lr_dpoint_add_info           (const char *dpoint_name, char *info);
int lr_dpoint_instance_add_info        (long dpoint_handle, char *info);


double lr_get_transaction_duration       (char * trans_name);
double lr_get_trans_instance_duration    (long trans_handle);
double lr_get_transaction_think_time     (char * trans_name);
double lr_get_trans_instance_think_time  (long trans_handle);
double lr_get_transaction_wasted_time    (char * trans_name);
double lr_get_trans_instance_wasted_time (long trans_handle);
int    lr_get_transaction_status		 (char * trans_name);
int	   lr_get_trans_instance_status		 (long trans_handle);

 



int lr_set_transaction_status(int status);

 



int lr_set_transaction_status_by_name(int status, const char *trans_name);
int lr_set_transaction_instance_status(int status, long trans_handle);


typedef void* merc_timer_handle_t;
 

merc_timer_handle_t lr_start_timer();
double lr_end_timer(merc_timer_handle_t timer_handle);


 
 
 
 
 
 











 



int   lr_rendezvous  (char * rendezvous_name);
 




int   lr_rendezvous_ex (char * rendezvous_name);



 
 
 
 
 
char *lr_get_vuser_ip (void);
void   lr_whoami (int *vuser_id, char ** sgroup, int *scid);
char *	  lr_get_host_name (void);
char *	  lr_get_master_host_name (void);

 
long     lr_get_attrib_long	(char * attr_name);
char *   lr_get_attrib_string	(char * attr_name);
double   lr_get_attrib_double      (char * attr_name);

char * lr_paramarr_idx(const char * paramArrayName, unsigned int index);
char * lr_paramarr_random(const char * paramArrayName);
int    lr_paramarr_len(const char * paramArrayName);

int	lr_param_unique(const char * paramName);
int lr_param_sprintf(const char * paramName, const char * format, ...);


 
 
static void *ci_this_context = 0;






 








void lr_continue_on_error (int lr_continue);
char *   lr_decrypt (const char *EncodedString);


 
 
 
 
 
 



 







 















void   lr_abort (void);
void lr_exit(int exit_option, int exit_status);
void lr_abort_ex (unsigned long flags);

void   lr_peek_events (void);


 
 
 
 
 


void   lr_think_time (double secs);

 


void lr_force_think_time (double secs);


 
 
 
 
 



















int   lr_msg (char * fmt, ...);
int   lr_debug_message (unsigned int msg_class,
									    char * format,
										...);
# 505 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
void   lr_new_prefix (int type,
                                 char * filename,
                                 int line);
# 508 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
int   lr_log_message (char * fmt, ...);
int   lr_message (char * fmt, ...);
int   lr_error_message (char * fmt, ...);
int   lr_output_message (char * fmt, ...);
int   lr_vuser_status_message (char * fmt, ...);
int   lr_error_message_without_fileline (char * fmt, ...);
int   lr_fail_trans_with_error (char * fmt, ...);

 
 
 
 
 
# 531 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"

 
 
 
 
 





int   lr_next_row ( char * table);
int lr_advance_param ( char * param);



														  
														  

														  
														  

													      
 


char *   lr_eval_string (char * str);
int   lr_eval_string_ext (const char *in_str,
                                     unsigned long const in_len,
                                     char ** const out_str,
                                     unsigned long * const out_len,
                                     unsigned long const options,
                                     const char *file,
								     long const line);
# 565 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
void   lr_eval_string_ext_free (char * * pstr);

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
int lr_param_increment (char * dst_name,
                              char * src_name);
# 588 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"













											  
											  

											  
											  
											  

int	  lr_save_var (char *              param_val,
							  unsigned long const param_val_len,
							  unsigned long const options,
							  char *			  param_name);
# 612 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
int   lr_save_string (const char * param_val, const char * param_name);
int   lr_free_parameter (const char * param_name);
int   lr_save_int (const int param_val, const char * param_name);
int   lr_save_timestamp (const char * tmstampParam, ...);
int   lr_save_param_regexp (const char *bufferToScan, unsigned int bufSize, ...);

int   lr_convert_double_to_integer (const char *source_param_name, const char * target_param_name);
int   lr_convert_double_to_double (const char *source_param_name, const char *format_string, const char * target_param_name);

 
 
 
 
 
 
# 683 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
void   lr_save_datetime (const char *format, int offset, const char *name);









 











 
 
 
 
 






 



char * lr_error_context_get_entry (char * key);

 



long   lr_error_context_get_error_id (void);


 
 
 

int lr_table_get_rows_num (char * param_name);

int lr_table_get_cols_num (char * param_name);

char * lr_table_get_cell_by_col_index (char * param_name, int row, int col);

char * lr_table_get_cell_by_col_name (char * param_name, int row, const char* col_name);

int lr_table_get_column_name_by_index (char * param_name, int col, 
											char * * const col_name,
											int * col_name_len);
# 744 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"

int lr_table_get_column_name_by_index_free (char * col_name);

 
 
 
 
# 759 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
int   lr_zip (const char* param1, const char* param2);
int   lr_unzip (const char* param1, const char* param2);

 
 
 
 
 
 
 
 

 
 
 
 
 
 
int   lr_param_substit (char * file,
                                   int const line,
                                   char * in_str,
                                   int const in_len,
                                   char * * const out_str,
                                   int * const out_len);
# 783 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
void   lr_param_substit_free (char * * pstr);


 
# 795 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"





char *   lrfnc_eval_string (char * str,
                                      char * file_name,
                                      long const line_num);
# 803 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"


int   lrfnc_save_string ( const char * param_val,
                                     const char * param_name,
                                     const char * file_name,
                                     long const line_num);
# 809 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"

int   lrfnc_free_parameter (const char * param_name );







typedef struct _lr_timestamp_param
{
	int iDigits;
}lr_timestamp_param;

extern const lr_timestamp_param default_timestamp_param;

int   lrfnc_save_timestamp (const char * param_name, const lr_timestamp_param* time_param);

int lr_save_searched_string(char *buffer, long buf_size, unsigned int occurrence,
			    char *search_string, int offset, unsigned int param_val_len, 
			    char *param_name);

 
char *   lr_string (char * str);

 
# 905 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"

int   lr_save_value (char * param_val,
                                unsigned long const param_val_len,
                                unsigned long const options,
                                char * param_name,
                                char * file_name,
                                long const line_num);
# 912 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"


 
 
 
 
 











int   lr_printf (char * fmt, ...);
 
int   lr_set_debug_message (unsigned int msg_class,
                                       unsigned int swtch);
# 934 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
unsigned int   lr_get_debug_message (void);


 
 
 
 
 

void   lr_double_think_time ( double secs);
void   lr_usleep (long);


 
 
 
 
 
 




int *   lr_localtime (long offset);


int   lr_send_port (long port);


# 1010 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"



struct _lr_declare_identifier{
	char signature[24];
	char value[128];
};

int   lr_pt_abort (void);

void vuser_declaration (void);






# 1039 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"


# 1051 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
















 
 
 
 
 







int    _lr_declare_transaction   (char * transaction_name);


 
 
 
 
 







int   _lr_declare_rendezvous  (char * rendezvous_name);

 
 
 
 
 

 
int lr_enable_ip_spoofing();
int lr_disable_ip_spoofing();


 




int lr_convert_string_encoding(char *sourceString, char *fromEncoding, char *toEncoding, char *paramName);


 
int lr_db_connect (char * pFirstArg, ...);
int lr_db_disconnect (char * pFirstArg,	...);
int lr_db_executeSQLStatement (char * pFirstArg, ...);
int lr_db_dataset_action(char * pFirstArg, ...);
int lr_checkpoint(char * pFirstArg,	...);
int lr_db_getvalue(char * pFirstArg, ...);







 
 

















# 1 "c:\\it\\scripts\\tcp_6_ecom_us_view_audit_order_history\\\\combined_TCP_6_ECOM_US_View_Audit_Order_History.c" 2

# 1 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/SharedParameter.h" 1



 
 
 
 
# 100 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/SharedParameter.h"





typedef int PVCI2;
typedef int VTCERR2;

 
 
 

 
extern PVCI2    vtc_connect(char *servername, int portnum, int options);
extern VTCERR2  vtc_disconnect(int pvci);
extern VTCERR2  vtc_get_last_error(int pvci);

 
extern VTCERR2  vtc_query_column(int pvci, char *columnName, int columnIndex, char **outvalue);
extern VTCERR2  vtc_query_row(int pvci, int columnIndex, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_send_message(int pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_if_unique(int pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_row1(int pvci, char *columnNames, char *messages, char *delimiter,  unsigned char sendflag, unsigned short *outUpdates);
extern VTCERR2  vtc_update_message(int pvci, char *column, int index , char *message, unsigned short *outRc);
extern VTCERR2  vtc_update_message_ifequals(int pvci, char	*columnName, int index,	char *message, char	*ifmessage,	unsigned short 	*outRc);
extern VTCERR2  vtc_update_row1(int pvci, char *columnNames, int index , char *messages, char *delimiter, unsigned short *outUpdates);
extern VTCERR2  vtc_retrieve_message(int pvci, char *column, char **outvalue);
extern VTCERR2  vtc_retrieve_messages1(int pvci, char *columnNames, char *delimiter, char ***outvalues);
extern VTCERR2  vtc_retrieve_row(int pvci, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_increment(int pvci, char *column, int index , int incrValue, int *outValue);
extern VTCERR2  vtc_clear_message(int pvci, char *column, int index , unsigned short *outRc);
extern VTCERR2  vtc_clear_column(int pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_clear_row(int pvci, int rowIndex, unsigned short *outRc);

extern VTCERR2  vtc_create_column(int pvci, char *column,unsigned short *outRc);
extern VTCERR2  vtc_column_size(int pvci, char *column, int *size);
extern VTCERR2  vtc_ensure_index(int pvci, char *column, unsigned short *outRc);
extern VTCERR2  vtc_drop_index(int pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_noop(int pvci);

 
extern void vtc_free(char *msg);
extern void vtc_free_list(char **msglist);

 


 




 




















 




 
 
 

extern VTCERR2  lrvtc_connect(char *servername, int portnum, int options);
extern VTCERR2  lrvtc_disconnect();
extern VTCERR2  lrvtc_query_column(char *columnName, int columnIndex);
extern VTCERR2  lrvtc_query_row(int columnIndex);
extern VTCERR2  lrvtc_send_message(char *columnName, char *message);
extern VTCERR2  lrvtc_send_if_unique(char *columnName, char *message);
extern VTCERR2  lrvtc_send_row1(char *columnNames, char *messages, char *delimiter,  unsigned char sendflag);
extern VTCERR2  lrvtc_update_message(char *columnName, int index , char *message);
extern VTCERR2  lrvtc_update_message_ifequals(char *columnName, int index, char 	*message, char *ifmessage);
extern VTCERR2  lrvtc_update_row1(char *columnNames, int index , char *messages, char *delimiter);
extern VTCERR2  lrvtc_retrieve_message(char *columnName);
extern VTCERR2  lrvtc_retrieve_messages1(char *columnNames, char *delimiter);
extern VTCERR2  lrvtc_retrieve_row();
extern int     lrvtc_increment(char *columnName, int index , int incrValue);
extern VTCERR2  lrvtc_clear_message(char *columnName, int index);
extern VTCERR2  lrvtc_clear_column(char *columnName);
extern VTCERR2  lrvtc_clear_row(int rowIndex);
extern VTCERR2  lrvtc_create_column(char *columnName);
extern int     lrvtc_column_size(char *columnName);
extern VTCERR2  lrvtc_ensure_index(char *columnName);
extern VTCERR2  lrvtc_drop_index(char *columnName);

extern VTCERR2  lrvtc_noop();

 
 
 

                               


 
 
 





















# 2 "c:\\it\\scripts\\tcp_6_ecom_us_view_audit_order_history\\\\combined_TCP_6_ECOM_US_View_Audit_Order_History.c" 2

# 1 "globals.h" 1



 
 

# 1 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/web_api.h" 1







# 1 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h" 1























































 




 








 
 
 

  int
	web_add_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_add_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
	
  int
	web_add_auto_header(
		const char *		mpszHeader,
		const char *		mpszValue);

  int
	web_add_header(
		const char *		mpszHeader,
		const char *		mpszValue);
  int
	web_add_cookie(
		const char *		mpszCookie);
  int
	web_cleanup_auto_headers(void);
  int
	web_cleanup_cookies(void);
  int
	web_concurrent_end(
		const char * const	mpszReserved,
										 
		...								 
	);
  int
	web_concurrent_start(
		const char * const	mpszConcurrentGroupName,
										 
										 
		...								 
										 
	);
  int
	web_create_html_param(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim);
  int
	web_create_html_param_ex(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim,
		const char *		mpszNum);
  int
	web_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_disable_keep_alive(void);
  int
	web_enable_keep_alive(void);
  int
	web_find(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_get_int_property(
		const int			miHttpInfoType);
  int
	web_image(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_image_check(
		const char *		mpszName,
		...);
  int
	web_java_check(
		const char *		mpszName,
		...);
  int
	web_link(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

	
  int
	web_global_verification(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
  int
	web_reg_find(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
				
  int
	web_reg_save_param(
		const char *		mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 

  int
	web_convert_param(
		const char * 		mpszParamName, 
										 
		...);							 
										 
										 


										 

										 
  int
	web_remove_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
				
  int
	web_remove_auto_header(
		const char *		mpszHeaderName,
		...);							 
										 



  int
	web_remove_cookie(
		const char *		mpszCookie);

  int
	web_save_header(
		const char *		mpszType,	 
		const char *		mpszName);	 
  int
	web_set_certificate(
		const char *		mpszIndex);
  int
	web_set_certificate_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_set_connections_limit(
		const char *		mpszLimit);
  int
	web_set_max_html_param_len(
		const char *		mpszLen);
  int
	web_set_max_retries(
		const char *		mpszMaxRetries);
  int
	web_set_proxy(
		const char *		mpszProxyHost);
  int
	web_set_pac(
		const char *		mpszPacUrl);
  int
	web_set_proxy_bypass(
		const char *		mpszBypass);
  int
	web_set_secure_proxy(
		const char *		mpszProxyHost);
  int
	web_set_sockets_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue
	);
  int
	web_set_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue,
		...								 
	);
  int
	web_set_timeout(
		const char *		mpszWhat,
		const char *		mpszTimeout);
  int
	web_set_user(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);

  int
	web_sjis_to_euc_param(
		const char *		mpszParamName,
										 
		const char *		mpszParamValSjis);
										 

  int
	web_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_submit_form(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	spdy_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_set_proxy_bypass_local(
		const char * mpszNoLocal
		);

  int 
	web_cache_cleanup(void);

  int
	web_create_html_query(
		const char* mpszStartQuery,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_create_radio_button_param(
		const char *NameFiled,
		const char *NameAndVal,
		const char *ParamName
		);

  int
	web_convert_from_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										
  int
	web_convert_to_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_ex(
		const char * mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_xpath(
		const char * mpszParamName,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_regexp(
		 const char * mpszParamName,
		 ...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_run(
		const char * mpszCode,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_reset(void);










# 716 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h"


# 729 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h"



























# 767 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h"

 
 
 


  int
	FormSubmit(
		const char *		mpszFormName,
		...);
  int
	InitWebVuser(void);
  int
	SetUser(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);
  int
	TerminateWebVuser(void);
  int
	URL(
		const char *		mpszUrlName);
























# 835 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h"



 
 
 






# 9 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/web_api.h" 2

















 







 














  int
	web_reg_add_cookie(
		const char *		mpszCookie,
		...);							 
										 

  int
	web_report_data_point(
		const char *		mpszEventType,
		const char *		mpszEventName,
		const char *		mpszDataPointName,
		const char *		mpszLAST);	 
										 
										 
										 

  int
	web_text_link(
		const char *		mpszStepName,
		...);

  int
	web_element(
		const char *		mpszStepName,
		...);

  int
	web_image_link(
		const char *		mpszStepName,
		...);

  int
	web_static_image(
		const char *		mpszStepName,
		...);

  int
	web_image_submit(
		const char *		mpszStepName,
		...);

  int
	web_button(
		const char *		mpszStepName,
		...);

  int
	web_edit_field(
		const char *		mpszStepName,
		...);

  int
	web_radio_group(
		const char *		mpszStepName,
		...);

  int
	web_check_box(
		const char *		mpszStepName,
		...);

  int
	web_list(
		const char *		mpszStepName,
		...);

  int
	web_text_area(
		const char *		mpszStepName,
		...);

  int
	web_map_area(
		const char *		mpszStepName,
		...);

  int
	web_eval_java_script(
		const char *		mpszStepName,
		...);

  int
	web_reg_dialog(
		const char *		mpszArg1,
		...);

  int
	web_reg_cross_step_download(
		const char *		mpszArg1,
		...);

  int
	web_browser(
		const char *		mpszStepName,
		...);

  int
	web_control(
		const char *		mpszStepName,
		...);

  int
	web_set_rts_key(
		const char *		mpszArg1,
		...);

  int
	web_save_param_length(
		const char * 		mpszParamName,
		...);

  int
	web_save_timestamp_param(
		const char * 		mpszParamName,
		...);

  int
	web_load_cache(
		const char *		mpszStepName,
		...);							 
										 

  int
	web_dump_cache(
		const char *		mpszStepName,
		...);							 
										 
										 

  int
	web_reg_find_in_log(
		const char *		mpszArg1,
		...);							 
										 
										 

  int
	web_get_sockets_info(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 

  int
	web_add_cookie_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

  int
	web_hook_java_script(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

 
 
 
 
 
 
 
 
 
 
 
 
  int
	web_reg_async_attributes(
		const char *		mpszArg,
		...
	);

 
 
 
 
 
 
  int
	web_sync(
		 const char *		mpszArg1,
		 ...
	);

 
 
 
 
  int
	web_stop_async(
		const char *		mpszArg1,
		...
	);

 
 
 
 
 

 
 
 

typedef enum WEB_ASYNC_CB_RC_ENUM_T
{
	WEB_ASYNC_CB_RC_OK,				 

	WEB_ASYNC_CB_RC_ABORT_ASYNC_NOT_ERROR,
	WEB_ASYNC_CB_RC_ABORT_ASYNC_ERROR,
										 
										 
										 
										 
	WEB_ASYNC_CB_RC_ENUM_COUNT
} WEB_ASYNC_CB_RC_ENUM;

 
 
 

typedef enum WEB_CONVERS_CB_CALL_REASON_ENUM_T
{
	WEB_CONVERS_CB_CALL_REASON_BUFFER_RECEIVED,
	WEB_CONVERS_CB_CALL_REASON_END_OF_TASK,

	WEB_CONVERS_CB_CALL_REASON_ENUM_COUNT
} WEB_CONVERS_CB_CALL_REASON_ENUM;

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

typedef
int														 
	(*RequestCB_t)();

typedef
int														 
	(*ResponseBodyBufferCB_t)(
		  const char *		aLastBufferStr,
		  int				aLastBufferLen,
		  const char *		aAccumulatedStr,
		  int				aAccumulatedLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseCB_t)(
		  const char *		aResponseHeadersStr,
		  int				aResponseHeadersLen,
		  const char *		aResponseBodyStr,
		  int				aResponseBodyLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseHeadersCB_t)(
		  int				aHttpStatusCode,
		  const char *		aAccumulatedHeadersStr,
		  int				aAccumulatedHeadersLen);



 
 
 

typedef enum WEB_CONVERS_UTIL_RC_ENUM_T
{
	WEB_CONVERS_UTIL_RC_OK,
	WEB_CONVERS_UTIL_RC_CONVERS_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_TASK_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_UNAVIALABLE,
	WEB_CONVERS_UTIL_RC_INVALID_ARGUMENT,

	WEB_CONVERS_UTIL_RC_ENUM_COUNT
} WEB_CONVERS_UTIL_RC_ENUM;

 
 
 

  int					 
	web_util_set_request_url(
		  const char *		aUrlStr);

  int					 
	web_util_set_request_body(
		  const char *		aRequestBodyStr);

  int					 
	web_util_set_formatted_request_body(
		  const char *		aRequestBodyStr);


 
 
 
 
 

 
 
 
 
 

 
 
 
 
 
 
 
 

 
 
  int
web_websocket_connect(
		 const char *	mpszArg1,
		 ...
		 );


 
 
 
 
 																						
  int
web_websocket_send(
	   const char *		mpszArg1,
		...
	   );

 
 
 
 
 
 
  int
web_websocket_close(
		const char *	mpszArg1,
		...
		);

 
typedef
void														
(*OnOpen_t)(
			  const char* connectionID,  
			  const char * responseHeader,  
			  int length  
);

typedef
void														
(*OnMessage_t)(
	  const char* connectionID,  
	  int isbinary,  
	  const char * data,  
	  int length  
	);

typedef
void														
(*OnError_t)(
	  const char* connectionID,  
	  const char * message,  
	  int length  
	);

typedef
void														
(*OnClose_t)(
	  const char* connectionID,  
	  int isClosedByClient,  
	  int code,  
	  const char* reason,  
	  int length  
	 );
 
 
 
 
 





# 7 "globals.h" 2

# 1 "lrw_custom_body.h" 1
 




# 8 "globals.h" 2

# 1 "AsyncCallbacks.c" 1
 
 
 
int Poll_0_RequestCB()
{
	 

	 
	 
	 

	 

	web_save_timestamp_param("TimeStamp_Poll_0_0","LAST");

	 
	 
	web_util_set_request_url("{pUrl}/sessiontracking/ping.jsp?_dc={TimeStamp_Poll_0_0}");

	 
	 

	return WEB_ASYNC_CB_RC_OK;
}

int Poll_0_ResponseCB(
	const char *	aResponseHeadersStr,
	int				aResponseHeadersLen,
	const char *	aResponseBodyStr,
	int				aResponseBodyLen,
	int				aHttpStatusCode)
{
	 

	 
	 

	return WEB_ASYNC_CB_RC_OK;
}

# 9 "globals.h" 2


 
 


# 3 "c:\\it\\scripts\\tcp_6_ecom_us_view_audit_order_history\\\\combined_TCP_6_ECOM_US_View_Audit_Order_History.c" 2

# 1 "vuser_init.c" 1
 







vuser_init()
{

	web_cache_cleanup();
	
	web_cleanup_cookies();
	
	web_set_max_html_param_len("9999");
	
	 
	
	web_set_sockets_option("IGNORE_PREMATURE_SHUTDOWN", "1");
	
	web_set_sockets_option("CLOSE_KEEPALIVE_CONNECTIONS", "1");
	
	web_reg_save_param("c_SAML_Request",
	                   "LB=name=\"SAMLRequest\" value=\"",
	                   "RB=\"/>",
	                   "ORD=1",
	                   "Search=ALL","LAST");
	
		
	lr_save_string("TCP_06_ECOM_US_View_Audit_Order_History","sTestCaseName");
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_01_Launch"));
	
	 

	
	web_url("index.html", 
		"URL={pURL}/manh/index.html", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTML",
		"LAST");

	 

	web_submit_data("SSO", 
		"Action={pURL_SSO}/profile/SAML2/POST/SSO", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t2.inf", 
		"Mode=HTML", 
		"ITEMDATA", 
		"Name=SAMLRequest", "Value={c_SAML_Request}", "ENDITEM", 
		"LAST");

	 
	
	lr_end_transaction(lr_eval_string("{sTestCaseName}_01_Launch"),2);
	
	lr_think_time(1);
	
	 

	lr_start_transaction(lr_eval_string("{sTestCaseName}_02_Login"));
	
	web_custom_request("j_spring_security_check",
		"URL={pURL_SSO}/j_spring_security_check", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"Mode=HTML", 
		"Body=j_username={pUserName}&j_password={pPassword}", 
		"LAST");

	web_url("ping.jsp", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466760698128", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t4.inf", 
		"Mode=HTML", 
		"LAST");

	 

	web_url("ping.jsp_2", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466760765357", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t5.inf", 
		"Mode=HTML", 
		"LAST");

	lr_end_transaction(lr_eval_string("{sTestCaseName}_02_Login"),2);

	lr_think_time(1);
	
	return 0;
}
# 4 "c:\\it\\scripts\\tcp_6_ecom_us_view_audit_order_history\\\\combined_TCP_6_ECOM_US_View_Audit_Order_History.c" 2

# 1 "Action.c" 1
Action()
{
	char *r;
	web_url("ping.jsp_3", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466760780724", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t6.inf", 
		"Mode=HTML", 
		"LAST");

	 
	
	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_03_Open_Sales_Order_Screen"));
	
	web_custom_request("windows", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14271268&regionId=-1&businessUnitId=-1", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t7.inf", 
		"Mode=HTML", 
		"EncType=", 
		"LAST");
	
	web_reg_save_param("c_MANHCSRFToken",
	                   "LB=\"MANH-CSRFToken\" type=\"hidden\" name=\"MANH-CSRFToken\" value=\"",
	                   "RB=\"/>",
	                   "ORD=1",
	                   "Search=ALL","LAST");
	
	web_reg_save_param("c_dataTable",
                       "LB=dataTable$:$",
                       "RB=\" />",
                       "ORD=1",
                       "LAST");

	web_reg_save_param("c_ViewState01",
                       "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
                       "RB=\" autocomplete=\"",
                       "ORD=1",
                       "LAST");

	web_url("StoreOrderList.jsflps", 
		"URL={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271268", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t8.inf", 
		"Mode=HTML", 
		"LAST");

 
# 83 "Action.c"
 







	web_url("ping.jsp_4", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466760796081", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t9.inf", 
		"Mode=HTML", 
		"LAST");

	lr_end_transaction(lr_eval_string("{sTestCaseName}_03_Open_Sales_Order_Screen"),2);

	lr_think_time(1);

 


	 
# 119 "Action.c"

	 
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_04_Search_Sales_Order"));

 


	 
# 138 "Action.c"

	web_reg_save_param("c_ViewState02",
                       "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
                       "RB=\" autocomplete=\"",
                       "ORD=1",
                       "LAST");
	                   
	web_reg_save_param("c_orderIDPkHiddenParam",
                   "LB=input type=\"hidden\" value=\"",
                   "RB=\" id=\"dataForm:SOList_entityListView:salesOrderData",
                   "ORD=1",
                   "Search=ALL","LAST");
	
	web_reg_save_param("c_ExternalCustomerId",
                   "LB=<span id=\"dataForm:SOList_entityListView:salesOrderData:0:customerCode1\">",
                   "RB=</span>",
                   "ORD=1",
                   "Search=ALL","LAST");
	
	web_reg_save_param("c_inp_hdn_orderCreatedDTTM",
                   "LB=<div id='dataForm:SOList_entityListView:salesOrderData_body_tr0_td12_view' class='dshow'>&#160;",
                   "RB=</div>",
                   "ORD=1",
                   "Search=ALL","LAST");
	
	web_custom_request("StoreOrderList.jsflps_2", 
		"URL={pURL}/olm/salesorder/StoreOrderList.jsflps", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271268", 
		"Snapshot=t12.inf", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded; charset=UTF-8", 
		"Body=AJAXREQUEST=_viewRoot&dataForm=dataForm&uniqueToken=1&MANH-CSRFToken={c_MANHCSRFToken}&helpurlEle=%2Flcom%2Fcommon%2Fjsp%2Fhelphelper.jsp%3Fserver%3D58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B%26uri%3D%252Folm%252Fsalesorder%252FStoreOrderList.jsflps&dataForm%3AFacility_popup_Facility_Contact_dataTable_deleteHidden=&dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedRows=&"
		"dataForm%3AFacility_popup_Facility_Contact_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AsortDir=desc&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AcolCount=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableClicked=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableResized=false&Facility_popup_Facility_Contact_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs_pageallrowskey=&"
		"dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedIdList=&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs_allselectedrowskey=Facility_popup_Facility_Contact_dataTable%24%3A%24{c_dataTable}&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_deleteHidden=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedRows=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AsortDir=desc&"
		"dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AcolCount=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AtableClicked=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AtableResized=false&Facility_popup_Credit_Limit_List_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_pageallrowskey=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedIdList=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_allselectedrowskey="
		"Facility_popup_Credit_Limit_List_dataTable%24%3A%24{c_dataTable}&dataForm%3AfltrListFltrId%3AfieldName=&dataForm%3AfltrListFltrId%3AfilterName=FL_{pUserName}&dataForm%3AfltrListFltrId%3Aowner={pUserName}&dataForm%3AfltrListFltrId%3AobjectType=FL_FILTER&dataForm%3AfltrListFltrId%3AfilterObjectType=&dataForm%3AfltrListFltrId%3Afield0value1=&dataForm%3AfltrListFltrId%3Afield0=FILTER.FILTER_NAME&dataForm%3AfltrListFltrId%3Afield0operator=&dataForm%3AfltrListFltrId%3Afield1value1=&"
		"dataForm%3AfltrListFltrId%3Afield1=FILTER.IS_DEFAULT&dataForm%3AfltrListFltrId%3Afield1operator=&dataForm%3AfltrListFltrId%3Afield2value1=&dataForm%3AfltrListFltrId%3Afield2=FILTER.IS_PRIVATE&dataForm%3AfltrListFltrId%3Afield2operator=&dataForm%3AfltrListFltrId%3Afield3value1=&dataForm%3AfltrListFltrId%3Afield3=FILTER.OWNER&dataForm%3AfltrListFltrId%3Afield3operator=&dataForm%3AfltrListFltrId%3Afield4value1=&dataForm%3AfltrListFltrId%3Afield4=FILTER.IS_DELETED&"
		"dataForm%3AfltrListFltrId%3Afield4operator=&dataForm%3AfltrListFltrId%3AfltrCondition=&dataForm%3AfltrListFltrId%3AfltrCrtSel=&windowId=screen-14271268&windowId=screen-14271268&targetLink=&channelTypeHiddenParam=&fromViewPageParam=false&tabToBeSelected=&IsOrderConfirmedHiddenParam=&CustomerOrderTypeHiddenParam=&IsOrderCANCELEDHiddenParam=&orderNumberHiddenParam=&orderIDPkHiddenParam=&addOrderLineBtnClicked=false&fromOrder=true&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AradioSelect=quick"
		"&dataForm%3ASOList_entityListView%3ASOList_filterId2%3A_fltrExpColTxt=DONE&dataForm%3ASOList_entityListView%3ASOList_filterId2%3A_filtrExpColState=collapsed&dataForm%3ASOList_entityListView%3ASOList_filterId2%3A_filtrExpIconSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2Farrow_expand.gif&dataForm%3ASOList_entityListView%3ASOList_filterId2%3A_filtrColIconSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2Farrow_collapse.gif&dataForm%3ASOList_entityListView%3ASOList_filterId2%3A_filtrdropDownSrc="
		"%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2FarrowDown.gif&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield0=Sales%20Channel&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield0operator=%3D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubObject0=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield0value1=%5B%5D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield3=Order%20no.&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield3operator=%3D&"
		"dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubObject3=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield3value1ecId=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield3value1={pOrderNumber}&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield4=Customer&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield4operator=%3D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubObject4=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield4value1ecId=&"
		"dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield4value1=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield5=Order%20Type&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield5operator=%3D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubObject5=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield5value1=%5B%5D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield7=Status&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield7operator=%3D&"
		"dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubObject7=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield7value1=%5B%5D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield11=Destination%20Facility&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield11operator=%3D&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AsubObject11=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield11value1ecId=&"
		"dataForm%3ASOList_entityListView%3ASOList_filterId2%3Afield11value1=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AcurrentAppliedFilterId=-1&dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_quickFilterGroupButton_mainButtonCategory=-1&dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_quickFilterGroupButton_mainButtonIndex=-1&dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_quickFilterGroupButton_changeDefault=false&"
		"dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_savedFilterGroupButton_mainButtonCategory=-1&dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_savedFilterGroupButton_mainButtonIndex=-1&dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2_savedFilterGroupButton_changeDefault=false&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AdummyToGetPrefix=&dataForm%3ASOList_entityListView%3ASOList_filterId2%3Aowner={pUserName}&customParams%20="
		"windowId%3Dscreen-14271268&queryPersistParameter=%26windowId%3Dscreen-14271268&dataForm%3ASOList_entityListView%3ASOList_filterId2%3AobjectType=DOM%20ORDER&isJSF=true&filterScreenType=ON_SCREEN&dataForm%3ASOList_entityListView%3AsalesOrderData%3Apager%3ApageInput=&dataForm%3ASOList_entityListView%3AsalesOrderData%3ApagerBoxValue=&dataForm%3ASOList_entityListView%3AsalesOrderData%3AisPaginationEvent=&dataForm%3ASOList_entityListView%3AsalesOrderData%3ApagerAction=&"
		"dataForm%3ASOList_entityListView%3AsalesOrderData_deleteHidden=&dataForm%3ASOList_entityListView%3AsalesOrderData_selectedRows=&dataForm%3ASOList_entityListView%3AsalesOrderData%3AisSortButtonClick=po.lastUpdatedDttm&dataForm%3ASOList_entityListView%3AsalesOrderData%3AsortDir=desc&dataForm%3ASOList_entityListView%3AsalesOrderData%3AcolCount=&dataForm%3ASOList_entityListView%3AsalesOrderData%3AtableClicked=&dataForm%3ASOList_entityListView%3AsalesOrderData%3AtableResized=false&"
		"dataForm%3ASOList_entityListView%3AsalesOrderData%3A0%3APK_0=DUMMYROW&orderNbrHidden=&orderIDPkHidden=&IsOrderConfirmedHidden=&CustomerOrderTypeHidden=&IsOrderCANCELEDHidden=&isEditable=&isEditWarning=&isAddOL=&isConfirm=&isSource=&isSourceWarning=&isAllocate=&isDOCreate=&isDORelease=&isCancel=&isReserve=&isSourceByRate=&isDeAllocate=&isHold=&isUnHold=&isMassUpdate=&isSendUpdate=&markForDeletion=&soFulfillStatus=&dataForm%3ASOList_entityListView%3AsalesOrderData%3A0%3AchannelTypeCode=&"
		"salesOrderData_hdnMaxIndexHldr=0&dataForm%3ASOList_entityListView%3AsalesOrderData_trs_pageallrowskey=&dataForm%3ASOList_entityListView%3AsalesOrderData_selectedIdList=&dataForm%3ASOList_entityListView%3AsalesOrderData_trs_allselectedrowskey=salesOrderData%24%3A%24{c_dataTable}&moreActionTargetLinksoheaderbuttons=&moreActionButtonPressedsoheaderbuttons=&backingBeanName=&javax.faces.ViewState={c_ViewState01}&fltrApplyFromQF=true&"
		"dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2apply=dataForm%3ASOList_entityListView%3ASOList_filterId2%3ASOList_filterId2apply&reRenderParent=SOList_rerenderPanel&fltrClientId=dataForm%3ASOList_entityListView%3ASOList_filterId2&", 
		"LAST");

 


	 
# 210 "Action.c"

	lr_end_transaction(lr_eval_string("{sTestCaseName}_04_Search_Sales_Order"),2);

	lr_think_time(1);

 


	 
# 229 "Action.c"

	 
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_05_Open_Sales_Order_Details"));

	web_reg_save_param("c_ViewState03",
                       "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
                       "RB=\" autocomplete=\"",
                       "ORD=1",
                       "LAST");
	
	web_reg_save_param("c_PreferredShippingAddrMenu",
                       "LB=dataForm:CMRegPreferredShippingAddrMenu\" name=\"dataForm:CMRegPreferredShippingAddrMenu\" size=\"1\">	<option value=\"",
                       "RB=\">",
                       "ORD=1",
                       "LAST");
	
	web_reg_save_param("c_PreferredBillingAddrMenu",
                       "LB=dataForm:CMRegPreferredBillingAddrMenu\" name=\"dataForm:CMRegPreferredBillingAddrMenu\" size=\"1\">	<option value=\"",
                       "RB=\">",
                       "ORD=1",
                       "LAST");
	
	web_reg_save_param("c_PreferredPaymentInfoMenu",
                       "LB=dataForm:CMRegPreferredPaymentInfoMenu\" name=\"dataForm:CMRegPreferredPaymentInfoMenu\" size=\"1\">	<option value=\"",
                       "RB=\">",
                       "ORD=1",
                       "LAST");
	
	web_reg_save_param("c_inp_hdn_orderVersion",
                       "LB=<input id=\"dataForm:post_COEdit_inp_hdn_orderVersion\" type=\"hidden\" name=\"dataForm:post_COEdit_inp_hdn_orderVersion\" value=\"",
                       "RB=\" />",
                       "ORD=1",
                       "LAST");

	web_reg_save_param("c_inp_hdn_for_order_status",
                       "LB=<input type=\"hidden\" id=\"inp_hdn_for_order_status\" name=\"inp_hdn_for_order_status\" value=\"",
                       "RB=\" />",
                       "ORD=1",
                       "LAST");
	
	web_reg_save_param("c_hdn_for_discount_code",
                       "LB=<input type=\"hidden\" id=\"hdn_for_discount_code\" name=\"hdn_for_discount_code\" value=\"",
                       "RB=\" />",
                       "ORD=1",
                       "LAST");
	
	web_reg_save_param("c_createCO_fn",
                       "LB=<span id=\"dataForm:op_txt_postorder_view_createCO_fn\" class=\"captionData\">",
                       "RB=</span>",
                       "ORD=1",
                       "LAST");	
	
	web_reg_save_param("c_createCO_pno",
                       "LB=<span id=\"dataForm:op_txt_postorder_view_createCO_pno\" class=\"captionData\">",
                       "RB=</span>",
                       "ORD=1",
                       "LAST");
	
	web_reg_save_param("c_inp_hdn_orderStatus",
                       "LB=<span id=\"dataForm:op_txt_postorder_view_createCO_status\" class=\"captionData\">",
                       "RB=</span>",
                       "ORD=1",
                       "LAST");
	
	web_reg_save_param("c_hid_txt_postorder_view_createCO_ln",
                       "LB=<input id=\"dataForm:hid_txt_postorder_view_createCO_ln\" type=\"hidden\" name=\"dataForm:hid_txt_postorder_view_createCO_ln\" value=\"",
                       "RB=\" />",
                       "ORD=1",
                       "LAST");
	
	web_reg_save_param("c_hid_txt_postorder_view_createCO_email",
                       "LB=<span id=\"dataForm:op_txt_postorder_view_createCO_email\" class=\"captionData\">",
                       "RB=</span>",
                       "ORD=1",
                       "LAST");
	

	
	 




	
	web_reg_save_param("c_updateAdditionalDetailsActive",
                       "LB=<input type=\"hidden\" id=\"additional_details_tsk\" name=\"updateAdditionalDetailsActive\" value=\"",
                       "RB=\" />",
                       "ORD=1",
                       "LAST");
	
	web_reg_save_param("c_coROILTable_hdnMaxIndexHldr",
                       "LB=<input type=\"hidden\" id=\"coROILTable_hdnMaxIndexHldr\" name=\"coROILTable_hdnMaxIndexHldr\" value=\"",
                       "RB=\" />",
                       "ORD=1",
                       "LAST");
	
	web_reg_save_param("c_remainingToPay",
                       "LB=<input type=\"hidden\" name=\"remainingToPay\" value=\"",
                       "RB=\" />",
                       "ORD=1",
                       "LAST");
	
	web_reg_save_param("c_coNotesTable",
                       "LB=<input type=\"hidden\" id=\"dataForm:coNotesTable_trs_allselectedrowskey\" name=\"dataForm:coNotesTable_trs_allselectedrowskey\" value=\"coNotesTable$:$",
                       "RB=\" />",
                       "ORD=1",
                       "LAST");
	
	web_submit_data("StoreOrderList.jsflps_3",
		"Action={pURL}/olm/salesorder/StoreOrderList.jsflps", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer={pURL}/olm/salesorder/StoreOrderList.jsflps?windowId=screen-14271268", 
		"Snapshot=t15.inf", 
		"Mode=HTML", 
		"ITEMDATA", 
		"Name=dataForm", "Value=dataForm", "ENDITEM", 
		"Name=uniqueToken", "Value=1", "ENDITEM", 
		"Name=MANH-CSRFToken", "Value={c_MANHCSRFToken}", "ENDITEM", 
		"Name=helpurlEle", "Value=/lcom/common/jsp/helphelper.jsp?server=58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B&uri=%2Folm%2Fsalesorder%2FStoreOrderList.jsflps", "ENDITEM", 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_deleteHidden", "Value=", "ENDITEM", 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_selectedRows", "Value=", "ENDITEM", 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:isSortButtonClick", "Value=", "ENDITEM", 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:sortDir", "Value=desc", "ENDITEM", 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:colCount", "Value=", "ENDITEM", 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:tableClicked", "Value=", "ENDITEM", 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:tableResized", "Value=false", "ENDITEM", 
		"Name=Facility_popup_Facility_Contact_dataTable_hdnMaxIndexHldr", "Value=0", "ENDITEM", 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_trs_pageallrowskey", "Value=", "ENDITEM", 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_selectedRows", "Value=", "ENDITEM", 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_selectedIdList", "Value=", "ENDITEM", 
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_trs_allselectedrowskey", "Value=Facility_popup_Facility_Contact_dataTable$:${c_dataTable}", "ENDITEM", 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_deleteHidden", "Value=", "ENDITEM", 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_selectedRows", "Value=", "ENDITEM", 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:isSortButtonClick", "Value=", "ENDITEM", 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:sortDir", "Value=desc", "ENDITEM", 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:colCount", "Value=", "ENDITEM", 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:tableClicked", "Value=", "ENDITEM", 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:tableResized", "Value=false", "ENDITEM", 
		"Name=Facility_popup_Credit_Limit_List_dataTable_hdnMaxIndexHldr", "Value=0", "ENDITEM", 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_trs_pageallrowskey", "Value=", "ENDITEM", 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_selectedRows", "Value=", "ENDITEM", 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_selectedIdList", "Value=", "ENDITEM", 
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_trs_allselectedrowskey", "Value=Facility_popup_Credit_Limit_List_dataTable$:${c_dataTable}", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:fieldName", "Value=", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:filterName", "Value=FL_{pUserName}", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:owner", "Value={pUserName}", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:objectType", "Value=FL_FILTER", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:filterObjectType", "Value=", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:field0value1", "Value=", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:field0", "Value=FILTER.FILTER_NAME", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:field0operator", "Value=", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:field1value1", "Value=", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:field1", "Value=FILTER.IS_DEFAULT", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:field1operator", "Value=", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:field2value1", "Value=", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:field2", "Value=FILTER.IS_PRIVATE", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:field2operator", "Value=", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:field3value1", "Value=", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:field3", "Value=FILTER.OWNER", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:field3operator", "Value=", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:field4value1", "Value=", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:field4", "Value=FILTER.IS_DELETED", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:field4operator", "Value=", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:fltrCondition", "Value=", "ENDITEM", 
		"Name=dataForm:fltrListFltrId:fltrCrtSel", "Value=", "ENDITEM", 
		"Name=windowId", "Value=screen-14271268", "ENDITEM", 
		"Name=targetLink", "Value=", "ENDITEM", 
		"Name=channelTypeHiddenParam", "Value=", "ENDITEM", 
		"Name=fromViewPageParam", "Value=false", "ENDITEM", 
		"Name=tabToBeSelected", "Value=", "ENDITEM", 
		"Name=IsOrderConfirmedHiddenParam", "Value=", "ENDITEM", 
		"Name=CustomerOrderTypeHiddenParam", "Value=", "ENDITEM", 
		"Name=IsOrderCANCELEDHiddenParam", "Value=", "ENDITEM", 
		"Name=orderNumberHiddenParam", "Value=", "ENDITEM", 
		"Name=orderIDPkHiddenParam", "Value=", "ENDITEM", 
		"Name=addOrderLineBtnClicked", "Value=false", "ENDITEM", 
		"Name=fromOrder", "Value=true", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:radioSelect", "Value=quick", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:_fltrExpColTxt", "Value=DONE", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:_filtrExpColState", "Value=collapsed", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:_filtrExpIconSrc", "Value=/lps/resources/themes/icons/mablue/arrow_expand.gif", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:_filtrColIconSrc", "Value=/lps/resources/themes/icons/mablue/arrow_collapse.gif", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:_filtrdropDownSrc", "Value=/lps/resources/themes/icons/mablue/arrowDown.gif", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field0", "Value=Sales Channel", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field0operator", "Value==", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject0", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field0value1", "Value=[]", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field3", "Value=Order no.", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field3operator", "Value==", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject3", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field3value1ecId", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field3value1", "Value={pOrderNumber}", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field4", "Value=Customer", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field4operator", "Value==", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject4", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field4value1ecId", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field4value1", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field5", "Value=Order Type", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field5operator", "Value==", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject5", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field5value1", "Value=[]", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field7", "Value=Status", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field7operator", "Value==", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject7", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field7value1", "Value=[]", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field11", "Value=Destination Facility", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field11operator", "Value==", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:subObject11", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field11value1ecId", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:field11value1", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:currentAppliedFilterId", "Value=-1", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_quickFilterGroupButton_mainButtonCategory", "Value=-1", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_quickFilterGroupButton_mainButtonIndex", "Value=-1", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_quickFilterGroupButton_changeDefault", "Value=false", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_savedFilterGroupButton_mainButtonCategory", "Value=-1", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_savedFilterGroupButton_mainButtonIndex", "Value=-1", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:SOList_filterId2_savedFilterGroupButton_changeDefault", "Value=false", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:dummyToGetPrefix", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:filterId", "Value=2147483647", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:owner", "Value={pUserName}", "ENDITEM", 
		"Name=customParams ", "Value=&&&", "ENDITEM", 
		"Name=queryPersistParameter", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:SOList_filterId2:objectType", "Value=DOM ORDER", "ENDITEM", 
		"Name=isJSF", "Value=true", "ENDITEM", 
		"Name=filterScreenType", "Value=ON_SCREEN", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:salesOrderData:pagerBoxValue", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:salesOrderData:isPaginationEvent", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:salesOrderData:pagerAction", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:salesOrderData_deleteHidden", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:salesOrderData_selectedRows", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:salesOrderData:isSortButtonClick", "Value=po.lastUpdatedDttm", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:salesOrderData:sortDir", "Value=desc", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:salesOrderData:colCount", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:salesOrderData:tableClicked", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:salesOrderData:tableResized", "Value=false", "ENDITEM", 
		"Name=salesOrderData_hdnMaxIndexHldr", "Value=1", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:salesOrderData:0:PK_0", "Value={c_orderIDPkHiddenParam}", "ENDITEM", 
		"Name=orderNbrHidden", "Value={pOrderNumber}", "ENDITEM", 
		"Name=orderIDPkHidden", "Value={c_orderIDPkHiddenParam}", "ENDITEM", 
		"Name=IsOrderConfirmedHidden", "Value=true", "ENDITEM", 
		"Name=CustomerOrderTypeHidden", "Value=4", "ENDITEM", 
		"Name=IsOrderCANCELEDHidden", "Value=true", "ENDITEM", 
		"Name=isEditable", "Value=false", "ENDITEM", 
		"Name=isEditWarning", "Value=false", "ENDITEM", 
		"Name=isAddOL", "Value=false", "ENDITEM", 
		"Name=isConfirm", "Value=false", "ENDITEM", 
		"Name=isSource", "Value=false", "ENDITEM", 
		"Name=isSourceWarning", "Value=false", "ENDITEM", 
		"Name=isAllocate", "Value=false", "ENDITEM", 
		"Name=isDOCreate", "Value=false", "ENDITEM", 
		"Name=isDORelease", "Value=false", "ENDITEM", 
		"Name=isCancel", "Value=false", "ENDITEM", 
		"Name=isReserve", "Value=false", "ENDITEM", 
		"Name=isSourceByRate", "Value=false", "ENDITEM", 
		"Name=isDeAllocate", "Value=false", "ENDITEM", 
		"Name=isHold", "Value=false", "ENDITEM", 
		"Name=isUnHold", "Value=false", "ENDITEM", 
		"Name=isMassUpdate", "Value=false", "ENDITEM", 
		"Name=isSendUpdate", "Value=true", "ENDITEM", 
		"Name=markForDeletion", "Value=true", "ENDITEM", 
		"Name=soFulfillStatus", "Value=940", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:salesOrderData:0:channelTypeCode", "Value=20", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:salesOrderData_trs_pageallrowskey", "Value={c_orderIDPkHiddenParam}#:#", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:salesOrderData_selectedRows", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:salesOrderData_selectedIdList", "Value=", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:salesOrderData_trs_allselectedrowskey", "Value=salesOrderData$:${c_dataTable}", "ENDITEM", 
		"Name=moreActionTargetLinksoheaderbuttons", "Value=", "ENDITEM", 
		"Name=moreActionButtonPressedsoheaderbuttons", "Value=", "ENDITEM", 
		"Name=backingBeanName", "Value=", "ENDITEM", 
		"Name=javax.faces.ViewState", "Value={c_ViewState02}", "ENDITEM", 
		"Name=dataForm:SOList_entityListView:salesOrderData:0:defaultactionbutton", "Value=dataForm:SOList_entityListView:salesOrderData:0:defaultactionbutton", "ENDITEM", 
		"Name=orderIDPk", "Value={c_orderIDPkHiddenParam}", "ENDITEM", 
		"Name=orderNumber", "Value={pOrderNumber}", "ENDITEM", 
		"Name=channelType", "Value=20", "ENDITEM", 
		"Name=IsOrderConfirmed", "Value=true", "ENDITEM", 
		"Name=CustomerOrderType", "Value=4", "ENDITEM", 
		"Name=IsOrderCANCELED", "Value=true", "ENDITEM", 
		"LAST");

	r=lr_eval_string("{c_inp_hdn_orderStatus}");
	
	lr_output_message("value for r is %s",r);
	
	 







	
	
	if (strcmp(r,"Canceled")==0)
	{
		lr_save_string("true","c_inp_hdn_orderCancel");
	}
	else
	{
		lr_save_string("false","c_inp_hdn_orderCancel");
	}
	
 


	 
# 548 "Action.c"

	lr_end_transaction(lr_eval_string("{sTestCaseName}_05_Open_Sales_Order_Details"),2);

	lr_think_time(1);

 


	 
# 567 "Action.c"

 


	 
# 582 "Action.c"

	 
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_06_View_Audit_Details_of_Order"));
	
	web_reg_save_param("c_ViewState04",
                       "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
                       "RB=\" autocomplete=\"",
                       "ORD=1",
                       "LAST");
	
	web_custom_request("ViewCustomerOrder.jsflps", 
		"URL={pURL}/doms/dom/selling/co/customerorder/ViewCustomerOrder.jsflps", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/olm/salesorder/StoreOrderList.jsflps", 
		"Snapshot=t19.inf", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded; charset=UTF-8", 
		"Body=AJAXREQUEST=_viewRoot&dataForm=dataForm&uniqueToken=1&MANH-CSRFToken={c_MANHCSRFToken}&helpurlEle=%2Flcom%2Fcommon%2Fjsp%2Fhelphelper.jsp%3Fserver%3D58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B%26uri%3D%252Fdoms%252Fdom%252Fselling%252Fco%252Fcustomerorder%252FViewCustomerOrder.jsflps%253FpageName%253DviewOrderDetails&dataForm%3AsellingExceptionStkTrcPage_stackTrace_inpArea=&dataForm%3AcoNotesTable_deleteHidden=&"
		"dataForm%3AcoNotesTable_selectedRows=&dataForm%3AcoNotesTable%3AisSortButtonClick=&dataForm%3AcoNotesTable%3AsortDir=desc&dataForm%3AcoNotesTable%3AcolCount=&dataForm%3AcoNotesTable%3AtableClicked=&dataForm%3AcoNotesTable%3AtableResized=false&dataForm%3AcoNotesTable%3A0%3APK_0=DUMMYROW&dataForm%3AcoNotesTable%3A0%3AcoNoteTypeColValue=%20&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteSeq=&dataForm%3AcoNotesTable%3A0%3AcoNotesPopupNoteToBeDeletedFlag=&dataForm%3AcoNotesTable%3A0%3AmarkForDelete=&"
		"dataForm%3AcoNotesTable%3A0%3AcoNotesColValue=&coNotesTable_hdnMaxIndexHldr=0&dataForm%3AcoNotesTable_trs_pageallrowskey=&dataForm%3AcoNotesTable_selectedIdList=&dataForm%3AcoNotesTable_trs_allselectedrowskey=coNotesTable%24%3A%24{c_coNotesTable}&dataForm%3AcoViewNotesTable_deleteHidden=&dataForm%3AcoViewNotesTable_selectedRows=&dataForm%3AcoViewNotesTable%3AisSortButtonClick=&dataForm%3AcoViewNotesTable%3AsortDir=desc&dataForm%3AcoViewNotesTable%3AcolCount=&dataForm%3AcoViewNotesTable%3AtableClicked"
		"=&dataForm%3AcoViewNotesTable%3AtableResized=false&coViewNotesTable_hdnMaxIndexHldr=0&dataForm%3AcoViewNotesTable_trs_pageallrowskey=&dataForm%3AcoViewNotesTable_selectedIdList=&dataForm%3AcoViewNotesTable_trs_allselectedrowskey=coViewNotesTable%24%3A%24{c_coNotesTable}&dataForm%3Aco_hold_rc_popup_sel_one_menu=525&dataForm%3Aco_hold_rc_popup_inp_txt=&dataForm%3AcocancelListOfIdsToBeCancelledHdn=&dataForm%3Aco_cancel_rc_popup_sel_one_menu=664&dataForm%3Aco_cancel_rc_popup_inp_txt=&"
		"dataForm%3ACMRegPreferredShippingAddrMenu={c_PreferredShippingAddrMenu}&dataForm%3ACMRegPreferredBillingAddrMenu={c_PreferredBillingAddrMenu}&dataForm%3ACMRegPreferredShippingMethodMenu=(none)&dataForm%3ACMRegPreferredPaymentInfoMenu={c_PreferredPaymentInfoMenu}&dataForm%3AcustCommNoteText=&itemPriceOverrideItemIdHdn=&itemPriceOverrideItemDescriptionHdn=&itemPriceOverrideItemOriginalPriceHdn=&itemPriceOverrideItemCurrentPriceHdn=&itemPriceOverrideItemUndergonePriceOverrideHdn=false&itemPriceOverrideorderLineIdHdn=&itemPriceOverrideCurrencySymbolIdHdn=&"
		"itemPriceOverrideOrderLineNumberHdn=&itemPriceOverrideReqQtyHdn=&itemPriceOverrideReqUomHdn=&isEligibleForPriceOverridePrmHdn=&conversionFactorPrmHdn=&showOverridePriceTabInpHdn=true&showOverridePriceTabInpHdn=true&dataForm%3AdisplayingPriceHistoryTabInpHdn=false&ajaxTabClicked=&itemPriceOverridePopupEditable_tabPanel_SubmitOnTabClick=false&itemPriceOverridePopupEditable_tabPanel_selectedTab=itemPriceOverridePopupEditablePriceHistoryTab&dataForm%3Aj_id198=false&"
		"dataForm%3AitemPriceOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedRows=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AisSortButtonClick=orc.createdDttm&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableClicked=&"
		"dataForm%3AitemPriceOverridePopupPriceHistoryListTable%3AtableResized=false&itemPriceOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=0&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AitemPriceOverridePopupPriceHistoryListTable_trs_allselectedrowskey=itemPriceOverridePopupPriceHistoryListTable%24%3A%24{c_coNotesTable}&dataForm%3AdisplayingSnHChargeHistoryTabHdn=true&"
		"dataForm%3AdisplaySnHChargeHistoryTabOnlyHdn=true&dataForm%3AdisplayingSnHChargeHistoryTabInpHdn=true&snhChargeOverridePopupEditable_tabPanel_SubmitOnTabClick=false&snhChargeOverridePopupEditable_tabPanel_selectedTab=snhChargeOverridePopupEditablePriceHistoryTab&dataForm%3Aj_id227=snhChargeOverridePopupEditableTab&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_deleteHidden=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedRows=&"
		"dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AisSortButtonClick=ORC.CREATED_DTTM&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AsortDir=desc&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AcolCount=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableClicked=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable%3AtableResized=false&snhChargeOverridePopupPriceHistoryListTable_hdnMaxIndexHldr=0&"
		"dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_pageallrowskey=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_selectedIdList=&dataForm%3AsnhChargeOverridePopupPriceHistoryListTable_trs_allselectedrowskey=snhChargeOverridePopupPriceHistoryListTable%24%3A%24{c_coNotesTable}&dataForm%3AfltrListFltrId%3AfieldName=&dataForm%3AfltrListFltrId%3AfilterName=FL_{pUserName}&dataForm%3AfltrListFltrId%3Aowner={pUserName}&dataForm%3AfltrListFltrId%3AobjectType=FL_FILTER&"
		"dataForm%3AfltrListFltrId%3AfilterObjectType=&dataForm%3AfltrListFltrId%3Afield0value1=&dataForm%3AfltrListFltrId%3Afield0=FILTER.FILTER_NAME&dataForm%3AfltrListFltrId%3Afield0operator=&dataForm%3AfltrListFltrId%3Afield1value1=&dataForm%3AfltrListFltrId%3Afield1=FILTER.IS_DEFAULT&dataForm%3AfltrListFltrId%3Afield1operator=&dataForm%3AfltrListFltrId%3Afield2value1=&dataForm%3AfltrListFltrId%3Afield2=FILTER.IS_PRIVATE&dataForm%3AfltrListFltrId%3Afield2operator=&"
		"dataForm%3AfltrListFltrId%3Afield3value1=&dataForm%3AfltrListFltrId%3Afield3=FILTER.OWNER&dataForm%3AfltrListFltrId%3Afield3operator=&dataForm%3AfltrListFltrId%3Afield4value1=&dataForm%3AfltrListFltrId%3Afield4=FILTER.IS_DELETED&dataForm%3AfltrListFltrId%3Afield4operator=&dataForm%3AfltrListFltrId%3AfltrCondition=&dataForm%3AfltrListFltrId%3AfltrCrtSel=&windowId=screen-14271268&domainGroupId=110&dataForm%3Ainp_hdn_for_invoking_cco_bb=&dataForm%3Ainp_hdn_for_invoking_ccnf_bb=&orderNumber={pOrderNumber}&"
		"dataForm%3Apost_COEdit_inp_hdn_orderVersion={c_inp_hdn_orderVersion}&orderVersion={c_inp_hdn_orderVersion}&CustomerId=&ExternalCustomerId={c_ExternalCustomerId}&isExchangeOrder=false&hdnRMANo=&inp_hdn_for_order_status={c_inp_hdn_for_order_status}&hdn_for_discount_code={c_hdn_for_discount_code}&dataForm%3Ahid_postorder_view_createCO_ordno={pOrderNumber}&dataForm%3Apost_COView_inp_hdn_orderConfirmed=TRUE&dataForm%3Apost_COView_inp_hdn_orderCreatedDTTM={c_inp_hdn_orderCreatedDTTM}&dataForm%3Ahid_txt_postorder_view_createCO_fn={c_createCO_fn}&dataForm%3Ahid_txt_postorder_view_createCO_pno={c_createCO_pno}"
		"&dataForm%3Apost_COView_inp_hdn_orderStatus={c_inp_hdn_orderStatus}&dataForm%3Apost_COEView_inp_hdn_orderStatusId={c_inp_hdn_for_order_status}&dataForm%3Apost_COView_inp_hdn_paymentStatus=AUTHORIZED&dataForm%3Apost_COView_inp_hdn_paymentStatusId=20&dataForm%3Ahid_txt_postorder_view_createCO_ln={c_hid_txt_postorder_view_createCO_ln}&dataForm%3Ainp_hidden_postorder_createCO_customerId_out=&dataForm%3Ainp_hidden_postorder_createCO_extcustomerId_out={c_ExternalCustomerId}&dataForm%3Ainp_hidden_postorder_createCO_customerType_out=&dataForm%3Ahid_txt_postorder_view_createCO_ordType=USECOM&"
		"dataForm%3Ahid_txt_postorder_view_createCO_ordTypeId=4&dataForm%3Ahid_txt_postorder_view_createCO_stId=&dataForm%3Ahid_txt_postorder_view_createCO_email={c_hid_txt_postorder_view_createCO_email}&dataForm%3Apost_COView_inp_hdn_orderCancel={c_inp_hdn_orderCancel}&dataForm%3Apost_COView_inp_hdn_totalNoOfNotesStr=0&exchangeOrderIdStr=&loadDefaultNavigationFlow=&dataForm%3Adrop_down_page_ids_som=viewAuditDetails&loadNotesFromCategory=10&targetLink=&updateOrderHeaderActive=false&addAppeasementsActive=false&updatePaymentsActive=false&"
		"updateShippingInfoActive=false&updateQuantityActive=false&cancelItemActive=false&addItemActive=false&updatePromotionsActive=false&createReturnActive=false&updateAdditionalDetailsActive={c_updateAdditionalDetailsActive}&overridePriceActive=false&overrideSnHChargeActive=false&taskDeterminantLoaded=true&contentPageName=viewOrderDetails&dataForm%3AcoROCustomerOrderIdHidden={c_orderIDPkHiddenParam}&dataForm%3AcoROILTable%3Apager%3ApageInput=&dataForm%3AcoROILTable%3ApagerBoxValue=&dataForm%3AcoROILTable%3AisPaginationEvent=&"
		"dataForm%3AcoROILTable%3ApagerAction=&dataForm%3AcoROILTable_deleteHidden=&dataForm%3AcoROILTable_selectedRows=&dataForm%3AcoROILTable%3AisSortButtonClick=col.orderLineNumber&dataForm%3AcoROILTable%3AsortDir=asc&dataForm%3AcoROILTable%3AcolCount=&dataForm%3AcoROILTable%3AtableClicked=&dataForm%3AcoROILTable%3AtableResized=false&coROILTable_hdnMaxIndexHldr={c_coROILTable_hdnMaxIndexHldr}&remainingToPay={c_remainingToPay}&eligibleForSnHOverride=false&CustomerOrderId={c_orderIDPkHiddenParam}&orderIDPk={c_orderIDPkHiddenParam}&mainPageName=&"
		"moreActionTargetLinkviewCustomerOrderNavFlowFooterNextBtnPanel=&moreActionButtonPressedviewCustomerOrderNavFlowFooterNextBtnPanel=&moreActionTargetLinkviewCustomerOrderNavFlowFooterHoldCancelPanel=&moreActionButtonPressedviewCustomerOrderNavFlowFooterHoldCancelPanel=&backingBeanName=&javax.faces.ViewState={c_ViewState03}&dataForm%3Aj_id333=dataForm%3Aj_id333&", 
		"LAST");

	lr_end_transaction(lr_eval_string("{sTestCaseName}_06_View_Audit_Details_of_Order"),2);

	lr_think_time(1);

 


	 
# 642 "Action.c"
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
	
		return 0;
}
# 5 "c:\\it\\scripts\\tcp_6_ecom_us_view_audit_order_history\\\\combined_TCP_6_ECOM_US_View_Audit_Order_History.c" 2

# 1 "vuser_end.c" 1
vuser_end()
{

 


	 
# 18 "vuser_end.c"
	 
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_07_Logout"));

	web_add_cookie("filterExpandState=true; DOMAIN=njitldomapp01.tcphq.tcpcorp.local.com");

 


	 
# 43 "vuser_end.c"

 


 


	web_url("logout", 
		"URL={pURL}/saml/logout", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t25.inf", 
		"Mode=HTML", 
		"EXTRARES", 
		"Url={pURL_SSO}/images/loading.gif", "Referer={pURL_SSO}/manh/resources/css/mip.css", "ENDITEM", 
		"LAST");

	web_url("miplogout", 
		"URL={pURL_SSO}/miplogout", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t26.inf", 
		"Mode=HTML", 
		"LAST");

	lr_end_transaction(lr_eval_string("{sTestCaseName}_07_Logout"),2); 

	return 0;
}
# 6 "c:\\it\\scripts\\tcp_6_ecom_us_view_audit_order_history\\\\combined_TCP_6_ECOM_US_View_Audit_Order_History.c" 2

