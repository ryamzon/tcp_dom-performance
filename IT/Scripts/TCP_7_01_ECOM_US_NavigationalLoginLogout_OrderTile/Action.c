Action()
{
	/* Search Order in Order Tile */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_03_Order_Lookup_from_Order_tile"));

	/*web_custom_request("1043", 
		"URL={pURL}/services/rest/lps/TileService/tile/1043?_dc=1465973779730", 
		"Method=PUT", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t6.inf", 
		"Mode=HTML", 
		"Body={\"TileViewObject\":{\"id\":1043,\"isShowIcon\":false,\"isShowTitle\":false,\"workspaceId\":1427,\"title\":\"NOT-USED\",\"tileClass\":\"DS.view.tile.OrderTile\",\"color\":\"blue\",\"icon\":\"default-icon\",\"scale\":\"large\",\"header\":\"\",\"content\":\"\",\"location\":\"{\\\"left\\\":149,\\\"top\\\":149}\",\"screenOptionsClass\":\"\",\"screenOptionsScreenCfg\":\"null\",\"screenOptionsViewCfg\":\"null\",\"screenOptionsWindowCfg\":\"null\"}}", 
		LAST);*/

	web_reg_save_param("c_First_Name",
	                   "LB=customerFirstName\":\"",
	                   "RB=\",\"customerLastName",
	                   "ORD=1",
	                   "Search=ALL",LAST);

	web_reg_save_param("c_Last_Name",
                   "LB=customerLastName\":\"",
                   "RB=\",\"customerPhone",
                   "ORD=1",
                   "Search=ALL",LAST);
	
	web_reg_save_param("c_Phone_Number",
                   "LB=\"customerPhone\":\"",
                   "RB=\",\"customerEmail",
                   "ORD=1",
                   "Search=ALL",LAST);

	web_reg_save_param("c_Email_Id",
                   "LB=customerEmail\":\"",
                   "RB=\"},\"",
                   "ORD=1",
                   "Search=ALL",LAST);
	
	web_reg_save_param("c_externalCustomerId",
                   "LB=customerInfo\":{\"externalCustomerId\":",
                   "RB=,\"customerFirstName",
                   "ORD=1",
                   "Search=ALL",LAST);
	
	lr_save_string("{\"customerOrderSearchCriteria\":{\"entityType\":\"All\",\"orderNumber\":\"","p1");
	
	lr_output_message("%d",lr_eval_string("{p1}"));
	
	lr_save_string("\",\"parentOrderNumber\":\"\",\"createdFromDate\":\"\",\"createdToDate\":\"\",\"noOfRecordsPerPage\":10,\"customerInfo\":\"\",\"customerBasicInfo\":\"\",\"sortingCriterion\":{\"sortField\":\"createdDTTM\",\"sortDirection\":\"DESC\"},\"currentPageNumber\":0}}","p2");
	
	lr_output_message("%d",lr_eval_string("{p2}"));
	
	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");
	
	web_custom_request("customerOrderAndTransactionList", 
		"URL={pURL}/services/olm/customerorder/customerOrderAndTransactionList?_dc=1465973780171&page=1&start=0&limit=50&sort=%5B%7B%22property%22%3A%22orderCreatedDate%22%2C%22direction%22%3A%22DESC%22%7D%5D", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t7.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={p1}{p_Order_Number}{p2}", 
		LAST);

	
	web_custom_request("windows", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14271248&regionId=-1&businessUnitId=-1", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t8.inf", 
		"Mode=HTML", 
		"EncType=", 
		LAST);

	web_url("ping.jsp_3", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1465973853912", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t9.inf", 
		"Mode=HTML", 
		LAST);

	lr_save_string("{\"companyParameterIdList\":{\"parameterList\":[\"CURRENCY_USED\",\"EXT_CUSTOMER_ID_GENERATION\",\"USE_DELIVERY_ZONE\",\"DEFAULT_SHIP_VIA\",\"DEFAULT_ORDER_TYPE\",\"REQUIRES_VALIDATION_FOR_PAYMENT_AUTHORIZATION\",\"REQUIRES_VALIDATION_FOR_PAYMENT_SETTLEMENT\",\"CHARGE_HANDLING_STRATEGY\",\"AUTOMATIC_PROMOTION_VALUE\",\"ITEM_PRICE_EXPIRY_DATE_EXTENSION_DAYS\",\"ITEM_PRICE_SCHEDULER_INTERVAL_IN_DAYS\",\"DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE\",\"VIEW_STORE_LEVEL_PRICES\",\"NETWORK_LEVEL_VIEW\",\""
		"FACILITY_LEVEL_VIEW\"]}}","p3");
	
	lr_output_message("%d",lr_eval_string("{p3}"));
	
	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");
	
	web_custom_request("companyParameterList", 
		"URL={pURL}/services/olm/basedata/companyParameter/companyParameterList?_dc=1465973860641&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t10.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={p3}", 
		LAST);
	
	
	lr_save_string("{\"customerOrderSearchCriteria\":{\"entityType\":\"All\",\"orderNumber\":","p4");
	
	lr_output_message("%d",lr_eval_string("{p4}"));
	
	lr_save_string(",\"parentOrderNumber\":\"\",\"createdFromDate\":\"\",\"createdToDate\":\"\",\"noOfRecordsPerPage\":1000,\"customerInfo\":{\"customerFirstName\":\"","p5");
	
	lr_output_message("%d",lr_eval_string("{p5}"));
	
	lr_save_string("\",\"customerLastName\":\"","p6");
	
	lr_output_message("%d",lr_eval_string("{p6}"));
		
	lr_save_string("\",\"customField1\":\"\",\"customField2\":\"\",\"customField3\":\"\",\""
		"customField4\":\"\",\"customField5\":\"\",\"customField6\":\"\",\"customField7\":\"\",\"customField8\":\"\",\"customField9\":\"\",\"customField10\":\"\"},\"customerBasicInfo\":\"\",\"sortingCriterion\":{\"sortField\":\"createdDTTM\",\"sortDirection\":\"DESC\"},\"currentPageNumber\":0}}","p7");
	
	lr_output_message("%d",lr_eval_string("{p7}"));

	//web_reg_find("Text=04:51 EDT",LAST);
	
	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");
	
	web_custom_request("customerOrderAndTransactionList_2", 
		"URL={pURL}/services/olm/customerorder/customerOrderAndTransactionList?_dc=1465973859734&page=1&start=0&limit=50&sort=%5B%7B%22property%22%3A%22orderCreatedDate%22%2C%22direction%22%3A%22DESC%22%7D%5D", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t11.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={p4}{p_Order_Number}{p5}{c_First_Name}{p6}{c_Last_Name}\",\"customerFullName\":\"{c_First_Name} {c_Last_Name}\",\"customerEmail\":\"{c_Email_Id}\",\"customerPhone\":\"{c_Phone_Number}\",\"customerId\":\"\",\"externalCustomerId\":\"{c_externalCustomerId}{p7}", 
		LAST);

	web_url("ping.jsp_4", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1465973877267", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t12.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_03_Order_Lookup_from_Order_tile"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_5", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1465973892645", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t13.inf", 
		"Mode=HTML", 
		LAST);

	web_url("ping.jsp_6", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1465973908006", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t14.inf", 
		"Mode=HTML", 
		LAST);

	web_url("ping.jsp_7", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1465973923384", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t15.inf", 
		"Mode=HTML", 
		LAST);

	web_url("ping.jsp_9", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1465973956248", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t19.inf", 
		"Mode=HTML", 
		LAST);

	
	return 0;
}
