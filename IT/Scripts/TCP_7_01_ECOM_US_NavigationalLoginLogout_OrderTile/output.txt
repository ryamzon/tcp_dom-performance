Virtual User Script started at : 9/29/2017 2:35:46 AM
Starting action vuser_init.
Web Turbo Replay of LoadRunner 12.0.0 for Windows 2008 R2; build 2079 (Jun 17 2014 10:56:12)  	[MsgId: MMSG-27143]
Run mode: HTML  	[MsgId: MMSG-26993]
Run-Time Settings file: "C:\IT\Scripts\TCP_7_01_ECOM_US_NavigationalLoginLogout_OrderTile\\default.cfg"  	[MsgId: MMSG-27141]
vuser_init.c(12): web_cache_cleanup started  	[MsgId: MMSG-26355]
vuser_init.c(12): web_cache_cleanup was successful  	[MsgId: MMSG-26392]
vuser_init.c(14): web_cleanup_cookies started  	[MsgId: MMSG-26355]
vuser_init.c(14): web_cleanup_cookies was successful  	[MsgId: MMSG-26392]
vuser_init.c(16): web_set_max_html_param_len started  	[MsgId: MMSG-26355]
vuser_init.c(16): web_set_max_html_param_len was successful  	[MsgId: MMSG-26392]
vuser_init.c(20): web_set_sockets_option started  	[MsgId: MMSG-26355]
vuser_init.c(20): web_set_sockets_option was successful  	[MsgId: MMSG-26392]
vuser_init.c(22): web_set_sockets_option started  	[MsgId: MMSG-26355]
vuser_init.c(22): web_set_sockets_option was successful  	[MsgId: MMSG-26392]
vuser_init.c(24): web_reg_save_param started  	[MsgId: MMSG-26355]
vuser_init.c(24): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
vuser_init.c(30): Notify: Saving Parameter "sTestCaseName = TCP_07_1_ECOM_US_NavigationalLoginLogout_OrderTile".
vuser_init.c(32): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_1_ECOM_US_NavigationalLoginLogout_OrderTile"
vuser_init.c(32): Notify: Transaction "TCP_07_1_ECOM_US_NavigationalLoginLogout_OrderTile_01_Launch" started.
vuser_init.c(36): web_url("index.html") started  	[MsgId: MMSG-26355]
vuser_init.c(36): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(36): Notify: Saving Parameter "c_SAML_Request = PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c2FtbDJwOkF1dGhuUmVxdWVzdCB4bWxuczpzYW1sMnA9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpwcm90b2NvbCIgQXNzZXJ0aW9uQ29uc3VtZXJTZXJ2aWNlVVJMPSJodHRwOi8vZW9tLWNhLXBlcmYudGNwaHEudGNwY29ycC5sb2NhbC5jb206MzAwMDAvc2FtbC9TU08vYWxpYXMvY2EiIERlc3RpbmF0aW9uPSJodHRwczovL0VPTS1NSVAtUGVyZi5jaGlsZHJlbnNwbGFjZS5jb206MTUwMDEvcHJvZmlsZS9TQU1MMi9QT1NUL1NTTyIgRm9yY2VBdXRobj0iZmFsc2UiIElEPSJhM2k0OWU3M2RiNTZhZ2ZpYTNkMDNoZjQwZGFkajMiIElzUGFzc2l2ZT0iZmFsc2UiIElzc3VlSW5zdGFudD0iMjAxNy0wOS0yOVQwNjozNTo0Ni41NTVaIiBQcm90b2NvbEJpbmRpbmc9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpiaW5kaW5nczpIVFRQLUFydGlmYWN0IiBWZXJzaW9uPSIyLjAiPjxzYW1sMjpJc3N1ZXIgeG1sbnM6c2FtbDI9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iPmNhPC9zYW1sMjpJc3N1ZXI+PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+PGRzOlNpZ25lZEluZm8+PGRzOkNhbm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48ZHM6U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3JzYS1zaGExIi8+PGRzOlJlZmVyZW5jZSBVUkk9IiNhM2k0OWU3M2RiNTZhZ2ZpYTNkMDNoZjQwZGFkajMiPjxkczpUcmFuc2Zvcm1zPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48L2RzOlRyYW5zZm9ybXM+PGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNzaGExIi8+PGRzOkRpZ2VzdFZhbHVlPndrZjhTS0QydkwrY29nNjRDTXh5c3gySEJ3az08L2RzOkRpZ2VzdFZhbHVlPjwvZHM6UmVmZXJlbmNlPjwvZHM6U2lnbmVkSW5mbz48ZHM6U2lnbmF0dXJlVmFsdWU+REdTWmtiazlRNTZPbXNCVmRIbCt1TkFUMjZZWno0NGUvTy9NaFJKUU4wTTRjRVJjNkkvb2RjYkxzTDZtQ3ZUR3EyRGhFT1VSVFFzTXljdDF1THNHOHluUUF4ZUdGaUlyZzdOOUx6RFJtZ0cybnBxenAxWHFTb1J5VDBHRTZJWEdMcTBIZFd3TTlRRkwzQUw5ZHk1TEFDbWFtSmVFdGFvdnBpdlZNTUJIVVQ5bkRJN1dVRnkvcDFSekNOSGdLQ3pDSHN5NzZzT29GWitMK255d252MmhpVy92K2YrSWE1bFkxM2lDVWxJTE0yUXZiYjlnM2ZjbDNDbVhVa1NKR1p4Y2crVUYwZnczS0owKzU5UE4xMmtLa0NmWWd3d0d4YVdqMkttQzZTQWltRjBlYThmaTlKUnBQZXVLbnBuNFk5WFlKWHNBWGtJYktER2VDVFBGRXVQd253PT08L2RzOlNpZ25hdHVyZVZhbHVlPjxkczpLZXlJbmZvPjxkczpYNTA5RGF0YT48ZHM6WDUwOUNlcnRpZmljYXRlPk1JSURCVENDQWUwQ0JGS0RuRkV3RFFZSktvWklodmNOQVFFTkJRQXdSekVMTUFrR0ExVUVCaE1DVlZNeElqQWdCZ05WQkFvTUdVMWgKYm1oaGRIUmhiaUJCYzNOdlkybGhkR1Z6TENCSmJtTXhGREFTQmdOVkJBTU1DM053TG0xaGJtZ3VZMjl0TUI0WERURXpNVEV4TXpFMQpNelUwTlZvWERUTXpNVEV3T0RFMU16VTBOVm93UnpFTE1Ba0dBMVVFQmhNQ1ZWTXhJakFnQmdOVkJBb01HVTFoYm1oaGRIUmhiaUJCCmMzTnZZMmxoZEdWekxDQkpibU14RkRBU0JnTlZCQU1NQzNOd0xtMWhibWd1WTI5dE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0MKQVE4QU1JSUJDZ0tDQVFFQWpSeXA0QXRacHV1TnZORFExakhTc1o2REdHUGlPQ0xaWlBvV3d4NHowNXFjRzQ4MTFMVVhabEZvL05aawpGeHY1bG1kamx2SWI0bzgzbGx1ajFkSWNFSnFCUEpxcEtZOS95SStwdUVtMUFFSjlqT0NIZitjeWREQjM2dmU3M1VtdndaWjBETFNvCmxaVjl5NW1yYUhaaDBLV1paZ2lianRZcCtybEFTa0hiTUI2ZWNZVzZPWlJPWXNoR05ES2tpSSt3ZHFsTXY4SjdNTldRaXlMTlFLc0EKQlFJbXd5dy9lQ05rdHFRWDMvclpNdXpqSVRVMG0xbWZtTUNTR3JybEJEZzJqR2hJNndxYlVPQXQwUk96cysvK3ZpOTIyMzdadnNVQwo5cDRXWko0S0pJK0lwaTROL3hGckZReU1RKzdXNTZQaFVpTmk0eUUvc3FNb3pMZDJrSStKTlFJREFRQUJNQTBHQ1NxR1NJYjNEUUVCCkRRVUFBNElCQVFDRjZpeHFwVXo5ZUY4aU5LQTIrczZLdlFrNUh2enF0OTNYaFlCcURPWTlwNEMzL3NIVHNsdCtxM3oxcG9MRmdqQm8KYW9oNWl5aklkTytudi83SUR1Z2hVdHJLWVFpaUZPbGlQNlFaL2RSNnNqT0VvL29IVVRFNXNNejBaOGVwcVJBYlVwTlUyQzFNRXE5agpUdmpGUXJQcm01T01JazlmaHdPOE1DdmoydmdlZmQ2dHdza3F5Ylp5TDlMTFdBTXdIb0FTY3VTOFBtUVU0eE1aT1RNM3FkVDlFQkVJCjBIY0N2dmxSc0dDTTFQZFFTWTBtZ2ZSUThNRjVCMGhEOVlaanF5ZWRIREFpaEExeW9IRW5sd2JFbnR5Y0lQc3dZcUQ3Zm4wNXM4cU8KdFU0ZFRnaUxqUmpSMmVHcXhDZlZ6cWd2V3VpdnhmczRVWjIyem5MSTRaZ2RUY3FDPC9kczpYNTA5Q2VydGlmaWNhdGU+PC9kczpYNTA5RGF0YT48L2RzOktleUluZm8+PC9kczpTaWduYXR1cmU+PHNhbWwycDpOYW1lSURQb2xpY3kgRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6bmFtZWlkLWZvcm1hdDp1bnNwZWNpZmllZCIvPjwvc2FtbDJwOkF1dGhuUmVxdWVzdD4=".
vuser_init.c(36): web_url("index.html") was successful, 4660 body bytes, 357 header bytes  	[MsgId: MMSG-26386]
vuser_init.c(46): web_set_sockets_option started  	[MsgId: MMSG-26355]
vuser_init.c(46): web_set_sockets_option was successful  	[MsgId: MMSG-26392]
vuser_init.c(48): web_submit_data("SSO") started  	[MsgId: MMSG-26355]
vuser_init.c(48): Notify: Next row for parameter pURL_SSO = 1 [table  = pURL_SSO].
vuser_init.c(48): Notify: Parameter Substitution: parameter "pURL_SSO" =  "https://eom-mip-perf.childrensplace.com:15001"
vuser_init.c(48): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(48): Notify: Parameter Substitution: parameter "c_SAML_Request" =  "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c2FtbDJwOkF1dGhuUmVxdWVzdCB4bWxuczpzYW1sMnA9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpwcm90b2NvbCIgQXNzZXJ0aW9uQ29uc3VtZXJTZXJ2aWNlVVJMPSJodHRwOi8vZW9tLWNhLXBlcmYudGNwaHEudGNwY29ycC5sb2NhbC5jb206MzAwMDAvc2FtbC9TU08vYWxpYXMvY2EiIERlc3RpbmF0aW9uPSJodHRwczovL0VPTS1NSVAtUGVyZi5jaGlsZHJlbnNwbGFjZS5jb206MTUwMDEvcHJvZmlsZS9TQU1MMi9QT1NUL1NTTyIgRm9yY2VBdXRobj0iZmFsc2UiIElEPSJhM2k0OWU3M2RiNTZhZ2ZpYTNkMDNoZjQwZGFkajMiIElzUGFzc2l2ZT0iZmFsc2UiIElzc3VlSW5zdGFudD0iMjAxNy0wOS0yOVQwNjozNTo0Ni41NTVaIiBQcm90b2NvbEJpbmRpbmc9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpiaW5kaW5nczpIVFRQLUFydGlmYWN0IiBWZXJzaW9uPSIyLjAiPjxzYW1sMjpJc3N1ZXIgeG1sbnM6c2FtbDI9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iPmNhPC9zYW1sMjpJc3N1ZXI+PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+PGRzOlNpZ25lZEluZm8+PGRzOkNhbm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48ZHM6U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3JzYS1zaGExIi8+PGRzOlJlZmVyZW5jZSBVUkk9IiNhM2k0OWU3M2RiNTZhZ2ZpYTNkMDNoZjQwZGFkajMiPjxkczpUcmFuc2Zvcm1zPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48L2RzOlRyYW5zZm9ybXM+PGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNzaGExIi8+PGRzOkRpZ2VzdFZhbHVlPndrZjhTS0QydkwrY29nNjRDTXh5c3gySEJ3az08L2RzOkRpZ2VzdFZhbHVlPjwvZHM6UmVmZXJlbmNlPjwvZHM6U2lnbmVkSW5mbz48ZHM6U2lnbmF0dXJlVmFsdWU+REdTWmtiazlRNTZPbXNCVmRIbCt1TkFUMjZZWno0NGUvTy9NaFJKUU4wTTRjRVJjNkkvb2RjYkxzTDZtQ3ZUR3EyRGhFT1VSVFFzTXljdDF1THNHOHluUUF4ZUdGaUlyZzdOOUx6RFJtZ0cybnBxenAxWHFTb1J5VDBHRTZJWEdMcTBIZFd3TTlRRkwzQUw5ZHk1TEFDbWFtSmVFdGFvdnBpdlZNTUJIVVQ5bkRJN1dVRnkvcDFSekNOSGdLQ3pDSHN5NzZzT29GWitMK255d252MmhpVy92K2YrSWE1bFkxM2lDVWxJTE0yUXZiYjlnM2ZjbDNDbVhVa1NKR1p4Y2crVUYwZnczS0owKzU5UE4xMmtLa0NmWWd3d0d4YVdqMkttQzZTQWltRjBlYThmaTlKUnBQZXVLbnBuNFk5WFlKWHNBWGtJYktER2VDVFBGRXVQd253PT08L2RzOlNpZ25hdHVyZVZhbHVlPjxkczpLZXlJbmZvPjxkczpYNTA5RGF0YT48ZHM6WDUwOUNlcnRpZmljYXRlPk1JSURCVENDQWUwQ0JGS0RuRkV3RFFZSktvWklodmNOQVFFTkJRQXdSekVMTUFrR0ExVUVCaE1DVlZNeElqQWdCZ05WQkFvTUdVMWgKYm1oaGRIUmhiaUJCYzNOdlkybGhkR1Z6TENCSmJtTXhGREFTQmdOVkJBTU1DM053TG0xaGJtZ3VZMjl0TUI0WERURXpNVEV4TXpFMQpNelUwTlZvWERUTXpNVEV3T0RFMU16VTBOVm93UnpFTE1Ba0dBMVVFQmhNQ1ZWTXhJakFnQmdOVkJBb01HVTFoYm1oaGRIUmhiaUJCCmMzTnZZMmxoZEdWekxDQkpibU14RkRBU0JnTlZCQU1NQzNOd0xtMWhibWd1WTI5dE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0MKQVE4QU1JSUJDZ0tDQVFFQWpSeXA0QXRacHV1TnZORFExakhTc1o2REdHUGlPQ0xaWlBvV3d4NHowNXFjRzQ4MTFMVVhabEZvL05aawpGeHY1bG1kamx2SWI0bzgzbGx1ajFkSWNFSnFCUEpxcEtZOS95SStwdUVtMUFFSjlqT0NIZitjeWREQjM2dmU3M1VtdndaWjBETFNvCmxaVjl5NW1yYUhaaDBLV1paZ2lianRZcCtybEFTa0hiTUI2ZWNZVzZPWlJPWXNoR05ES2tpSSt3ZHFsTXY4SjdNTldRaXlMTlFLc0EKQlFJbXd5dy9lQ05rdHFRWDMvclpNdXpqSVRVMG0xbWZtTUNTR3JybEJEZzJqR2hJNndxYlVPQXQwUk96cysvK3ZpOTIyMzdadnNVQwo5cDRXWko0S0pJK0lwaTROL3hGckZReU1RKzdXNTZQaFVpTmk0eUUvc3FNb3pMZDJrSStKTlFJREFRQUJNQTBHQ1NxR1NJYjNEUUVCCkRRVUFBNElCQVFDRjZpeHFwVXo5ZUY4aU5LQTIrczZLdlFrNUh2enF0OTNYaFlCcURPWTlwNEMzL3NIVHNsdCtxM3oxcG9MRmdqQm8KYW9oNWl5aklkTytudi83SUR1Z2hVdHJLWVFpaUZPbGlQNlFaL2RSNnNqT0VvL29IVVRFNXNNejBaOGVwcVJBYlVwTlUyQzFNRXE5agpUdmpGUXJQcm01T01JazlmaHdPOE1DdmoydmdlZmQ2dHdza3F5Ylp5TDlMTFdBTXdIb0FTY3VTOFBtUVU0eE1aT1RNM3FkVDlFQkVJCjBIY0N2dmxSc0dDTTFQZFFTWTBtZ2ZSUThNRjVCMGhEOVlaanF5ZWRIREFpaEExeW9IRW5sd2JFbnR5Y0lQc3dZcUQ3Zm4wNXM4cU8KdFU0ZFRnaUxqUmpSMmVHcXhDZlZ6cWd2V3VpdnhmczRVWjIyem5MSTRaZ2RUY3FDPC9kczpYNTA5Q2VydGlmaWNhdGU+PC9kczpYNTA5RGF0YT48L2RzOktleUluZm8+PC9kczpTaWduYXR1cmU+PHNhbWwycDpOYW1lSURQb2xpY3kgRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6bmFtZWlkLWZvcm1hdDp1bnNwZWNpZmllZCIvPjwvc2FtbDJwOkF1dGhuUmVxdWVzdD4="
vuser_init.c(48): Redirecting "https://eom-mip-perf.childrensplace.com:15001/profile/SAML2/POST/SSO" (redirection depth is 0)  	[MsgId: MMSG-26694]
vuser_init.c(48): To location "https://eom-mip-perf.childrensplace.com:15001/AuthnEngine"  	[MsgId: MMSG-26693]
vuser_init.c(48): Redirecting "https://eom-mip-perf.childrensplace.com:15001/AuthnEngine" (redirection depth is 1)  	[MsgId: MMSG-26694]
vuser_init.c(48): To location "https://eom-mip-perf.childrensplace.com:15001/Authn/RemoteUser?loginContextKey=50b61752-52ab-4fbe-961f-987e39c4022a"  	[MsgId: MMSG-26693]
vuser_init.c(48): Redirecting "https://eom-mip-perf.childrensplace.com:15001/Authn/RemoteUser?loginContextKey=50b61752-52ab-4fbe-961f-987e39c4022a" (redirection depth is 2)  	[MsgId: MMSG-26694]
vuser_init.c(48): To location "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26693]
vuser_init.c(48): Found resource "https://eom-mip-perf.childrensplace.com:15001/ext/resources/css/ext-all.css" in HTML "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26659]
vuser_init.c(48): Found resource "https://eom-mip-perf.childrensplace.com:15001/manh/resources/css/mip.css" in HTML "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26659]
vuser_init.c(48): Found resource "https://eom-mip-perf.childrensplace.com:15001/ext/ext-all.js" in HTML "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26659]
vuser_init.c(48): Found resource "https://eom-mip-perf.childrensplace.com:15001/app.js" in HTML "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26659]
vuser_init.c(48): web_submit_data("SSO") was successful, 462743 body bytes, 2796 header bytes, 6426 chunking overhead bytes  	[MsgId: MMSG-26385]
vuser_init.c(60): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_1_ECOM_US_NavigationalLoginLogout_OrderTile"
vuser_init.c(60): Notify: Transaction "TCP_07_1_ECOM_US_NavigationalLoginLogout_OrderTile_01_Launch" ended with "Pass" status (Duration: 1.6966 Wasted Time: 1.3325).
vuser_init.c(66): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_1_ECOM_US_NavigationalLoginLogout_OrderTile"
vuser_init.c(66): Notify: Transaction "TCP_07_1_ECOM_US_NavigationalLoginLogout_OrderTile_02_Login" started.
vuser_init.c(68): web_custom_request("j_spring_security_check") started  	[MsgId: MMSG-26355]
vuser_init.c(68): Notify: Next row for parameter pURL_SSO = 1 [table  = pURL_SSO].
vuser_init.c(68): Notify: Parameter Substitution: parameter "pURL_SSO" =  "https://eom-mip-perf.childrensplace.com:15001"
vuser_init.c(68): Notify: Parameter Substitution: parameter "pUserName" =  "User044"
vuser_init.c(68): Notify: Next row for parameter pPassword = 1 [table  = pPassword].
vuser_init.c(68): Notify: Parameter Substitution: parameter "pPassword" =  "password"
vuser_init.c(68): Redirecting "https://eom-mip-perf.childrensplace.com:15001/j_spring_security_check" (redirection depth is 0)  	[MsgId: MMSG-26694]
vuser_init.c(68): To location "https://eom-mip-perf.childrensplace.com:15001/Authn/RemoteUser?loginContextKey=50b61752-52ab-4fbe-961f-987e39c4022a"  	[MsgId: MMSG-26693]
vuser_init.c(68): Redirecting "https://eom-mip-perf.childrensplace.com:15001/Authn/RemoteUser?loginContextKey=50b61752-52ab-4fbe-961f-987e39c4022a" (redirection depth is 1)  	[MsgId: MMSG-26694]
vuser_init.c(68): To location "https://eom-mip-perf.childrensplace.com:15001/profile/SAML2/POST/SSO"  	[MsgId: MMSG-26693]
vuser_init.c(68): Redirecting "https://eom-mip-perf.childrensplace.com:15001/profile/SAML2/POST/SSO" (redirection depth is 2)  	[MsgId: MMSG-26694]
vuser_init.c(68): To location "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/saml/SSO/alias/ca?SAMLart=AAQAAf5QSaNfZd4JBmYl5vT6TLHqylbwuOEb4z1xHDa0BM8w4W12ZKH0cNo%3D"  	[MsgId: MMSG-26693]
vuser_init.c(68): Redirecting "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/saml/SSO/alias/ca?SAMLart=AAQAAf5QSaNfZd4JBmYl5vT6TLHqylbwuOEb4z1xHDa0BM8w4W12ZKH0cNo%3D" (redirection depth is 3)  	[MsgId: MMSG-26694]
vuser_init.c(68): To location "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26693]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/extstyles" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/ext/ext-all.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/ext/ext-theme-neptune.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/hammer.min.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/overrides.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/webtop.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/bootstrap.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/webtop-overrides.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/mps.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/html2canvas.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/notifications/portal.min.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/notifications/atmosphere-min.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/lps/resources/common/scripts/3rdparty/jquery/jquery.min.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/highcharts/js/highcharts.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/highcharts/js/highcharts-more.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(68): web_custom_request("j_spring_security_check") was successful, 2034925 body bytes, 6518 header bytes  	[MsgId: MMSG-26386]
vuser_init.c(80): web_url("ping.jsp") started  	[MsgId: MMSG-26355]
vuser_init.c(80): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(80): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(80): web_url("ping.jsp") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
vuser_init.c(92): web_url("ping.jsp_2") started  	[MsgId: MMSG-26355]
vuser_init.c(92): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(92): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(92): web_url("ping.jsp_2") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
vuser_init.c(102): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_1_ECOM_US_NavigationalLoginLogout_OrderTile"
vuser_init.c(102): Notify: Transaction "TCP_07_1_ECOM_US_NavigationalLoginLogout_OrderTile_02_Login" ended with "Pass" status (Duration: 0.6471 Wasted Time: 0.1833).
Ending action vuser_init.
Running Vuser...
Starting iteration 1.
Starting action Action.
Action.c(5): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_1_ECOM_US_NavigationalLoginLogout_OrderTile"
Action.c(5): Notify: Transaction "TCP_07_1_ECOM_US_NavigationalLoginLogout_OrderTile_03_Order_Lookup_from_Order_tile" started.
Action.c(19): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(19): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(25): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(25): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(31): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(31): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(37): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(37): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(43): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(43): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(49): Notify: Saving Parameter "p1 = {"customerOrderSearchCriteria":{"entityType":"All","orderNumber":"".
Action.c(51): Notify: Parameter Substitution: parameter "p1" =  "{"customerOrderSearchCriteria":{"entityType":"All","orderNumber":""
Action.c(51): 110845456
Action.c(53): Notify: Saving Parameter "p2 = ","parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":10,"customerInfo":"","customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}".
Action.c(55): Notify: Parameter Substitution: parameter "p2" =  "","parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":10,"customerInfo":"","customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}"
Action.c(55): 111500960
Action.c(57): web_set_user started  	[MsgId: MMSG-26355]
Action.c(57): Notify: Parameter Substitution: parameter "pUserName" =  "User044"
Action.c(57): Notify: Next row for parameter pPassword = 1 [table  = pPassword].
Action.c(57): Notify: Parameter Substitution: parameter "pPassword" =  "password"
Action.c(57): web_set_user was successful  	[MsgId: MMSG-26392]
Action.c(59): web_custom_request("customerOrderAndTransactionList") started  	[MsgId: MMSG-26355]
Action.c(59): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(59): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(59): Notify: Parameter Substitution: parameter "p1" =  "{"customerOrderSearchCriteria":{"entityType":"All","orderNumber":""
Action.c(59): Notify: Parameter Substitution: parameter "p_Order_Number" =  "70460000411"
Action.c(59): Notify: Parameter Substitution: parameter "p2" =  "","parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":10,"customerInfo":"","customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}"
Action.c(59): Notify: Saving Parameter "c_externalCustomerId = 64003958".
Action.c(59): Notify: Saving Parameter "c_First_Name = Data Dump".
Action.c(59): Notify: Saving Parameter "c_Last_Name = Data Dump".
Action.c(59): Notify: Saving Parameter "c_Phone_Number = 0000000000".
Action.c(59): Notify: Saving Parameter "c_Email_Id = datadump@manh.com".
Action.c(59): web_custom_request("customerOrderAndTransactionList") was successful, 556 body bytes, 186 header bytes, 12 chunking overhead bytes  	[MsgId: MMSG-26385]
Action.c(73): web_custom_request("windows") started  	[MsgId: MMSG-26355]
Action.c(73): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(73): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(73): web_custom_request("windows") was successful, 24 body bytes, 186 header bytes, 11 chunking overhead bytes  	[MsgId: MMSG-26385]
Action.c(85): web_url("ping.jsp_3") started  	[MsgId: MMSG-26355]
Action.c(85): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(85): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(85): web_url("ping.jsp_3") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Action.c(95): Notify: Saving Parameter "p3 = {"companyParameterIdList":{"parameterList":["CURRENCY_USED","EXT_CUSTOMER_ID_GENERATION","USE_DELIVERY_ZONE","DEFAULT_SHIP_VIA","DEFAULT_ORDER_TYPE","REQUIRES_VALIDATION_FOR_PAYMENT_AUTHORIZATION","REQUIRES_VALIDATION_FOR_PAYMENT_SETTLEMENT","CHARGE_HANDLING_STRATEGY","AUTOMATIC_PROMOTION_VALUE","ITEM_PRICE_EXPIRY_DATE_EXTENSION_DAYS","ITEM_PRICE_SCHEDULER_INTERVAL_IN_DAYS","DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE","VIEW_STORE_LEVEL_PRICES","NETWORK_LEVEL_VIEW","FACILITY_LEVEL_VIEW"]}}".
Action.c(98): Notify: Parameter Substitution: parameter "p3" =  "{"companyParameterIdList":{"parameterList":["CURRENCY_USED","EXT_CUSTOMER_ID_GENERATION","USE_DELIVERY_ZONE","DEFAULT_SHIP_VIA","DEFAULT_ORDER_TYPE","REQUIRES_VALIDATION_FOR_PAYMENT_AUTHORIZATION","REQUIRES_VALIDATION_FOR_PAYMENT_SETTLEMENT","CHARGE_HANDLING_STRATEGY","AUTOMATIC_PROMOTION_VALUE","ITEM_PRICE_EXPIRY_DATE_EXTENSION_DAYS","ITEM_PRICE_SCHEDULER_INTERVAL_IN_DAYS","DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE","VIEW_STORE_LEVEL_PRICES","NETWORK_LEVEL_VIEW","FACILITY_LEVEL_VIEW"]}}"
Action.c(98): 111652368
Action.c(100): web_set_user started  	[MsgId: MMSG-26355]
Action.c(100): Notify: Parameter Substitution: parameter "pUserName" =  "User044"
Action.c(100): Notify: Next row for parameter pPassword = 1 [table  = pPassword].
Action.c(100): Notify: Parameter Substitution: parameter "pPassword" =  "password"
Action.c(100): web_set_user was successful  	[MsgId: MMSG-26392]
Action.c(102): web_custom_request("companyParameterList") started  	[MsgId: MMSG-26355]
Action.c(102): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(102): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(102): Notify: Parameter Substitution: parameter "p3" =  "{"companyParameterIdList":{"parameterList":["CURRENCY_USED","EXT_CUSTOMER_ID_GENERATION","USE_DELIVERY_ZONE","DEFAULT_SHIP_VIA","DEFAULT_ORDER_TYPE","REQUIRES_VALIDATION_FOR_PAYMENT_AUTHORIZATION","REQUIRES_VALIDATION_FOR_PAYMENT_SETTLEMENT","CHARGE_HANDLING_STRATEGY","AUTOMATIC_PROMOTION_VALUE","ITEM_PRICE_EXPIRY_DATE_EXTENSION_DAYS","ITEM_PRICE_SCHEDULER_INTERVAL_IN_DAYS","DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE","VIEW_STORE_LEVEL_PRICES","NETWORK_LEVEL_VIEW","FACILITY_LEVEL_VIEW"]}}"
Action.c(102): web_custom_request("companyParameterList") was successful, 377 body bytes, 233 header bytes, 12 chunking overhead bytes  	[MsgId: MMSG-26385]
Action.c(116): Notify: Saving Parameter "p4 = {"customerOrderSearchCriteria":{"entityType":"All","orderNumber":".
Action.c(118): Notify: Parameter Substitution: parameter "p4" =  "{"customerOrderSearchCriteria":{"entityType":"All","orderNumber":"
Action.c(118): 110845248
Action.c(120): Notify: Saving Parameter "p5 = ,"parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":1000,"customerInfo":{"customerFirstName":"".
Action.c(122): Notify: Parameter Substitution: parameter "p5" =  ","parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":1000,"customerInfo":{"customerFirstName":""
Action.c(122): 111180392
Action.c(124): Notify: Saving Parameter "p6 = ","customerLastName":"".
Action.c(126): Notify: Parameter Substitution: parameter "p6" =  "","customerLastName":""
Action.c(126): 111297528
Action.c(128): Notify: Saving Parameter "p7 = ","customField1":"","customField2":"","customField3":"","customField4":"","customField5":"","customField6":"","customField7":"","customField8":"","customField9":"","customField10":""},"customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}".
Action.c(131): Notify: Parameter Substitution: parameter "p7" =  "","customField1":"","customField2":"","customField3":"","customField4":"","customField5":"","customField6":"","customField7":"","customField8":"","customField9":"","customField10":""},"customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}"
Action.c(131): 111558888
Action.c(135): web_set_user started  	[MsgId: MMSG-26355]
Action.c(135): Notify: Parameter Substitution: parameter "pUserName" =  "User044"
Action.c(135): Notify: Next row for parameter pPassword = 1 [table  = pPassword].
Action.c(135): Notify: Parameter Substitution: parameter "pPassword" =  "password"
Action.c(135): web_set_user was successful  	[MsgId: MMSG-26392]
Action.c(137): web_custom_request("customerOrderAndTransactionList_2") started  	[MsgId: MMSG-26355]
Action.c(137): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(137): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(137): Notify: Parameter Substitution: parameter "p4" =  "{"customerOrderSearchCriteria":{"entityType":"All","orderNumber":"
Action.c(137): Notify: Parameter Substitution: parameter "p_Order_Number" =  "70460000411"
Action.c(137): Notify: Parameter Substitution: parameter "p5" =  ","parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":1000,"customerInfo":{"customerFirstName":""
Action.c(137): Notify: Parameter Substitution: parameter "c_First_Name" =  "Data Dump"
Action.c(137): Notify: Parameter Substitution: parameter "p6" =  "","customerLastName":""
Action.c(137): Notify: Parameter Substitution: parameter "c_Last_Name" =  "Data Dump"
Action.c(137): Notify: Parameter Substitution: parameter "c_First_Name" =  "Data Dump"
Action.c(137): Notify: Parameter Substitution: parameter "c_Last_Name" =  "Data Dump"
Action.c(137): Notify: Parameter Substitution: parameter "c_Email_Id" =  "datadump@manh.com"
Action.c(137): Notify: Parameter Substitution: parameter "c_Phone_Number" =  "0000000000"
Action.c(137): Notify: Parameter Substitution: parameter "c_externalCustomerId" =  "64003958"
Action.c(137): Notify: Parameter Substitution: parameter "p7" =  "","customField1":"","customField2":"","customField3":"","customField4":"","customField5":"","customField6":"","customField7":"","customField8":"","customField9":"","customField10":""},"customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}"
Action.c(137): web_custom_request("customerOrderAndTransactionList_2") was successful, 556 body bytes, 186 header bytes, 12 chunking overhead bytes  	[MsgId: MMSG-26385]
Action.c(150): web_url("ping.jsp_4") started  	[MsgId: MMSG-26355]
Action.c(150): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(150): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(150): web_url("ping.jsp_4") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Action.c(160): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_1_ECOM_US_NavigationalLoginLogout_OrderTile"
Action.c(160): Notify: Transaction "TCP_07_1_ECOM_US_NavigationalLoginLogout_OrderTile_03_Order_Lookup_from_Order_tile" ended with "Pass" status (Duration: 0.4140 Wasted Time: 0.0083).
Action.c(164): web_url("ping.jsp_5") started  	[MsgId: MMSG-26355]
Action.c(164): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(164): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(164): web_url("ping.jsp_5") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Action.c(174): web_url("ping.jsp_6") started  	[MsgId: MMSG-26355]
Action.c(174): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(174): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(174): web_url("ping.jsp_6") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Action.c(184): web_url("ping.jsp_7") started  	[MsgId: MMSG-26355]
Action.c(184): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(184): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(184): web_url("ping.jsp_7") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Action.c(194): web_url("ping.jsp_9") started  	[MsgId: MMSG-26355]
Action.c(194): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(194): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(194): web_url("ping.jsp_9") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Ending action Action.
Ending iteration 1.
Ending Vuser...
Starting action vuser_end.
vuser_end.c(6): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_1_ECOM_US_NavigationalLoginLogout_OrderTile"
vuser_end.c(6): Notify: Transaction "TCP_07_1_ECOM_US_NavigationalLoginLogout_OrderTile_04_Logout" started.
vuser_end.c(8): web_url("logout") started  	[MsgId: MMSG-26355]
vuser_end.c(8): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_end.c(8): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_end.c(8): Redirecting "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/saml/logout" (redirection depth is 0)  	[MsgId: MMSG-26694]
vuser_end.c(8): To location "https://EOM-MIP-Perf.childrensplace.com:15001/profile/SAML2/Redirect/SLO?SAMLRequest=nVJPb9sgFP8qFnfbgKEYFLualFWylKxZ0%2B6wGwHsUtngGTz1489Om6rtoYcdebzfX73N9fPQJ3%2FNFKx3FUAZBIlxymvrugo83N%2BkJbiuN0EOPR7Fznd%2Bjnfmz2xCTBakC%2BLlqwLz5ISXwQbh5GCCiEocv%2B13AmdQjJOPXvkeJNsFaJ2MZ7XHGMcg8vz77T7dN4f0YKY2U4%2B215NxYeylMpnyg0AUQpQvJK3tTb6y4vzOaDsZFfPj7hYkzbYCEtFOPhWwe8IQkwLjorUFgx3DLTdyydWEMJvGhShdrACGiKWQp5jfwytRUEF4VhTsN0h%2BXdpYrIPX7OIMnt5n%2FjqyDMFMa0xQK7nJ35NcKH8soGb7P5TJjZ8GGb9eXydWp%2B15VcxLoUbZ1hoNklX55yz79TlVYLAjSI6HT1MlQf2wKEJCLv5fHL%2Bdw9GEtajGafNcn4imZcmYYphxyQmVpSoUPNEroiQzqqUEK04YKrBmhGpoEOKalxwjfaKIvmp8Yn2bfji9%2Bh8%3D&SigAlg=http%3A%2F%2Fwww.w3.org%2F2000%2F09%2Fxmldsig%23rsa-sha1&Signature=Jte4M4AEhUvdYLxxBdeLWNNbxjrm%2FMbl0b%2B%2B50%2FV2F0YzwwiyK13s09MxOjiDRttuRnGtAxmWyy8S0N6txIydcL09SbbO8TVdFet9Wfiqv%2FgqvvyoVRiBJiaqzXaLB2jkfgG5Q8LQXFCcmhxe9HfgLcr9tgj8jeaZGt7Bo%2BSETQZlKMla9lz3p2RTWksuEMYJTGHOf1yzIpOtscUQWpIKcArkmHLP4qrDHSyjANke%2FueRghg6uN7zRB%2F4ryDlWYgV0IA0aUjqK%2FzR%2BhAHnHb5v7u1Uj1He%2BdpNCpbFE20R%2BHUedEGuFj4uHbSbLv%2Fu6EJqMCfvIz3gdHhDuOudrlAA%3D%3D"  	[MsgId: MMSG-26693]
vuser_end.c(8): Found resource "https://EOM-MIP-Perf.childrensplace.com:15001/manh/resources/css/mip.css" in HTML "https://EOM-MIP-Perf.childrensplace.com:15001/profile/SAML2/Redirect/SLO?SAMLRequest=nVJPb9sgFP8qFnfbgKEYFLualFWylKxZ0%2B6wGwHsUtngGTz1489Om6rtoYcdebzfX73N9fPQJ3%2FNFKx3FUAZBIlxymvrugo83N%2BkJbiuN0EOPR7Fznd%2Bjnfmz2xCTBakC%2BLlqwLz5ISXwQbh5GCCiEocv%2B13AmdQjJOPXvkeJNsFaJ2MZ7XHGMcg8vz77T7dN4f0YKY2U4%2B215NxYeylMpnyg0AUQpQvJK3tTb6y4vzOaDsZFfPj7hYkzbYCEtFOPhWwe8IQkwLjorUFgx3DLTdyydWEMJvGhShdrACGiKWQp5jfwytRUEF4VhTsN0h%2BXdpYrIPX7OIMnt5n%2FjqyDMFMa0xQK7nJ35NcKH8soGb7P5TJjZ8GGb9eXydWp%2B15VcxLoUbZ1hoNklX55yz79TlVYLAjSI6HT1MlQf2wKEJCLv5fHL%2Bdw9GEtajGafNcn4imZcmYYphxyQmVpSoUPNEroiQzqqUEK04YKrBmhGpoEOKalxwjfaKIvmp8Yn2bfji9%2Bh8%3D&SigAlg=http%3A%2F%2Fwww.w3.org%2F2000%2F09%2Fxmldsig%23rsa-sha1&Signature=Jte4M4AEhUvdYLxxBdeLWNNbxjrm%2FMbl0b%2B%2B50%2FV2F0YzwwiyK13s09MxOjiDRttuRnGtAxmWyy8S0N6txIydcL09SbbO8TVdFet9Wfiqv%2FgqvvyoVRiBJiaqzXaLB2jkfgG5Q8LQXFCcmhxe9HfgLcr9tgj8jeaZGt7Bo%2BSETQZlKMla9lz3p2RTWksuEMYJTGHOf1yzIpOtscUQWpIKcArkmHLP4qrDHSyjANke%2FueRghg6uN7zRB%2F4ryDlWYgV0IA0aUjqK%2FzR%2BhAHnHb5v7u1Uj1He%2BdpNCpbFE20R%2BHUedEGuFj4uHbSbLv%2Fu6EJqMCfvIz3gdHhDuOudrlAA%3D%3D"  	[MsgId: MMSG-26659]
vuser_end.c(8): Found resource "https://EOM-MIP-Perf.childrensplace.com:15001/ext/ext-all.js" in HTML "https://EOM-MIP-Perf.childrensplace.com:15001/profile/SAML2/Redirect/SLO?SAMLRequest=nVJPb9sgFP8qFnfbgKEYFLualFWylKxZ0%2B6wGwHsUtngGTz1489Om6rtoYcdebzfX73N9fPQJ3%2FNFKx3FUAZBIlxymvrugo83N%2BkJbiuN0EOPR7Fznd%2Bjnfmz2xCTBakC%2BLlqwLz5ISXwQbh5GCCiEocv%2B13AmdQjJOPXvkeJNsFaJ2MZ7XHGMcg8vz77T7dN4f0YKY2U4%2B215NxYeylMpnyg0AUQpQvJK3tTb6y4vzOaDsZFfPj7hYkzbYCEtFOPhWwe8IQkwLjorUFgx3DLTdyydWEMJvGhShdrACGiKWQp5jfwytRUEF4VhTsN0h%2BXdpYrIPX7OIMnt5n%2FjqyDMFMa0xQK7nJ35NcKH8soGb7P5TJjZ8GGb9eXydWp%2B15VcxLoUbZ1hoNklX55yz79TlVYLAjSI6HT1MlQf2wKEJCLv5fHL%2Bdw9GEtajGafNcn4imZcmYYphxyQmVpSoUPNEroiQzqqUEK04YKrBmhGpoEOKalxwjfaKIvmp8Yn2bfji9%2Bh8%3D&SigAlg=http%3A%2F%2Fwww.w3.org%2F2000%2F09%2Fxmldsig%23rsa-sha1&Signature=Jte4M4AEhUvdYLxxBdeLWNNbxjrm%2FMbl0b%2B%2B50%2FV2F0YzwwiyK13s09MxOjiDRttuRnGtAxmWyy8S0N6txIydcL09SbbO8TVdFet9Wfiqv%2FgqvvyoVRiBJiaqzXaLB2jkfgG5Q8LQXFCcmhxe9HfgLcr9tgj8jeaZGt7Bo%2BSETQZlKMla9lz3p2RTWksuEMYJTGHOf1yzIpOtscUQWpIKcArkmHLP4qrDHSyjANke%2FueRghg6uN7zRB%2F4ryDlWYgV0IA0aUjqK%2FzR%2BhAHnHb5v7u1Uj1He%2BdpNCpbFE20R%2BHUedEGuFj4uHbSbLv%2Fu6EJqMCfvIz3gdHhDuOudrlAA%3D%3D"  	[MsgId: MMSG-26659]
vuser_end.c(8): web_url("logout") was successful, 461203 body bytes, 2492 header bytes, 6390 chunking overhead bytes  	[MsgId: MMSG-26385]
vuser_end.c(18): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_1_ECOM_US_NavigationalLoginLogout_OrderTile"
vuser_end.c(18): Notify: Transaction "TCP_07_1_ECOM_US_NavigationalLoginLogout_OrderTile_04_Logout" ended with "Pass" status (Duration: 0.4203 Wasted Time: 0.2948).
Ending action vuser_end.
Vuser Terminated.
