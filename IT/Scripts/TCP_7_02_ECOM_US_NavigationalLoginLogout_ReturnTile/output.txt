Virtual User Script started at : 9/29/2017 2:33:57 AM
Starting action vuser_init.
Web Turbo Replay of LoadRunner 12.0.0 for Windows 2008 R2; build 2079 (Jun 17 2014 10:56:12)  	[MsgId: MMSG-27143]
Run mode: HTML  	[MsgId: MMSG-26993]
Run-Time Settings file: "C:\IT\Scripts\TCP_7_02_ECOM_US_NavigationalLoginLogout_ReturnTile\\default.cfg"  	[MsgId: MMSG-27141]
vuser_init.c(3): web_cache_cleanup started  	[MsgId: MMSG-26355]
vuser_init.c(3): web_cache_cleanup was successful  	[MsgId: MMSG-26392]
vuser_init.c(5): web_cleanup_cookies started  	[MsgId: MMSG-26355]
vuser_init.c(5): web_cleanup_cookies was successful  	[MsgId: MMSG-26392]
vuser_init.c(7): web_set_max_html_param_len started  	[MsgId: MMSG-26355]
vuser_init.c(7): web_set_max_html_param_len was successful  	[MsgId: MMSG-26392]
vuser_init.c(11): web_set_sockets_option started  	[MsgId: MMSG-26355]
vuser_init.c(11): web_set_sockets_option was successful  	[MsgId: MMSG-26392]
vuser_init.c(13): web_set_sockets_option started  	[MsgId: MMSG-26355]
vuser_init.c(13): web_set_sockets_option was successful  	[MsgId: MMSG-26392]
vuser_init.c(17): web_reg_save_param started  	[MsgId: MMSG-26355]
vuser_init.c(17): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
vuser_init.c(23): Notify: Saving Parameter "sTestCaseName = TCP_07_2_ECOM_US_NavigationalLoginLogout_ReturnTile".
vuser_init.c(25): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_2_ECOM_US_NavigationalLoginLogout_ReturnTile"
vuser_init.c(25): Notify: Transaction "TCP_07_2_ECOM_US_NavigationalLoginLogout_ReturnTile_01_Launch" started.
vuser_init.c(29): web_url("index.html") started  	[MsgId: MMSG-26355]
vuser_init.c(29): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(29): Notify: Saving Parameter "c_SAML_Request = PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c2FtbDJwOkF1dGhuUmVxdWVzdCB4bWxuczpzYW1sMnA9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpwcm90b2NvbCIgQXNzZXJ0aW9uQ29uc3VtZXJTZXJ2aWNlVVJMPSJodHRwOi8vZW9tLWNhLXBlcmYudGNwaHEudGNwY29ycC5sb2NhbC5jb206MzAwMDAvc2FtbC9TU08vYWxpYXMvY2EiIERlc3RpbmF0aW9uPSJodHRwczovL0VPTS1NSVAtUGVyZi5jaGlsZHJlbnNwbGFjZS5jb206MTUwMDEvcHJvZmlsZS9TQU1MMi9QT1NUL1NTTyIgRm9yY2VBdXRobj0iZmFsc2UiIElEPSJhMmJiNWVhZ2M3ZzcxNTQ2OTgyaWRkYjFiZDVkMGEiIElzUGFzc2l2ZT0iZmFsc2UiIElzc3VlSW5zdGFudD0iMjAxNy0wOS0yOVQwNjozMzo1Ny44NTJaIiBQcm90b2NvbEJpbmRpbmc9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpiaW5kaW5nczpIVFRQLUFydGlmYWN0IiBWZXJzaW9uPSIyLjAiPjxzYW1sMjpJc3N1ZXIgeG1sbnM6c2FtbDI9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iPmNhPC9zYW1sMjpJc3N1ZXI+PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+PGRzOlNpZ25lZEluZm8+PGRzOkNhbm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48ZHM6U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3JzYS1zaGExIi8+PGRzOlJlZmVyZW5jZSBVUkk9IiNhMmJiNWVhZ2M3ZzcxNTQ2OTgyaWRkYjFiZDVkMGEiPjxkczpUcmFuc2Zvcm1zPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48L2RzOlRyYW5zZm9ybXM+PGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNzaGExIi8+PGRzOkRpZ2VzdFZhbHVlPlRGajBxaS9QLzlZVytuMHM3V1pPMFloVEVBdz08L2RzOkRpZ2VzdFZhbHVlPjwvZHM6UmVmZXJlbmNlPjwvZHM6U2lnbmVkSW5mbz48ZHM6U2lnbmF0dXJlVmFsdWU+WHh0c05tUWVmMWdUeGlnUURxLzV3S3hRSitBa2RtZ1dPWHpPWjRpeit4UlVtenZrRjVFNS9ZdzFHS2RReFNGMkl4T1RLMGpFQnRNaGRkUU1pbENkM0pnN25ldEFEZGI5RzlTcTh2eEEvVU1nczNDUWF1RFl1blRSazZnOUR4Qnl0RGdCaHF5SFVrbVI1SDl6OEpMWGliTDlBbm8rU29iUmRmelNzcUFKY2lETEYwc0pTWFkyQ0JuTkxmdGYvUHduM2h5QkZqN1Rpcm5xTnduSHFncUFLVjgxSFd3anJpbG5sMGVMSUtCSy9RT3FiZkFRN3pKL1pRUStHL016Y2w4WFoxNGhhc1U5bllyL0ZVblg2ME9sVG9hTTdnZ0F5U2RDL2FLZUlMVzhQTGxzMk1GVWVFUEtuTDd4WjhTZDRUUnM5QVhSTytVQnRXZzRPbVdDZUQxU3BBPT08L2RzOlNpZ25hdHVyZVZhbHVlPjxkczpLZXlJbmZvPjxkczpYNTA5RGF0YT48ZHM6WDUwOUNlcnRpZmljYXRlPk1JSURCVENDQWUwQ0JGS0RuRkV3RFFZSktvWklodmNOQVFFTkJRQXdSekVMTUFrR0ExVUVCaE1DVlZNeElqQWdCZ05WQkFvTUdVMWgKYm1oaGRIUmhiaUJCYzNOdlkybGhkR1Z6TENCSmJtTXhGREFTQmdOVkJBTU1DM053TG0xaGJtZ3VZMjl0TUI0WERURXpNVEV4TXpFMQpNelUwTlZvWERUTXpNVEV3T0RFMU16VTBOVm93UnpFTE1Ba0dBMVVFQmhNQ1ZWTXhJakFnQmdOVkJBb01HVTFoYm1oaGRIUmhiaUJCCmMzTnZZMmxoZEdWekxDQkpibU14RkRBU0JnTlZCQU1NQzNOd0xtMWhibWd1WTI5dE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0MKQVE4QU1JSUJDZ0tDQVFFQWpSeXA0QXRacHV1TnZORFExakhTc1o2REdHUGlPQ0xaWlBvV3d4NHowNXFjRzQ4MTFMVVhabEZvL05aawpGeHY1bG1kamx2SWI0bzgzbGx1ajFkSWNFSnFCUEpxcEtZOS95SStwdUVtMUFFSjlqT0NIZitjeWREQjM2dmU3M1VtdndaWjBETFNvCmxaVjl5NW1yYUhaaDBLV1paZ2lianRZcCtybEFTa0hiTUI2ZWNZVzZPWlJPWXNoR05ES2tpSSt3ZHFsTXY4SjdNTldRaXlMTlFLc0EKQlFJbXd5dy9lQ05rdHFRWDMvclpNdXpqSVRVMG0xbWZtTUNTR3JybEJEZzJqR2hJNndxYlVPQXQwUk96cysvK3ZpOTIyMzdadnNVQwo5cDRXWko0S0pJK0lwaTROL3hGckZReU1RKzdXNTZQaFVpTmk0eUUvc3FNb3pMZDJrSStKTlFJREFRQUJNQTBHQ1NxR1NJYjNEUUVCCkRRVUFBNElCQVFDRjZpeHFwVXo5ZUY4aU5LQTIrczZLdlFrNUh2enF0OTNYaFlCcURPWTlwNEMzL3NIVHNsdCtxM3oxcG9MRmdqQm8KYW9oNWl5aklkTytudi83SUR1Z2hVdHJLWVFpaUZPbGlQNlFaL2RSNnNqT0VvL29IVVRFNXNNejBaOGVwcVJBYlVwTlUyQzFNRXE5agpUdmpGUXJQcm01T01JazlmaHdPOE1DdmoydmdlZmQ2dHdza3F5Ylp5TDlMTFdBTXdIb0FTY3VTOFBtUVU0eE1aT1RNM3FkVDlFQkVJCjBIY0N2dmxSc0dDTTFQZFFTWTBtZ2ZSUThNRjVCMGhEOVlaanF5ZWRIREFpaEExeW9IRW5sd2JFbnR5Y0lQc3dZcUQ3Zm4wNXM4cU8KdFU0ZFRnaUxqUmpSMmVHcXhDZlZ6cWd2V3VpdnhmczRVWjIyem5MSTRaZ2RUY3FDPC9kczpYNTA5Q2VydGlmaWNhdGU+PC9kczpYNTA5RGF0YT48L2RzOktleUluZm8+PC9kczpTaWduYXR1cmU+PHNhbWwycDpOYW1lSURQb2xpY3kgRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6bmFtZWlkLWZvcm1hdDp1bnNwZWNpZmllZCIvPjwvc2FtbDJwOkF1dGhuUmVxdWVzdD4=".
vuser_init.c(29): web_url("index.html") was successful, 4660 body bytes, 357 header bytes  	[MsgId: MMSG-26386]
vuser_init.c(39): web_set_sockets_option started  	[MsgId: MMSG-26355]
vuser_init.c(39): web_set_sockets_option was successful  	[MsgId: MMSG-26392]
vuser_init.c(41): web_submit_data("SSO") started  	[MsgId: MMSG-26355]
vuser_init.c(41): Notify: Next row for parameter pURL_SSO = 1 [table  = pURL_SSO].
vuser_init.c(41): Notify: Parameter Substitution: parameter "pURL_SSO" =  "https://eom-mip-perf.childrensplace.com:15001"
vuser_init.c(41): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(41): Notify: Parameter Substitution: parameter "c_SAML_Request" =  "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c2FtbDJwOkF1dGhuUmVxdWVzdCB4bWxuczpzYW1sMnA9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpwcm90b2NvbCIgQXNzZXJ0aW9uQ29uc3VtZXJTZXJ2aWNlVVJMPSJodHRwOi8vZW9tLWNhLXBlcmYudGNwaHEudGNwY29ycC5sb2NhbC5jb206MzAwMDAvc2FtbC9TU08vYWxpYXMvY2EiIERlc3RpbmF0aW9uPSJodHRwczovL0VPTS1NSVAtUGVyZi5jaGlsZHJlbnNwbGFjZS5jb206MTUwMDEvcHJvZmlsZS9TQU1MMi9QT1NUL1NTTyIgRm9yY2VBdXRobj0iZmFsc2UiIElEPSJhMmJiNWVhZ2M3ZzcxNTQ2OTgyaWRkYjFiZDVkMGEiIElzUGFzc2l2ZT0iZmFsc2UiIElzc3VlSW5zdGFudD0iMjAxNy0wOS0yOVQwNjozMzo1Ny44NTJaIiBQcm90b2NvbEJpbmRpbmc9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpiaW5kaW5nczpIVFRQLUFydGlmYWN0IiBWZXJzaW9uPSIyLjAiPjxzYW1sMjpJc3N1ZXIgeG1sbnM6c2FtbDI9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iPmNhPC9zYW1sMjpJc3N1ZXI+PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+PGRzOlNpZ25lZEluZm8+PGRzOkNhbm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48ZHM6U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3JzYS1zaGExIi8+PGRzOlJlZmVyZW5jZSBVUkk9IiNhMmJiNWVhZ2M3ZzcxNTQ2OTgyaWRkYjFiZDVkMGEiPjxkczpUcmFuc2Zvcm1zPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48L2RzOlRyYW5zZm9ybXM+PGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNzaGExIi8+PGRzOkRpZ2VzdFZhbHVlPlRGajBxaS9QLzlZVytuMHM3V1pPMFloVEVBdz08L2RzOkRpZ2VzdFZhbHVlPjwvZHM6UmVmZXJlbmNlPjwvZHM6U2lnbmVkSW5mbz48ZHM6U2lnbmF0dXJlVmFsdWU+WHh0c05tUWVmMWdUeGlnUURxLzV3S3hRSitBa2RtZ1dPWHpPWjRpeit4UlVtenZrRjVFNS9ZdzFHS2RReFNGMkl4T1RLMGpFQnRNaGRkUU1pbENkM0pnN25ldEFEZGI5RzlTcTh2eEEvVU1nczNDUWF1RFl1blRSazZnOUR4Qnl0RGdCaHF5SFVrbVI1SDl6OEpMWGliTDlBbm8rU29iUmRmelNzcUFKY2lETEYwc0pTWFkyQ0JuTkxmdGYvUHduM2h5QkZqN1Rpcm5xTnduSHFncUFLVjgxSFd3anJpbG5sMGVMSUtCSy9RT3FiZkFRN3pKL1pRUStHL016Y2w4WFoxNGhhc1U5bllyL0ZVblg2ME9sVG9hTTdnZ0F5U2RDL2FLZUlMVzhQTGxzMk1GVWVFUEtuTDd4WjhTZDRUUnM5QVhSTytVQnRXZzRPbVdDZUQxU3BBPT08L2RzOlNpZ25hdHVyZVZhbHVlPjxkczpLZXlJbmZvPjxkczpYNTA5RGF0YT48ZHM6WDUwOUNlcnRpZmljYXRlPk1JSURCVENDQWUwQ0JGS0RuRkV3RFFZSktvWklodmNOQVFFTkJRQXdSekVMTUFrR0ExVUVCaE1DVlZNeElqQWdCZ05WQkFvTUdVMWgKYm1oaGRIUmhiaUJCYzNOdlkybGhkR1Z6TENCSmJtTXhGREFTQmdOVkJBTU1DM053TG0xaGJtZ3VZMjl0TUI0WERURXpNVEV4TXpFMQpNelUwTlZvWERUTXpNVEV3T0RFMU16VTBOVm93UnpFTE1Ba0dBMVVFQmhNQ1ZWTXhJakFnQmdOVkJBb01HVTFoYm1oaGRIUmhiaUJCCmMzTnZZMmxoZEdWekxDQkpibU14RkRBU0JnTlZCQU1NQzNOd0xtMWhibWd1WTI5dE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0MKQVE4QU1JSUJDZ0tDQVFFQWpSeXA0QXRacHV1TnZORFExakhTc1o2REdHUGlPQ0xaWlBvV3d4NHowNXFjRzQ4MTFMVVhabEZvL05aawpGeHY1bG1kamx2SWI0bzgzbGx1ajFkSWNFSnFCUEpxcEtZOS95SStwdUVtMUFFSjlqT0NIZitjeWREQjM2dmU3M1VtdndaWjBETFNvCmxaVjl5NW1yYUhaaDBLV1paZ2lianRZcCtybEFTa0hiTUI2ZWNZVzZPWlJPWXNoR05ES2tpSSt3ZHFsTXY4SjdNTldRaXlMTlFLc0EKQlFJbXd5dy9lQ05rdHFRWDMvclpNdXpqSVRVMG0xbWZtTUNTR3JybEJEZzJqR2hJNndxYlVPQXQwUk96cysvK3ZpOTIyMzdadnNVQwo5cDRXWko0S0pJK0lwaTROL3hGckZReU1RKzdXNTZQaFVpTmk0eUUvc3FNb3pMZDJrSStKTlFJREFRQUJNQTBHQ1NxR1NJYjNEUUVCCkRRVUFBNElCQVFDRjZpeHFwVXo5ZUY4aU5LQTIrczZLdlFrNUh2enF0OTNYaFlCcURPWTlwNEMzL3NIVHNsdCtxM3oxcG9MRmdqQm8KYW9oNWl5aklkTytudi83SUR1Z2hVdHJLWVFpaUZPbGlQNlFaL2RSNnNqT0VvL29IVVRFNXNNejBaOGVwcVJBYlVwTlUyQzFNRXE5agpUdmpGUXJQcm01T01JazlmaHdPOE1DdmoydmdlZmQ2dHdza3F5Ylp5TDlMTFdBTXdIb0FTY3VTOFBtUVU0eE1aT1RNM3FkVDlFQkVJCjBIY0N2dmxSc0dDTTFQZFFTWTBtZ2ZSUThNRjVCMGhEOVlaanF5ZWRIREFpaEExeW9IRW5sd2JFbnR5Y0lQc3dZcUQ3Zm4wNXM4cU8KdFU0ZFRnaUxqUmpSMmVHcXhDZlZ6cWd2V3VpdnhmczRVWjIyem5MSTRaZ2RUY3FDPC9kczpYNTA5Q2VydGlmaWNhdGU+PC9kczpYNTA5RGF0YT48L2RzOktleUluZm8+PC9kczpTaWduYXR1cmU+PHNhbWwycDpOYW1lSURQb2xpY3kgRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6bmFtZWlkLWZvcm1hdDp1bnNwZWNpZmllZCIvPjwvc2FtbDJwOkF1dGhuUmVxdWVzdD4="
vuser_init.c(41): Redirecting "https://eom-mip-perf.childrensplace.com:15001/profile/SAML2/POST/SSO" (redirection depth is 0)  	[MsgId: MMSG-26694]
vuser_init.c(41): To location "https://eom-mip-perf.childrensplace.com:15001/AuthnEngine"  	[MsgId: MMSG-26693]
vuser_init.c(41): Redirecting "https://eom-mip-perf.childrensplace.com:15001/AuthnEngine" (redirection depth is 1)  	[MsgId: MMSG-26694]
vuser_init.c(41): To location "https://eom-mip-perf.childrensplace.com:15001/Authn/RemoteUser?loginContextKey=cf50f6ff-2166-4876-968f-d72305208972"  	[MsgId: MMSG-26693]
vuser_init.c(41): Redirecting "https://eom-mip-perf.childrensplace.com:15001/Authn/RemoteUser?loginContextKey=cf50f6ff-2166-4876-968f-d72305208972" (redirection depth is 2)  	[MsgId: MMSG-26694]
vuser_init.c(41): To location "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26693]
vuser_init.c(41): Found resource "https://eom-mip-perf.childrensplace.com:15001/ext/resources/css/ext-all.css" in HTML "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26659]
vuser_init.c(41): Found resource "https://eom-mip-perf.childrensplace.com:15001/manh/resources/css/mip.css" in HTML "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26659]
vuser_init.c(41): Found resource "https://eom-mip-perf.childrensplace.com:15001/ext/ext-all.js" in HTML "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26659]
vuser_init.c(41): Found resource "https://eom-mip-perf.childrensplace.com:15001/app.js" in HTML "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26659]
vuser_init.c(41): web_submit_data("SSO") was successful, 462743 body bytes, 2796 header bytes, 6426 chunking overhead bytes  	[MsgId: MMSG-26385]
vuser_init.c(53): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_2_ECOM_US_NavigationalLoginLogout_ReturnTile"
vuser_init.c(53): Notify: Transaction "TCP_07_2_ECOM_US_NavigationalLoginLogout_ReturnTile_01_Launch" ended with "Pass" status (Duration: 1.6952 Wasted Time: 1.3255).
vuser_init.c(59): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_2_ECOM_US_NavigationalLoginLogout_ReturnTile"
vuser_init.c(59): Notify: Transaction "TCP_07_2_ECOM_US_NavigationalLoginLogout_ReturnTile_02_Login" started.
vuser_init.c(61): web_custom_request("j_spring_security_check") started  	[MsgId: MMSG-26355]
vuser_init.c(61): Notify: Next row for parameter pURL_SSO = 1 [table  = pURL_SSO].
vuser_init.c(61): Notify: Parameter Substitution: parameter "pURL_SSO" =  "https://eom-mip-perf.childrensplace.com:15001"
vuser_init.c(61): Notify: Parameter Substitution: parameter "pUserName" =  "User051"
vuser_init.c(61): Notify: Next row for parameter pPassword = 1 [table  = pPassword].
vuser_init.c(61): Notify: Parameter Substitution: parameter "pPassword" =  "password"
vuser_init.c(61): Redirecting "https://eom-mip-perf.childrensplace.com:15001/j_spring_security_check" (redirection depth is 0)  	[MsgId: MMSG-26694]
vuser_init.c(61): To location "https://eom-mip-perf.childrensplace.com:15001/Authn/RemoteUser?loginContextKey=cf50f6ff-2166-4876-968f-d72305208972"  	[MsgId: MMSG-26693]
vuser_init.c(61): Redirecting "https://eom-mip-perf.childrensplace.com:15001/Authn/RemoteUser?loginContextKey=cf50f6ff-2166-4876-968f-d72305208972" (redirection depth is 1)  	[MsgId: MMSG-26694]
vuser_init.c(61): To location "https://eom-mip-perf.childrensplace.com:15001/profile/SAML2/POST/SSO"  	[MsgId: MMSG-26693]
vuser_init.c(61): Redirecting "https://eom-mip-perf.childrensplace.com:15001/profile/SAML2/POST/SSO" (redirection depth is 2)  	[MsgId: MMSG-26694]
vuser_init.c(61): To location "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/saml/SSO/alias/ca?SAMLart=AAQAAf5QSaNfZd4JBmYl5vT6TLHqylbwcupm%2FRfV1qYBvA1X%2FL%2FPU5zqqLA%3D"  	[MsgId: MMSG-26693]
vuser_init.c(61): Redirecting "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/saml/SSO/alias/ca?SAMLart=AAQAAf5QSaNfZd4JBmYl5vT6TLHqylbwcupm%2FRfV1qYBvA1X%2FL%2FPU5zqqLA%3D" (redirection depth is 3)  	[MsgId: MMSG-26694]
vuser_init.c(61): To location "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26693]
vuser_init.c(61): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/extstyles" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(61): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/ext/ext-all.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(61): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/ext/ext-theme-neptune.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(61): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/hammer.min.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(61): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/overrides.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(61): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/webtop.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(61): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/bootstrap.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(61): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/webtop-overrides.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(61): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/mps.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(61): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/html2canvas.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(61): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/notifications/portal.min.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(61): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/notifications/atmosphere-min.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(61): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/lps/resources/common/scripts/3rdparty/jquery/jquery.min.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(61): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/highcharts/js/highcharts.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(61): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/highcharts/js/highcharts-more.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(61): web_custom_request("j_spring_security_check") was successful, 2034925 body bytes, 6524 header bytes  	[MsgId: MMSG-26386]
vuser_init.c(73): web_url("ping.jsp") started  	[MsgId: MMSG-26355]
vuser_init.c(73): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(73): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(73): web_url("ping.jsp") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
vuser_init.c(83): web_url("ping.jsp_2") started  	[MsgId: MMSG-26355]
vuser_init.c(83): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(83): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(83): web_url("ping.jsp_2") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
vuser_init.c(96): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_2_ECOM_US_NavigationalLoginLogout_ReturnTile"
vuser_init.c(96): Notify: Transaction "TCP_07_2_ECOM_US_NavigationalLoginLogout_ReturnTile_02_Login" ended with "Pass" status (Duration: 0.7489 Wasted Time: 0.2045).
Ending action vuser_init.
Running Vuser...
Starting iteration 1.
Starting action Action.
Action.c(5): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_2_ECOM_US_NavigationalLoginLogout_ReturnTile"
Action.c(5): Notify: Transaction "TCP_07_2_ECOM_US_NavigationalLoginLogout_ReturnTile_03_Return_Number_Lookup_From_Return_Tile" started.
Action.c(7): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(7): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(13): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(13): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(19): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(19): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(25): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(25): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(31): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(31): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(37): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(37): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(43): Notify: Saving Parameter "p1 = {"returnOrderSearchCriteria":{"entityType":"All","orderNumber":"".
Action.c(45): Notify: Parameter Substitution: parameter "p1" =  "{"returnOrderSearchCriteria":{"entityType":"All","orderNumber":""
Action.c(45): 6060128
Action.c(47): Notify: Saving Parameter "p2 = ","parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":10,"customerInfo":"","customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}".
Action.c(49): Notify: Parameter Substitution: parameter "p2" =  "","parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":10,"customerInfo":"","customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}"
Action.c(49): 114459480
Action.c(51): web_set_user started  	[MsgId: MMSG-26355]
Action.c(51): Notify: Parameter Substitution: parameter "pUserName" =  "User051"
Action.c(51): Notify: Next row for parameter pPassword = 1 [table  = pPassword].
Action.c(51): Notify: Parameter Substitution: parameter "pPassword" =  "password"
Action.c(51): web_set_user was successful  	[MsgId: MMSG-26392]
Action.c(53): web_custom_request("returnList") started  	[MsgId: MMSG-26355]
Action.c(53): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(53): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(53): Notify: Parameter Substitution: parameter "p1" =  "{"returnOrderSearchCriteria":{"entityType":"All","orderNumber":""
Action.c(53): Notify: Parameter Substitution: parameter "pReturn_Order" =  "000064833"
Action.c(53): Notify: Parameter Substitution: parameter "p2" =  "","parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":10,"customerInfo":"","customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}"
Action.c(53): Notify: Saving Parameter "c_externalCustomerId = 64003958".
Action.c(53): Notify: Saving Parameter "c_First_Name = Data Dump".
Action.c(53): Notify: Saving Parameter "c_Last_Name = Data Dump".
Action.c(53): Notify: Saving Parameter "c_Phone_Number = 0000000000".
Action.c(53): Notify: Saving Parameter "c_Email_Id = datadump@manh.com".
Action.c(53): Notify: Saving Parameter "c_OrderNumber = 70460002010".
Action.c(53): web_custom_request("returnList") was successful, 539 body bytes, 186 header bytes, 12 chunking overhead bytes  	[MsgId: MMSG-26385]
Action.c(66): web_custom_request("windows") started  	[MsgId: MMSG-26355]
Action.c(66): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(66): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(66): web_custom_request("windows") was successful, 24 body bytes, 186 header bytes, 11 chunking overhead bytes  	[MsgId: MMSG-26385]
Action.c(78): web_url("ping.jsp_3") started  	[MsgId: MMSG-26355]
Action.c(78): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(78): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(78): web_url("ping.jsp_3") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Action.c(88): Notify: Saving Parameter "p3 = {"companyParameterIdList":{"parameterList":["CURRENCY_USED","EXT_CUSTOMER_ID_GENERATION","USE_DELIVERY_ZONE","DEFAULT_SHIP_VIA","DEFAULT_ORDER_TYPE","REQUIRES_VALIDATION_FOR_PAYMENT_AUTHORIZATION","REQUIRES_VALIDATION_FOR_PAYMENT_SETTLEMENT","CHARGE_HANDLING_STRATEGY","AUTOMATIC_PROMOTION_VALUE","ITEM_PRICE_EXPIRY_DATE_EXTENSION_DAYS","ITEM_PRICE_SCHEDULER_INTERVAL_IN_DAYS","DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE","VIEW_STORE_LEVEL_PRICES","NETWORK_LEVEL_VIEW","FACILITY_LEVEL_VIEW"]}}".
Action.c(91): Notify: Parameter Substitution: parameter "p3" =  "{"companyParameterIdList":{"parameterList":["CURRENCY_USED","EXT_CUSTOMER_ID_GENERATION","USE_DELIVERY_ZONE","DEFAULT_SHIP_VIA","DEFAULT_ORDER_TYPE","REQUIRES_VALIDATION_FOR_PAYMENT_AUTHORIZATION","REQUIRES_VALIDATION_FOR_PAYMENT_SETTLEMENT","CHARGE_HANDLING_STRATEGY","AUTOMATIC_PROMOTION_VALUE","ITEM_PRICE_EXPIRY_DATE_EXTENSION_DAYS","ITEM_PRICE_SCHEDULER_INTERVAL_IN_DAYS","DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE","VIEW_STORE_LEVEL_PRICES","NETWORK_LEVEL_VIEW","FACILITY_LEVEL_VIEW"]}}"
Action.c(91): 114531944
Action.c(93): web_set_user started  	[MsgId: MMSG-26355]
Action.c(93): Notify: Parameter Substitution: parameter "pUserName" =  "User051"
Action.c(93): Notify: Next row for parameter pPassword = 1 [table  = pPassword].
Action.c(93): Notify: Parameter Substitution: parameter "pPassword" =  "password"
Action.c(93): web_set_user was successful  	[MsgId: MMSG-26392]
Action.c(95): web_custom_request("companyParameterList") started  	[MsgId: MMSG-26355]
Action.c(95): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(95): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(95): Notify: Parameter Substitution: parameter "p3" =  "{"companyParameterIdList":{"parameterList":["CURRENCY_USED","EXT_CUSTOMER_ID_GENERATION","USE_DELIVERY_ZONE","DEFAULT_SHIP_VIA","DEFAULT_ORDER_TYPE","REQUIRES_VALIDATION_FOR_PAYMENT_AUTHORIZATION","REQUIRES_VALIDATION_FOR_PAYMENT_SETTLEMENT","CHARGE_HANDLING_STRATEGY","AUTOMATIC_PROMOTION_VALUE","ITEM_PRICE_EXPIRY_DATE_EXTENSION_DAYS","ITEM_PRICE_SCHEDULER_INTERVAL_IN_DAYS","DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE","VIEW_STORE_LEVEL_PRICES","NETWORK_LEVEL_VIEW","FACILITY_LEVEL_VIEW"]}}"
Action.c(95): web_custom_request("companyParameterList") was successful, 377 body bytes, 233 header bytes, 12 chunking overhead bytes  	[MsgId: MMSG-26385]
Action.c(109): Notify: Saving Parameter "p4 = {"customerOrderSearchCriteria":{"entityType":"All","orderNumber":".
Action.c(111): Notify: Parameter Substitution: parameter "p4" =  "{"customerOrderSearchCriteria":{"entityType":"All","orderNumber":"
Action.c(111): 5892744
Action.c(113): Notify: Saving Parameter "p5 = ,"parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":1000,"customerInfo":{"customerFirstName":"".
Action.c(115): Notify: Parameter Substitution: parameter "p5" =  ","parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":1000,"customerInfo":{"customerFirstName":""
Action.c(115): 113652688
Action.c(117): Notify: Saving Parameter "p6 = ","customField1":"","customField2":"","customField3":"","customField4":"","customField5":"","customField6":"","customField7":"","customField8":"","customField9":"","customField10":""},"customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}".
Action.c(120): Notify: Parameter Substitution: parameter "p6" =  "","customField1":"","customField2":"","customField3":"","customField4":"","customField5":"","customField6":"","customField7":"","customField8":"","customField9":"","customField10":""},"customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}"
Action.c(120): 114063904
Action.c(124): web_set_user started  	[MsgId: MMSG-26355]
Action.c(124): Notify: Parameter Substitution: parameter "pUserName" =  "User051"
Action.c(124): Notify: Next row for parameter pPassword = 1 [table  = pPassword].
Action.c(124): Notify: Parameter Substitution: parameter "pPassword" =  "password"
Action.c(124): web_set_user was successful  	[MsgId: MMSG-26392]
Action.c(126): web_custom_request("customerOrderAndTransactionList") started  	[MsgId: MMSG-26355]
Action.c(126): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(126): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(126): Notify: Parameter Substitution: parameter "p4" =  "{"customerOrderSearchCriteria":{"entityType":"All","orderNumber":"
Action.c(126): Notify: Parameter Substitution: parameter "c_OrderNumber" =  "70460002010"
Action.c(126): Notify: Parameter Substitution: parameter "p5" =  ","parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":1000,"customerInfo":{"customerFirstName":""
Action.c(126): Notify: Parameter Substitution: parameter "c_First_Name" =  "Data Dump"
Action.c(126): Notify: Parameter Substitution: parameter "c_Last_Name" =  "Data Dump"
Action.c(126): Notify: Parameter Substitution: parameter "c_First_Name" =  "Data Dump"
Action.c(126): Notify: Parameter Substitution: parameter "c_Last_Name" =  "Data Dump"
Action.c(126): Notify: Parameter Substitution: parameter "c_Email_Id" =  "datadump@manh.com"
Action.c(126): Notify: Parameter Substitution: parameter "c_Phone_Number" =  "0000000000"
Action.c(126): Notify: Parameter Substitution: parameter "c_externalCustomerId" =  "64003958"
Action.c(126): Notify: Parameter Substitution: parameter "p6" =  "","customField1":"","customField2":"","customField3":"","customField4":"","customField5":"","customField6":"","customField7":"","customField8":"","customField9":"","customField10":""},"customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}"
Action.c(126): web_custom_request("customerOrderAndTransactionList") was successful, 556 body bytes, 186 header bytes, 12 chunking overhead bytes  	[MsgId: MMSG-26385]
Action.c(139): web_url("ping.jsp_4") started  	[MsgId: MMSG-26355]
Action.c(139): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(139): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(139): web_url("ping.jsp_4") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Action.c(149): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_2_ECOM_US_NavigationalLoginLogout_ReturnTile"
Action.c(149): Notify: Transaction "TCP_07_2_ECOM_US_NavigationalLoginLogout_ReturnTile_03_Return_Number_Lookup_From_Return_Tile" ended with "Pass" status (Duration: 2.1309 Wasted Time: 0.0069).
Action.c(153): web_url("ping.jsp_5") started  	[MsgId: MMSG-26355]
Action.c(153): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(153): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(153): web_url("ping.jsp_5") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Action.c(163): web_url("ping.jsp_6") started  	[MsgId: MMSG-26355]
Action.c(163): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(163): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(163): web_url("ping.jsp_6") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Ending action Action.
Ending iteration 1.
Ending Vuser...
Starting action vuser_end.
vuser_end.c(5): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_2_ECOM_US_NavigationalLoginLogout_ReturnTile"
vuser_end.c(5): Notify: Transaction "TCP_07_2_ECOM_US_NavigationalLoginLogout_ReturnTile_04_Logout" started.
vuser_end.c(7): web_url("logout") started  	[MsgId: MMSG-26355]
vuser_end.c(7): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_end.c(7): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_end.c(7): Redirecting "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/saml/logout" (redirection depth is 0)  	[MsgId: MMSG-26694]
vuser_end.c(7): To location "https://EOM-MIP-Perf.childrensplace.com:15001/profile/SAML2/Redirect/SLO?SAMLRequest=nVLNbqMwEH4V5DtgbEyMFagqZSshJdts0%2B6hN2MPrSUwrG1WffyFtKnaHnrYgw%2Beme9vNNurl6GP%2FoLzZrQVyhKMIrBq1MY%2BVejh%2Fibm6Kreejn0ZBL78Wmcwx38mcGHaEFaL15bFZqdFaP0xgsrB%2FAiKHG6PuwFSbCY3BhGNfYo2i1AY2U4qz2HMHmRpj9uD%2FGhOcZHcF2ink2vHVg%2F9VJBosZBZAzjLF1IOtNDurKS9A60caBCetrfoqjZVUgSzUFlkrKcc1YwuuFcs6LNjFxetgx5P0NjfZA2VIjgbBPjMiblPS4EzQUmyQJ9RNHvyzYW6%2BgtuziD3cfM30eW3oNbY6JayW36keRC%2BXMBNbv%2FoYxuRjfI8P34WjE67s6jYl4WCsp0BjSKVuVfs%2BzXr6vQYCYUnY5fqkqi%2BmFRxCy7%2BH91%2FH4OJ%2FDrohqr4aUGKhXbKGB5WVJSEJ1LQnLeMsqKvGuVzmlXYlowwgjlDHihSqqx7DLKW0LaN40vrO%2FVT6dX%2FwM%3D&SigAlg=http%3A%2F%2Fwww.w3.org%2F2000%2F09%2Fxmldsig%23rsa-sha1&Signature=dN5oVVUWp4Z8nZOYPB6U2AY1V0eMncGaSiHTsEv3sQvU8eiploXqZLsyJ9JAwU57J%2Bvgmb2l53l9KytZ6FBtM5c%2BzYy9r572jr0sycK5G51FIzOdW1DRaOs3I%2Fumi8eu9crojr8h1ajBo4gHO4O8JVIZc1qa3zWxTudUZk8XD4kWCA0Sx5cy7HNkoEPMEH%2BRxIEDw3FfLdRT5knBaz1ybATGIgV6ZvMLxloRuvVhFZ3V5V9C5Kxr9nkf3VkV3C%2BfQkMJnUADt%2FbQV224qTh%2FUUl13Uu9ZOcO2AEd7lIEQLL1mVMI8ZQgvxCDgqcmDqxQVpeT0G8NXu1qA3xMxhJjUg%3D%3D"  	[MsgId: MMSG-26693]
vuser_end.c(7): Found resource "https://EOM-MIP-Perf.childrensplace.com:15001/manh/resources/css/mip.css" in HTML "https://EOM-MIP-Perf.childrensplace.com:15001/profile/SAML2/Redirect/SLO?SAMLRequest=nVLNbqMwEH4V5DtgbEyMFagqZSshJdts0%2B6hN2MPrSUwrG1WffyFtKnaHnrYgw%2Beme9vNNurl6GP%2FoLzZrQVyhKMIrBq1MY%2BVejh%2Fibm6Kreejn0ZBL78Wmcwx38mcGHaEFaL15bFZqdFaP0xgsrB%2FAiKHG6PuwFSbCY3BhGNfYo2i1AY2U4qz2HMHmRpj9uD%2FGhOcZHcF2ink2vHVg%2F9VJBosZBZAzjLF1IOtNDurKS9A60caBCetrfoqjZVUgSzUFlkrKcc1YwuuFcs6LNjFxetgx5P0NjfZA2VIjgbBPjMiblPS4EzQUmyQJ9RNHvyzYW6%2BgtuziD3cfM30eW3oNbY6JayW36keRC%2BXMBNbv%2FoYxuRjfI8P34WjE67s6jYl4WCsp0BjSKVuVfs%2BzXr6vQYCYUnY5fqkqi%2BmFRxCy7%2BH91%2FH4OJ%2FDrohqr4aUGKhXbKGB5WVJSEJ1LQnLeMsqKvGuVzmlXYlowwgjlDHihSqqx7DLKW0LaN40vrO%2FVT6dX%2FwM%3D&SigAlg=http%3A%2F%2Fwww.w3.org%2F2000%2F09%2Fxmldsig%23rsa-sha1&Signature=dN5oVVUWp4Z8nZOYPB6U2AY1V0eMncGaSiHTsEv3sQvU8eiploXqZLsyJ9JAwU57J%2Bvgmb2l53l9KytZ6FBtM5c%2BzYy9r572jr0sycK5G51FIzOdW1DRaOs3I%2Fumi8eu9crojr8h1ajBo4gHO4O8JVIZc1qa3zWxTudUZk8XD4kWCA0Sx5cy7HNkoEPMEH%2BRxIEDw3FfLdRT5knBaz1ybATGIgV6ZvMLxloRuvVhFZ3V5V9C5Kxr9nkf3VkV3C%2BfQkMJnUADt%2FbQV224qTh%2FUUl13Uu9ZOcO2AEd7lIEQLL1mVMI8ZQgvxCDgqcmDqxQVpeT0G8NXu1qA3xMxhJjUg%3D%3D"  	[MsgId: MMSG-26659]
vuser_end.c(7): Found resource "https://EOM-MIP-Perf.childrensplace.com:15001/ext/ext-all.js" in HTML "https://EOM-MIP-Perf.childrensplace.com:15001/profile/SAML2/Redirect/SLO?SAMLRequest=nVLNbqMwEH4V5DtgbEyMFagqZSshJdts0%2B6hN2MPrSUwrG1WffyFtKnaHnrYgw%2Beme9vNNurl6GP%2FoLzZrQVyhKMIrBq1MY%2BVejh%2Fibm6Kreejn0ZBL78Wmcwx38mcGHaEFaL15bFZqdFaP0xgsrB%2FAiKHG6PuwFSbCY3BhGNfYo2i1AY2U4qz2HMHmRpj9uD%2FGhOcZHcF2ink2vHVg%2F9VJBosZBZAzjLF1IOtNDurKS9A60caBCetrfoqjZVUgSzUFlkrKcc1YwuuFcs6LNjFxetgx5P0NjfZA2VIjgbBPjMiblPS4EzQUmyQJ9RNHvyzYW6%2BgtuziD3cfM30eW3oNbY6JayW36keRC%2BXMBNbv%2FoYxuRjfI8P34WjE67s6jYl4WCsp0BjSKVuVfs%2BzXr6vQYCYUnY5fqkqi%2BmFRxCy7%2BH91%2FH4OJ%2FDrohqr4aUGKhXbKGB5WVJSEJ1LQnLeMsqKvGuVzmlXYlowwgjlDHihSqqx7DLKW0LaN40vrO%2FVT6dX%2FwM%3D&SigAlg=http%3A%2F%2Fwww.w3.org%2F2000%2F09%2Fxmldsig%23rsa-sha1&Signature=dN5oVVUWp4Z8nZOYPB6U2AY1V0eMncGaSiHTsEv3sQvU8eiploXqZLsyJ9JAwU57J%2Bvgmb2l53l9KytZ6FBtM5c%2BzYy9r572jr0sycK5G51FIzOdW1DRaOs3I%2Fumi8eu9crojr8h1ajBo4gHO4O8JVIZc1qa3zWxTudUZk8XD4kWCA0Sx5cy7HNkoEPMEH%2BRxIEDw3FfLdRT5knBaz1ybATGIgV6ZvMLxloRuvVhFZ3V5V9C5Kxr9nkf3VkV3C%2BfQkMJnUADt%2FbQV224qTh%2FUUl13Uu9ZOcO2AEd7lIEQLL1mVMI8ZQgvxCDgqcmDqxQVpeT0G8NXu1qA3xMxhJjUg%3D%3D"  	[MsgId: MMSG-26659]
vuser_end.c(7): web_url("logout") was successful, 461203 body bytes, 2492 header bytes, 6390 chunking overhead bytes  	[MsgId: MMSG-26385]
vuser_end.c(17): web_url("miplogout") started  	[MsgId: MMSG-26355]
vuser_end.c(17): Notify: Next row for parameter pURL_SSO = 1 [table  = pURL_SSO].
vuser_end.c(17): Notify: Parameter Substitution: parameter "pURL_SSO" =  "https://eom-mip-perf.childrensplace.com:15001"
vuser_end.c(17): Redirecting "https://eom-mip-perf.childrensplace.com:15001/miplogout" (redirection depth is 0)  	[MsgId: MMSG-26694]
vuser_end.c(17): To location "https://eom-mip-perf.childrensplace.com:15001/SLOServlet?finish"  	[MsgId: MMSG-26693]
vuser_end.c(17): Redirecting "https://eom-mip-perf.childrensplace.com:15001/SLOServlet?finish" (redirection depth is 1)  	[MsgId: MMSG-26694]
vuser_end.c(17): To location "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/saml/SingleLogout/alias/ca?SAMLResponse=fVLLasMwEPwVo7stW35GxAmlIRBIL02aQy9lK68TgS25Xrm0f1%2Fn1ZZSIliBtLM7syNN5x9t471jT9qakkVByDw0ylba7Ev2tF36BZvPpgRtIzq5tns7uEekzhpCbyw1JM%2B5kg29kRZIkzTQIkmn5ObuYS1FEMqut84q2zBvgeS0AXeiOzjXSc7Rtr4Cv8O%2BDpzqDm%2FHXdm%2BCxqroAmUbWUcjosfufhm1NbgWQuHRgNxBcxbLUr2Ek3yGJJa5EVeZZlSNWKYRmIMlYgqFyPMXPVvbclAVAWqCOI0KYo0S%2BO8KKo0e400jBGNcKIBV4YcGFcyEUa5H058MdmGmYwTGYogzSfPzNtdLRzHZRfD5Km4%2F%2B3TbZuACPujNcxb2r4Fdxt%2BvNGVX5%2BgEo3T7pPNWt1N%2BW%2F%2B7%2BfbOHAD%2FTne2wq9HTQD3iajE1puBqWQiPHZheSnLf%2F3l8y%2BAA%3D%3D&SigAlg=http%3A%2F%2Fwww.w3.org%2F2000%2F09%2Fxmldsig%23rsa-sha1&Signature=NBb5uANL3Kvfya4TxLaPogqRDkUGYvvGahATrYWdTxkXO%2FaLJ4XIqzEoc6rUulaa3z793%2F4EX%2FrWo1IE4W669ZCQhavtRt1GsqL3LNLZF5e6p%2B1hPSJ4XKpadjseCa73lzq1CFlA%2F7%2FtCeS%2FfjHeDFiZB3yzVU0rh3Fv%2BWyChPFJAo3liImlGZ7Kog0%2Fc6d%2FWLPhxBDalJ7fYDFcoSkCoaoizYZKbIW6MVeH2ilp18%2FZXHkYFqspzVLjyX%2Bcpy%2FyUFT7jKHFv3eTafZRgHEfnrYdDlOj3yDvFaJEsasagoS96u3bMjHIG%2Bt%2BXi77KU6y0gw7Rq%2FosmFkAmUstbjpwA%3D%3D"  	[MsgId: MMSG-26693]
vuser_end.c(17): Redirecting "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/saml/SingleLogout/alias/ca?SAMLResponse=fVLLasMwEPwVo7stW35GxAmlIRBIL02aQy9lK68TgS25Xrm0f1%2Fn1ZZSIliBtLM7syNN5x9t471jT9qakkVByDw0ylba7Ev2tF36BZvPpgRtIzq5tns7uEekzhpCbyw1JM%2B5kg29kRZIkzTQIkmn5ObuYS1FEMqut84q2zBvgeS0AXeiOzjXSc7Rtr4Cv8O%2BDpzqDm%2FHXdm%2BCxqroAmUbWUcjosfufhm1NbgWQuHRgNxBcxbLUr2Ek3yGJJa5EVeZZlSNWKYRmIMlYgqFyPMXPVvbclAVAWqCOI0KYo0S%2BO8KKo0e400jBGNcKIBV4YcGFcyEUa5H058MdmGmYwTGYogzSfPzNtdLRzHZRfD5Km4%2F%2B3TbZuACPujNcxb2r4Fdxt%2BvNGVX5%2BgEo3T7pPNWt1N%2BW%2F%2B7%2BfbOHAD%2FTne2wq9HTQD3iajE1puBqWQiPHZheSnLf%2F3l8y%2BAA%3D%3D&SigAlg=http%3A%2F%2Fwww.w3.org%2F2000%2F09%2Fxmldsig%23rsa-sha1&Signature=NBb5uANL3Kvfya4TxLaPogqRDkUGYvvGahATrYWdTxkXO%2FaLJ4XIqzEoc6rUulaa3z793%2F4EX%2FrWo1IE4W669ZCQhavtRt1GsqL3LNLZF5e6p%2B1hPSJ4XKpadjseCa73lzq1CFlA%2F7%2FtCeS%2FfjHeDFiZB3yzVU0rh3Fv%2BWyChPFJAo3liImlGZ7Kog0%2Fc6d%2FWLPhxBDalJ7fYDFcoSkCoaoizYZKbIW6MVeH2ilp18%2FZXHkYFqspzVLjyX%2Bcpy%2FyUFT7jKHFv3eTafZRgHEfnrYdDlOj3yDvFaJEsasagoS96u3bMjHIG%2Bt%2BXi77KU6y0gw7Rq%2FosmFkAmUstbjpwA%3D%3D" (redirection depth is 2)  	[MsgId: MMSG-26694]
vuser_end.c(17): To location "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/logout.jsp"  	[MsgId: MMSG-26693]
vuser_end.c(17): web_url("miplogout") was successful, 1082 body bytes, 2212 header bytes  	[MsgId: MMSG-26386]
vuser_end.c(27): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_07_2_ECOM_US_NavigationalLoginLogout_ReturnTile"
vuser_end.c(27): Notify: Transaction "TCP_07_2_ECOM_US_NavigationalLoginLogout_ReturnTile_04_Logout" ended with "Pass" status (Duration: 0.2968 Wasted Time: 0.0878).
Ending action vuser_end.
Vuser Terminated.
