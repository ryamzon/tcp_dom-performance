/*
 * File: mps/Application.js
 *
 */

/**
 * Represents a MPS 2.0 Application.
 * Used to initialize and launch individual applications.
 * @docauthor Karan Selvaraj
 */
Ext.define('Manh.mps.Application', {
    alternateClassName : [
        'Manh.mps.webtop.common.Application'
    ],
    
    mixins : {
        observable : 'Ext.util.Observable'
    },
    
    config : {
        /**
         * @cfg {Number} id
         * The id of this Application.
         */
        id : 0,
        
        /**
         * @cfg {String} name
         * The name of your application. This will also be the namespace for your views, controllers
         * models and stores. Don't use spaces or special characters in the name.
         */
        name : '',
        
        /**
         * @cfg {String} appFolder
         * The path to the directory which contains all application's classes.
         * This path will be registered via {@link Ext.Loader#setPath} for the namespace specified
         * in the {@link #name name} config.
         */
        appFolder : ''
    },
    
    /**
     * @property {Object} paths
     * Additional load paths to add to Ext.Loader.
     * See {@link Ext.Loader#paths} config for more details.
     */
    
    /**
     * @cfg {String[]} requires
     * @member Ext.Class
     * List of classes that have to be loaded before instantiating this class.
     */
    
    constructor : function (config) {
        var me = this,
        paths;
         
        me.mixins.observable.constructor.call(me, config);
        me.initConfig(config);
        
        paths = me.paths || {};
        
        if (me.name && me.appFolder) {
            paths[me.name] = me.appFolder;
        }
        
        Ext.Loader.setPath(paths);
        
        me.init(me);
        me.onBeforeLaunch();
    },
    /**
     * A template method that is called when your application boots. It is called before the
     * {@link Manh.mps.webtop.Application Application}'s launch function is executed so gives a hook point to run any code before
     * your Workspace/Tile is created.
     *
     * @param {Manh.mps.webtop.Application} application
     * @template
     */
    init : Ext.emptyFn,
    /**
     * @method
     * @template
     * Called automatically when the page has completely loaded. This is an empty function that should be
     * overridden by each application that needs to take action on page load.
     * @param {String} profile The detected application profile
     * @return {Boolean} By default, the Application will dispatch to the configured startup controller and
     * action immediately after running the launch function. Return false to prevent this behavior.
     */
    launch : Ext.emptyFn,
    /**
     * @private
     */
    onBeforeLaunch : function () {
        var me = this;
        
        /*
         * <Future Use>
         * Pre-launch actions go here
         */
        
        me.launch.call(this.scope || this);
        me.launched = true;
        me.fireEvent('launch', this);
    }
    
});
