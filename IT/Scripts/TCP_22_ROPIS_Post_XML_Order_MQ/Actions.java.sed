/*
 * Example WebSphere MQ LoadRunner script (written in Java)
 * 
 * Script Description: 
 *     This script puts a message on a queue, then gets a response message from 
 *     another queue.
 *
 * You will probably need to add the following jar files to your classpath
 *   - com.ibm.mq.jar
 *   - connector.jar
 *   - com.ibm.mq.jmqi.jar
 *   - com.ibm.mq.headers.jar
 *   - com.ibm.mq.commonservices.jar
 */
 
import lrapi.lr;
import com.ibm.mq.*;
import java.io.*;
 
public class Actions
{

   // Variables used by more than one method
    String queueMgrName = "PERF.TCP.QMGR";
    
    String putQueueName = "TCP.TO.MIF.ROPISCUSTOMER.ORDER";
    
    String getQueueName = "TCP.TO.MIF.ROPISCUSTOMER.ORDER";
    
    StringBuffer idBuffer = null;
    FileWriter idFile = null;
	BufferedWriter idWriter = null;
 
    MQQueueManager queueMgr = null;
    MQQueue getQueue = null;
    MQQueue putQueue = null;
    MQPutMessageOptions pmo = new MQPutMessageOptions();
    MQGetMessageOptions gmo = new MQGetMessageOptions();
    MQMessage requestMsg = new MQMessage();
    MQMessage responseMsg = new MQMessage();
    String msgBody = null;
 
    public int init() throws Throwable {
        // Open a connection to the queue manager and the put/get queues
        try {
            // As values set in the MQEnvironment class take effect when the 
            // MQQueueManager constructor is called, you must set the values 
            // in the MQEnvironment class before you construct an MQQueueManager object.
            MQEnvironment.hostname="10.18.1.21";
            
            MQEnvironment.port=1414;
            
            MQEnvironment.channel = "TCP.SVRCONN.CHANNEL";
            
            queueMgr = new MQQueueManager(queueMgrName);
            
            idFile = new FileWriter("C:\\ROPIS_12_LineItem_PostXML_US.txt");
			idWriter = new BufferedWriter(idFile);
			idBuffer = new StringBuffer();
 
            // Access the put/get queues. Note the open options used.
            putQueue = queueMgr.accessQueue(putQueueName, MQC.MQOO_BIND_NOT_FIXED | MQC.MQOO_OUTPUT);
            getQueue= queueMgr.accessQueue(getQueueName, MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_OUTPUT);
            
        } catch(Exception e) {
            lr.error_message("Error connecting to queue manager or accessing queues.");
            lr.exit(lr.EXIT_VUSER, lr.FAIL);
        }
        //lr_output_message("%s",getQueue);
 
        return 0;
    }//end of init
  
    public int action() throws Throwable {
    	
        // This is an XML message that will be put on the queue. Could do some fancy 
        // things with XML classes here if necessary.
        // The message string can contain {parameters} if lr.eval_string() is used.
       MQMessage textMessage = new MQMessage(); // creating a object textMessage
	     textMessage.format = "MQSTR";
	     MQPutMessageOptions pmo = new MQPutMessageOptions();
	     StringBuffer strBuf = new StringBuffer(); // StringBuffer is also string but varying
	    strBuf.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><tXML xsi:noNamespaceSchemaLocation=\"dom-order.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"+lr.eval_string("<Header>")+""+lr.eval_string("<Source>")+"WCS</Source>"+lr.eval_string("<Action_Type>")+"Update</Action_Type>"+lr.eval_string("<Reference_ID>")+""+lr.eval_string("{pOrderID}")+"</Reference_ID>"+lr.eval_string("<Message_Type>")+"Customer Order ROPIS</Message_Type>"+lr.eval_string("<Company_ID>")+"1</Company_ID></Header>"+lr.eval_string("<Message>")+""+lr.eval_string("<Order>")+""+lr.eval_string("<OrderNumber>")+""+lr.eval_string("{pOrderID}")+"</OrderNumber>"+lr.eval_string("<OrderCaptureDate>")+""+lr.eval_string("{pOrderDate}")+" 00:01 EDT</OrderCaptureDate>"+lr.eval_string("<ExternalOrderNumber>")+""+lr.eval_string("{pOrderID}")+"</ExternalOrderNumber>"+lr.eval_string("<OrderType>")+"USROPIS</OrderType>"+lr.eval_string("<OrderCurrency>")+"USD</OrderCurrency>"+lr.eval_string("<EnteredBy>")+"WCS</EnteredBy>"+lr.eval_string("<EntryType>")+"Web</EntryType>"+lr.eval_string("<Confirmed>")+"true</Confirmed>"+lr.eval_string("<Canceled>")+"false</Canceled>"+lr.eval_string("<OnHold>")+"false</OnHold>"+lr.eval_string("<PaymentStatus>")+"</PaymentStatus>"+lr.eval_string("<CustomerInfo>")+""+lr.eval_string("<CustomerId>")+"202373138</CustomerId>"+lr.eval_string("<CustomerFirstName>")+""+lr.eval_string("{pFirst_Name}")+"</CustomerFirstName>"+lr.eval_string("<CustomerLastName>")+""+lr.eval_string("{pLast_Name}")+"</CustomerLastName>"+lr.eval_string("<CustomerPhone>")+""+lr.eval_string("{pTelephone}")+"</CustomerPhone>"+lr.eval_string("<CustomerEmail>")+""+lr.eval_string("{pEmail}")+"</CustomerEmail></CustomerInfo>"+lr.eval_string("<ReferenceFields>")+""+lr.eval_string("<ReferenceField1>")+"English</ReferenceField1>"+lr.eval_string("<ReferenceField4>")+"</ReferenceField4>"+lr.eval_string("<ReferenceField5>")+"</ReferenceField5>"+lr.eval_string("<ReferenceField7>")+"</ReferenceField7></ReferenceFields>"+lr.eval_string("<OrderLines>")+""+lr.eval_string("<OrderLine>")+""+lr.eval_string("<LineNumber>")+"1</LineNumber>"+lr.eval_string("<ItemID>")+""+lr.eval_string("{pItemID}")+"</ItemID>"+lr.eval_string("<AllocationInfo>")+""+lr.eval_string("<FulfillmentFacility>")+""+lr.eval_string("{pFacilityId}")+"</FulfillmentFacility></AllocationInfo>"+lr.eval_string("<PriceInfo>")+""+lr.eval_string("<Price>")+""+lr.eval_string("{pPrice}")+"</Price></PriceInfo>"+lr.eval_string("<Quantity>")+""+lr.eval_string("<OrderedQty>")+""+lr.eval_string("{pQty}")+"</OrderedQty>"+lr.eval_string("<OrderedQtyUOM>")+"eaches</OrderedQtyUOM></Quantity>"+lr.eval_string("<ShippingInfo>")+""+lr.eval_string("<RequestedDeliveryBy>")+""+lr.eval_string("{pDeliveryDate}")+" 23:59 EDT</RequestedDeliveryBy>"+lr.eval_string("<ShipToFacility>")+""+lr.eval_string("{pFacilityId}")+"</ShipToFacility>"+lr.eval_string("<DeliveryOption>")+"Customer pickup</DeliveryOption></ShippingInfo>"+lr.eval_string("<Notes>")+""+lr.eval_string("<Note>")+""+lr.eval_string("<NoteType>")+"WCSItemAttributes</NoteType>"+lr.eval_string("<NoteText>")+"Testing</NoteText></Note></Notes>"+lr.eval_string("<LineReferenceFields>")+""+lr.eval_string("<ReferenceField1>")+""+lr.eval_string("{pReferenceId}")+"</ReferenceField1>"+lr.eval_string("<ReferenceNumber1>")+"19.50</ReferenceNumber1>"+lr.eval_string("<ReferenceNumber2>")+"0.00</ReferenceNumber2></LineReferenceFields></OrderLine>"+lr.eval_string("<OrderLine>")+""+lr.eval_string("<LineNumber>")+"2</LineNumber>"+lr.eval_string("<ItemID>")+""+lr.eval_string("{pItemID}")+"</ItemID>"+lr.eval_string("<AllocationInfo>")+""+lr.eval_string("<FulfillmentFacility>")+""+lr.eval_string("{pFacilityId}")+"</FulfillmentFacility></AllocationInfo>"+lr.eval_string("<PriceInfo>")+""+lr.eval_string("<Price>")+""+lr.eval_string("{pPrice}")+"</Price></PriceInfo>"+lr.eval_string("<Quantity>")+""+lr.eval_string("<OrderedQty>")+""+lr.eval_string("{pQty}")+"</OrderedQty>"+lr.eval_string("<OrderedQtyUOM>")+"eaches</OrderedQtyUOM></Quantity>"+lr.eval_string("<ShippingInfo>")+""+lr.eval_string("<RequestedDeliveryBy>")+""+lr.eval_string("{pDeliveryDate}")+" 23:59 EDT</RequestedDeliveryBy>"+lr.eval_string("<ShipToFacility>")+""+lr.eval_string("{pFacilityId}")+"</ShipToFacility>"+lr.eval_string("<DeliveryOption>")+"Customer pickup</DeliveryOption></ShippingInfo>"+lr.eval_string("<Notes>")+""+lr.eval_string("<Note>")+""+lr.eval_string("<NoteType>")+"WCSItemAttributes</NoteType>"+lr.eval_string("<NoteText>")+"Testing</NoteText></Note></Notes>"+lr.eval_string("<LineReferenceFields>")+""+lr.eval_string("<ReferenceField1>")+""+lr.eval_string("{pReferenceId}")+"</ReferenceField1>"+lr.eval_string("<ReferenceNumber1>")+"19.50</ReferenceNumber1>"+lr.eval_string("<ReferenceNumber2>")+"0.00</ReferenceNumber2></LineReferenceFields></OrderLine>");
	    strBuf.append(""+lr.eval_string("<OrderLine>")+""+lr.eval_string("<LineNumber>")+"3</LineNumber>"+lr.eval_string("<ItemID>")+""+lr.eval_string("{pItemID}")+"</ItemID>"+lr.eval_string("<AllocationInfo>")+""+lr.eval_string("<FulfillmentFacility>")+""+lr.eval_string("{pFacilityId}")+"</FulfillmentFacility></AllocationInfo>"+lr.eval_string("<PriceInfo>")+""+lr.eval_string("<Price>")+""+lr.eval_string("{pPrice}")+"</Price></PriceInfo>"+lr.eval_string("<Quantity>")+""+lr.eval_string("<OrderedQty>")+""+lr.eval_string("{pQty}")+"</OrderedQty>"+lr.eval_string("<OrderedQtyUOM>")+"eaches</OrderedQtyUOM></Quantity>"+lr.eval_string("<ShippingInfo>")+""+lr.eval_string("<RequestedDeliveryBy>")+""+lr.eval_string("{pDeliveryDate}")+" 23:59 EDT</RequestedDeliveryBy>"+lr.eval_string("<ShipToFacility>")+""+lr.eval_string("{pFacilityId}")+"</ShipToFacility>"+lr.eval_string("<DeliveryOption>")+"Customer pickup</DeliveryOption></ShippingInfo>"+lr.eval_string("<Notes>")+""+lr.eval_string("<Note>")+""+lr.eval_string("<NoteType>")+"WCSItemAttributes</NoteType>"+lr.eval_string("<NoteText>")+"Testing</NoteText></Note></Notes>"+lr.eval_string("<LineReferenceFields>")+""+lr.eval_string("<ReferenceField1>")+""+lr.eval_string("{pReferenceId}")+"</ReferenceField1>"+lr.eval_string("<ReferenceNumber1>")+"19.50</ReferenceNumber1>"+lr.eval_string("<ReferenceNumber2>")+"0.00</ReferenceNumber2></LineReferenceFields></OrderLine>"+lr.eval_string("<OrderLine>")+""+lr.eval_string("<LineNumber>")+"4</LineNumber>"+lr.eval_string("<ItemID>")+""+lr.eval_string("{pItemID}")+"</ItemID>"+lr.eval_string("<AllocationInfo>")+""+lr.eval_string("<FulfillmentFacility>")+""+lr.eval_string("{pFacilityId}")+"</FulfillmentFacility></AllocationInfo>"+lr.eval_string("<PriceInfo>")+""+lr.eval_string("<Price>")+""+lr.eval_string("{pPrice}")+"</Price></PriceInfo>"+lr.eval_string("<Quantity>")+""+lr.eval_string("<OrderedQty>")+""+lr.eval_string("{pQty}")+"</OrderedQty>"+lr.eval_string("<OrderedQtyUOM>")+"eaches</OrderedQtyUOM></Quantity>"+lr.eval_string("<ShippingInfo>")+""+lr.eval_string("<RequestedDeliveryBy>")+""+lr.eval_string("{pDeliveryDate}")+" 23:59 EDT</RequestedDeliveryBy>"+lr.eval_string("<ShipToFacility>")+""+lr.eval_string("{pFacilityId}")+"</ShipToFacility>"+lr.eval_string("<DeliveryOption>")+"Customer pickup</DeliveryOption></ShippingInfo>"+lr.eval_string("<Notes>")+""+lr.eval_string("<Note>")+""+lr.eval_string("<NoteType>")+"WCSItemAttributes</NoteType>"+lr.eval_string("<NoteText>")+"Testing</NoteText></Note></Notes>"+lr.eval_string("<LineReferenceFields>")+""+lr.eval_string("<ReferenceField1>")+""+lr.eval_string("{pReferenceId}")+"</ReferenceField1>"+lr.eval_string("<ReferenceNumber1>")+"19.50</ReferenceNumber1>"+lr.eval_string("<ReferenceNumber2>")+"0.00</ReferenceNumber2></LineReferenceFields></OrderLine>"+lr.eval_string("<OrderLine>")+""+lr.eval_string("<LineNumber>")+"5</LineNumber>"+lr.eval_string("<ItemID>")+""+lr.eval_string("{pItemID}")+"</ItemID>"+lr.eval_string("<AllocationInfo>")+""+lr.eval_string("<FulfillmentFacility>")+""+lr.eval_string("{pFacilityId}")+"</FulfillmentFacility></AllocationInfo>"+lr.eval_string("<PriceInfo>")+""+lr.eval_string("<Price>")+""+lr.eval_string("{pPrice}")+"</Price></PriceInfo>"+lr.eval_string("<Quantity>")+""+lr.eval_string("<OrderedQty>")+""+lr.eval_string("{pQty}")+"</OrderedQty>"+lr.eval_string("<OrderedQtyUOM>")+"eaches</OrderedQtyUOM></Quantity>"+lr.eval_string("<ShippingInfo>")+""+lr.eval_string("<RequestedDeliveryBy>")+""+lr.eval_string("{pDeliveryDate}")+" 23:59 EDT</RequestedDeliveryBy>"+lr.eval_string("<ShipToFacility>")+""+lr.eval_string("{pFacilityId}")+"</ShipToFacility>"+lr.eval_string("<DeliveryOption>")+"Customer pickup</DeliveryOption></ShippingInfo>"+lr.eval_string("<Notes>")+""+lr.eval_string("<Note>")+""+lr.eval_string("<NoteType>")+"WCSItemAttributes</NoteType>"+lr.eval_string("<NoteText>")+"Testing</NoteText></Note></Notes>"+lr.eval_string("<LineReferenceFields>")+""+lr.eval_string("<ReferenceField1>")+""+lr.eval_string("{pReferenceId}")+"</ReferenceField1>"+lr.eval_string("<ReferenceNumber1>")+"19.50</ReferenceNumber1>"+lr.eval_string("<ReferenceNumber2>")+"0.00</ReferenceNumber2></LineReferenceFields></OrderLine>"+lr.eval_string("<OrderLine>")+""+lr.eval_string("<LineNumber>")+"6</LineNumber>"+lr.eval_string("<ItemID>")+""+lr.eval_string("{pItemID}")+"</ItemID>"+lr.eval_string("<AllocationInfo>")+""+lr.eval_string("<FulfillmentFacility>")+""+lr.eval_string("{pFacilityId}")+"</FulfillmentFacility></AllocationInfo>"+lr.eval_string("<PriceInfo>")+""+lr.eval_string("<Price>")+""+lr.eval_string("{pPrice}")+"</Price></PriceInfo>"+lr.eval_string("<Quantity>")+""+lr.eval_string("<OrderedQty>")+""+lr.eval_string("{pQty}")+"</OrderedQty>"+lr.eval_string("<OrderedQtyUOM>")+"eaches</OrderedQtyUOM></Quantity>"+lr.eval_string("<ShippingInfo>")+""+lr.eval_string("<RequestedDeliveryBy>")+""+lr.eval_string("{pDeliveryDate}")+" 23:59 EDT</RequestedDeliveryBy>"+lr.eval_string("<ShipToFacility>")+""+lr.eval_string("{pFacilityId}")+"</ShipToFacility>"+lr.eval_string("<DeliveryOption>")+"Customer pickup</DeliveryOption></ShippingInfo>"+lr.eval_string("<Notes>")+""+lr.eval_string("<Note>")+""+lr.eval_string("<NoteType>")+"WCSItemAttributes</NoteType>"+lr.eval_string("<NoteText>")+"Testing</NoteText></Note></Notes>"+lr.eval_string("<LineReferenceFields>")+""+lr.eval_string("<ReferenceField1>")+""+lr.eval_string("{pReferenceId}")+"</ReferenceField1>"+lr.eval_string("<ReferenceNumber1>")+"19.50</ReferenceNumber1>"+lr.eval_string("<ReferenceNumber2>")+"0.00</ReferenceNumber2></LineReferenceFields></OrderLine>"+lr.eval_string("<OrderLine>")+""+lr.eval_string("<LineNumber>")+"7</LineNumber>"+lr.eval_string("<ItemID>")+""+lr.eval_string("{pItemID}")+"</ItemID>"+lr.eval_string("<AllocationInfo>")+""+lr.eval_string("<FulfillmentFacility>")+""+lr.eval_string("{pFacilityId}")+"</FulfillmentFacility></AllocationInfo>"+lr.eval_string("<PriceInfo>")+""+lr.eval_string("<Price>")+""+lr.eval_string("{pPrice}")+"</Price></PriceInfo>"+lr.eval_string("<Quantity>")+""+lr.eval_string("<OrderedQty>")+""+lr.eval_string("{pQty}")+"</OrderedQty>"+lr.eval_string("<OrderedQtyUOM>")+"eaches</OrderedQtyUOM></Quantity>"+lr.eval_string("<ShippingInfo>")+""+lr.eval_string("<RequestedDeliveryBy>")+""+lr.eval_string("{pDeliveryDate}")+" 23:59 EDT</RequestedDeliveryBy>"+lr.eval_string("<ShipToFacility>")+""+lr.eval_string("{pFacilityId}")+"</ShipToFacility>"+lr.eval_string("<DeliveryOption>")+"Customer pickup</DeliveryOption></ShippingInfo>"+lr.eval_string("<Notes>")+""+lr.eval_string("<Note>")+""+lr.eval_string("<NoteType>")+"WCSItemAttributes</NoteType>"+lr.eval_string("<NoteText>")+"Testing</NoteText></Note></Notes>"+lr.eval_string("<LineReferenceFields>")+""+lr.eval_string("<ReferenceField1>")+""+lr.eval_string("{pReferenceId}")+"</ReferenceField1>"+lr.eval_string("<ReferenceNumber1>")+"19.50</ReferenceNumber1>"+lr.eval_string("<ReferenceNumber2>")+"0.00</ReferenceNumber2></LineReferenceFields></OrderLine>");
	    strBuf.append(""+lr.eval_string("<OrderLine>")+""+lr.eval_string("<LineNumber>")+"8</LineNumber>"+lr.eval_string("<ItemID>")+""+lr.eval_string("{pItemID}")+"</ItemID>"+lr.eval_string("<AllocationInfo>")+""+lr.eval_string("<FulfillmentFacility>")+""+lr.eval_string("{pFacilityId}")+"</FulfillmentFacility></AllocationInfo>"+lr.eval_string("<PriceInfo>")+""+lr.eval_string("<Price>")+""+lr.eval_string("{pPrice}")+"</Price></PriceInfo>"+lr.eval_string("<Quantity>")+""+lr.eval_string("<OrderedQty>")+""+lr.eval_string("{pQty}")+"</OrderedQty>"+lr.eval_string("<OrderedQtyUOM>")+"eaches</OrderedQtyUOM></Quantity>"+lr.eval_string("<ShippingInfo>")+""+lr.eval_string("<RequestedDeliveryBy>")+""+lr.eval_string("{pDeliveryDate}")+" 23:59 EDT</RequestedDeliveryBy>"+lr.eval_string("<ShipToFacility>")+""+lr.eval_string("{pFacilityId}")+"</ShipToFacility>"+lr.eval_string("<DeliveryOption>")+"Customer pickup</DeliveryOption></ShippingInfo>"+lr.eval_string("<Notes>")+""+lr.eval_string("<Note>")+""+lr.eval_string("<NoteType>")+"WCSItemAttributes</NoteType>"+lr.eval_string("<NoteText>")+"Testing</NoteText></Note></Notes>"+lr.eval_string("<LineReferenceFields>")+""+lr.eval_string("<ReferenceField1>")+""+lr.eval_string("{pReferenceId}")+"</ReferenceField1>"+lr.eval_string("<ReferenceNumber1>")+"19.50</ReferenceNumber1>"+lr.eval_string("<ReferenceNumber2>")+"0.00</ReferenceNumber2></LineReferenceFields></OrderLine>"+lr.eval_string("<OrderLine>")+""+lr.eval_string("<LineNumber>")+"9</LineNumber>"+lr.eval_string("<ItemID>")+""+lr.eval_string("{pItemID}")+"</ItemID>"+lr.eval_string("<AllocationInfo>")+""+lr.eval_string("<FulfillmentFacility>")+""+lr.eval_string("{pFacilityId}")+"</FulfillmentFacility></AllocationInfo>"+lr.eval_string("<PriceInfo>")+""+lr.eval_string("<Price>")+""+lr.eval_string("{pPrice}")+"</Price></PriceInfo>"+lr.eval_string("<Quantity>")+""+lr.eval_string("<OrderedQty>")+""+lr.eval_string("{pQty}")+"</OrderedQty>"+lr.eval_string("<OrderedQtyUOM>")+"eaches</OrderedQtyUOM></Quantity>"+lr.eval_string("<ShippingInfo>")+""+lr.eval_string("<RequestedDeliveryBy>")+""+lr.eval_string("{pDeliveryDate}")+" 23:59 EDT</RequestedDeliveryBy>"+lr.eval_string("<ShipToFacility>")+""+lr.eval_string("{pFacilityId}")+"</ShipToFacility>"+lr.eval_string("<DeliveryOption>")+"Customer pickup</DeliveryOption></ShippingInfo>"+lr.eval_string("<Notes>")+""+lr.eval_string("<Note>")+""+lr.eval_string("<NoteType>")+"WCSItemAttributes</NoteType>"+lr.eval_string("<NoteText>")+"Testing</NoteText></Note></Notes>"+lr.eval_string("<LineReferenceFields>")+""+lr.eval_string("<ReferenceField1>")+""+lr.eval_string("{pReferenceId}")+"</ReferenceField1>"+lr.eval_string("<ReferenceNumber1>")+"19.50</ReferenceNumber1>"+lr.eval_string("<ReferenceNumber2>")+"0.00</ReferenceNumber2></LineReferenceFields></OrderLine>"+lr.eval_string("<OrderLine>")+""+lr.eval_string("<LineNumber>")+"10</LineNumber>"+lr.eval_string("<ItemID>")+""+lr.eval_string("{pItemID}")+"</ItemID>"+lr.eval_string("<AllocationInfo>")+""+lr.eval_string("<FulfillmentFacility>")+""+lr.eval_string("{pFacilityId}")+"</FulfillmentFacility></AllocationInfo>"+lr.eval_string("<PriceInfo>")+""+lr.eval_string("<Price>")+""+lr.eval_string("{pPrice}")+"</Price></PriceInfo>"+lr.eval_string("<Quantity>")+""+lr.eval_string("<OrderedQty>")+""+lr.eval_string("{pQty}")+"</OrderedQty>"+lr.eval_string("<OrderedQtyUOM>")+"eaches</OrderedQtyUOM></Quantity>"+lr.eval_string("<ShippingInfo>")+""+lr.eval_string("<RequestedDeliveryBy>")+""+lr.eval_string("{pDeliveryDate}")+" 23:59 EDT</RequestedDeliveryBy>"+lr.eval_string("<ShipToFacility>")+""+lr.eval_string("{pFacilityId}")+"</ShipToFacility>"+lr.eval_string("<DeliveryOption>")+"Customer pickup</DeliveryOption></ShippingInfo>"+lr.eval_string("<Notes>")+""+lr.eval_string("<Note>")+""+lr.eval_string("<NoteType>")+"WCSItemAttributes</NoteType>"+lr.eval_string("<NoteText>")+"Testing</NoteText></Note></Notes>"+lr.eval_string("<LineReferenceFields>")+""+lr.eval_string("<ReferenceField1>")+""+lr.eval_string("{pReferenceId}")+"</ReferenceField1>"+lr.eval_string("<ReferenceNumber1>")+"19.50</ReferenceNumber1>"+lr.eval_string("<ReferenceNumber2>")+"0.00</ReferenceNumber2></LineReferenceFields></OrderLine>"+lr.eval_string("<OrderLine>")+""+lr.eval_string("<LineNumber>")+"11</LineNumber>"+lr.eval_string("<ItemID>")+""+lr.eval_string("{pItemID}")+"</ItemID>"+lr.eval_string("<AllocationInfo>")+""+lr.eval_string("<FulfillmentFacility>")+""+lr.eval_string("{pFacilityId}")+"</FulfillmentFacility></AllocationInfo>"+lr.eval_string("<PriceInfo>")+""+lr.eval_string("<Price>")+""+lr.eval_string("{pPrice}")+"</Price></PriceInfo>"+lr.eval_string("<Quantity>")+""+lr.eval_string("<OrderedQty>")+""+lr.eval_string("{pQty}")+"</OrderedQty>"+lr.eval_string("<OrderedQtyUOM>")+"eaches</OrderedQtyUOM></Quantity>"+lr.eval_string("<ShippingInfo>")+""+lr.eval_string("<RequestedDeliveryBy>")+""+lr.eval_string("{pDeliveryDate}")+" 23:59 EDT</RequestedDeliveryBy>"+lr.eval_string("<ShipToFacility>")+""+lr.eval_string("{pFacilityId}")+"</ShipToFacility>"+lr.eval_string("<DeliveryOption>")+"Customer pickup</DeliveryOption></ShippingInfo>"+lr.eval_string("<Notes>")+""+lr.eval_string("<Note>")+""+lr.eval_string("<NoteType>")+"WCSItemAttributes</NoteType>"+lr.eval_string("<NoteText>")+"Testing</NoteText></Note></Notes>"+lr.eval_string("<LineReferenceFields>")+""+lr.eval_string("<ReferenceField1>")+""+lr.eval_string("{pReferenceId}")+"</ReferenceField1>"+lr.eval_string("<ReferenceNumber1>")+"19.50</ReferenceNumber1>"+lr.eval_string("<ReferenceNumber2>")+"0.00</ReferenceNumber2></LineReferenceFields></OrderLine>"+lr.eval_string("<OrderLine>")+""+lr.eval_string("<LineNumber>")+"12</LineNumber>"+lr.eval_string("<ItemID>")+""+lr.eval_string("{pItemID}")+"</ItemID>"+lr.eval_string("<AllocationInfo>")+""+lr.eval_string("<FulfillmentFacility>")+""+lr.eval_string("{pFacilityId}")+"</FulfillmentFacility></AllocationInfo>"+lr.eval_string("<PriceInfo>")+""+lr.eval_string("<Price>")+""+lr.eval_string("{pPrice}")+"</Price></PriceInfo>"+lr.eval_string("<Quantity>")+""+lr.eval_string("<OrderedQty>")+""+lr.eval_string("{pQty}")+"</OrderedQty>"+lr.eval_string("<OrderedQtyUOM>")+"eaches</OrderedQtyUOM></Quantity>"+lr.eval_string("<ShippingInfo>")+""+lr.eval_string("<RequestedDeliveryBy>")+""+lr.eval_string("{pDeliveryDate}")+" 23:59 EDT</RequestedDeliveryBy>"+lr.eval_string("<ShipToFacility>")+""+lr.eval_string("{pFacilityId}")+"</ShipToFacility>"+lr.eval_string("<DeliveryOption>")+"Customer pickup</DeliveryOption></ShippingInfo>"+lr.eval_string("<Notes>")+""+lr.eval_string("<Note>")+""+lr.eval_string("<NoteType>")+"WCSItemAttributes</NoteType>"+lr.eval_string("<NoteText>")+"Testing</NoteText></Note></Notes>"+lr.eval_string("<LineReferenceFields>")+""+lr.eval_string("<ReferenceField1>")+""+lr.eval_string("{pReferenceId}")+"</ReferenceField1>"+lr.eval_string("<ReferenceNumber1>")+"19.50</ReferenceNumber1>"+lr.eval_string("<ReferenceNumber2>")+"0.00</ReferenceNumber2></LineReferenceFields></OrderLine></OrderLines></Order></Message></tXML>");
	    	
	     // strBuf.append(""+lr.eval_string("<OrderNumber>")+""+lr.eval_string(""+lr.eval_string(""+lr.eval_string("{pOrderID}")+"")+"")+"</OrderNumber>");
	    
	     
        System.out.println(strBuf);
       	String data = lr.eval_string("{pOrderID}");
		System.out.println(data);
		idWriter.write(data);
		idWriter.write(",");
		
		String data2 = lr.eval_string("{pFacilityId}");
		System.out.println(data2);
        idWriter.write(data2);
        idWriter.write(",");
		
		String data3 = lr.eval_string("{pFirst_Name}");
		System.out.println(data3);
        idWriter.write(data3);
        idWriter.write("\n");
      
//        lr.output_message("%s"
         //textMessage.writeString(strBuf.toString());
//        msgBody=strBuf;
        // Clear the message objects on each iteration.
        requestMsg.clearMessage();
        responseMsg.clearMessage();
 
        // Create a message object and put it on the request queue
        lr.start_transaction("TCP_1_ROPIS_Post_Order_XML");
        
        try {
            pmo.options = MQC.MQPMO_NEW_MSG_ID; // The queue manager replaces the contents of the MsgId field in MQMD with a new message identifier.
            requestMsg.replyToQueueName = getQueueName; // the response should be put on this queue
            requestMsg.report=MQC.MQRO_PASS_MSG_ID; //If a report or reply is generated as a result of this message, the MsgId of this message is copied to the MsgId of the report or reply message.
            requestMsg.format = MQC.MQFMT_STRING; // Set message format. The application message data can be either an SBCS string (single-byte character set), or a DBCS string (double-byte character set). 
            requestMsg.messageType=MQC.MQMT_REQUEST; // The message is one that requires a reply.
            requestMsg.writeString(strBuf.toString()); // message payload msgBody
            putQueue.put(requestMsg, pmo);
            
//            String data2 = lr.eval_string("gdh");
//            idWriter.write(data2);
//            idWriter.write("\n");
           } 
        
        catch(Exception e) 
        {
        	lr.error_message("Error sending message.");
        	lr.exit(lr.EXIT_VUSER, lr.FAIL);
        }
        
        lr.end_transaction("TCP_1_ROPIS_Post_Order_XML", lr.AUTO);
        
        lr.think_time(1);
        
 
        return 0;
    
    }//end of action
    
    public int end() throws Throwable 
    {
        // 	Close all the connections
        try 
        {
            putQueue.close();
            getQueue.close();
            queueMgr.close();
            idWriter.close();
        } 
        
        catch(Exception e)
              {
            lr.error_message("Exception in closing the connections");
            lr.exit(lr.EXIT_VUSER, lr.FAIL);
              }
 
        return 0;
    }//end of end

}