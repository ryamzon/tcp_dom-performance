/*
 * Example WebSphere MQ LoadRunner script (written in Java)
 * 
 * Script Description: 
 *     This script puts a message on a queue, then gets a response message from 
 *     another queue.
 *
 * You will probably need to add the following jar files to your classpath
 *   - com.ibm.mq.jar
 *   - connector.jar
 *   - com.ibm.mq.jmqi.jar
 *   - com.ibm.mq.headers.jar
 *   - com.ibm.mq.commonservices.jar
 */
 
import lrapi.lr;
import com.ibm.mq.*;
import java.io.*;
 
public class Actions
{
    // Variables used by more than one method
    String queueMgrName = "PERF.DOM.QMGR";
    String putQueueName = "TCP.TO.MIF.KOUNT.RESPONSE";//"TCPMQSI3.QMTCPMQSI3T.QL.INTDOM.WCS.DOM.ORD";
    String getQueueName = "TCP.TO.MIF.KOUNT.RESPONSE";//"TCPMQSI3.QMTCPMQSI3T.QL.INTDOM.WCS.DOM.ORD";
    
    StringBuffer idBuffer = null;
    FileWriter idFile = null;
	BufferedWriter idWriter = null;
 
    MQQueueManager queueMgr = null;
    MQQueue getQueue = null;
    MQQueue putQueue = null;
    MQPutMessageOptions pmo = new MQPutMessageOptions();
    MQGetMessageOptions gmo = new MQGetMessageOptions();
    MQMessage requestMsg = new MQMessage();
    MQMessage responseMsg = new MQMessage();
    String msgBody = null;
 
    public int init() throws Throwable {
        // Open a connection to the queue manager and the put/get queues
        try {
            // As values set in the MQEnvironment class take effect when the 
            // MQQueueManager constructor is called, you must set the values 
            // in the MQEnvironment class before you construct an MQQueueManager object.
            MQEnvironment.hostname="10.18.1.21";
            MQEnvironment.port=1415;
            MQEnvironment.channel = "DOM.SVRCONN.CHANNEL";
            queueMgr = new MQQueueManager(queueMgrName);
            
            idFile = new FileWriter("C:\\FraudResponse_CA.txt");
			idWriter = new BufferedWriter(idFile);
			idBuffer = new StringBuffer();
 
            // Access the put/get queues. Note the open options used.
            putQueue = queueMgr.accessQueue(putQueueName, MQC.MQOO_BIND_NOT_FIXED | MQC.MQOO_OUTPUT);
            getQueue= queueMgr.accessQueue(getQueueName, MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_OUTPUT);
            
        } catch(Exception e) {
            lr.error_message("Error connecting to queue manager or accessing queues.");
            lr.exit(lr.EXIT_VUSER, lr.FAIL);
        }
        //lr_output_message("%s",getQueue);
 
        return 0;
    }//end of init
 
    public int action() throws Throwable {
        // This is an XML message that will be put on the queue. Could do some fancy 
        // things with XML classes here if necessary.
        // The message string can contain {parameters} if lr.eval_string() is used.
       MQMessage textMessage = new MQMessage(); // creating a object textMessage
	     textMessage.format = "MQSTR";
	     MQPutMessageOptions pmo = new MQPutMessageOptions();
	     StringBuffer strBuf = new StringBuffer(); // StringBuffer is also string but varying
	  
	     strBuf.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"+lr.eval_string("<tXML>")+""+lr.eval_string("<Header>")+""+lr.eval_string("<Source>")+"Host</Source>"+lr.eval_string("<Message_Type>")+"FraudResponse</Message_Type>"+lr.eval_string("<Company_ID>")+"2</Company_ID>"+lr.eval_string("<Msg_Locale>")+"English (United States)</Msg_Locale>"+lr.eval_string("<Msg_Time_Zone>")+"America/New_Yor</Msg_Time_Zone>"+lr.eval_string("<Version>")+"0</Version></Header>"+lr.eval_string("<Message>")+""+lr.eval_string("<FraudResponse>")+""+lr.eval_string("<ResponseDetail>")+""+lr.eval_string("<version>")+"0630</version>"+lr.eval_string("<mode>")+"Q</mode>"+lr.eval_string("<merchantId>")+"104100</merchantId>"+lr.eval_string("<sessionId>")+""+lr.eval_string("{pOrderID}")+"</sessionId>"+lr.eval_string("<transactionId>")+"PM3703B3K1TH</transactionId>"+lr.eval_string("<orderNumber>")+""+lr.eval_string("{pOrderID}")+"</orderNumber>"+lr.eval_string("<auto>")+"A</auto>"+lr.eval_string("<reason>")+"</reason>"+lr.eval_string("<score>")+"31</score>"+lr.eval_string("<geox>")+"CA</geox>"+lr.eval_string("<brand>")+"GIFT</brand>"+lr.eval_string("<velo>")+"0</velo>"+lr.eval_string("<vmax>")+"0</vmax>"+lr.eval_string("<network>")+"N</network>"+lr.eval_string("<region>")+"</region>"+lr.eval_string("<kaptcha>")+"Y</kaptcha>"+lr.eval_string("<site>")+"DOM_CA</site>"+lr.eval_string("<proxy>")+"N</proxy>"+lr.eval_string("<emails>")+"1</emails>"+lr.eval_string("<httpCountry>")+"SG</httpCountry>"+lr.eval_string("<timeZone>")+"240</timeZone>"+lr.eval_string("<cards>")+"1</cards>"+lr.eval_string("<pcRemote>")+"N</pcRemote>"+lr.eval_string("<devices>")+"1</devices>"+lr.eval_string("<deviceLayers>")+"7163E0E4CE...1D05FC02D5.0FD483EC44</deviceLayers>"+lr.eval_string("<mobileForwarder>")+"N</mobileForwarder>"+lr.eval_string("<voiceDevice>")+"N</voiceDevice>"+lr.eval_string("<localTime>")+""+lr.eval_string("{pOrderDate}")+"</localTime>"+lr.eval_string("<mobileType>")+"ANDROID</mobileType>"+lr.eval_string("<fingerPrint>")+"BD86ECB02028C94228646F3D03288A99</fingerPrint>"+lr.eval_string("<flash>")+"N</flash>"+lr.eval_string("<language>")+"EN</language>"+lr.eval_string("<country>")+"CA</country>"+lr.eval_string("<javaScript>")+"Y</javaScript>"+lr.eval_string("<cookies>")+"Y</cookies>"+lr.eval_string("<mobileDevice>")+"Y</mobileDevice>"+lr.eval_string("<warningCount>")+"0</warningCount>"+lr.eval_string("<errorCount>")+"0</errorCount></ResponseDetail>"+lr.eval_string("<TransactionStatus>")+"SUCCESSFUL</TransactionStatus></FraudResponse></Message></tXML>");
	     
         
        System.out.println(strBuf);
        String data = lr.eval_string("{pOrderID}");
		System.out.println(data);
		idWriter.write(data);
		idWriter.write(",");


         // Clear the message objects on each iteration.
        requestMsg.clearMessage();
        responseMsg.clearMessage();
 
        // Create a message object and put it on the request queue
        lr.start_transaction("TCP_2_2_ECOM_CA_Fraud_Response_01_Post");
        
        try {
            pmo.options = MQC.MQPMO_NEW_MSG_ID; // The queue manager replaces the contents of the MsgId field in MQMD with a new message identifier.
            requestMsg.replyToQueueName = getQueueName; // the response should be put on this queue
            requestMsg.report=MQC.MQRO_PASS_MSG_ID; //If a report or reply is generated as a result of this message, the MsgId of this message is copied to the MsgId of the report or reply message.
            requestMsg.format = MQC.MQFMT_STRING; // Set message format. The application message data can be either an SBCS string (single-byte character set), or a DBCS string (double-byte character set). 
            requestMsg.messageType=MQC.MQMT_REQUEST; // The message is one that requires a reply.
            requestMsg.writeString(strBuf.toString()); // message payload msgBody
            putQueue.put(requestMsg, pmo);
            
            String data2 = lr.eval_string("{pTimeStamp}");
            idWriter.write(data2);
            idWriter.write("\n");
        } 
        
        catch(Exception e) 
        {
        	lr.error_message("Error sending message.");
        	lr.exit(lr.EXIT_VUSER, lr.FAIL);
        }
        
        lr.end_transaction("TCP_2_2_ECOM_CA_Fraud_Response_01_Post", lr.AUTO);
        
        lr.think_time(1);

        return 0;
    
    }//end of action
    
    public int end() throws Throwable 
    {
        // 	Close all the connections
        try 
        {
            putQueue.close();
            getQueue.close();
            queueMgr.close();
            idWriter.close();
        }
        
        catch(Exception e)
              {
            lr.error_message("Exception in closing the connections");
            lr.exit(lr.EXIT_VUSER, lr.FAIL);
              }
 
        return 0;
    }//end of end
}