Virtual User Script started at : 8/24/2017 2:41:16 AM
Starting action vuser_init.
Web Turbo Replay of LoadRunner 12.0.0 for Windows 2008 R2; build 2079 (Jun 17 2014 10:56:12)  	[MsgId: MMSG-27143]
Run mode: HTML  	[MsgId: MMSG-26993]
Run-Time Settings file: "C:\IT\Scripts\TCP_9_ECOM_US_Cancel_Order\\default.cfg"  	[MsgId: MMSG-27141]
vuser_init.c(12): web_cache_cleanup started  	[MsgId: MMSG-26355]
vuser_init.c(12): web_cache_cleanup was successful  	[MsgId: MMSG-26392]
vuser_init.c(14): web_cleanup_cookies started  	[MsgId: MMSG-26355]
vuser_init.c(14): web_cleanup_cookies was successful  	[MsgId: MMSG-26392]
vuser_init.c(16): web_set_max_html_param_len started  	[MsgId: MMSG-26355]
vuser_init.c(16): web_set_max_html_param_len was successful  	[MsgId: MMSG-26392]
vuser_init.c(18): web_set_sockets_option started  	[MsgId: MMSG-26355]
vuser_init.c(18): web_set_sockets_option was successful  	[MsgId: MMSG-26392]
vuser_init.c(20): web_set_sockets_option started  	[MsgId: MMSG-26355]
vuser_init.c(20): web_set_sockets_option was successful  	[MsgId: MMSG-26392]
vuser_init.c(22): web_set_sockets_option started  	[MsgId: MMSG-26355]
vuser_init.c(22): web_set_sockets_option was successful  	[MsgId: MMSG-26392]
vuser_init.c(33): web_reg_save_param started  	[MsgId: MMSG-26355]
vuser_init.c(33): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
vuser_init.c(39): Notify: Saving Parameter "sTestCaseName = TCP_09_ECOM_US_Cancel_Order".
vuser_init.c(41): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_09_ECOM_US_Cancel_Order"
vuser_init.c(41): Notify: Transaction "TCP_09_ECOM_US_Cancel_Order_01_Launch" started.
vuser_init.c(45): web_url("index.html") started  	[MsgId: MMSG-26355]
vuser_init.c(45): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(45): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/favicon.ico" (specified by argument number 10)  	[MsgId: MMSG-26577]
vuser_init.c(45): Notify: Saving Parameter "c_SAML_Request = PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c2FtbDJwOkF1dGhuUmVxdWVzdCB4bWxuczpzYW1sMnA9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpwcm90b2NvbCIgQXNzZXJ0aW9uQ29uc3VtZXJTZXJ2aWNlVVJMPSJodHRwOi8vZW9tLWNhLXBlcmYudGNwaHEudGNwY29ycC5sb2NhbC5jb206MzAwMDAvc2FtbC9TU08vYWxpYXMvY2EiIERlc3RpbmF0aW9uPSJodHRwczovL2VvbS1taXAtcGVyZi5jaGlsZHJlbnNwbGFjZS5jb206MTUwMDEvcHJvZmlsZS9TQU1MMi9QT1NUL1NTTyIgRm9yY2VBdXRobj0iZmFsc2UiIElEPSJhMzdkaTM2YWM0MzY2MDI2NGQyOGJmMGFhN2UyNTliIiBJc1Bhc3NpdmU9ImZhbHNlIiBJc3N1ZUluc3RhbnQ9IjIwMTctMDgtMjRUMDY6NDE6MTkuODgxWiIgUHJvdG9jb2xCaW5kaW5nPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YmluZGluZ3M6SFRUUC1BcnRpZmFjdCIgVmVyc2lvbj0iMi4wIj48c2FtbDI6SXNzdWVyIHhtbG5zOnNhbWwyPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YXNzZXJ0aW9uIj5jYTwvc2FtbDI6SXNzdWVyPjxkczpTaWduYXR1cmUgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxkczpTaWduZWRJbmZvPjxkczpDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8+PGRzOlNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNyc2Etc2hhMSIvPjxkczpSZWZlcmVuY2UgVVJJPSIjYTM3ZGkzNmFjNDM2NjAyNjRkMjhiZjBhYTdlMjU5YiI+PGRzOlRyYW5zZm9ybXM+PGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNlbnZlbG9wZWQtc2lnbmF0dXJlIi8+PGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIvPjwvZHM6VHJhbnNmb3Jtcz48ZHM6RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3NoYTEiLz48ZHM6RGlnZXN0VmFsdWU+UlBqZkt4Z25aa0VEeklzV05kTklXL2pZMDlVPTwvZHM6RGlnZXN0VmFsdWU+PC9kczpSZWZlcmVuY2U+PC9kczpTaWduZWRJbmZvPjxkczpTaWduYXR1cmVWYWx1ZT5hT3d5MzlzOXF4S1dzVEJnTzk2RzRhODFTUDgwYlJYTG81dzZUZWtlMktjeG1vWVFtS2Q1Q003MEJGanlTWmFGT0dUQ3A4d2FRK3BqNTZZaWVBTWlWVU1yQlFyellteWhkYXo4YTROM3JaVFh4Ry9oVFRGcU1tZ2FYWDdWQm85UzRwOENWamQvQjcrY2oxVlFmUFl5YkFOdkxGdGxVM0JpK2dLeWxocTRiT0syVDcwaUViVkNnU3RWNEJOOFJ3ZjFBUTNEdmxmN3J1QktQcU5CMGxjcGNMeUsvbW5rSzg0MWdnV2xuNGhyT2licHdoN0hmUjVPanRFUFZ6WlFkWDhrSWlIcnA3b1JvTXNBTDBRaG9SMk5SR3BtUVBGK25UVEoveVRVVmwxbEd4UWlzY1dpSVZnT2YvSzRBZFdLb0Z4dXNVRnVxeHgwTll6eEZDQnNvL0IrSHc9PTwvZHM6U2lnbmF0dXJlVmFsdWU+PGRzOktleUluZm8+PGRzOlg1MDlEYXRhPjxkczpYNTA5Q2VydGlmaWNhdGU+TUlJREJUQ0NBZTBDQkZLRG5GRXdEUVlKS29aSWh2Y05BUUVOQlFBd1J6RUxNQWtHQTFVRUJoTUNWVk14SWpBZ0JnTlZCQW9NR1UxaApibWhoZEhSaGJpQkJjM052WTJsaGRHVnpMQ0JKYm1NeEZEQVNCZ05WQkFNTUMzTndMbTFoYm1ndVkyOXRNQjRYRFRFek1URXhNekUxCk16VTBOVm9YRFRNek1URXdPREUxTXpVME5Wb3dSekVMTUFrR0ExVUVCaE1DVlZNeElqQWdCZ05WQkFvTUdVMWhibWhoZEhSaGJpQkIKYzNOdlkybGhkR1Z6TENCSmJtTXhGREFTQmdOVkJBTU1DM053TG0xaGJtZ3VZMjl0TUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQwpBUThBTUlJQkNnS0NBUUVBalJ5cDRBdFpwdXVOdk5EUTFqSFNzWjZER0dQaU9DTFpaUG9Xd3g0ejA1cWNHNDgxMUxVWFpsRm8vTlprCkZ4djVsbWRqbHZJYjRvODNsbHVqMWRJY0VKcUJQSnFwS1k5L3lJK3B1RW0xQUVKOWpPQ0hmK2N5ZERCMzZ2ZTczVW12d1paMERMU28KbFpWOXk1bXJhSFpoMEtXWlpnaWJqdFlwK3JsQVNrSGJNQjZlY1lXNk9aUk9Zc2hHTkRLa2lJK3dkcWxNdjhKN01OV1FpeUxOUUtzQQpCUUltd3l3L2VDTmt0cVFYMy9yWk11empJVFUwbTFtZm1NQ1NHcnJsQkRnMmpHaEk2d3FiVU9BdDBST3pzKy8rdmk5MjIzN1p2c1VDCjlwNFdaSjRLSkkrSXBpNE4veEZyRlF5TVErN1c1NlBoVWlOaTR5RS9zcU1vekxkMmtJK0pOUUlEQVFBQk1BMEdDU3FHU0liM0RRRUIKRFFVQUE0SUJBUUNGNml4cXBVejllRjhpTktBMitzNkt2UWs1SHZ6cXQ5M1hoWUJxRE9ZOXA0QzMvc0hUc2x0K3EzejFwb0xGZ2pCbwphb2g1aXlqSWRPK252LzdJRHVnaFV0cktZUWlpRk9saVA2UVovZFI2c2pPRW8vb0hVVEU1c016MFo4ZXBxUkFiVXBOVTJDMU1FcTlqClR2akZRclBybTVPTUlrOWZod084TUN2ajJ2Z2VmZDZ0d3NrcXliWnlMOUxMV0FNd0hvQVNjdVM4UG1RVTR4TVpPVE0zcWRUOUVCRUkKMEhjQ3Z2bFJzR0NNMVBkUVNZMG1nZlJROE1GNUIwaEQ5WVpqcXllZEhEQWloQTF5b0hFbmx3YkVudHljSVBzd1lxRDdmbjA1czhxTwp0VTRkVGdpTGpSalIyZUdxeENmVnpxZ3ZXdWl2eGZzNFVaMjJ6bkxJNFpnZFRjcUM8L2RzOlg1MDlDZXJ0aWZpY2F0ZT48L2RzOlg1MDlEYXRhPjwvZHM6S2V5SW5mbz48L2RzOlNpZ25hdHVyZT48c2FtbDJwOk5hbWVJRFBvbGljeSBGb3JtYXQ9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpuYW1laWQtZm9ybWF0OnVuc3BlY2lmaWVkIi8+PC9zYW1sMnA6QXV0aG5SZXF1ZXN0Pg==".
vuser_init.c(45): web_url("index.html") was successful, 5814 body bytes, 589 header bytes  	[MsgId: MMSG-26386]
vuser_init.c(57): web_set_sockets_option started  	[MsgId: MMSG-26355]
vuser_init.c(57): web_set_sockets_option was successful  	[MsgId: MMSG-26392]
vuser_init.c(59): web_submit_data("SSO") started  	[MsgId: MMSG-26355]
vuser_init.c(59): Notify: Next row for parameter pURL_SSO = 1 [table  = pURL_SSO].
vuser_init.c(59): Notify: Parameter Substitution: parameter "pURL_SSO" =  "https://eom-mip-perf.childrensplace.com:15001"
vuser_init.c(59): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(59): Notify: Parameter Substitution: parameter "c_SAML_Request" =  "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c2FtbDJwOkF1dGhuUmVxdWVzdCB4bWxuczpzYW1sMnA9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpwcm90b2NvbCIgQXNzZXJ0aW9uQ29uc3VtZXJTZXJ2aWNlVVJMPSJodHRwOi8vZW9tLWNhLXBlcmYudGNwaHEudGNwY29ycC5sb2NhbC5jb206MzAwMDAvc2FtbC9TU08vYWxpYXMvY2EiIERlc3RpbmF0aW9uPSJodHRwczovL2VvbS1taXAtcGVyZi5jaGlsZHJlbnNwbGFjZS5jb206MTUwMDEvcHJvZmlsZS9TQU1MMi9QT1NUL1NTTyIgRm9yY2VBdXRobj0iZmFsc2UiIElEPSJhMzdkaTM2YWM0MzY2MDI2NGQyOGJmMGFhN2UyNTliIiBJc1Bhc3NpdmU9ImZhbHNlIiBJc3N1ZUluc3RhbnQ9IjIwMTctMDgtMjRUMDY6NDE6MTkuODgxWiIgUHJvdG9jb2xCaW5kaW5nPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YmluZGluZ3M6SFRUUC1BcnRpZmFjdCIgVmVyc2lvbj0iMi4wIj48c2FtbDI6SXNzdWVyIHhtbG5zOnNhbWwyPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YXNzZXJ0aW9uIj5jYTwvc2FtbDI6SXNzdWVyPjxkczpTaWduYXR1cmUgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxkczpTaWduZWRJbmZvPjxkczpDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8+PGRzOlNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNyc2Etc2hhMSIvPjxkczpSZWZlcmVuY2UgVVJJPSIjYTM3ZGkzNmFjNDM2NjAyNjRkMjhiZjBhYTdlMjU5YiI+PGRzOlRyYW5zZm9ybXM+PGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNlbnZlbG9wZWQtc2lnbmF0dXJlIi8+PGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIvPjwvZHM6VHJhbnNmb3Jtcz48ZHM6RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3NoYTEiLz48ZHM6RGlnZXN0VmFsdWU+UlBqZkt4Z25aa0VEeklzV05kTklXL2pZMDlVPTwvZHM6RGlnZXN0VmFsdWU+PC9kczpSZWZlcmVuY2U+PC9kczpTaWduZWRJbmZvPjxkczpTaWduYXR1cmVWYWx1ZT5hT3d5MzlzOXF4S1dzVEJnTzk2RzRhODFTUDgwYlJYTG81dzZUZWtlMktjeG1vWVFtS2Q1Q003MEJGanlTWmFGT0dUQ3A4d2FRK3BqNTZZaWVBTWlWVU1yQlFyellteWhkYXo4YTROM3JaVFh4Ry9oVFRGcU1tZ2FYWDdWQm85UzRwOENWamQvQjcrY2oxVlFmUFl5YkFOdkxGdGxVM0JpK2dLeWxocTRiT0syVDcwaUViVkNnU3RWNEJOOFJ3ZjFBUTNEdmxmN3J1QktQcU5CMGxjcGNMeUsvbW5rSzg0MWdnV2xuNGhyT2licHdoN0hmUjVPanRFUFZ6WlFkWDhrSWlIcnA3b1JvTXNBTDBRaG9SMk5SR3BtUVBGK25UVEoveVRVVmwxbEd4UWlzY1dpSVZnT2YvSzRBZFdLb0Z4dXNVRnVxeHgwTll6eEZDQnNvL0IrSHc9PTwvZHM6U2lnbmF0dXJlVmFsdWU+PGRzOktleUluZm8+PGRzOlg1MDlEYXRhPjxkczpYNTA5Q2VydGlmaWNhdGU+TUlJREJUQ0NBZTBDQkZLRG5GRXdEUVlKS29aSWh2Y05BUUVOQlFBd1J6RUxNQWtHQTFVRUJoTUNWVk14SWpBZ0JnTlZCQW9NR1UxaApibWhoZEhSaGJpQkJjM052WTJsaGRHVnpMQ0JKYm1NeEZEQVNCZ05WQkFNTUMzTndMbTFoYm1ndVkyOXRNQjRYRFRFek1URXhNekUxCk16VTBOVm9YRFRNek1URXdPREUxTXpVME5Wb3dSekVMTUFrR0ExVUVCaE1DVlZNeElqQWdCZ05WQkFvTUdVMWhibWhoZEhSaGJpQkIKYzNOdlkybGhkR1Z6TENCSmJtTXhGREFTQmdOVkJBTU1DM053TG0xaGJtZ3VZMjl0TUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQwpBUThBTUlJQkNnS0NBUUVBalJ5cDRBdFpwdXVOdk5EUTFqSFNzWjZER0dQaU9DTFpaUG9Xd3g0ejA1cWNHNDgxMUxVWFpsRm8vTlprCkZ4djVsbWRqbHZJYjRvODNsbHVqMWRJY0VKcUJQSnFwS1k5L3lJK3B1RW0xQUVKOWpPQ0hmK2N5ZERCMzZ2ZTczVW12d1paMERMU28KbFpWOXk1bXJhSFpoMEtXWlpnaWJqdFlwK3JsQVNrSGJNQjZlY1lXNk9aUk9Zc2hHTkRLa2lJK3dkcWxNdjhKN01OV1FpeUxOUUtzQQpCUUltd3l3L2VDTmt0cVFYMy9yWk11empJVFUwbTFtZm1NQ1NHcnJsQkRnMmpHaEk2d3FiVU9BdDBST3pzKy8rdmk5MjIzN1p2c1VDCjlwNFdaSjRLSkkrSXBpNE4veEZyRlF5TVErN1c1NlBoVWlOaTR5RS9zcU1vekxkMmtJK0pOUUlEQVFBQk1BMEdDU3FHU0liM0RRRUIKRFFVQUE0SUJBUUNGNml4cXBVejllRjhpTktBMitzNkt2UWs1SHZ6cXQ5M1hoWUJxRE9ZOXA0QzMvc0hUc2x0K3EzejFwb0xGZ2pCbwphb2g1aXlqSWRPK252LzdJRHVnaFV0cktZUWlpRk9saVA2UVovZFI2c2pPRW8vb0hVVEU1c016MFo4ZXBxUkFiVXBOVTJDMU1FcTlqClR2akZRclBybTVPTUlrOWZod084TUN2ajJ2Z2VmZDZ0d3NrcXliWnlMOUxMV0FNd0hvQVNjdVM4UG1RVTR4TVpPVE0zcWRUOUVCRUkKMEhjQ3Z2bFJzR0NNMVBkUVNZMG1nZlJROE1GNUIwaEQ5WVpqcXllZEhEQWloQTF5b0hFbmx3YkVudHljSVBzd1lxRDdmbjA1czhxTwp0VTRkVGdpTGpSalIyZUdxeENmVnpxZ3ZXdWl2eGZzNFVaMjJ6bkxJNFpnZFRjcUM8L2RzOlg1MDlDZXJ0aWZpY2F0ZT48L2RzOlg1MDlEYXRhPjwvZHM6S2V5SW5mbz48L2RzOlNpZ25hdHVyZT48c2FtbDJwOk5hbWVJRFBvbGljeSBGb3JtYXQ9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpuYW1laWQtZm9ybWF0OnVuc3BlY2lmaWVkIi8+PC9zYW1sMnA6QXV0aG5SZXF1ZXN0Pg=="
vuser_init.c(59): Redirecting "https://eom-mip-perf.childrensplace.com:15001/profile/SAML2/POST/SSO" (redirection depth is 0)  	[MsgId: MMSG-26694]
vuser_init.c(59): To location "https://eom-mip-perf.childrensplace.com:15001/AuthnEngine"  	[MsgId: MMSG-26693]
vuser_init.c(59): Redirecting "https://eom-mip-perf.childrensplace.com:15001/AuthnEngine" (redirection depth is 1)  	[MsgId: MMSG-26694]
vuser_init.c(59): To location "https://eom-mip-perf.childrensplace.com:15001/Authn/RemoteUser?loginContextKey=d3252fc4-ed0a-43ed-a21f-d59713a7bdc1"  	[MsgId: MMSG-26693]
vuser_init.c(59): Redirecting "https://eom-mip-perf.childrensplace.com:15001/Authn/RemoteUser?loginContextKey=d3252fc4-ed0a-43ed-a21f-d59713a7bdc1" (redirection depth is 2)  	[MsgId: MMSG-26694]
vuser_init.c(59): To location "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26693]
vuser_init.c(59): Found resource "https://eom-mip-perf.childrensplace.com:15001/ext/resources/css/ext-all.css" in HTML "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26659]
vuser_init.c(59): Found resource "https://eom-mip-perf.childrensplace.com:15001/manh/resources/css/mip.css" in HTML "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26659]
vuser_init.c(59): Found resource "https://eom-mip-perf.childrensplace.com:15001/ext/ext-all.js" in HTML "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26659]
vuser_init.c(59): Found resource "https://eom-mip-perf.childrensplace.com:15001/app.js" in HTML "https://eom-mip-perf.childrensplace.com:15001/login.jsp"  	[MsgId: MMSG-26659]
vuser_init.c(59): web_submit_data("SSO") was successful, 462766 body bytes, 2796 header bytes, 6426 chunking overhead bytes  	[MsgId: MMSG-26385]
vuser_init.c(87): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_09_ECOM_US_Cancel_Order"
vuser_init.c(87): Notify: Transaction "TCP_09_ECOM_US_Cancel_Order_01_Launch" ended with "Pass" status (Duration: 9.4873 Wasted Time: 6.7318).
vuser_init.c(93): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_09_ECOM_US_Cancel_Order"
vuser_init.c(93): Notify: Transaction "TCP_09_ECOM_US_Cancel_Order_02_Login" started.
vuser_init.c(95): web_submit_data("j_spring_security_check") started  	[MsgId: MMSG-26355]
vuser_init.c(95): Notify: Next row for parameter pURL_SSO = 1 [table  = pURL_SSO].
vuser_init.c(95): Notify: Parameter Substitution: parameter "pURL_SSO" =  "https://eom-mip-perf.childrensplace.com:15001"
vuser_init.c(95): Notify: Parameter Substitution: parameter "pUserName" =  "User094"
vuser_init.c(95): Notify: Next row for parameter pPassword = 1 [table  = pPassword].
vuser_init.c(95): Notify: Parameter Substitution: parameter "pPassword" =  "password"
vuser_init.c(95): Redirecting "https://eom-mip-perf.childrensplace.com:15001/j_spring_security_check" (redirection depth is 0)  	[MsgId: MMSG-26694]
vuser_init.c(95): To location "https://eom-mip-perf.childrensplace.com:15001/Authn/RemoteUser?loginContextKey=d3252fc4-ed0a-43ed-a21f-d59713a7bdc1"  	[MsgId: MMSG-26693]
vuser_init.c(95): Redirecting "https://eom-mip-perf.childrensplace.com:15001/Authn/RemoteUser?loginContextKey=d3252fc4-ed0a-43ed-a21f-d59713a7bdc1" (redirection depth is 1)  	[MsgId: MMSG-26694]
vuser_init.c(95): To location "https://eom-mip-perf.childrensplace.com:15001/profile/SAML2/POST/SSO"  	[MsgId: MMSG-26693]
vuser_init.c(95): Redirecting "https://eom-mip-perf.childrensplace.com:15001/profile/SAML2/POST/SSO" (redirection depth is 2)  	[MsgId: MMSG-26694]
vuser_init.c(95): To location "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/saml/SSO/alias/ca?SAMLart=AAQAAf5QSaNfZd4JBmYl5vT6TLHqylbw8VznY9fUeyIdPdSP3furwA723PI%3D"  	[MsgId: MMSG-26693]
vuser_init.c(95): Redirecting "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/saml/SSO/alias/ca?SAMLart=AAQAAf5QSaNfZd4JBmYl5vT6TLHqylbw8VznY9fUeyIdPdSP3furwA723PI%3D" (redirection depth is 3)  	[MsgId: MMSG-26694]
vuser_init.c(95): To location "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26693]
vuser_init.c(95): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/extstyles" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(95): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/ext/ext-all.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(95): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/ext/ext-theme-neptune.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(95): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/hammer.min.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(95): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/overrides.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(95): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/webtop.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(95): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/bootstrap.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(95): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/webtop-overrides.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(95): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/mps.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(95): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/html2canvas.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(95): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/notifications/portal.min.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(95): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/notifications/atmosphere-min.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(95): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/lps/resources/common/scripts/3rdparty/jquery/jquery.min.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(95): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/highcharts/js/highcharts.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(95): Found resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/js/highcharts/js/highcharts-more.js" in HTML "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/index.html"  	[MsgId: MMSG-26659]
vuser_init.c(95): web_submit_data("j_spring_security_check") was successful, 2034925 body bytes, 6518 header bytes  	[MsgId: MMSG-26386]
vuser_init.c(309): web_url("ping.jsp") started  	[MsgId: MMSG-26355]
vuser_init.c(309): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(309): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(309): web_url("ping.jsp") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
vuser_init.c(336): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_09_ECOM_US_Cancel_Order"
vuser_init.c(336): Notify: Transaction "TCP_09_ECOM_US_Cancel_Order_02_Login" ended with "Pass" status (Duration: 17.2955 Wasted Time: 11.5191).
vuser_init.c(340): web_url("ping.jsp_2") started  	[MsgId: MMSG-26355]
vuser_init.c(340): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(340): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(340): web_url("ping.jsp_2") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
vuser_init.c(350): web_url("ping.jsp_3") started  	[MsgId: MMSG-26355]
vuser_init.c(350): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(350): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
vuser_init.c(350): web_url("ping.jsp_3") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Ending action vuser_init.
Running Vuser...
Starting iteration 1.
Starting action Action.
Action.c(6): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_09_ECOM_US_Cancel_Order"
Action.c(6): Notify: Transaction "TCP_09_ECOM_US_Cancel_Order_03_Goto_Order_tile_enter_order_and_click_on_search" started.
Action.c(8): web_url("ping.jsp_4") started  	[MsgId: MMSG-26355]
Action.c(8): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(8): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(8): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(8): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(8): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/olm/resources/icons/trigger/search_trigger.png" (specified by argument number 10)  	[MsgId: MMSG-26577]
Action.c(8): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/olm/ds/model/customertransaction/OrderSearchRequest.js?_dc=1467027853594" (specified by argument number 13)  	[MsgId: MMSG-26577]
Action.c(8): web_url("ping.jsp_4") was successful, 5252 body bytes, 802 header bytes  	[MsgId: MMSG-26386]
Action.c(21): web_convert_from_formatted started  	[MsgId: MMSG-26355]
Action.c(21): Notify: Parameter Substitution: parameter "pOrder_No" =  "6044590058"
Action.c(21): Notify: Saving Parameter "DFE_BODY = {"customerOrderSearchCriteria":{"entityType":"All","orderNumber":"6044590058","parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":10,"customerInfo":"","customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}".
Action.c(21): web_convert_from_formatted was successful  	[MsgId: MMSG-26392]
Action.c(45): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(45): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(52): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(52): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(59): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(59): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(66): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(66): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(73): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(73): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(79): web_set_user started  	[MsgId: MMSG-26355]
Action.c(79): Notify: Parameter Substitution: parameter "pUserName" =  "User094"
Action.c(79): Notify: Next row for parameter pPassword = 1 [table  = pPassword].
Action.c(79): Notify: Parameter Substitution: parameter "pPassword" =  "password"
Action.c(79): web_set_user was successful  	[MsgId: MMSG-26392]
Action.c(81): web_custom_request("customerOrderAndTransactionList") started  	[MsgId: MMSG-26355]
Action.c(81): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(81): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(81): Notify: Parameter Substitution: parameter "DFE_BODY" =  "{"customerOrderSearchCriteria":{"entityType":"All","orderNumber":"6044590058","parentOrderNumber":"","createdFromDate":"","createdToDate":"","noOfRecordsPerPage":10,"customerInfo":"","customerBasicInfo":"","sortingCriterion":{"sortField":"createdDTTM","sortDirection":"DESC"},"currentPageNumber":0}}"
Action.c(81): Notify: Saving Parameter "c_externalCustomerId = 64003958".
Action.c(81): Notify: Saving Parameter "c_First_Name = Data Dump".
Action.c(81): Notify: Saving Parameter "c_Last_Name = Data Dump".
Action.c(81): Notify: Saving Parameter "c_Phone_Number = 0000000000".
Action.c(81): Notify: Saving Parameter "c_Email_Id = datadump@manh.com".
Action.c(81): web_custom_request("customerOrderAndTransactionList") was successful, 554 body bytes, 186 header bytes, 12 chunking overhead bytes  	[MsgId: MMSG-26385]
Action.c(94): web_url("ping.jsp_5") started  	[MsgId: MMSG-26355]
Action.c(94): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(94): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(94): web_url("ping.jsp_5") was successful, 15 body bytes, 194 header bytes  	[MsgId: MMSG-26386]
Action.c(272): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_09_ECOM_US_Cancel_Order"
Action.c(272): Notify: Transaction "TCP_09_ECOM_US_Cancel_Order_03_Goto_Order_tile_enter_order_and_click_on_search" ended with "Pass" status (Duration: 15.2646 Wasted Time: 2.4427).
Action.c(278): Notify: Parameter Substitution: parameter "sTestCaseName" =  "TCP_09_ECOM_US_Cancel_Order"
Action.c(278): Notify: Transaction "TCP_09_ECOM_US_Cancel_Order_04_Click_on_Order_searched" started.
Action.c(280): web_custom_request("windows") started  	[MsgId: MMSG-26355]
Action.c(280): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(280): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(280): web_custom_request("windows") was successful, 24 body bytes, 186 header bytes, 11 chunking overhead bytes  	[MsgId: MMSG-26385]
Action.c(420): web_reg_save_param started  	[MsgId: MMSG-26355]
Action.c(420): Registering web_reg_save_param was successful  	[MsgId: MMSG-26390]
Action.c(426): web_url("ping.jsp_6") started  	[MsgId: MMSG-26355]
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Notify: Parameter Substitution: parameter "pOrder_No" =  "6044590058"
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Notify: Parameter Substitution: parameter "pURL" =  "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000"
Action.c(426): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/olm/common/view/payment/AbstractPaymentView.js?_dc=1467027964979" (specified by argument number 10)  	[MsgId: MMSG-26577]
Action.c(426): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/services/olm/systemProperties/getKey?_dc=1467027969007&keyName=BING_MAP_KEY" (specified by argument number 13)  	[MsgId: MMSG-26577]
Action.c(426): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/olm/resources/icons/ds/customer/email.png" (specified by argument number 16)  	[MsgId: MMSG-26577]
Action.c(426): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/olm/resources/icons/ds/customer/phone.png" (specified by argument number 19)  	[MsgId: MMSG-26577]
Action.c(426): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/olm/resources/icons/ds/customer/customer_id.png" (specified by argument number 22)  	[MsgId: MMSG-26577]
Action.c(426): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/olm/resources/icons/ds/search/customer.png" (specified by argument number 25)  	[MsgId: MMSG-26577]
Action.c(426): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/olm/resources/icons/ds/search/order.png" (specified by argument number 28)  	[MsgId: MMSG-26577]
Action.c(426): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/olm/resources/icons/ds/search/return.png" (specified by argument number 31)  	[MsgId: MMSG-26577]
Action.c(426): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/olm/resources/icons/ds/search/item.png" (specified by argument number 34)  	[MsgId: MMSG-26577]
Action.c(426): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/olm/resources/icons/collapse/collapseArrow.png" (specified by argument number 37)  	[MsgId: MMSG-26577]
Action.c(426): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/services/olm/customerorder/customerOrderDetails?_dc=1467027969205&responseType=FullCustomerOrder&customerOrderNumber=6044590058&page=1&start=0&limit=25" (specified by argument number 40)  	[MsgId: MMSG-26577]
Action.c(426): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/olm/resources/icons/radio/radio_button.png" (specified by argument number 43)  	[MsgId: MMSG-26577]
Action.c(426): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/olm/resources/icons/collapse/downArrow.png" (specified by argument number 46)  	[MsgId: MMSG-26577]
Action.c(426): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/resources/css/gray/images/grid/hd-pop.png" (specified by argument number 49)  	[MsgId: MMSG-26577]
Action.c(426): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/olm/resources/icons/splitter/drag_divider.png" (specified by argument number 52)  	[MsgId: MMSG-26577]
Action.c(426): Downloading resource "http://eom-ca-perf.tcphq.tcpcorp.local.com:30000/manh/olm/common/components/StoreLoadMask.js?_dc=1467027969210" (specified by argument number 55)  	[MsgId: MMSG-26577]
Action.c(426): Error -26377: No match found for the requested parameter "c_OrderId". Check whether the requested boundaries exist in the response data. Also, if the data you want to save exceeds 9999 bytes, use web_set_max_html_param_len to increase the parameter size, Snapshot Info [MSH 1 12]  	[MsgId: MERR-26377]
Action.c(426): Notify: Saving Parameter "c_OrderId = ".
Action.c(426): web_url("ping.jsp_6") highest severity level was "ERROR", 34056 body bytes, 4818 header bytes, 12 chunking overhead bytes  	[MsgId: MMSG-26387]
Action.c(426): Notify: Transaction "TCP_09_ECOM_US_Cancel_Order_04_Click_on_Order_searched" ended with "Fail" status (Duration: 19.7698 Wasted Time: 9.2949).
Ending action Action.
Ending iteration 1.
Ending Vuser...
Starting action vuser_end.
Abort was called from an action.
