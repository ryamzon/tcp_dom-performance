/* -------------------------------------------------------------------------------
	Script Title       : 
	Script Description : 
                        
                        
	Recorder Version   : 2739
   ------------------------------------------------------------------------------- */

vuser_init()
{

	web_cache_cleanup();
	
	web_cleanup_cookies();
	
	web_set_max_html_param_len("9999");
	
	web_set_sockets_option("SSL_VERSION", "TLS");
	
	web_set_sockets_option("CLOSE_KEEPALIVE_CONNECTIONS", "1");
	
	web_set_sockets_option("IGNORE_PREMATURE_SHUTDOWN", "1"); 

	/*Correlation comment - Do not change!  Original value='PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c2FtbDJwOkF1dGhuUmVxdWVzdCB4bWxuczpzYW1sMnA9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpwcm90b2NvbCIgQXNzZXJ0aW9uQ29uc3VtZXJTZXJ2aWNlVVJMPSJodHRwOi8vbmppdGxkb21hcHAwMS50Y3BocS50Y3Bjb3JwLmxvY2FsLmNvbTozMDAwMC9zYW1sL1NTTy9hbGlhcy9jYSIgRGVzdGluYXRpb249Imh0dHBzOi8vbmppdGxkb21hcHAwMS50Y3BocS50Y3Bjb3JwLmxvY2FsLmNvbToxNTAwMS9wcm9maWxlL1NBTUwyL1BPU1QvU1NPIiBGb3JjZUF1dGhuPSJmYWxzZSIgSUQ9ImEzM2Q4M2Q3NWQyZWIwZzQ1MjVkMjNiOWkwMjhlaTIiIElzUGFzc2l2ZT0iZmFsc2UiIElzc3VlSW5zdGFudD0iMjAxNi0wNi0yN1QxMTozOToyMi43MjZaIiBQcm90b2NvbEJpbmRpbmc9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpiaW5kaW5nczpIVFRQLUFydGlmYWN0IiBWZXJzaW9uPSIyLjAiPjxzYW1sMjpJc3N1ZXIgeG1sbnM6c2FtbDI9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iPmNhPC9zYW1sMjpJc3N1ZXI+PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+PGRzOlNpZ25lZEluZm8+PGRzOkNhbm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48ZHM6U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3JzYS1zaGExIi8+PGRzOlJlZmVyZW5jZSBVUkk9IiNhMzNkODNkNzVkMmViMGc0NTI1ZDIzYjlpMDI4ZWkyIj48ZHM6VHJhbnNmb3Jtcz48ZHM6VHJhbnNmb3JtIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI2VudmVsb3BlZC1zaWduYXR1cmUiLz48ZHM6VHJhbnNmb3JtIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8+PC9kczpUcmFuc2Zvcm1zPjxkczpEaWdlc3RNZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjc2hhMSIvPjxkczpEaWdlc3RWYWx1ZT5LNzNCU2xGUlZ0R05KUG50NzUreVRFNFBSa2s9PC9kczpEaWdlc3RWYWx1ZT48L2RzOlJlZmVyZW5jZT48L2RzOlNpZ25lZEluZm8+PGRzOlNpZ25hdHVyZVZhbHVlPk82RkF5K0RYL1Y0Z3k5MFVGTVR5dVlLWjhDb2RDVHplakNBemxMcURKMkZlcjBHM0ZHaXZnd3RaMHY4Z0ZQSWdaejh5enQzYmtvTlJZS1dMZFZ5Wks0U2tlZ3dFdElMYnY4TmJmeE0wenl3NlFEdXNITUNJQS82NzZjTzRvVXlhT21pQVpwSHROQktrTUhWT2ZydjNMTlBmeUg5MU1ERVdyMzQ4Ump5dW1qRlFJRG1Md21yM2NLWDR5V2hMUjV1NWIzeUs2L29lMWNKaGRuRVZTM1Jtd3RIKyt4MjB5aDQ5Y1drM1RidGJRZ0hsOFRlVUtGSU5kNFBzLzJsZzVlRVM4cXZ5NlFsU3pJTnhFb0sxQUR5VG0wUG1MNkdxZGRuQ0ppN20xZ0R3NnZmMmNQdmhObUN5ajk4a3RCWWNjUXB0a3oxR2RycDU4OXZ2TEhuSlpVU0NIdz09PC9kczpTaWduYXR1cmVWYWx1ZT48ZHM6S2V5SW5mbz48ZHM6WDUwOURhdGE+PGRzOlg1MDlDZXJ0aWZpY2F0ZT5NSUlEQlRDQ0FlMENCRktEbkZFd0RRWUpLb1pJaHZjTkFRRU5CUUF3UnpFTE1Ba0dBMVVFQmhNQ1ZWTXhJakFnQmdOVkJBb01HVTFoCmJtaGhkSFJoYmlCQmMzTnZZMmxoZEdWekxDQkpibU14RkRBU0JnTlZCQU1NQzNOd0xtMWhibWd1WTI5dE1CNFhEVEV6TVRFeE16RTEKTXpVME5Wb1hEVE16TVRFd09ERTFNelUwTlZvd1J6RUxNQWtHQTFVRUJoTUNWVk14SWpBZ0JnTlZCQW9NR1UxaGJtaGhkSFJoYmlCQgpjM052WTJsaGRHVnpMQ0JKYm1NeEZEQVNCZ05WQkFNTUMzTndMbTFoYm1ndVkyOXRNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DCkFROEFNSUlCQ2dLQ0FRRUFqUnlwNEF0WnB1dU52TkRRMWpIU3NaNkRHR1BpT0NMWlpQb1d3eDR6MDVxY0c0ODExTFVYWmxGby9OWmsKRnh2NWxtZGpsdkliNG84M2xsdWoxZEljRUpxQlBKcXBLWTkveUkrcHVFbTFBRUo5ak9DSGYrY3lkREIzNnZlNzNVbXZ3WlowRExTbwpsWlY5eTVtcmFIWmgwS1daWmdpYmp0WXArcmxBU2tIYk1CNmVjWVc2T1pST1lzaEdOREtraUkrd2RxbE12OEo3TU5XUWl5TE5RS3NBCkJRSW13eXcvZUNOa3RxUVgzL3JaTXV6aklUVTBtMW1mbU1DU0dycmxCRGcyakdoSTZ3cWJVT0F0MFJPenMrLyt2aTkyMjM3WnZzVUMKOXA0V1pKNEtKSStJcGk0Ti94RnJGUXlNUSs3VzU2UGhVaU5pNHlFL3NxTW96TGQya0krSk5RSURBUUFCTUEwR0NTcUdTSWIzRFFFQgpEUVVBQTRJQkFRQ0Y2aXhxcFV6OWVGOGlOS0EyK3M2S3ZRazVIdnpxdDkzWGhZQnFET1k5cDRDMy9zSFRzbHQrcTN6MXBvTEZnakJvCmFvaDVpeWpJZE8rbnYvN0lEdWdoVXRyS1lRaWlGT2xpUDZRWi9kUjZzak9Fby9vSFVURTVzTXowWjhlcHFSQWJVcE5VMkMxTUVxOWoKVHZqRlFyUHJtNU9NSWs5Zmh3TzhNQ3ZqMnZnZWZkNnR3c2txeWJaeUw5TExXQU13SG9BU2N1UzhQbVFVNHhNWk9UTTNxZFQ5RUJFSQowSGNDdnZsUnNHQ00xUGRRU1kwbWdmUlE4TUY1QjBoRDlZWmpxeWVkSERBaWhBMXlvSEVubHdiRW50eWNJUHN3WXFEN2ZuMDVzOHFPCnRVNGRUZ2lMalJqUjJlR3F4Q2ZWenFndld1aXZ4ZnM0VVoyMnpuTEk0WmdkVGNxQzwvZHM6WDUwOUNlcnRpZmljYXRlPjwvZHM6WDUwOURhdGE+PC9kczpLZXlJbmZvPjwvZHM6U2lnbmF0dXJlPjxzYW1sMnA6TmFtZUlEUG9saWN5IEZvcm1hdD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOm5hbWVpZC1mb3JtYXQ6dW5zcGVjaWZpZWQiLz48L3NhbWwycDpBdXRoblJlcXVlc3Q+' Name ='CorrelationParameter' Type ='ResponseBased'*/
/*	web_reg_save_param_regexp(
		"ParamName=CorrelationParameter",
		"RegExp=name=\"SAMLRequest\"\\ value=\"(.*?)\"/>\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ ",
		SEARCH_FILTERS,
		"Scope=Body",
		"IgnoreRedirections=No",
		LAST); */
		
	web_reg_save_param("c_SAML_Request",
	                   "LB=name=\"SAMLRequest\" value=\"",
	                   "RB=\"/>",
	                   "ORD=1",
	                   "Search=ALL",LAST);
	
	lr_save_string("TCP_09_ECOM_US_Cancel_Order","sTestCaseName");
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_01_Launch"));
	
	/* Launch */

	web_url("index.html", 
		"URL={pURL}/manh/index.html", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=../favicon.ico", "Referer=", ENDITEM, 
		LAST);

	web_set_sockets_option("SSL_VERSION", "TLS");

	web_submit_data("SSO",
		"Action={pURL_SSO}/profile/SAML2/POST/SSO",
		"Method=POST",
		"TargetFrame=",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t2.inf",
		"Mode=HTML",
		ITEMDATA,
		"Name=SAMLRequest", "Value={c_SAML_Request}", ENDITEM,
	/*	EXTRARES,
		"URL=/ext/resources/ext-theme-classic/ext-theme-classic-all.css", "Referer={pURL_SSO}/ext/resources/css/ext-all.css", ENDITEM,
		"URL=/manh/view/LoadExtjsLocale.js?_dc=1467027597026", "Referer={pURL_SSO}/login.jsp", ENDITEM,
		"URL=/manh/view/LoginScreen.js?_dc=1467027597025", "Referer={pURL_SSO}/login.jsp", ENDITEM,
		"URL=/ext/locale/ext-lang-en.js", "Referer={pURL_SSO}/login.jsp", ENDITEM,
		"URL=/manh/view/MipDynamicCSS.js?_dc=1467027597882", "Referer={pURL_SSO}/login.jsp", ENDITEM,
		"URL=/manh/resources/images/ma_login_logo.png", "Referer={pURL_SSO}/login.jsp", ENDITEM,
		"URL=/manh/resources/images/error_icon.png", "Referer={pURL_SSO}/login.jsp", ENDITEM,
		"URL=/ext/resources/ext-theme-classic/images/form/exclamation.gif", "Referer={pURL_SSO}/ext/resources/ext-theme-classic/ext-theme-classic-all.css", ENDITEM,
		"URL=/manh/resources/images/warning_icon.png", "Referer={pURL_SSO}/login.jsp", ENDITEM,
		"URL=/ext/resources/ext-theme-classic/images/form/text-bg.gif", "Referer={pURL_SSO}/ext/resources/ext-theme-classic/ext-theme-classic-all.css", ENDITEM,
		"URL=/manh/resources/images/signinbtn_disabled.gif", "Referer={pURL_SSO}/login.jsp", ENDITEM,
		"URL=/favicon.ico", ENDITEM,
		"URL=/ext/resources/ext-theme-classic/images/grid/invalid_line.gif", "Referer={pURL_SSO}/ext/resources/ext-theme-classic/ext-theme-classic-all.css", ENDITEM,
		"URL=/manh/resources/images/signinbtn_normal_hover_click.gif", "Referer={pURL_SSO}/login.jsp", ENDITEM,
		"URL=/ext/resources/ext-theme-classic/images/grid/loading.gif", "Referer={pURL_SSO}/ext/resources/ext-theme-classic/ext-theme-classic-all.css", ENDITEM, */
		LAST);
	
	lr_end_transaction(lr_eval_string("{sTestCaseName}_01_Launch"),LR_AUTO);
	
	lr_think_time(1);
	
	/* Login */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_02_Login"));

	web_submit_data("j_spring_security_check", 
		"Action={pURL_SSO}/j_spring_security_check", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"Mode=HTML", 
		"EncodeAtSign=YES", 
		ITEMDATA, 
		"Name=j_username", "Value={pUserName}", ENDITEM, 
		"Name=j_password", "Value={pPassword}", ENDITEM, 
	/*	EXTRARES, 
		"Url={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/resources/css/gray/mps-gray.css", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/resources/css/gray/webtop-gray.css", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/Util.js?_dc=1467027706359", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/utils/AppHelper.js?_dc=1467027706361", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/utils/DataLoader.js?_dc=1467027706361", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/Application.js?_dc=1467027706362", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/custom/print/FormPanelRenderer.js?_dc=1467027706362", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/custom/print/Printer.js?_dc=1467027706362", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/custom/print/GridPanelRenderer.js?_dc=1467027706362", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/custom/print/ChartRenderer.js?_dc=1467027706363", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/utils/NotificationManager.js?_dc=1467027706363", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/utils/Portlet.js?_dc=1467027706363", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/store/menu/FullMenus.js?_dc=1467027706901", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/store/Regions.js?_dc=1467027706902", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/store/menu/InfoMenus.js?_dc=1467027706902", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/store/menu/QuickMenus.js?_dc=1467027706901", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/filter/utils/ExpressionBuilder.js?_dc=1467027706363", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/store/Users.js?_dc=1467027706903", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/custom/print/BaseRenderer.js?_dc=1467027708144", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/view/SolutionSelector.js?_dc=1467027706904", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/model/UserPreference.js?_dc=1467027706904", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/store/Themes.js?_dc=1467027706904", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/store/Solutions.js?_dc=1467027706903", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/store/Notifications.js?_dc=1467027708159", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/model/menu/Info.js?_dc=1467027708572", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/model/menu/Menu.js?_dc=1467027708571", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/dashboard/viewDashboard/view/dashboard/util/ExtractUtil.js?_dc=1467027708565", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/dashboard/viewDashboard/store/Query.js?_dc=1467027708564", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/model/Notification.js?_dc=1467027708160", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/model/BusinessUnit.js?_dc=1467027708574", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/dashboard/viewDashboard/model/Query.js?_dc=1467027709619", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/model/Theme.js?_dc=1467027709131", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/model/Solution.js?_dc=1467027709130", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/model/User.js?_dc=1467027709127", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/model/Application.js?_dc=1467027710059", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/model/SolutionXref.js?_dc=1467027710060", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/model/Region.js?_dc=1467027710811", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/view/Solutions.js?_dc=1467027711240", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/view/Menu.js?_dc=1467027711241", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/LabelService/labels/2?_dc=1467027710803", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/LabelService/labels/1?_dc=1467027710801", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/NotificationService/notifications?_dc=1467027710851&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/model/Workspace.js?_dc=1467027711693", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/utils/MenuSearch.js?_dc=1467027713898", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/model/menu/Section.js?_dc=1467027713898", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/model/menu/Part.js?_dc=1467027713899", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/model/menu/Item.js?_dc=1467027713900", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/view/menu/AddToQuick.js?_dc=1467027714357", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/resources/css/gray/images/loadmask/loading.gif", "Referer={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", ENDITEM, 
		"Url={pURL}/services/rest/lps/SolutionService/solution/list?_dc=1467027715801&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/UserService/user/current?_dc=1467027716682&solutionId=42&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/MenuService/menu/quick?_dc=1467027716685&solutionId=42&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/MenuService/menu/full?_dc=1467027716689&solutionId=42&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/MenuService/menu/info?_dc=1467027716691&solutionId=42&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/ext/locale/ext-lang-en_US.js?_dc=1467027717521", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/ThemeService/themes?_dc=1467027716693&solutionId=42&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/WorkspaceService/workspace?_dc=1467027717523&solutionId=42&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/view/unified/User.js?_dc=1467027721309", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/apps/Sif.js?_dc=1467027721377", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/apps/Dom.js?_dc=1467027721378", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/apps/Lg.js?_dc=1467027721378", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/apps/Cbo.js?_dc=1467027721378", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/apps/Kiosk.js?_dc=1467027721378", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/apps/Pod.js?_dc=1467027721378", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/apps/Tpe.js?_dc=1467027721378", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/apps/Mrf.js?_dc=1467027721379", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/apps/Da.js?_dc=1467027721380", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/apps/Rlm.js?_dc=1467027721379", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/apps/Eem.js?_dc=1467027721379", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/cbo/util/CBOCommonMAUI.js?_dc=1467027722244", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/resources/wallpapers/lightblue.jpg", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/eem/selector/store/ShipperMain.js?_dc=1467027722909", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/eem/selector/model/Shipper.js?_dc=1467027723337", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/eem/selector/store/StoreSelectionStore.js?_dc=1467027723771", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/resources/css/ds/ds-all.css", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/resources/css/dom.css", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/resources/css/fonts.css", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/resources/css/scrollbar.css", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/eem/PostLoginService/storeSelectionLabel?_dc=1467027724160&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/resources/css/modern-gen.css", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/resources/css/gray/inventory-gray.css", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/resources/css/rlm.css", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/resources/css/modern.css", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0&s=1", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/utils/audit/AppAuditor.js?_dc=1467027724171", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/utils/audit/Logger.js?_dc=1467027726388", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/utils/event/EventBus.js?_dc=1467027726827", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/utils/tools/Constants.js?_dc=1467027727190", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/utils/tools/Dimensions.js?_dc=1467027728117", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/utils/tools/BusinessConstants.js?_dc=1467027728565", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/utils/tools/Formatter.js?_dc=1467027728990", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/utils/tools/DisplayUtil.js?_dc=1467027730552", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/label/CommonLabels.js?_dc=1467027731280", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/utils/tools/StoreLocatorUtil.js?_dc=1467027732041", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/utils/tools/StoreUtil.js?_dc=1467027732539", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/utils/tools/MessageUtil.js?_dc=1467027733992", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/utils/tools/ValidationUtil.js?_dc=1467027734624", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/utils/event/EventRegistry.js?_dc=1467027735045", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/utils/vtype/CustomVTypes.js?_dc=1467027735734", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/auth/StatusCheck.js?_dc=1467027737280", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/auth/Auth.js?_dc=1467027738390", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/auth/PermissionCode.js?_dc=1467027739551", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/auth/Permission.js?_dc=1467027739947", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/auth/PermissionReasonConstant.js?_dc=1467027740375", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/utils/tools/OrderUtil.js?_dc=1467027740784", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/label/StaticValues.js?_dc=1467027741202", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//platform/util/FilterLabels.js?_dc=1467027741619", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//platform/util/SysCodeLabels.js?_dc=1467027741989", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/label/TileLabels.js?_dc=1467027742454", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/ds/screen/OrderScreenLabels.js?_dc=1467027742881", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/ds/screen/ItemScreenLabels.js?_dc=1467027744029", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/ds/screen/TransactionScreenLabels.js?_dc=1467027744864", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/rlm/screen/ReturnScreenLabels.js?_dc=1467027745236", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/ds/view/customertransaction/OrderTransactionLabels.js?_dc=1467027746087", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/rlm/view/customertransaction/return/ReturnTransactionLabels.js?_dc=1467027751756", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/rlm/view/returnstatus/ReturnStatusLabels.js?_dc=1467027753464", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/XtypeFactory.js?_dc=1467027753857", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/ScreenHandler.js?_dc=1467027754273", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/useractivity/ActivityTrackerConstants.js?_dc=1467027754688", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/useractivity/ActivityTrackerCodes.js?_dc=1467027755098", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/components/ext/ComboBox.js?_dc=1467027755520", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/components/ext/Trigger.js?_dc=1467027755935", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/components/ext/Text.js?_dc=1467027756351", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/components/ext/Date.js?_dc=1467027756771", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/components/ext/Image.js?_dc=1467027757148", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/components/ext/Checkbox.js?_dc=1467027757613", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/components/ext/Button.js?_dc=1467027758022", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/components/ext/Radio.js?_dc=1467027758441", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/components/ext/Label.js?_dc=1467027758863", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/store/proxy/Rest.js?_dc=1467027759484", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/components/ext/ExtWriter.js?_dc=1467027759903", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/useractivity/ActivityTracker.js?_dc=1467027760333", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/store/EventStore.js?_dc=1467027760747", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/workflow/view/agenthistory/AgentHistoryListViewLabels.js?_dc=1467027761116", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/inventory/screen/CommerceViewScreenLabels.js?_dc=1467027761571", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/inventory/common/util/InventoryConstants.js?_dc=1467027762834", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/Util.js?_dc=1467027763200", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/inventory/screen/PerpetualInventoryScreenLabels.js?_dc=1467027763675", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/inventory/screen/OnhandActivityScreenLabels.js?_dc=1467027764091", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/inventory/screen/OutageScreenLabels.js?_dc=1467027764923", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/inventory/screen/SegmentedInventoryScreenLabels.js?_dc=1467027765346", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/inventory/screen/ErrorListScreenLabels.js?_dc=1467027765724", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/ds/view/item/details/ItemTileConfiguration.js?_dc=1467027767046", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/utils/tools/MODUtil.js?_dc=1467027767422", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/model/BaseModel.js?_dc=1467027767846", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/LabelService/labels/280020?_dc=1467027768271", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/LabelService/labels/280022?_dc=1467027773481", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/LabelService/labels/280024?_dc=1467027777433", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/LabelService/labels/280026?_dc=1467027778383", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/LabelService/labels/280028?_dc=1467027779145", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/LabelService/labels/280030?_dc=1467027780371", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/LabelService/labels/280032?_dc=1467027780792", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/LabelService/labels/280034?_dc=1467027781629", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/LabelService/labels/280036?_dc=1467027782077", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/LabelService/labels/280040?_dc=1467027782482", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/lps/LabelService/labels/280042?_dc=1467027783214", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/CustomRegistry.js?_dc=1467027784486", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/auth/TCHPPermissionCode.js?_dc=1467027784887", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/auth/TCHPPermission.js?_dc=1467027785292", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/auth/TCHPAuth.js?_dc=1467027785849", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/view/popup/notes/TCHPNotes.js?_dc=1467027786745", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/view/popup/notes/TCHPNoteList.js?_dc=1467027787108", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/ds/view/customerorder/discounts/coupons/TCHPCouponList.js?_dc=1467027787583", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/rlm/view/returns/TCHPCOItems.js?_dc=1467027787999", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/ds/view/customerorder/TCHPCustomerOrderHeaderDetails.js?_dc=1467027788418", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/ds/view/customertransaction/order/TCHPOrderHeader.js?_dc=1467027788864", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/util/auth/TCHPStatusCheck.js?_dc=1467027789269", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/olm/syscodes/loadModFlags?_dc=1467027789692&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/tpe/shipperselector/store/ShipperMain.js?_dc=1467027789707", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/tpe/PostLoginService/PostLoginService/getSelectionList?_dc=1467027790474&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/resources/icons/64/default-icon.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/ds/view/tile/ItemTile.js?_dc=1467027790551", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/ds/view/tile/OrderTile.js?_dc=1467027790552", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/ds/view/tile/CustomerTile.js?_dc=1467027790552", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/services/rest/eem/PostLoginService/selectionListWithPagination?_dc=1467027790689&page=1&start=0&limit=50", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/mps/resources/icons/64/toolbox.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/tpe/shipperselector/store/Shipper.js?_dc=1467027791084", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/view/tile/BaseTile.js?_dc=1467027792011", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/rlm/view/tile/ReturnTile.js?_dc=1467027790617", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/view/tile/BaseTile.js?_dc=1467027792012", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/view/tile/BaseTile.js?_dc=1467027792870", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/view/tile/BaseTile.js?_dc=1467027792807", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/rlm/view/tile/content/ReturnTileContent.js?_dc=1467027793269", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/view/tile/content/TileContent.js?_dc=1467027793752", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/view/search/CustomerSearchCombo.js?_dc=1467027794141", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/store/customer/CustomerSearchStore.js?_dc=1467027794587", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/model/customer/CustomerInfoModel.js?_dc=1467027794991", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/components/SearchCombo.js?_dc=1467027795422", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/view/search/CustomerOrderSearchCombo.js?_dc=1467027795853", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/ds/store/customertransaction/OrderListStore.js?_dc=1467027796228", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/model/customerorder/OrderModel.js?_dc=1467027796676", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm//common/view/search/ReturnOrderSearchCombo.js?_dc=1467027797195", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/rlm/store/customertransaction/ReturnListStore.js?_dc=1467027798580", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/rlm/model/returns/ReturnOrderModel.js?_dc=1467027798961", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/ds/view/tile/content/CustomerTileContent.js?_dc=1467027800559", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/ds/view/tile/content/OrderTileContent.js?_dc=1467027800973", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/olm/ds/view/tile/content/ItemTileContent.js?_dc=1467027801398", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url={pURL}/manh/tpe/shipperselector/model/Shipper.js?_dc=1467027801798", "Referer={pURL}/manh/index.html", ENDITEM,*/ 
		LAST);

	web_url("ping.jsp", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467027717494", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t4.inf", 
		"Mode=HTML", 
	/*	EXTRARES, 
		"Url=../manh/mps/resources/images/ma-logo.png", "Referer={pURL}/manh/resources/css/gray/webtop-gray.css", ENDITEM, 
		"Url=../manh/mps/resources/icons/ma-icon.png", "Referer={pURL}/manh/resources/css/gray/mps-gray.css", ENDITEM, 
		"Url=../manh/resources/css/gray/images/button/default-toolbar-medium-arrow.png", "Referer={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", ENDITEM, 
		"Url=../manh/mps/resources/icons/home.png", "Referer={pURL}/manh/resources/css/gray/webtop-gray.css", ENDITEM, 
		"Url=../manh/mps/resources/icons/help.png", "Referer={pURL}/manh/resources/css/gray/mps-gray.css", ENDITEM, 
		"Url=../manh/mps/resources/icons/windows.png", "Referer={pURL}/manh/resources/css/gray/webtop-gray.css", ENDITEM, 
		"Url=../manh/mps/resources/icons/window.png", "Referer={pURL}/manh/resources/css/gray/webtop-gray.css", ENDITEM, 
		"Url=../manh/mps/resources/icons/user.png", "Referer={pURL}/manh/resources/css/gray/mps-gray.css", ENDITEM, 
		"Url=../manh/resources/css/gray/images/form/trigger.png", "Referer={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", ENDITEM, 
		"Url=../manh/resources/css/gray/images/form/exclamation.png", "Referer={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/search/return_black.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/search/item_black.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/search/order_black.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/search/customer_black.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, */
		LAST);

	/*Possible OAUTH authorization was detected. It is recommended to correlate the authorization parameters.*/

	lr_end_transaction(lr_eval_string("{sTestCaseName}_02_Login"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_2", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467027812184", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t5.inf", 
		"Mode=HTML", 
		LAST);

	web_url("ping.jsp_3", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1467027827653", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t6.inf", 
		"Mode=HTML", 
		LAST);

	return 0;
}
