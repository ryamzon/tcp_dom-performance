vuser_end()
{

	/* Logout */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_09_Logout"));

	web_url("mouse.png", 
		"URL={pURL}/manh/mps/resources/icons/mouse.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer={pURL}/manh/resources/css/gray/mps-gray.css", 
		"Snapshot=t56.inf", 
		LAST);

	web_url("user.png", 
		"URL={pURL}/manh/mps/resources/icons/64/user.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t57.inf", 
		LAST);

	web_url("checkbox.png", 
		"URL={pURL}/manh/resources/css/gray/images/form/checkbox.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", 
		"Snapshot=t58.inf", 
		LAST);

	web_url("default-medium-arrow.png", 
		"URL={pURL}/manh/resources/css/gray/images/button/default-medium-arrow.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", 
		"Snapshot=t59.inf", 
		LAST);

	web_url("logout", 
		"URL={pURL}/saml/logout", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t60.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url={pURL_SSO}/images/loading.gif", "Referer={pURL_SSO}/manh/resources/css/mip.css", ENDITEM, 
		LAST);

	web_url("miplogout", 
		"URL={pURL_SSO}/miplogout", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t61.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_09_Logout"),LR_AUTO); 

	return 0;
}