Action()
{

	
	web_set_user("TESTSUPERUSER4", "Place2017!","10.18.3.185:30000");
		
	web_url("soapui-updates-os.xml", 
		"URL=http://dl.eviware.com/version-update/soapui-updates-os.xml", 
		"Resource=0", 
		"RecContentType=text/xml", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTML", 
		LAST);

	web_add_header("Authorization","Basic VEVTVFNVUEVSVVNFUjQ6UGxhY2UyMDE3IQ==");
	
	lr_think_time(1);
			
	lr_start_transaction("TCP_25_BOPIS_REST_Call_Order_History");

		web_custom_request("customerOrderAndTransactionList", 
		"URL=http://10.18.3.185:30000/services/olm/customerorder/customerOrderAndTransactionList", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=", 
		"Snapshot=t14.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={\n\"customerOrderSearchCriteria\":{\n\t\"sortingCriterion\":{\n\t\"sortField\":\"createdDTTM\",\n\t\"sortDirection\":\"DESC\"\n\t},\n\t\n\t\"entityType\":\"ALL\",\n\t\n\t\"customerInfo\":{\n\t\t\"customerFirstName\":\"{pFirstName}\",\n\t\t\"customerLastName\":\"{pLastName}\",\n\t\t\"customerEmail\":\"{pEmail}\",\n\t\t}\n\t}\n\n}", 
		LAST);

	lr_end_transaction("TCP_25_BOPIS_REST_Call_Order_History", LR_AUTO);

	return 0;
}