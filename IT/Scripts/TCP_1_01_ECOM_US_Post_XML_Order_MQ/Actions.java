/*
 * Example WebSphere MQ LoadRunner script (written in Java)
 * 
 * Script Description: 
 *     This script puts a message on a queue, then gets a response message from 
 *     another queue.
 *
 * You will probably need to add the following jar files to your classpath
 *   - com.ibm.mq.jar
 *   - connector.jar
 *   - com.ibm.mq.jmqi.jar
 *   - com.ibm.mq.headers.jar
 *   - com.ibm.mq.commonservices.jar
 */
 
import lrapi.lr;
import com.ibm.mq.*;
import java.io.*;
 
public class Actions
{

   // Variables used by more than one method
    String queueMgrName = "PERF.TCP.QMGR";
    
    String putQueueName = "TCP.TO.MIF.CUSTOMER.ORDER";
    
    String getQueueName = "TCP.TO.MIF.CUSTOMER.ORDER";
    
    StringBuffer idBuffer = null;
    FileWriter idFile = null;
	BufferedWriter idWriter = null;
 
    MQQueueManager queueMgr = null;
    MQQueue getQueue = null;
    MQQueue putQueue = null;
    MQPutMessageOptions pmo = new MQPutMessageOptions();
    MQGetMessageOptions gmo = new MQGetMessageOptions();
    MQMessage requestMsg = new MQMessage();
    MQMessage responseMsg = new MQMessage();
    String msgBody = null;
 
    public int init() throws Throwable {
        // Open a connection to the queue manager and the put/get queues
        try {
            // As values set in the MQEnvironment class take effect when the 
            // MQQueueManager constructor is called, you must set the values 
            // in the MQEnvironment class before you construct an MQQueueManager object.
            MQEnvironment.hostname="10.18.1.21";
            
            MQEnvironment.port=1414;
            
            MQEnvironment.channel = "TCP.SVRCONN.CHANNEL";
            
            queueMgr = new MQQueueManager(queueMgrName);
            
            idFile = new FileWriter("C:\\PostXML_US.txt");
			idWriter = new BufferedWriter(idFile);
			idBuffer = new StringBuffer();
 
            // Access the put/get queues. Note the open options used.
            putQueue = queueMgr.accessQueue(putQueueName, MQC.MQOO_BIND_NOT_FIXED | MQC.MQOO_OUTPUT);
            getQueue= queueMgr.accessQueue(getQueueName, MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_OUTPUT);
            
        } catch(Exception e) {
            lr.error_message("Error connecting to queue manager or accessing queues.");
            lr.exit(lr.EXIT_VUSER, lr.FAIL);
        }
        //lr_output_message("%s",getQueue);
 
        return 0;
    }//end of init
 
    public int action() throws Throwable {
    	
        // This is an XML message that will be put on the queue. Could do some fancy 
        // things with XML classes here if necessary.
        // The message string can contain {parameters} if lr.eval_string() is used.
       MQMessage textMessage = new MQMessage(); // creating a object textMessage
	     textMessage.format = "MQSTR";
	     MQPutMessageOptions pmo = new MQPutMessageOptions();
	     StringBuffer strBuf = new StringBuffer(); // StringBuffer is also string but varying
	     strBuf.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><tXML xsi:noNamespaceSchemaLocation=\"dom-order.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><Header><Source>WCS</Source><Action_Type>Update</Action_Type><Reference_ID>"+lr.eval_string("{pOrderSet}")+""+lr.eval_string("{pOrderID}")+"</Reference_ID><Message_Type>Customer Order</Message_Type><Company_ID>1</Company_ID></Header><Message><Order><OrderNumber>"+lr.eval_string("{pOrderSet}")+""+lr.eval_string("{pOrderID}")+"</OrderNumber><OrderCaptureDate>"+lr.eval_string("{pOrderDate}")+" EDT</OrderCaptureDate><ExternalOrderNumber>"+lr.eval_string("{pOrderSet}")+""+lr.eval_string("{pOrderID}")+"</ExternalOrderNumber><OrderType>USECOM</OrderType><OrderCurrency>USD</OrderCurrency><EnteredBy>WCS</EnteredBy><EntryType>Web</EntryType><Confirmed>true</Confirmed><Canceled>false</Canceled><OnHold>false</OnHold><PaymentStatus>AUTHORIZED</PaymentStatus><CustomerInfo><CustomerId>64003958</CustomerId></CustomerInfo><PaymentDetails><PaymentDetail><ExternalPaymentDetailId>"+lr.eval_string("{pExtPaymentID}")+"</ExternalPaymentDetailId><PaymentMethod>Credit Card</PaymentMethod><CardType>Visa</CardType><AccountNumber>0858054349865539</AccountNumber><AccountDisplayNumber>************5539</AccountDisplayNumber><NameAsOnCard>Joan Lovejoy</NameAsOnCard><CardExpiryMonth>03</CardExpiryMonth><CardExpiryYear>2018</CardExpiryYear><ReqAuthorizationAmount>"+lr.eval_string("{pAuthorization_Amount}")+"</ReqAuthorizationAmount><CurrencyCode>USD</CurrencyCode><ChargeSequence>1</ChargeSequence><Encrypted>true</Encrypted><ReferenceFields><ReferenceField1>I3</ReferenceField1></ReferenceFields><BillToDetail><BillToFirstName>Joan</BillToFirstName><BillToLastName>Lovejoy</BillToLastName><BillToAddressLine1>16 Leach Hill Rd</BillToAddressLine1><BillToAddressLine2/><BillToCity>Pine City</BillToCity><BillToState>NY</BillToState><BillToPostalCode>14871</BillToPostalCode><BillToCountry>US</BillToCountry><BillToPhone>6077343794</BillToPhone><BillToEmail>JYUSAVAGE@YAHOO.COM</BillToEmail></BillToDetail><PaymentTransactionDetails><PaymentTransactionDetail><ExternalPaymentTransactionId>"+lr.eval_string("{pExtPaymentID}")+"</ExternalPaymentTransactionId><TransactionType>Authorization</TransactionType><RequestedAmount>"+lr.eval_string("{pAuthorization_Amount}")+"</RequestedAmount><RequestId>"+lr.eval_string("{pRequestID}")+"</RequestId><FollowOnToken/><RequestedDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</RequestedDTTM><ProcessedAmount>"+lr.eval_string("{pAuthorization_Amount}")+"</ProcessedAmount><TransactionDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TransactionDTTM><TransactionExpiryDate>"+lr.eval_string("{pExpiryDate}")+" EDT</TransactionExpiryDate><TransactionDecision>Success</TransactionDecision><TransactionDecisionDescription>Success</TransactionDecisionDescription></PaymentTransactionDetail></PaymentTransactionDetails></PaymentDetail></PaymentDetails><ChargeDetails><ChargeDetail><ExtChargeDetailId>1</ExtChargeDetailId><ChargeCategory>Shipping</ChargeCategory><ChargeAmount>5.00</ChargeAmount><OriginalChargeAmount>5.00</OriginalChargeAmount><IsOverridden>TRUE</IsOverridden><OverrideReason><ReasonCode>OT</ReasonCode><Comments>SHIPPING_DISCOUNT_$</Comments></OverrideReason></ChargeDetail></ChargeDetails><ReferenceFields><ReferenceField1>English</ReferenceField1><ReferenceField2/><ReferenceField4>M80180250314</ReferenceField4><ReferenceField5/><ReferenceField7>99.197.24.98</ReferenceField7><ReferenceNumber1>0</ReferenceNumber1><ReferenceNumber2>0</ReferenceNumber2><ReferenceNumber3/><ReferenceNumber4>325</ReferenceNumber4></ReferenceFields><OrderLines><OrderLine><LineNumber>01</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>"+lr.eval_string("{pPrice}")+"</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>UGNR</ShipVia><ShippingAddress><ShipToFirstName>Joan</ShipToFirstName><ShipToLastName>Lovejoy</ShipToLastName><ShipToAddressLine1>16 Leach Hill Rd</ShipToAddressLine1><ShipToAddressLine2/><ShipToCity>Pine City</ShipToCity><ShipToState>NY</ShipToState><ShipToPostalCode>14871</ShipToPostalCode><ShipToCounty>CHEMUNG</ShipToCounty><ShipToCountry>US</ShipToCountry><ShipToPhone>6077343794</ShipToPhone><ShipToEmail>JYUSAVAGE@YAHOO.COM</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>01</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>US</TaxLocationName><TaxName>State</TaxName><TaxAmount>1.20</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test Notes</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00440001306488</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>24.95</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>02</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>"+lr.eval_string("{pPrice}")+"</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>UGNR</ShipVia><ShippingAddress><ShipToFirstName>Joan</ShipToFirstName><ShipToLastName>Lovejoy</ShipToLastName><ShipToAddressLine1>16 Leach Hill Rd</ShipToAddressLine1><ShipToAddressLine2/><ShipToCity>Pine City</ShipToCity><ShipToState>NY</ShipToState><ShipToPostalCode>14871</ShipToPostalCode><ShipToCounty>CHEMUNG</ShipToCounty><ShipToCountry>US</ShipToCountry><ShipToPhone>6077343794</ShipToPhone><ShipToEmail>JYUSAVAGE@YAHOO.COM</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>02</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>US</TaxLocationName><TaxName>State</TaxName><TaxAmount>1.20</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test Notes</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00440001306488</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>24.95</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>03</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>"+lr.eval_string("{pPrice}")+"</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>UGNR</ShipVia><ShippingAddress><ShipToFirstName>Joan</ShipToFirstName><ShipToLastName>Lovejoy</ShipToLastName><ShipToAddressLine1>16 Leach Hill Rd</ShipToAddressLine1><ShipToAddressLine2/><ShipToCity>Pine City</ShipToCity><ShipToState>NY</ShipToState><ShipToPostalCode>14871</ShipToPostalCode><ShipToCounty>CHEMUNG</ShipToCounty><ShipToCountry>US</ShipToCountry><ShipToPhone>6077343794</ShipToPhone><ShipToEmail>JYUSAVAGE@YAHOO.COM</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>03</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>US</TaxLocationName><TaxName>State</TaxName><TaxAmount>1.20</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test Notes</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00440001306488</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>24.95</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>04</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>"+lr.eval_string("{pPrice}")+"</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>UGNR</ShipVia><ShippingAddress><ShipToFirstName>Joan</ShipToFirstName><ShipToLastName>Lovejoy</ShipToLastName><ShipToAddressLine1>16 Leach Hill Rd</ShipToAddressLine1><ShipToAddressLine2/><ShipToCity>Pine City</ShipToCity><ShipToState>NY</ShipToState><ShipToPostalCode>14871</ShipToPostalCode><ShipToCounty>CHEMUNG</ShipToCounty><ShipToCountry>US</ShipToCountry><ShipToPhone>6077343794</ShipToPhone><ShipToEmail>JYUSAVAGE@YAHOO.COM</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>04</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>US</TaxLocationName><TaxName>State</TaxName><TaxAmount>1.20</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test Notes</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00440001306488</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>24.95</ReferenceNumber2></LineReferenceFields></OrderLine>");
	     strBuf.append("<OrderLine><LineNumber>05</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>"+lr.eval_string("{pPrice}")+"</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>UGNR</ShipVia><ShippingAddress><ShipToFirstName>Joan</ShipToFirstName><ShipToLastName>Lovejoy</ShipToLastName><ShipToAddressLine1>16 Leach Hill Rd</ShipToAddressLine1><ShipToAddressLine2/><ShipToCity>Pine City</ShipToCity><ShipToState>NY</ShipToState><ShipToPostalCode>14871</ShipToPostalCode><ShipToCounty>CHEMUNG</ShipToCounty><ShipToCountry>US</ShipToCountry><ShipToPhone>6077343794</ShipToPhone><ShipToEmail>JYUSAVAGE@YAHOO.COM</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>05</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>US</TaxLocationName><TaxName>State</TaxName><TaxAmount>1.20</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test Notes</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00440001306488</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>24.95</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>06</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>"+lr.eval_string("{pPrice}")+"</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>UGNR</ShipVia><ShippingAddress><ShipToFirstName>Joan</ShipToFirstName><ShipToLastName>Lovejoy</ShipToLastName><ShipToAddressLine1>16 Leach Hill Rd</ShipToAddressLine1><ShipToAddressLine2/><ShipToCity>Pine City</ShipToCity><ShipToState>NY</ShipToState><ShipToPostalCode>14871</ShipToPostalCode><ShipToCounty>CHEMUNG</ShipToCounty><ShipToCountry>US</ShipToCountry><ShipToPhone>6077343794</ShipToPhone><ShipToEmail>JYUSAVAGE@YAHOO.COM</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>06</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>US</TaxLocationName><TaxName>State</TaxName><TaxAmount>1.20</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test Notes</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00440001306488</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>24.95</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>07</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>"+lr.eval_string("{pPrice}")+"</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>UGNR</ShipVia><ShippingAddress><ShipToFirstName>Joan</ShipToFirstName><ShipToLastName>Lovejoy</ShipToLastName><ShipToAddressLine1>16 Leach Hill Rd</ShipToAddressLine1><ShipToAddressLine2/><ShipToCity>Pine City</ShipToCity><ShipToState>NY</ShipToState><ShipToPostalCode>14871</ShipToPostalCode><ShipToCounty>CHEMUNG</ShipToCounty><ShipToCountry>US</ShipToCountry><ShipToPhone>6077343794</ShipToPhone><ShipToEmail>JYUSAVAGE@YAHOO.COM</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>07</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>US</TaxLocationName><TaxName>State</TaxName><TaxAmount>1.20</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test Notes</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00440001306488</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>24.95</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>08</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>"+lr.eval_string("{pPrice}")+"</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>UGNR</ShipVia><ShippingAddress><ShipToFirstName>Joan</ShipToFirstName><ShipToLastName>Lovejoy</ShipToLastName><ShipToAddressLine1>16 Leach Hill Rd</ShipToAddressLine1><ShipToAddressLine2/><ShipToCity>Pine City</ShipToCity><ShipToState>NY</ShipToState><ShipToPostalCode>14871</ShipToPostalCode><ShipToCounty>CHEMUNG</ShipToCounty><ShipToCountry>US</ShipToCountry><ShipToPhone>6077343794</ShipToPhone><ShipToEmail>JYUSAVAGE@YAHOO.COM</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>08</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>US</TaxLocationName><TaxName>State</TaxName><TaxAmount>1.20</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test Notes</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00440001306488</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>24.95</ReferenceNumber2></LineReferenceFields></OrderLine>");
	     strBuf.append("<OrderLine><LineNumber>09</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>"+lr.eval_string("{pPrice}")+"</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>UGNR</ShipVia><ShippingAddress><ShipToFirstName>Joan</ShipToFirstName><ShipToLastName>Lovejoy</ShipToLastName><ShipToAddressLine1>16 Leach Hill Rd</ShipToAddressLine1><ShipToAddressLine2/><ShipToCity>Pine City</ShipToCity><ShipToState>NY</ShipToState><ShipToPostalCode>14871</ShipToPostalCode><ShipToCounty>CHEMUNG</ShipToCounty><ShipToCountry>US</ShipToCountry><ShipToPhone>6077343794</ShipToPhone><ShipToEmail>JYUSAVAGE@YAHOO.COM</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>09</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>US</TaxLocationName><TaxName>State</TaxName><TaxAmount>1.20</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test Notes</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00440001306488</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>24.95</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>10</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>"+lr.eval_string("{pPrice}")+"</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>UGNR</ShipVia><ShippingAddress><ShipToFirstName>Joan</ShipToFirstName><ShipToLastName>Lovejoy</ShipToLastName><ShipToAddressLine1>16 Leach Hill Rd</ShipToAddressLine1><ShipToAddressLine2/><ShipToCity>Pine City</ShipToCity><ShipToState>NY</ShipToState><ShipToPostalCode>14871</ShipToPostalCode><ShipToCounty>CHEMUNG</ShipToCounty><ShipToCountry>US</ShipToCountry><ShipToPhone>6077343794</ShipToPhone><ShipToEmail>JYUSAVAGE@YAHOO.COM</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>10</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>US</TaxLocationName><TaxName>State</TaxName><TaxAmount>1.20</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test Notes</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00440001306488</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>24.95</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>11</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>"+lr.eval_string("{pPrice}")+"</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>UGNR</ShipVia><ShippingAddress><ShipToFirstName>Joan</ShipToFirstName><ShipToLastName>Lovejoy</ShipToLastName><ShipToAddressLine1>16 Leach Hill Rd</ShipToAddressLine1><ShipToAddressLine2/><ShipToCity>Pine City</ShipToCity><ShipToState>NY</ShipToState><ShipToPostalCode>14871</ShipToPostalCode><ShipToCounty>CHEMUNG</ShipToCounty><ShipToCountry>US</ShipToCountry><ShipToPhone>6077343794</ShipToPhone><ShipToEmail>JYUSAVAGE@YAHOO.COM</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>11</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>US</TaxLocationName><TaxName>State</TaxName><TaxAmount>1.20</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test Notes</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00440001306488</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>24.95</ReferenceNumber2></LineReferenceFields></OrderLine><OrderLine><LineNumber>12</LineNumber><ItemID>"+lr.eval_string("{pItemID}")+"</ItemID><PriceInfo><Price>"+lr.eval_string("{pPrice}")+"</Price></PriceInfo><Quantity><OrderedQty>"+lr.eval_string("{pQty}")+"</OrderedQty><OrderedQtyUOM>eaches</OrderedQtyUOM></Quantity><ShippingInfo><ShipVia>UGNR</ShipVia><ShippingAddress><ShipToFirstName>Joan</ShipToFirstName><ShipToLastName>Lovejoy</ShipToLastName><ShipToAddressLine1>16 Leach Hill Rd</ShipToAddressLine1><ShipToAddressLine2/><ShipToCity>Pine City</ShipToCity><ShipToState>NY</ShipToState><ShipToPostalCode>14871</ShipToPostalCode><ShipToCounty>CHEMUNG</ShipToCounty><ShipToCountry>US</ShipToCountry><ShipToPhone>6077343794</ShipToPhone><ShipToEmail>JYUSAVAGE@YAHOO.COM</ShipToEmail></ShippingAddress></ShippingInfo><DiscountDetails/><TaxDetails><TaxDetail><ExtTaxDetailId>12</ExtTaxDetailId><TaxCategory>30</TaxCategory><TaxLocationName>US</TaxLocationName><TaxName>State</TaxName><TaxAmount>1.20</TaxAmount><TaxDTTM>"+lr.eval_string("{pOrderDate}")+" EDT</TaxDTTM></TaxDetail></TaxDetails><Notes><Note><NoteType>WCSItemAttributes</NoteType><NoteText>Test Notes</NoteText></Note></Notes><LineReferenceFields><ReferenceField1>00440001306488</ReferenceField1><ReferenceNumber1>24.95</ReferenceNumber1><ReferenceNumber2>24.95</ReferenceNumber2></LineReferenceFields></OrderLine></OrderLines></Order></Message></tXML>");
	    	   
	     // strBuf.append("<OrderNumber>"+lr.eval_string(""+lr.eval_string("{pOrderID}")+"")+"</OrderNumber>");
	     
        System.out.println(strBuf);
        String data = lr.eval_string("{pOrderID}");
        String data1= lr_eval_string("{pOrderSet}");
		String data3=lr.eval_string("{pSet}");
		System.out.println(data1+data);
		idWriter.write(data1+data);
		idWriter.write(",");
		idWriter.write(data3);
		idWriter.write(",");
//         lr.output_message("%s"
         //textMessage.writeString(strBuf.toString());
//        msgBody=strBuf;
        // Clear the message objects on each iteration.
        requestMsg.clearMessage();
        responseMsg.clearMessage();
 
        // Create a message object and put it on the request queue
       lr.start_transaction("TCP_8_ECOM_Post_XML_Order_MQ_01_Post");
        
        try {
            pmo.options = MQC.MQPMO_NEW_MSG_ID; // The queue manager replaces the contents of the MsgId field in MQMD with a new message identifier.
            requestMsg.replyToQueueName = getQueueName; // the response should be put on this queue
            requestMsg.report=MQC.MQRO_PASS_MSG_ID; //If a report or reply is generated as a result of this message, the MsgId of this message is copied to the MsgId of the report or reply message.
            requestMsg.format = MQC.MQFMT_STRING; // Set message format. The application message data can be either an SBCS string (single-byte character set), or a DBCS string (double-byte character set). 
            requestMsg.messageType=MQC.MQMT_REQUEST; // The message is one that requires a reply.
            requestMsg.writeString(strBuf.toString()); // message payload msgBody
           // putQueue.put(requestMsg, pmo);
            
            String data2 = lr.eval_string("{pTimeStamp}");
            idWriter.write(data2);
            idWriter.write("\n");
           } 
        
        catch(Exception e) 
        {
        	lr.error_message("Error sending message.");
        	lr.exit(lr.EXIT_VUSER, lr.FAIL);
        }
        
        lr.end_transaction("TCP_8_ECOM_Post_XML_Order_MQ_01_Post", lr.AUTO); 
        
        lr.think_time(1);
        
 
        return 0;
    
    }//end of action
    
    public int end() throws Throwable 
    {
        // 	Close all the connections
        try 
        {
            putQueue.close();
            getQueue.close();
            queueMgr.close();
            idWriter.close();
        } 
        
        catch(Exception e)
              {
            lr.error_message("Exception in closing the connections");
            lr.exit(lr.EXIT_VUSER, lr.FAIL);
              }
 
        return 0;
    }//end of end

}