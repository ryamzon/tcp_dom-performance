Action()
{

	web_url("ping.jsp_2", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466847871165", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t5.inf", 
		"Mode=HTML",
		LAST);

	web_url("ping.jsp_3", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466847886417", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t6.inf", 
		"Mode=HTML", 
		LAST);

	web_url("ping.jsp_4", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466847901784", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t7.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=../manh/olm/resources/icons/trigger/search_trigger.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=../manh/olm/ds/model/customertransaction/OrderSearchRequest.js?_dc=1466847915089", "Referer={pURL}/manh/index.html", ENDITEM, 
		LAST);

	/* Goto Return Tile, enter order number and click on search */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_03_Goto_Return_Tile_Enter_Order_and_click_on_search"));

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"customerOrderSearchCriteria\">"
						"<pair name=\"entityType\" type=\"str\">All</pair>"
						"<pair name=\"orderNumber\" type=\"str\">{pOrder_No}</pair>"
						"<pair name=\"parentOrderNumber\" type=\"str\"></pair>"
						"<pair name=\"createdFromDate\" type=\"str\"></pair>"
						"<pair name=\"createdToDate\" type=\"str\"></pair>"
						"<pair name=\"noOfRecordsPerPage\" type=\"num\">10</pair>"
						"<pair name=\"customerInfo\" type=\"str\"></pair>"
						"<pair name=\"customerBasicInfo\" type=\"str\"></pair>"
						"<object name=\"sortingCriterion\">"
							"<pair name=\"sortField\" type=\"str\">createdDTTM</pair>"
							"<pair name=\"sortDirection\" type=\"str\">DESC</pair>"
						"</object>"
						"<pair name=\"currentPageNumber\" type=\"num\">0</pair>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY", 
		LAST);
	
	
	/*Extract externalCustomerId value from response: "externalCustomerId":208222632, */
	
				web_reg_save_param("c_ExternalCustomerId",
	                   "LB=\"externalCustomerId\":",
	                   "RB=,",
	                   "ORD=1",
	                   "Search=ALL",LAST); 

	
	/* Extract customerEmail value from response: "customerEmail":"TRESSA.JOHNSON@YMAIL.COM", */
	
				web_reg_save_param("c_CustomerEmail",
	                   "LB=\"customerEmail\":\"",
	                   "RB=\"",
	                   "ORD=1",
	                   "Search=ALL",LAST);
	
	/* Extract customerFirstName value from response: "customerFirstName":"TRESSA", */
	
				web_reg_save_param("c_CustomerFirstName",
	                   "LB=\"customerFirstName\":\"",
	                   "RB=\",",
	                   "ORD=1",
	                   "Search=ALL",LAST);
	
	/* Extract customerLastName value from response: "customerLastName":"JOHNSON"," */
	
				web_reg_save_param("c_CustomerLastName",
	                   "LB=\"customerLastName\":\"",
	                   "RB=\",",
	                   "ORD=1",
	                   "Search=ALL",LAST);
	
		/* Extract customerPhone value from response: "customerPhone":5135196903, */
	
		web_reg_save_param("c_CustomerPhone",
	                   "LB=\"customerPhone\":\"",
	                   "RB=\",",
	                   "ORD=1",
	                   "Search=ALL",LAST); 
		
		/* "orderCapturedDate":"6\/25\/16 05:47 EDT", */
	
		/*	web_reg_save_param("c_OrderCapturedDate",
	                   "LB=\"orderCreatedDate\":\"",
	                   "RB=\",",
	                   "ORD=1",
	                   "Search=ALL",LAST); */			
			
			
		/* Extract authorizationAmount value from response: "authorizationAmount":34.95, */
	
		web_reg_save_param("c_Amount",
	                   "LB=\"grandTotal\":",
	                   "RB=,",
	                   "ORD=1",
	                   "Search=ALL",LAST);
				
			
		/* Extract orderStatus value from response: "orderStatus":"Pending","isExchange" */
	
		web_reg_save_param("c_OrderStatus",
                   "LB=\"orderStatus\":\"",
                   "RB=\",",
                   "ORD=1",
                   "Search=ALL",LAST); 
	
	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");
		
	web_custom_request("customerOrderAndTransactionList", 
		"URL={pURL}/services/olm/customerorder/customerOrderAndTransactionList?_dc=1466847915521&page=1&start=0&limit=50&sort=%5B%7B%22property%22%3A%22orderCreatedDate%22%2C%22direction%22%3A%22DESC%22%7D%5D", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t8.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY}", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_03_Goto_Return_Tile_Enter_Order_and_click_on_search"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_5", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466847919480", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t9.inf", 
		"Mode=HTML", 
		LAST);

	/* Click on searched order */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_04_Open_Searched_Order"));

	web_url("ping.jsp_6", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466847934753", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t10.inf", 
		"Mode=HTML", 
	/*	EXTRARES, 
		"Url=../manh/olm/rlm/screen/ReturnScreen.js?_dc=1466847935664", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnDetailsScreen.js?_dc=1466847936034", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/components/ScreenHeader.js?_dc=1466847936353", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/platform/util/plugins/Ellipsis.js?_dc=1466847936658", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/controller/ReturnController.js?_dc=1466847936970", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/model/returns/COItemListModel.js?_dc=1466847938928", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/model/returns/ReturnOrderLineModel.js?_dc=1466847939325", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/model/customerorder/AddressModel.js?_dc=1466847939633", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/chargedetail/ChargeDetailModel.js?_dc=1466847939929", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/ChargeDetailsModel.js?_dc=1466847940234", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/ChargeGroupDetailsModel.js?_dc=1466847940543", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/AvailableAddressesModel.js?_dc=1466847940847", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/view/QuickSearchPane.js?_dc=1466847941157", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnHeaderDetails.js?_dc=1466847941476", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/COItemList.js?_dc=1466847942025", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnItemList.js?_dc=1466847942294", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/detail/ViewReturnDetails.js?_dc=1466847942602", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnDetails.js?_dc=1466847944016", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/COItems.js?_dc=1466847944639", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/detail/ReturnActions.js?_dc=1466847945255", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnItem.js?_dc=1466847945569", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnItemReadOnly.js?_dc=1466847945883", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnActionOverridePopUp.js?_dc=1466847946184", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/components/Popup.js?_dc=1466847946488", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/utils/plugins/CloseOnEscape.js?_dc=1466847951702", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnTotalEstimation.js?_dc=1466847952679", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/detail/ReturnDetailInfo.js?_dc=1466847953750", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnItemsFrom.js?_dc=1466847954987", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/shipfromaddress/ViewShipFromAddress.js?_dc=1466847955454", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/shipfromaddress/ViewReturnsShippingMethod.js?_dc=1466847955716", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/EditShipFromAddress.js?_dc=1466847956002", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/NewShipFromAddress.js?_dc=1466847956345", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnCenter.js?_dc=1466847956629", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnEstimatedCharge.js?_dc=1466847956933", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnEstimatedRefund.js?_dc=1466847957344", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnItemsReshipTo.js?_dc=1466847957851", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/EditReturnItemDetail.js?_dc=1466847958482", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/BillingAddress.js?_dc=1466847959206", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ConfirmDeletePopUp.js?_dc=1466847959490", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnWaiveFeePopUp.js?_dc=1466847959803", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnChargeDetails.js?_dc=1466847960527", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnsAvailablePaymentOptions.js?_dc=1466847960841", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/view/payment/AvailablePaymentOptions.js?_dc=1466847961141", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnsShippingMethod.js?_dc=1466847961568", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/exchange/ExchangeLineItemsGroupHeader.js?_dc=1466847962654", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/view/orderline/OrderLineGroupHeader.js?_dc=1466847962963", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ConfirmPopUp.js?_dc=1466847963289", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/exchange/ExchangeOrderPaymentSummary.js?_dc=1466847963581", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/exchange/ExchangeItemsShipToAddressFooter.js?_dc=1466847964905", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/exchange/ShippingTab.js?_dc=1466847965218", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/view/shipping/ShippingTab.js?_dc=1466847965471", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnItemListReview.js?_dc=1466847966450", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ReturnsUsedPaymentOptions.js?_dc=1466847967069", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/view/payment/UsedPaymentOptions.js?_dc=1466847967372", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/exchange/ExchangeOrderPaymentFooter.js?_dc=1466847968708", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/shipfromaddress/AvailableAddressesMenu.js?_dc=1466847969009", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/exchange/ExchangeOrderShippingInfo.js?_dc=1466847969326", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/ViewReshipToAddress.js?_dc=1466847969625", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/view/returns/AddAddress.js?_dc=1466847970236", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/screen/ReturnStatusScreenLabels.js?_dc=1466847970757", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/store/returns/COItemListStore.js?_dc=1466847971098", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/store/returns/ReturnOrderStore.js?_dc=1466847971360", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/store/CustomerInfoStore.js?_dc=1466847971672", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/store/customerorder/CardTypeStore.js?_dc=1466847971982", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/CardTypeModel.js?_dc=1466847972285", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/store/customerorder/AccountTypeStore.js?_dc=1466847972544", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/AccountTypeModel.js?_dc=1466847972799", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/store/customerorder/CustomerTypeStore.js?_dc=1466847973109", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/customerorder/CustomerTypeModel.js?_dc=1466847973410", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/store/customerorder/OrderStore.js?_dc=1466847973722", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/store/returns/ReturnShippingMethodsStore.js?_dc=1466847974025", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/ds/model/basedata/ShipViaModel.js?_dc=1466847974330", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/store/ItemAvailabilityStore.js?_dc=1466847974640", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/common/model/storelocator/ItemAvailabilityModel.js?_dc=1466847974945", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/store/returns/GetReturnOrderStore.js?_dc=1466847975253", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/rlm/utils/tools/Constants.js?_dc=1466847975722", "Referer={pURL}/manh/index.html", ENDITEM, */
		
		LAST);
	
	web_custom_request("windows", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14271272&regionId=-1&businessUnitId=-1", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t11.inf", 
		"Mode=HTML", 
		"EncType=", 
	/*	EXTRARES, 
		"Url=../UIConfigService/uiconfig?_dc=1466847976219&screenId=280028&windowId=screen-14271272", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/controller/CommonController.js?_dc=1466847978019", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/controller/CustomerOrderController.js?_dc=1466847978020", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/CountryListStore.js?_dc=1466847978020", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/StateListStore.js?_dc=1466847978021", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/CompanyParameterListStore.js?_dc=1466847978021", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/CountryListModel.js?_dc=1466847978962", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/OrderTotalModel.js?_dc=1466847978977", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/PaymentDetailModel.js?_dc=1466847978977", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/TotalChargeModel.js?_dc=1466847978977", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/PaymentTransactionModel.js?_dc=1466847978978", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/RefFieldModel.js?_dc=1466847978979", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/NoteModel.js?_dc=1466847978978", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/OrderLineModel.js?_dc=1466847978979", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/OrderLineTotalsModel.js?_dc=1466847978979", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/ShippingInfoModel.js?_dc=1466847978979", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/PriceDetailsModel.js?_dc=1466847978980", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/AllocationDetailsModel.js?_dc=1466847978980", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/DiscountDetailsListModel.js?_dc=1466847978980", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/AllocationInfoModel.js?_dc=1466847978980", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/TotalDiscountDetailsModel.js?_dc=1466847978980", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/DiscountDetailModel.js?_dc=1466847978980", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/OrderLineQuantityDetailModel.js?_dc=1466847978981", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/ReasonCodeModel.js?_dc=1466847978981", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/CompanyParameterModel.js?_dc=1466847978981", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/AVSResponseModel.js?_dc=1466847978981", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/CustomerFullDetailsModel.js?_dc=1466847978981", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/orderline/DiscountModel.js?_dc=1466847978982", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/TriggerWorkflowRequest.js?_dc=1466847978982", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/storelocator/ItemFacilityModel.js?_dc=1466847978982", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/storelocator/FacilityInfoModel.js?_dc=1466847978983", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/storelocator/StoreDetailsModel.js?_dc=1466847978983", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/storelocator/StoreLocatorModel.js?_dc=1466847978982", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/ContextInfoModel.js?_dc=1466847978982", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/storelocator/StoreLocatorRequest.js?_dc=1466847978983", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/item/DeliveryOptionsModel.js?_dc=1466847978983", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/storelocator/FacilityAddressModel.js?_dc=1466847978983", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/NoteTypeModel.js?_dc=1466847978984", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/model/MessageModel.js?_dc=1466847978984", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/model/SysCodeModel.js?_dc=1466847978984", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/customerorder/shipping/LineItemShippingInfoModel.js?_dc=1466847978984", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/RelatedEntityModel.js?_dc=1466847978984", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/item/promotion/PromoProdAssocDetailsModel.js?_dc=1466847978985", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/item/promotion/PromotionDetailsModel.js?_dc=1466847978985", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/item/promotion/OfferedProductsModel.js?_dc=1466847978985", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/CustomFieldModel.js?_dc=1466847978985", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/model/RefTextFieldsModel.js?_dc=1466847978985", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/SearchField.js?_dc=1466847978986", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/EditableContainer.js?_dc=1466847978986", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/HyperLink.js?_dc=1466847978986", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/EmbeddedLink.js?_dc=1466847978986", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/MSBingMap.js?_dc=1466847978987", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/ConfirmationPopup.js?_dc=1466847978987", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/item/promotion/PromotionModel.js?_dc=1466847978984", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/MultiSelectContainer.js?_dc=1466847978987", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/PaymentException.js?_dc=1466847978988", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/component/BaseContainer.js?_dc=1466847978988", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/AmountField.js?_dc=1466847978988", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/component/BaseToolBar.js?_dc=1466847978989", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/component/BaseMenu.js?_dc=1466847978989", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/component/BaseGrid.js?_dc=1466847978989", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/ErrorMessagePopup.js?_dc=1466847978987", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/collapsibles/CollapsibleContainer.js?_dc=1466847978989", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/components/collapsibles/EditableCollapsibleContainer.js?_dc=1466847978990", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/utils/tools/OrderFormatter.js?_dc=1466847978990", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/AvailableCreditCard.js?_dc=1466847978990", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/AvailableECheck.js?_dc=1466847978990", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/AvailableGiftCard.js?_dc=1466847978991", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/ConfirmTab.js?_dc=1466847978991", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/PaymentTab.js?_dc=1466847978991", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/UsedCreditCard.js?_dc=1466847978992", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/UsedGiftCard.js?_dc=1466847978992", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/NewAddress.js?_dc=1466847978992", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/AddAddress.js?_dc=1466847978993", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/AddCreditCard.js?_dc=1466847978993", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/UsedECheck.js?_dc=1466847978992", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/AddECheck.js?_dc=1466847978993", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/AddGiftCard.js?_dc=1466847978993", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/PaymentTitle.js?_dc=1466847978994", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/EditCreditCard.js?_dc=1466847978994", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/EditECheck.js?_dc=1466847978994", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/PaymentFooter.js?_dc=1466847978995", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/PaymentSummary.js?_dc=1466847978995", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/mixin/PaymentAuthorization.js?_dc=1466847978995", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/ShippingFooter.js?_dc=1466847978995", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/Address.js?_dc=1466847978995", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/UsedAddresses.js?_dc=1466847978996", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/AvailableAddresses.js?_dc=1466847978996", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/AvailableShippingAddress.js?_dc=1466847978996", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/AvailableStoreAddress.js?_dc=1466847978996", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/EditShippingAddress.js?_dc=1466847978996", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/DeliveryOptions.js?_dc=1466847978997", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/UsedShippingAddress.js?_dc=1466847978997", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/ShippingMethod.js?_dc=1466847978997", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/LineItemShippingMethods.js?_dc=1466847978997", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/LineItemShippingMethod.js?_dc=1466847978998", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/AddShippingAddress.js?_dc=1466847978997", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/UsedStoreAddress.js?_dc=1466847978998", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/StoreLocator.js?_dc=1466847978998", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/ItemFacility.js?_dc=1466847978998", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/StoreLocatorItemInfo.js?_dc=1466847978998", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/shipping/AddressSummary.js?_dc=1466847978996", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/notes/Note.js?_dc=1466847978999", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/notes/Notes.js?_dc=1466847978999", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/notes/NoteList.js?_dc=1466847978999", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/avs/AVSRecommendation.js?_dc=1466847978999", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/avs/AVSSuccess.js?_dc=1466847978999", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/CustomerFullDetails.js?_dc=1466847979000", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/customer/AddCustomer.js?_dc=1466847979000", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/search/RelatedEntities.js?_dc=1466847979000", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/search/RelatedEntityFlyout.js?_dc=1466847979000", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/popup/avs/AVSFailure.js?_dc=1466847978999", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/util/CustomerOrderRequestManager.js?_dc=1466847979000", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/OrderUpdateStore.js?_dc=1466847979000", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/StoreLocatorStore.js?_dc=1466847979001", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/NoteTypeStore.js?_dc=1466847979001", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/CustomerDetailsStore.js?_dc=1466847979001", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/AVSStore.js?_dc=1466847979001", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/CustomerFullDetailsStore.js?_dc=1466847979001", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/TriggerWorkflowStore.js?_dc=1466847979002", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/RelatedEntityStore.js?_dc=1466847979002", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/model/StateListModel.js?_dc=1466847979254", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/store/SystemPropertyStore.js?_dc=1466847979002", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/store/SysCodeStore.js?_dc=1466847979001", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/CustomerPaymentInfoModel.js?_dc=1466847979811", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/CreditCardPaymentInfosModel.js?_dc=1466847979812", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/GiftCardPaymentInfosModel.js?_dc=1466847979815", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/CustomerPreferenceModel.js?_dc=1466847979816", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/ECheckPaymentInfosModel.js?_dc=1466847979814", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/CustomerDetailsModel.js?_dc=1466847979816", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/supplybalance/SupplyBalanceDetailsModel.js?_dc=1466847979817", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/supplybalance/SupplyBalanceResultModel.js?_dc=1466847979817", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/discounts/promotions/OrderPromotionsModel.js?_dc=1466847979817", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/discounts/promotions/ItemPromotionsModel.js?_dc=1466847979817", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/discounts/promotions/PromotionEntityModel.js?_dc=1466847979818", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/discounts/promotions/PromoDetailModel.js?_dc=1466847979818", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/shipping/ShippingMethodListModel.js?_dc=1466847979818", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/shipping/ItemShippingMethodsModel.js?_dc=1466847979819", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/shipping/ShippingMethodModel.js?_dc=1466847979820", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/override/PriceOverrideHistoryModel.js?_dc=1466847979820", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/customerorder/override/SnHOverrideHistoryModel.js?_dc=1466847979821", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/item/ItemUOMModel.js?_dc=1466847979822", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/item/ItemDetailsModel.js?_dc=1466847979822", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/item/SupportedUOMModel.js?_dc=1466847979823", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/item/DeliveryOption.js?_dc=1466847979824", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/item/ShippingMethodsModel.js?_dc=1466847979824", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/inventory/model/viewlist/AvailabilityDetailModel.js?_dc=1466847979824", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/inventory/model/viewlist/AvailabilityModel.js?_dc=1466847979825", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/item/ItemFullDetailsModel.js?_dc=1466847979822", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/inventory/model/viewlist/ViewDefinitionModel.js?_dc=1466847979826", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/CustomerOrderScreen.js?_dc=1466847979826", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/CustomerOrderDetail.js?_dc=1466847979827", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/CustomerOrderHeaderDetails.js?_dc=1466847979828", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/HoldAndCancel.js?_dc=1466847979828", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/override/ReasonCodePopUp.js?_dc=1466847979829", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/CODiscountAndCommunicationInfo.js?_dc=1466847979830", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/lineitem/COLineItemHeader.js?_dc=1466847979830", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/lineitem/COLineItem.js?_dc=1466847979831", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/lineitem/COLineItemDetails.js?_dc=1466847979832", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/model/item/ItemAdditionalDetailsModel.js?_dc=1466847979823", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/lineitem/COItemsAndShipping.js?_dc=1466847979832", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/lineitem/CustomerOrderLineHeader.js?_dc=1466847979833", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/lineitem/CustomerOrderLineDetails.js?_dc=1466847979833", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/tabs/CODetailsTabSection.js?_dc=1466847979833", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/CustomerOrderChargeDetails.js?_dc=1466847979830", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/tabs/ConfirmOrderPaymentSummary.js?_dc=1466847979834", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/UpdateRegisteredCustomerInfo.js?_dc=1466847979834", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/RegisterCustomer.js?_dc=1466847979835", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/AddressView.js?_dc=1466847979835", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/PreferredAddress.js?_dc=1466847979835", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/DiscountDetails.js?_dc=1466847979836", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/DiscountedLine.js?_dc=1466847979836", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/DiscountedOrder.js?_dc=1466847979837", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/DiscountedLineHeader.js?_dc=1466847979837", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/DiscountsChargeSummary.js?_dc=1466847979837", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/Promotions.js?_dc=1466847979837", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/AvailablePromotions.js?_dc=1466847979839", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/OrderPromotions.js?_dc=1466847979838", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/OrderAndLinePromotions.js?_dc=1466847979838", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/NoPromotion.js?_dc=1466847979838", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/LinePromotions.js?_dc=1466847979839", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/PromotedLine.js?_dc=1466847979840", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/PromotionDetail.js?_dc=1466847979840", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/AvailablePromotionLine.js?_dc=1466847979840", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/promotions/PromotedOrder.js?_dc=1466847979840", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/item/details/OfferedItemDescription.js?_dc=1466847979841", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/appeasements/OrderAndLineAppeasements.js?_dc=1466847979841", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/appeasements/OrderAppeasements.js?_dc=1466847979842", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/appeasements/Appeasements.js?_dc=1466847979841", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/appeasements/AppeasementDetails.js?_dc=1466847979842", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/appeasements/NewAppeasements.js?_dc=1466847979842", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/appeasements/AppliedAppeasements.js?_dc=1466847979842", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/appeasements/LineAppeasements.js?_dc=1466847979843", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/override/AbstractOverride.js?_dc=1466847979843", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/override/priceoverride/PriceOverride.js?_dc=1466847979843", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/override/priceoverride/ApplyPriceOverride.js?_dc=1466847979844", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/override/priceoverride/PriceOverrideHistory.js?_dc=1466847979844", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/override/snhoverride/SnHOverride.js?_dc=1466847979845", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/override/snhoverride/ApplySnHOverride.js?_dc=1466847979845", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/override/snhoverride/SnHOverrideHistory.js?_dc=1466847979845", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/coupons/Coupons.js?_dc=1466847979845", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/coupons/CouponList.js?_dc=1466847979846", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/coupons/CouponDetail.js?_dc=1466847979846", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/view/customerorder/discounts/coupons/CouponOrderLine.js?_dc=1466847979847", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/CustomerPaymentInfoStore.js?_dc=1466847979847", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/CustomerPreferenceStore.js?_dc=1466847979847", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/DeleteLinesStore.js?_dc=1466847979847", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/GiftCardStore.js?_dc=1466847979848", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/SupplyBalanceDetailsStore.js?_dc=1466847979849", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/discounts/promotions/OrderPromotionsStore.js?_dc=1466847979849", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/discounts/promotions/ItemPromotionsStore.js?_dc=1466847979850", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/ShippingMethodsStore.js?_dc=1466847979850", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/override/PriceOverrideHistoryStore.js?_dc=1466847979850", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/DeliveryOptionsStore.js?_dc=1466847979848", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/AvailableAddressesStore.js?_dc=1466847979848", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/override/SnHOverrideHistoryStore.js?_dc=1466847979850", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/discounts/promotions/ApplyLinePromotionStore.js?_dc=1466847979851", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/discounts/promotions/RemoveOrderPromotionStore.js?_dc=1466847979851", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/discounts/promotions/RemoveLinePromotionStore.js?_dc=1466847979852", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/basedata/ShipViaStore.js?_dc=1466847979852", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/item/ItemUOMStore.js?_dc=1466847979852", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/item/PromotionDetailsStore.js?_dc=1466847979852", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/discounts/promotions/ApplyBXGYPromotionStore.js?_dc=1466847979853", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/inventory/store/AvailabilityDetailStore.js?_dc=1466847979853", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/ds/store/customerorder/discounts/promotions/ApplyOrderPromotionStore.js?_dc=1466847979851", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/inventory/store/CommerceViewDefStore.js?_dc=1466847979853", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//platform/component/mixin/BasicMethods.js?_dc=1466847982815", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/view/payment/AbstractPaymentView.js?_dc=1466847983499", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm//common/utils/plugins/BaselineGrid.js?_dc=1466847988955", "Referer={pURL}/manh/index.html", ENDITEM, */
		
		LAST); 
	
	/* Extract itemId value from response: "itemId":2045962005, */
	
		web_reg_save_param("c_ItemID",
	                   "LB=\"itemId\":",
	                   "RB=,",
	                   "ORD={pItemID}",
	                   "Search=ALL",LAST);
	
	/* Extract customerOrderlineId value from response: "customerOrderlineId":2327913, */
	
		web_reg_save_param("c_CustomerOrderlineID",
	                   "LB=\"customerOrderlineId\":",
	                   "RB=,",
	                   "ORD={pItemID}",
	                   "Search=ALL",LAST);
	

	web_url("ping.jsp_7", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466847990722", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t12.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=../manh/olm/common/components/StoreLoadMask.js?_dc=1466847997130", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../services/olm/systemProperties/getKey?_dc=1466847996960&keyName=BING_MAP_KEY", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/customer/email.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/customer/customer_id.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../services/olm/returns/itemsinorder?_dc=1466847997482&jsonFileName=COItemList&customerOrderNumber={pOrder_No}&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/search/customer.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../manh/olm/resources/icons/ds/search/order.png", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../services/olm/customer/customerDetailsByOrder?_dc=1466847997561&orderNumber={pOrder_No}&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=../services/olm/location/countrylist?_dc=1466847997845&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM,  
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"companyParameterIdList\">"
						"<array name=\"parameterList\">"
							"<item type=\"str\">CURRENCY_USED</item>"
							"<item type=\"str\">EXT_CUSTOMER_ID_GENERATION</item>"
							"<item type=\"str\">USE_DELIVERY_ZONE</item>"
							"<item type=\"str\">DEFAULT_SHIP_VIA</item>"
							"<item type=\"str\">DEFAULT_ORDER_TYPE</item>"
							"<item type=\"str\">REQUIRES_VALIDATION_FOR_PAYMENT_AUTHORIZATION</item>"
							"<item type=\"str\">REQUIRES_VALIDATION_FOR_PAYMENT_SETTLEMENT</item>"
							"<item type=\"str\">CHARGE_HANDLING_STRATEGY</item>"
							"<item type=\"str\">AUTOMATIC_PROMOTION_VALUE</item>"
							"<item type=\"str\">ITEM_PRICE_EXPIRY_DATE_EXTENSION_DAYS</item>"
							"<item type=\"str\">ITEM_PRICE_SCHEDULER_INTERVAL_IN_DAYS</item>"
							"<item type=\"str\">DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE</item>"
							"<item type=\"str\">VIEW_STORE_LEVEL_PRICES</item>"
							"<item type=\"str\">NETWORK_LEVEL_VIEW</item>"
							"<item type=\"str\">FACILITY_LEVEL_VIEW</item>"
						"</array>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_1",		
		LAST);
	
	web_custom_request("companyParameterList", 
		"URL={pURL}/services/olm/basedata/companyParameter/companyParameterList?_dc=1466847997847&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t13.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_1}", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_04_Open_Searched_Order"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_8", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466848012527", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t14.inf", 
		"Mode=HTML", 
		LAST);

	/* Select OItem to be returned and click on Done */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_05_Select_Item_to_be_returned_and_click_on_Done"));

	web_url("ping.jsp_9", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466848027829", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t15.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=../manh/olm/common/model/EventModel.js?_dc=1466848028635", "Referer={pURL}/manh/index.html", ENDITEM, 
		LAST);

	web_convert_from_formatted("FormattedData=<HP_EXTENSION name=\"JsonXml\"><object><object name=\"ReturnOrder\"><pair name=\"orderNumber\" type=\"str\"></pair><pair name=\"orderId\" type=\"str\"></pair><pair name=\"orderType\" type=\"str\"></pair><pair name=\"tcCompanyId\" type=\"str\"></pair><pair name=\"customerOrderId\" type=\"str\"></pair><pair name=\"customerOrderNumber\" type=\"str\">{pOrder_No}</pair><pair name=\"orderStatus\" type=\"str\"></pair><pair name=\"orderStatusId\" type=\"str\"></pair><pair name=\"orderCapturedDate\" type=\"str\"></pair><pair name=\"orderCreatedDate\" type=\"str\"></pair><pair name=\"reponseStatus\" type=\"str\"></pair><pair name=\"orderConfirmed\" type=\"str\"></pair><pair name=\"orderCancelled\" type=\"str\"></pair><pair name=\"noOfPackages\" type=\"num\">0</pair><pair name=\"emailLabelsTo\" type=\"str\"></pair><pair name=\"returnCenter\" type=\"str\"></pair><pair name=\"shipFromAddress\" type=\"str\"></pair><pair name=\"shippingAddress\" type=\"str\"></pair><pair name=\"paymentDetail\" type=\"str\"></pair><pair name=\"orderTotals\" type=\"str\"></pair><pair name=\"isCharge\" type=\"str\"></pair><pair name=\"returnReferenceNumber\" type=\"str\"></pair><pair name=\"onHold\" type=\"str\"></pair><pair name=\"reasonCode\" type=\"str\"></pair><pair name=\"reasonCodeDescription\" type=\"str\"></pair><pair name=\"automatedReturn\" type=\"str\"></pair><object name=\"returnOrderLines\"><array name=\"returnOrderLine\"><object><pair name=\"orderLineId\" type=\"str\"></pair><pair name=\"orderLineNumber\" type=\"str\"></pair><pair name=\"itemId\" type=\"num\">{c_ItemID}</pair>" /* {c_ItemID} */
	                           "<pair name=\"itemSize\" type=\"str\"></pair><pair name=\"itemColor\" type=\"str\"></pair><pair name=\"itemStyle\" type=\"str\"></pair><pair name=\"imageURL\" type=\"str\"></pair><pair name=\"webURL\" type=\"str\"></pair><pair name=\"requestedQuantity\" type=\"str\"></pair><pair name=\"orderedQtyUOM\" type=\"str\"></pair><pair name=\"unitPriceAmount\" type=\"str\"></pair><pair name=\"itemDescription\" type=\"str\"></pair><pair name=\"orderLineStatus\" type=\"str\"></pair><pair name=\"receivingCondition\" type=\"str\"></pair><pair name=\"customerOrderlineId\" type=\"num\">{c_CustomerOrderlineID}</pair>" /* {c_CustomerOrderlineID} */
	                           "<pair name=\"receiptExpected\" type=\"str\"></pair><pair name=\"reasonCodes\" type=\"str\"></pair><pair name=\"lineTotal\" type=\"str\"></pair><pair name=\"formattedLineTotal\" type=\"str\"></pair><pair name=\"priceAmount\" type=\"str\"></pair><pair name=\"isReturnable\" type=\"str\"></pair><pair name=\"isExchangeable\" type=\"str\"></pair><pair name=\"returnQuantity\" type=\"num\">1</pair><pair name=\"invoicedQuantity\" type=\"str\"></pair><pair name=\"remainingReturnableQuantity\" type=\"str\"></pair><pair name=\"returnReason\" type=\"str\"></pair><pair name=\"returnAction\" type=\"str\">Return</pair><pair name=\"cancelled\" type=\"str\"></pair><pair name=\"priceDetails\" type=\"str\"></pair><pair name=\"formattedTotalLineChargeAmount\" type=\"str\"></pair><pair name=\"chargeDetails\" type=\"str\"></pair><object name=\"notes\"><array name=\"note\"></array></object><pair name=\"receivedQuantity\" type=\"str\"></pair><pair name=\"receiptStatus\" type=\"str\"></pair><pair name=\"onHold\" type=\"str\"></pair><pair name=\"holdReasonCode\" type=\"str\"></pair><pair name=\"customFields\" type=\"str\"></pair><pair name=\"localizedOrderlineStatus\" type=\"str\"></pair></object></array></object><object name=\"paymentDetails\"><array name=\"paymentDetail\"></array></object><object name=\"notes\"><array name=\"note\"></array></object><pair name=\"returnASN\" type=\"str\"></pair><pair name=\"invoiceDetails\" type=\"str\"></pair><pair name=\"processReturn\" type=\"str\"></pair><pair name=\"customFields\" type=\"str\"></pair><pair name=\"customerFullName\" type=\"str\"></pair><pair name=\"customerPhone\" type=\"str\"></pair></object></object></HP_EXTENSION>",
		"TargetParam=DFE_BODY_2", 
		LAST);
	
		
	/* Extract orderNumber value from response: "orderNumber":"000012469", */
	
	web_reg_save_param("c_ReturnOrder",
	                   "LB=\"orderNumber\":\"",
	                   "RB=\",",
	                   "ORD=1",
	                   "Search=ALL",LAST);
	
	lr_eval_string("{pTimestamp}");
	
	/* Extract orderId value from response: "orderId":1926145, */
	
	web_reg_save_param("c_OrderId",
	                   "LB=\"orderId\":",
	                   "RB=,",
	                   "ORD=1",
	                   "Search=ALL",LAST);
	
	/* Extract customerOrderId value from response: "customerOrderId":184935, */
	
	web_reg_save_param("c_CustomerOrderId",
	                   "LB=\"customerOrderId\":",
	                   "RB=,",
	                   "ORD=1",
	                   "Search=ALL",LAST);

	
	/* Extract addressLine1 value from response: "addressLine1":"2608 Hazelnut Ct"," */
	
		web_reg_save_param("c_AdressLine1",
	                   "LB=\"addressLine1\":\"",
	                   "RB=\",",
	                   "ORD=1",
	                   "Search=ALL",LAST);
	
	/* Extract city value from response: "city":"Hebron", */
	
		web_reg_save_param("c_City",
	                   "LB=\"city\":\"",
	                   "RB=\",",
	                   "ORD=1",
	                   "Search=ALL",LAST);
	
	/* Extract stateProv value from response: "stateProv":"KY", */
	
		web_reg_save_param("c_State",
	                   "LB=\"stateProv\":\"",
	                   "RB=\",",
	                   "ORD=1",
	                   "Search=ALL",LAST);	
	
	/* Extract postalCode value from response: "postalCode":41048, */
	
		web_reg_save_param("c_PostalCode",
	                   "LB=\"postalCode\":",
	                   "RB=,",
	                   "ORD=1",
	                   "Search=ALL",LAST);
	
	/* Extract county value from response: "county":"BOONE", */
	
		web_reg_save_param("c_County",
	                   "LB=\"county\":\"",
	                   "RB=\",",
	                   "ORD=1",
	                   "Search=ALL",LAST); 
	
	/* Extract orderLineId value from response: "orderLineId":25447666, */
	
		web_reg_save_param("c_orderLineId",
	                   "LB=\"orderLineId\":",
	                   "RB=,",
	                   "ORD=1",
	                   "Search=ALL",LAST);
	
	web_custom_request("createOrUpdate", 
		"URL={pURL}/services/olm/returns/createOrUpdate?_dc=1466848028628&jsonFileName=createOrUpdate", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t16.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_2}",
		EXTRARES, 
		"Url=../syscodes/syscodes?_dc=1466848031436&syscode=B032&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/resources/icons/common/delete_item.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=../syscodes/syscodes?_dc=1466848031442&syscode=B028&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM, 
		"Url=/manh/olm/resources/icons/collapse/expandArrow.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		"Url=itemsinorder?_dc=1466848031657&jsonFileName=COItemList&customerOrderNumber={pOrder_No}&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", ENDITEM,
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_05_Select_Item_to_be_returned_and_click_on_Done"),LR_AUTO);

	lr_think_time(1);
	
	/* Click on button Proceed to review */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_06_Click_on_button_Proceed_to_review"));

	web_url("ping.jsp_10", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466848044141", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t17.inf", 
		"Mode=HTML", 
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"events\">"
						"<array name=\"event\">"
							"<object>"
								"<pair name=\"transactionId\" type=\"str\">c98fc47a-3c90-402c-800c-a9d078bbc575</pair>"
								"<pair name=\"eventName\" type=\"str\">DONE</pair>"
								"<pair name=\"screenId\" type=\"num\">280010</pair>"
								"<pair name=\"userName\" type=\"str\">{pUserName}</pair>"
								"<pair name=\"logCategory\" type=\"str\">button</pair>"
								"<pair name=\"logLevel\" type=\"num\">4</pair>"
								"<pair name=\"eventTimeStamp\" type=\"str\">{pCurrentDate}</pair>"
								"<pair name=\"additionalInfo\" type=\"str\">Create a return/exchange</pair>"
							"</object>"
						"</array>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_3", 
		LAST);

	web_custom_request("saveUserActivity", 
		"URL={pURL}/services/olm/log/saveUserActivity?_dc=1466848046273&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t18.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_3}", 
		LAST);

	web_url("ping.jsp_11", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466848059469", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t19.inf", 
		"Mode=HTML", 
		LAST);

	web_convert_from_formatted("FormattedData=" 
	                           "<HP_EXTENSION name=\"JsonXml\">" 
	                           "<object><object name=\"ReturnOrder\">" 
	                           "<pair name=\"orderNumber\" type=\"str\">{c_ReturnOrder}</pair>" 
	                           "<pair name=\"orderId\" type=\"num\">{c_OrderId}</pair>" 
	                           "<pair name=\"orderType\" type=\"str\">RLM</pair>" 
	                           "<pair name=\"tcCompanyId\" type=\"num\">1</pair>" 
	                           "<pair name=\"customerOrderId\" type=\"num\">{c_CustomerOrderId}</pair>" 
	                           "<pair name=\"customerOrderNumber\" type=\"num\">{pOrder_No}</pair>" 
	                           "<pair name=\"orderStatus\" type=\"str\">{c_OrderStatus}</pair>" 
	                           "<pair name=\"orderStatusId\" type=\"num\">100</pair>" 
	                           "<pair name=\"orderCapturedDate\" type=\"str\">10/4/15 14:40 EDT</pair>" 
	                           "<pair name=\"orderCreatedDate\" type=\"str\">10/4/15 14:40 EDT</pair>" 
	                           "<pair name=\"reponseStatus\" type=\"str\"></pair>" 
	                           "<pair name=\"orderConfirmed\" type=\"bool\">false</pair>" 
	                           "<pair name=\"orderCancelled\" type=\"bool\">false</pair>" 
	                           "<pair name=\"noOfPackages\" type=\"num\">1</pair>" 
	                           "<pair name=\"emailLabelsTo\" type=\"str\">{c_CustomerEmail}</pair>" 
	                           "<pair name=\"returnCenter\" type=\"str\">SEDC-ECOM</pair>" 
	                           "<object name=\"shipFromAddress\">" 
	                           "<pair name=\"firstName\" type=\"str\">{c_CustomerFirstName}</pair>" 
	                           "<pair name=\"lastName\" type=\"str\">{c_CustomerLastName}</pair>" 
	                           "<pair name=\"addressLine1\" type=\"str\">{c_AdressLine1}</pair>" 
	                           "<pair name=\"city\" type=\"str\">{c_City}</pair>" 
	                           "<pair name=\"stateProv\" type=\"str\">{c_State}</pair>" 
	                           "<pair name=\"postalCode\" type=\"num\">{c_PostalCode}</pair>" 
	                           "<pair name=\"country\" type=\"str\">US</pair>" 
	                           "<pair name=\"county\" type=\"str\">{c_County}</pair>" 
	                           "<pair name=\"phone\" type=\"num\">{c_CustomerPhone}</pair>" /*{c_CustomerPhone} */
	                           "<pair name=\"email\" type=\"str\">{c_CustomerEmail}</pair>" 
	                           "</object>" 
	                           "<object name=\"shippingAddress\">" 
	                           "<pair name=\"firstName\" type=\"str\">SEDC-ECOM</pair>" 
	                           "<pair name=\"addressLine1\" type=\"str\">500 Plaza Drive</pair>" 
	                           "<pair name=\"city\" type=\"str\">Secaucus</pair>" 
	                           "<pair name=\"stateProv\" type=\"str\">NJ</pair>" 
	                           "<pair name=\"postalCode\" type=\"str\">07094</pair>" 
	                           "<pair name=\"country\" type=\"str\">US</pair>" 
	                           "<pair name=\"addressVerified\" type=\"bool\">false</pair>" 
	                           "</object>" 
	                           "<pair name=\"paymentDetail\" type=\"str\"></pair>" 
	                           "<object name=\"orderTotals\">" 
	                           "<pair name=\"totalCharges\" type=\"str\">0.00</pair>" 
	                           "<pair name=\"formattedTotalCharges\" type=\"str\">$0.00</pair>" 
	                           "<pair name=\"totalDiscounts\" type=\"str\">0.00</pair>" 
	                           "<pair name=\"formattedTotalDiscounts\" type=\"str\">$0.00</pair>" 
	                           "<pair name=\"totalTaxes\" type=\"str\">0.00</pair>" 
	                           "<pair name=\"formattedTotalTaxes\" type=\"str\">$0.00</pair>" 
	                           "<pair name=\"itemSubTotal\" type=\"num\">{c_Amount}</pair>" 
	                           "<pair name=\"formattedItemSubTotal\" type=\"str\">${c_Amount}</pair>" 
	                           "<pair name=\"grandTotal\" type=\"num\">{c_Amount}</pair>" 
	                           "<pair name=\"formattedGrandTotal\" type=\"str\">${c_Amount}</pair>" 
	                           "<pair name=\"remainingToPay\" type=\"num\">{c_Amount}</pair>" 
	                           "<pair name=\"formattedRemainingToPay\" type=\"str\">${c_Amount}</pair>" 
	                           "<pair name=\"amountPaid\" type=\"num\">{c_Amount}</pair>" 
	                           "<pair name=\"formattedAmountPaid\" type=\"str\">${c_Amount}</pair>" 
	                           "<object name=\"totalChargeDetails\"><object name=\"totalChargeDetail\"><pair name=\"chargeCategory\" type=\"str\">Shipping</pair><pair name=\"chargeValue\" type=\"str\">Not Available</pair><pair name=\"formattedChargeValue\" type=\"str\">Not Available</pair><pair name=\"showCurrency\" type=\"bool\">false</pair><pair name=\"overriddenSnHCharge\" type=\"bool\">false</pair><pair name=\"chargesNotAvailable\" type=\"bool\">true</pair><pair name=\"eligibleForSnHOverride\" type=\"bool\">false</pair></object></object><object name=\"totalDiscountDetails\"><pair name=\"totalPromotionsApplied\" type=\"num\">0</pair><pair name=\"totalPromotionAmount\" type=\"num\">0</pair><pair name=\"formattedTotalPromotionAmount\" type=\"str\">$0.00</pair><pair name=\"totalCouponsApplied\" type=\"num\">0</pair><pair name=\"totalCouponAmount\" type=\"num\">0</pair><pair name=\"formattedTotalCouponAmount\" type=\"str\">$0.00</pair><pair name=\"totalAppeasementsApplied\" type=\"num\">0</pair><pair name=\"totalAppeasementAmount\" type=\"num\">0</pair><pair name=\"formattedTotalAppeasementAmount\" type=\"str\">$0.00</pair></object></object><pair name=\"isCharge\" type=\"bool\">false</pair><pair name=\"returnReferenceNumber\" type=\"str\"></pair><pair name=\"onHold\" type=\"bool\">false</pair><pair name=\"reasonCode\" type=\"str\"></pair><pair name=\"reasonCodeDescription\" type=\"str\"></pair><pair name=\"automatedReturn\" type=\"bool\">false</pair><object name=\"chargeGroupDetails\"><array name=\"chargeGroupDetail\"></array></object><object name=\"returnOrderLines\"><array name=\"returnOrderLine\"><object><pair name=\"orderLineId\" type=\"num\">{c_orderLineId}</pair><pair name=\"orderLineNumber\" type=\"num\">1</pair><pair name=\"itemId\" type=\"num\">{c_ItemID}</pair>" /* {c_ItemID} */
	                           "<pair name=\"itemSize\" type=\"str\"></pair><pair name=\"itemColor\" type=\"str\"></pair><pair name=\"itemStyle\" type=\"str\"></pair><pair name=\"imageURL\" type=\"str\"></pair><pair name=\"webURL\" type=\"str\"></pair><pair name=\"requestedQuantity\" type=\"str\"></pair><pair name=\"orderedQtyUOM\" type=\"str\"></pair><pair name=\"unitPriceAmount\" type=\"str\"></pair><pair name=\"itemDescription\" type=\"str\"></pair><pair name=\"orderLineStatus\" type=\"str\"></pair><pair name=\"receivingCondition\" type=\"str\">USER Defined - B028</pair><pair name=\"customerOrderlineId\" type=\"num\">{c_CustomerOrderlineID}</pair>" /* {c_CustomerOrderlineID} */
	                           "<pair name=\"receiptExpected\" type=\"bool\">true</pair><pair name=\"reasonCodes\" type=\"str\"></pair><pair name=\"lineTotal\" type=\"str\"></pair><pair name=\"formattedLineTotal\" type=\"str\"></pair><pair name=\"priceAmount\" type=\"str\"></pair><pair name=\"isReturnable\" type=\"str\"></pair><pair name=\"isExchangeable\" type=\"str\"></pair><pair name=\"returnQuantity\" type=\"str\">1</pair><pair name=\"invoicedQuantity\" type=\"str\"></pair><pair name=\"remainingReturnableQuantity\" type=\"str\"></pair><pair name=\"returnReason\" type=\"str\">Customer Preference</pair><pair name=\"returnAction\" type=\"str\">Return</pair><pair name=\"cancelled\" type=\"str\"></pair><pair name=\"priceDetails\" type=\"str\"></pair><pair name=\"formattedTotalLineChargeAmount\" type=\"str\"></pair><pair name=\"chargeDetails\" type=\"str\"></pair><object name=\"notes\"><array name=\"note\"></array></object><pair name=\"receivedQuantity\" type=\"str\"></pair><pair name=\"receiptStatus\" type=\"str\"></pair><pair name=\"onHold\" type=\"str\"></pair><pair name=\"holdReasonCode\" type=\"str\"></pair><pair name=\"customFields\" type=\"str\"></pair><pair name=\"localizedOrderlineStatus\" type=\"str\"></pair></object></array></object><object name=\"paymentDetails\"><array name=\"paymentDetail\"></array></object><object name=\"notes\"><array name=\"note\"></array></object><pair name=\"returnASN\" type=\"str\"></pair><pair name=\"invoiceDetails\" type=\"str\"></pair><pair name=\"processReturn\" type=\"str\"></pair><pair name=\"customFields\" type=\"str\"></pair><pair name=\"customerFullName\" type=\"str\">{c_CustomerFirstName} {c_CustomerLastName}</pair><pair name=\"customerPhone\" type=\"num\">{c_CustomerPhone}</pair></object></object></HP_EXTENSION>",
		"TargetParam=DFE_BODY_4", 
		LAST);

	web_custom_request("createOrUpdate_2", 
		"URL={pURL}/services/olm/returns/createOrUpdate?_dc=1466848063223&jsonFileName=createOrUpdate", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t20.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_4}", 
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"events\">"
						"<array name=\"event\">"
							"<object>"
								"<pair name=\"transactionId\" type=\"str\">c98fc47a-3c90-402c-800c-a9d078bbc575</pair>"
								"<pair name=\"eventName\" type=\"str\">PROCEED TO REVIEW</pair>"
								"<pair name=\"screenId\" type=\"num\">280010</pair>"
								"<pair name=\"userName\" type=\"str\">{pUserName}</pair>"
								"<pair name=\"logCategory\" type=\"str\">button</pair>"
								"<pair name=\"logLevel\" type=\"num\">4</pair>"
								"<pair name=\"eventTimeStamp\" type=\"str\">{pCurrentDate}</pair>"
								"<pair name=\"additionalInfo\" type=\"str\">Proceed to review for RMA: {c_ReturnOrder}</pair>"
							"</object>"
						"</array>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_5", 
		LAST);

	web_custom_request("saveUserActivity_2", 
		"URL={pURL}/services/olm/log/saveUserActivity?_dc=1466848066277&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t21.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_5}", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_06_Click_on_button_Proceed_to_review"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_12", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466848076505", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t22.inf", 
		"Mode=HTML", 
		LAST);

	/* Click on Confirm Return button */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_07_Click_On_button_Confirm_Return"));

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"events\">"
						"<array name=\"event\">"
							"<object>"
								"<pair name=\"transactionId\" type=\"str\">c98fc47a-3c90-402c-800c-a9d078bbc575</pair>"
								"<pair name=\"eventName\" type=\"str\">CONFIRM RETURN</pair>"
								"<pair name=\"screenId\" type=\"num\">280010</pair>"
								"<pair name=\"userName\" type=\"str\">{pUserName}</pair>"
								"<pair name=\"logCategory\" type=\"str\">button</pair>"
								"<pair name=\"logLevel\" type=\"num\">4</pair>"
								"<pair name=\"eventTimeStamp\" type=\"str\">{pCurrentDate}</pair>"
								"<pair name=\"additionalInfo\" type=\"str\">confirming return order</pair>"
							"</object>"
						"</array>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_6", 
		LAST);

	web_custom_request("saveUserActivity_3", 
		"URL={pURL}/services/olm/log/saveUserActivity?_dc=1466848087860&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t23.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_6}", 
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"ReturnOrder\">"
						"<pair name=\"orderNumber\" type=\"str\">{c_ReturnOrder}</pair>"
						"<pair name=\"orderId\" type=\"num\">{c_OrderId}</pair>"
						"<pair name=\"orderType\" type=\"str\">RLM</pair>"
						"<pair name=\"tcCompanyId\" type=\"num\">1</pair>"
						"<pair name=\"customerOrderId\" type=\"num\">{c_CustomerOrderId}</pair>"
						"<pair name=\"customerOrderNumber\" type=\"num\">{pOrder_No}</pair>"
						"<pair name=\"orderStatus\" type=\"str\">{c_OrderStatus}</pair>"
						"<pair name=\"orderStatusId\" type=\"num\">100</pair>"
						"<pair name=\"orderCapturedDate\" type=\"str\">10/4/15 14:40 EDT</pair>"
						"<pair name=\"orderCreatedDate\" type=\"str\">10/4/15 14:40 EDT</pair>"
						"<pair name=\"reponseStatus\" type=\"str\"></pair>"
						"<pair name=\"orderConfirmed\" type=\"bool\">true</pair>"
						"<pair name=\"orderCancelled\" type=\"bool\">false</pair>"
						"<pair name=\"noOfPackages\" type=\"num\">1</pair>"
						"<pair name=\"emailLabelsTo\" type=\"str\">{c_CustomerEmail}</pair>"
						"<pair name=\"returnCenter\" type=\"str\">SEDC-ECOM</pair>"
						"<object name=\"shipFromAddress\">"
							"<pair name=\"firstName\" type=\"str\">{c_CustomerFirstName}</pair>"
							"<pair name=\"lastName\" type=\"str\">{c_CustomerLastName}</pair>"
							"<pair name=\"addressLine1\" type=\"str\">{c_AdressLine1}</pair>"
							"<pair name=\"city\" type=\"str\">{c_City}</pair>"
							"<pair name=\"stateProv\" type=\"str\">{c_State}</pair>"
							"<pair name=\"postalCode\" type=\"num\">{c_PostalCode}</pair>"
							"<pair name=\"country\" type=\"str\">US</pair>"
							"<pair name=\"county\" type=\"str\">{c_County}</pair>"
							"<pair name=\"phone\" type=\"num\">{c_CustomerPhone}</pair>"
							"<pair name=\"email\" type=\"str\">{c_CustomerEmail}</pair>"
						"</object>"
						"<object name=\"shippingAddress\">"
							"<pair name=\"firstName\" type=\"str\">SEDC-ECOM</pair>"
							"<pair name=\"addressLine1\" type=\"str\">500 Plaza Drive</pair>"
							"<pair name=\"city\" type=\"str\">Secaucus</pair>"
							"<pair name=\"stateProv\" type=\"str\">NJ</pair>"
							"<pair name=\"postalCode\" type=\"str\">07094</pair>"
							"<pair name=\"country\" type=\"str\">US</pair>"
							"<pair name=\"addressVerified\" type=\"bool\">true</pair>"
						"</object>"
						"<pair name=\"paymentDetail\" type=\"str\"></pair>"
						"<object name=\"orderTotals\">"
							"<pair name=\"totalCharges\" type=\"str\">0.00</pair>"
							"<pair name=\"formattedTotalCharges\" type=\"str\">$0.00</pair>"
							"<pair name=\"totalDiscounts\" type=\"str\">0.00</pair>"
							"<pair name=\"formattedTotalDiscounts\" type=\"str\">$0.00</pair>"
							"<pair name=\"totalTaxes\" type=\"str\">0.00</pair>"
							"<pair name=\"formattedTotalTaxes\" type=\"str\">$0.00</pair>"
							"<pair name=\"itemSubTotal\" type=\"num\">{c_Amount}</pair>"
							"<pair name=\"formattedItemSubTotal\" type=\"str\">${c_Amount}</pair>"
							"<pair name=\"grandTotal\" type=\"num\">{c_Amount}</pair>"
							"<pair name=\"formattedGrandTotal\" type=\"str\">${c_Amount}</pair>"
							"<pair name=\"remainingToPay\" type=\"num\">{c_Amount}</pair>"
							"<pair name=\"formattedRemainingToPay\" type=\"str\">${c_Amount}</pair>"
							"<pair name=\"amountPaid\" type=\"num\">{c_Amount}</pair>"
							"<pair name=\"formattedAmountPaid\" type=\"str\">${c_Amount}</pair>"
							"<object name=\"totalChargeDetails\">"
								"<object name=\"totalChargeDetail\">"
									"<pair name=\"chargeCategory\" type=\"str\">Shipping</pair>"
									"<pair name=\"chargeValue\" type=\"str\">Not Available</pair>"
									"<pair name=\"formattedChargeValue\" type=\"str\">Not Available</pair>"
									"<pair name=\"showCurrency\" type=\"bool\">false</pair>"
									"<pair name=\"overriddenSnHCharge\" type=\"bool\">false</pair>"
									"<pair name=\"chargesNotAvailable\" type=\"bool\">true</pair>"
									"<pair name=\"eligibleForSnHOverride\" type=\"bool\">false</pair>"
								"</object>"
							"</object>"
							"<object name=\"totalDiscountDetails\">"
								"<pair name=\"totalPromotionsApplied\" type=\"num\">0</pair>"
								"<pair name=\"totalPromotionAmount\" type=\"num\">0</pair>"
								"<pair name=\"formattedTotalPromotionAmount\" type=\"str\">$0.00</pair>"
								"<pair name=\"totalCouponsApplied\" type=\"num\">0</pair>"
								"<pair name=\"totalCouponAmount\" type=\"num\">0</pair>"
								"<pair name=\"formattedTotalCouponAmount\" type=\"str\">$0.00</pair>"
								"<pair name=\"totalAppeasementsApplied\" type=\"num\">0</pair>"
								"<pair name=\"totalAppeasementAmount\" type=\"num\">0</pair>"
								"<pair name=\"formattedTotalAppeasementAmount\" type=\"str\">$0.00</pair>"
							"</object>"
						"</object>"
						"<pair name=\"isCharge\" type=\"bool\">false</pair>"
						"<pair name=\"returnReferenceNumber\" type=\"str\"></pair>"
						"<pair name=\"onHold\" type=\"bool\">false</pair>"
						"<pair name=\"reasonCode\" type=\"str\"></pair>"
						"<pair name=\"reasonCodeDescription\" type=\"str\"></pair>"
						"<pair name=\"automatedReturn\" type=\"bool\">false</pair>"
						"<object name=\"chargeGroupDetails\">"
							"<array name=\"chargeGroupDetail\"></array>"
						"</object>"
						"<object name=\"returnOrderLines\">"
							"<array name=\"returnOrderLine\"></array>"
						"</object>"
						"<object name=\"paymentDetails\">"
							"<array name=\"paymentDetail\"></array>"
						"</object>"
						"<object name=\"notes\">"
							"<array name=\"note\"></array>"
						"</object>"
						"<pair name=\"returnASN\" type=\"str\"></pair>"
						"<pair name=\"invoiceDetails\" type=\"str\"></pair>"
						"<pair name=\"processReturn\" type=\"str\"></pair>"
						"<pair name=\"customFields\" type=\"str\"></pair>"
						"<pair name=\"customerFullName\" type=\"str\">{c_CustomerFirstName} {c_CustomerLastName}</pair>"
						"<pair name=\"customerPhone\" type=\"num\">{c_CustomerPhone}</pair>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_7", 
		LAST);

	web_custom_request("createOrUpdate_3", 
		"URL={pURL}/services/olm/returns/createOrUpdate?_dc=1466848087753&jsonFileName=createOrUpdate", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t24.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_7}", 
		LAST);

	web_url("ping.jsp_13", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466848092048", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t25.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_07_Click_On_button_Confirm_Return"),LR_AUTO);

	lr_think_time(1);

	/* Capture Return Order Number and close the popup message */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_08_Capture_Order_Number_and_close_the_popup_message"));

	web_url("ping.jsp_14", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466848109088", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t26.inf", 
		"Mode=HTML", 
		LAST);

	web_custom_request("windows_2", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14271272", 
		"Method=DELETE", 
		"TargetFrame=", 
		"Resource=0", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t27.inf", 
		"Mode=HTML", 
		LAST);

	web_custom_request("windows_3", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14271272", 
		"Method=DELETE", 
		"TargetFrame=", 
		"Resource=0", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t28.inf", 
		"Mode=HTML", 
		LAST);

	web_custom_request("windows_4", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14272587&regionId=-1&businessUnitId=-1", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t29.inf", 
		"Mode=HTML", 
		"EncType=", 
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"returnOrderSearchCriteria\">"
						"<pair name=\"entityType\" type=\"str\"></pair>"
						"<pair name=\"orderNumber\" type=\"str\"></pair>"
						"<pair name=\"parentOrderNumber\" type=\"num\">{pOrder_No}</pair>"
						"<pair name=\"createdFromDate\" type=\"str\"></pair>"
						"<pair name=\"createdToDate\" type=\"str\"></pair>"
						"<pair name=\"noOfRecordsPerPage\" type=\"num\">1000</pair>"
						"<object name=\"customerInfo\">"
							"<pair name=\"customerFirstName\" type=\"str\">{c_CustomerFirstName}</pair>"
							"<pair name=\"customerLastName\" type=\"str\">{c_CustomerLastName}</pair>"
							"<pair name=\"customerFullName\" type=\"str\">{c_CustomerFirstName} {c_CustomerLastName}</pair>"
							"<pair name=\"customerEmail\" type=\"str\">{c_CustomerEmail}</pair>"
							"<pair name=\"customerPhone\" type=\"str\">{c_CustomerPhone}</pair>"
							"<pair name=\"customerId\" type=\"str\"></pair>"
							"<pair name=\"externalCustomerId\" type=\"str\">{c_ExternalCustomerId}</pair>"/* {c_ExternalCustomerId} */
							"<pair name=\"customField1\" type=\"str\"></pair>"
							"<pair name=\"customField2\" type=\"str\"></pair>"
							"<pair name=\"customField3\" type=\"str\"></pair>"
							"<pair name=\"customField4\" type=\"str\"></pair>"
							"<pair name=\"customField5\" type=\"str\"></pair>"
							"<pair name=\"customField6\" type=\"str\"></pair>"
							"<pair name=\"customField7\" type=\"str\"></pair>"
							"<pair name=\"customField8\" type=\"str\"></pair>"
							"<pair name=\"customField9\" type=\"str\"></pair>"
							"<pair name=\"customField10\" type=\"str\"></pair>"
						"</object>"
						"<pair name=\"customerBasicInfo\" type=\"str\"></pair>"
						"<object name=\"sortingCriterion\">"
							"<pair name=\"sortField\" type=\"str\">createdDTTM</pair>"
							"<pair name=\"sortDirection\" type=\"str\">DESC</pair>"
						"</object>"
						"<pair name=\"currentPageNumber\" type=\"num\">0</pair>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_8", 
		LAST);

	web_custom_request("returnList", 
		"URL={pURL}/services/olm/returns/returnList?_dc=1466848176607&page=1&start=0&limit=50&sort=%5B%7B%22property%22%3A%22orderCreatedDate%22%2C%22direction%22%3A%22DESC%22%7D%5D", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t30.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_8}", 
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"customerOrderSearchCriteria\">"
						"<pair name=\"entityType\" type=\"str\">All</pair>"
						"<pair name=\"orderNumber\" type=\"num\">{pOrder_No}</pair>"
						"<pair name=\"parentOrderNumber\" type=\"str\"></pair>"
						"<pair name=\"createdFromDate\" type=\"str\"></pair>"
						"<pair name=\"createdToDate\" type=\"str\"></pair>"
						"<pair name=\"noOfRecordsPerPage\" type=\"num\">1000</pair>"
						"<object name=\"customerInfo\">"
							"<pair name=\"customerFirstName\" type=\"str\">{c_CustomerFirstName}</pair>"
							"<pair name=\"customerLastName\" type=\"str\">{c_CustomerLastName}</pair>"
							"<pair name=\"customerFullName\" type=\"str\">{c_CustomerFirstName} {c_CustomerLastName}</pair>"
							"<pair name=\"customerEmail\" type=\"str\">{c_CustomerEmail}</pair>"
							"<pair name=\"customerPhone\" type=\"str\">{c_CustomerPhone}</pair>"
							"<pair name=\"customerId\" type=\"str\"></pair>"
							"<pair name=\"externalCustomerId\" type=\"str\">{c_ExternalCustomerId}</pair>"
							"<pair name=\"customField1\" type=\"str\"></pair>"
							"<pair name=\"customField2\" type=\"str\"></pair>"
							"<pair name=\"customField3\" type=\"str\"></pair>"
							"<pair name=\"customField4\" type=\"str\"></pair>"
							"<pair name=\"customField5\" type=\"str\"></pair>"
							"<pair name=\"customField6\" type=\"str\"></pair>"
							"<pair name=\"customField7\" type=\"str\"></pair>"
							"<pair name=\"customField8\" type=\"str\"></pair>"
							"<pair name=\"customField9\" type=\"str\"></pair>"
							"<pair name=\"customField10\" type=\"str\"></pair>"
						"</object>"
						"<pair name=\"customerBasicInfo\" type=\"str\"></pair>"
						"<object name=\"sortingCriterion\">"
							"<pair name=\"sortField\" type=\"str\">createdDTTM</pair>"
							"<pair name=\"sortDirection\" type=\"str\">DESC</pair>"
						"</object>"
						"<pair name=\"currentPageNumber\" type=\"num\">0</pair>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_9", 
		LAST);

	web_custom_request("customerOrderAndTransactionList_2", 
		"URL={pURL}/services/olm/customerorder/customerOrderAndTransactionList?_dc=1466848176577&page=1&start=0&limit=50&sort=%5B%7B%22property%22%3A%22orderCreatedDate%22%2C%22direction%22%3A%22DESC%22%7D%5D", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t31.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_9}", 
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<pair name=\"orderId\" type=\"num\">{c_CustomerOrderId}</pair>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_10", 
		LAST);

	web_custom_request("loadIsReturnableOrder", 
		"URL={pURL}/services/olm/isReturnableOrderUpdateService/loadIsReturnableOrder?_dc=1466848180140&orderId={c_CustomerOrderId}&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t32.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_10}", 
		EXTRARES, 
		"Url=/manh/olm/resources/icons/transaction/CO_icon.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		LAST);

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<pair name=\"orderId\" type=\"num\">{c_CustomerOrderId}</pair>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_11", 
		LAST);

	web_custom_request("loadIsReturnableOrder_2", 
		"URL={pURL}/services/olm/isReturnableOrderUpdateService/loadIsReturnableOrder?_dc=1466848181270&orderId={c_CustomerOrderId}&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t33.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_11}", 
		EXTRARES, 
		"Url=/manh/olm/resources/icons/collapse/expandArrowWhite.png", "Referer={pURL}/manh/olm/resources/css/dom.css", ENDITEM, 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_08_Capture_Order_Number_and_close_the_popup_message"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_15", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466848189906", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t34.inf", 
		"Mode=HTML", 
		LAST);

	if ((file_stream = fopen(filename, "a+")) == NULL) //open file in append mode
	{ 
	lr_error_message ("Cannot open %s", filename); 
	return -1; 
	}


	fprintf (file_stream, "%s\n", lr_eval_string("THE PARAMETER IS :{c_ReturnOrder},{pOrder_No},{pTimestamp}")); //Parameter is parameter name to be written in a file

	fclose(file_stream);
	
	return 0;
}
