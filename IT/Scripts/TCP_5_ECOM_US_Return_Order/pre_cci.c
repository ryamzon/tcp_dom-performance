# 1 "c:\\it\\scripts\\tcp_5_ecom_us_return_order\\\\combined_TCP_5_ECOM_US_Return_Order.c"
# 1 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h" 1
 
 












 











# 103 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"








































































	

 



















 
 
 
 
 


 
 
 
 
 
 














int     lr_start_transaction   (char * transaction_name);
int lr_start_sub_transaction          (char * transaction_name, char * trans_parent);
long lr_start_transaction_instance    (char * transaction_name, long parent_handle);
int   lr_start_cross_vuser_transaction		(char * transaction_name, char * trans_id_param); 



int     lr_end_transaction     (char * transaction_name, int status);
int lr_end_sub_transaction            (char * transaction_name, int status);
int lr_end_transaction_instance       (long transaction, int status);
int   lr_end_cross_vuser_transaction	(char * transaction_name, char * trans_id_param, int status);


 
typedef char* lr_uuid_t;
 



lr_uuid_t lr_generate_uuid();

 


int lr_generate_uuid_free(lr_uuid_t uuid);

 



int lr_generate_uuid_on_buf(lr_uuid_t buf);

   
# 266 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
int lr_start_distributed_transaction  (char * transaction_name, lr_uuid_t correlator, long timeout  );

   







int lr_end_distributed_transaction  (lr_uuid_t correlator, int status);


double lr_stop_transaction            (char * transaction_name);
double lr_stop_transaction_instance   (long parent_handle);


void lr_resume_transaction           (char * trans_name);
void lr_resume_transaction_instance  (long trans_handle);


int lr_update_transaction            (const char *trans_name);


 
void lr_wasted_time(long time);


 
int lr_set_transaction(const char *name, double duration, int status);
 
long lr_set_transaction_instance(const char *name, double duration, int status, long parent_handle);


int   lr_user_data_point                      (char *, double);
long lr_user_data_point_instance                   (char *, double, long);
 



int lr_user_data_point_ex(const char *dp_name, double value, int log_flag);
long lr_user_data_point_instance_ex(const char *dp_name, double value, long parent_handle, int log_flag);


int lr_transaction_add_info      (const char *trans_name, char *info);
int lr_transaction_instance_add_info   (long trans_handle, char *info);
int lr_dpoint_add_info           (const char *dpoint_name, char *info);
int lr_dpoint_instance_add_info        (long dpoint_handle, char *info);


double lr_get_transaction_duration       (char * trans_name);
double lr_get_trans_instance_duration    (long trans_handle);
double lr_get_transaction_think_time     (char * trans_name);
double lr_get_trans_instance_think_time  (long trans_handle);
double lr_get_transaction_wasted_time    (char * trans_name);
double lr_get_trans_instance_wasted_time (long trans_handle);
int    lr_get_transaction_status		 (char * trans_name);
int	   lr_get_trans_instance_status		 (long trans_handle);

 



int lr_set_transaction_status(int status);

 



int lr_set_transaction_status_by_name(int status, const char *trans_name);
int lr_set_transaction_instance_status(int status, long trans_handle);


typedef void* merc_timer_handle_t;
 

merc_timer_handle_t lr_start_timer();
double lr_end_timer(merc_timer_handle_t timer_handle);


 
 
 
 
 
 











 



int   lr_rendezvous  (char * rendezvous_name);
 




int   lr_rendezvous_ex (char * rendezvous_name);



 
 
 
 
 
char *lr_get_vuser_ip (void);
void   lr_whoami (int *vuser_id, char ** sgroup, int *scid);
char *	  lr_get_host_name (void);
char *	  lr_get_master_host_name (void);

 
long     lr_get_attrib_long	(char * attr_name);
char *   lr_get_attrib_string	(char * attr_name);
double   lr_get_attrib_double      (char * attr_name);

char * lr_paramarr_idx(const char * paramArrayName, unsigned int index);
char * lr_paramarr_random(const char * paramArrayName);
int    lr_paramarr_len(const char * paramArrayName);

int	lr_param_unique(const char * paramName);
int lr_param_sprintf(const char * paramName, const char * format, ...);


 
 
static void *ci_this_context = 0;






 








void lr_continue_on_error (int lr_continue);
char *   lr_decrypt (const char *EncodedString);


 
 
 
 
 
 



 







 















void   lr_abort (void);
void lr_exit(int exit_option, int exit_status);
void lr_abort_ex (unsigned long flags);

void   lr_peek_events (void);


 
 
 
 
 


void   lr_think_time (double secs);

 


void lr_force_think_time (double secs);


 
 
 
 
 



















int   lr_msg (char * fmt, ...);
int   lr_debug_message (unsigned int msg_class,
									    char * format,
										...);
# 505 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
void   lr_new_prefix (int type,
                                 char * filename,
                                 int line);
# 508 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
int   lr_log_message (char * fmt, ...);
int   lr_message (char * fmt, ...);
int   lr_error_message (char * fmt, ...);
int   lr_output_message (char * fmt, ...);
int   lr_vuser_status_message (char * fmt, ...);
int   lr_error_message_without_fileline (char * fmt, ...);
int   lr_fail_trans_with_error (char * fmt, ...);

 
 
 
 
 
# 531 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"

 
 
 
 
 





int   lr_next_row ( char * table);
int lr_advance_param ( char * param);



														  
														  

														  
														  

													      
 


char *   lr_eval_string (char * str);
int   lr_eval_string_ext (const char *in_str,
                                     unsigned long const in_len,
                                     char ** const out_str,
                                     unsigned long * const out_len,
                                     unsigned long const options,
                                     const char *file,
								     long const line);
# 565 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
void   lr_eval_string_ext_free (char * * pstr);

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
int lr_param_increment (char * dst_name,
                              char * src_name);
# 588 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"













											  
											  

											  
											  
											  

int	  lr_save_var (char *              param_val,
							  unsigned long const param_val_len,
							  unsigned long const options,
							  char *			  param_name);
# 612 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
int   lr_save_string (const char * param_val, const char * param_name);
int   lr_free_parameter (const char * param_name);
int   lr_save_int (const int param_val, const char * param_name);
int   lr_save_timestamp (const char * tmstampParam, ...);
int   lr_save_param_regexp (const char *bufferToScan, unsigned int bufSize, ...);

int   lr_convert_double_to_integer (const char *source_param_name, const char * target_param_name);
int   lr_convert_double_to_double (const char *source_param_name, const char *format_string, const char * target_param_name);

 
 
 
 
 
 
# 683 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
void   lr_save_datetime (const char *format, int offset, const char *name);









 











 
 
 
 
 






 



char * lr_error_context_get_entry (char * key);

 



long   lr_error_context_get_error_id (void);


 
 
 

int lr_table_get_rows_num (char * param_name);

int lr_table_get_cols_num (char * param_name);

char * lr_table_get_cell_by_col_index (char * param_name, int row, int col);

char * lr_table_get_cell_by_col_name (char * param_name, int row, const char* col_name);

int lr_table_get_column_name_by_index (char * param_name, int col, 
											char * * const col_name,
											int * col_name_len);
# 744 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"

int lr_table_get_column_name_by_index_free (char * col_name);

 
 
 
 
# 759 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
int   lr_zip (const char* param1, const char* param2);
int   lr_unzip (const char* param1, const char* param2);

 
 
 
 
 
 
 
 

 
 
 
 
 
 
int   lr_param_substit (char * file,
                                   int const line,
                                   char * in_str,
                                   int const in_len,
                                   char * * const out_str,
                                   int * const out_len);
# 783 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
void   lr_param_substit_free (char * * pstr);


 
# 795 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"





char *   lrfnc_eval_string (char * str,
                                      char * file_name,
                                      long const line_num);
# 803 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"


int   lrfnc_save_string ( const char * param_val,
                                     const char * param_name,
                                     const char * file_name,
                                     long const line_num);
# 809 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"

int   lrfnc_free_parameter (const char * param_name );







typedef struct _lr_timestamp_param
{
	int iDigits;
}lr_timestamp_param;

extern const lr_timestamp_param default_timestamp_param;

int   lrfnc_save_timestamp (const char * param_name, const lr_timestamp_param* time_param);

int lr_save_searched_string(char *buffer, long buf_size, unsigned int occurrence,
			    char *search_string, int offset, unsigned int param_val_len, 
			    char *param_name);

 
char *   lr_string (char * str);

 
# 905 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"

int   lr_save_value (char * param_val,
                                unsigned long const param_val_len,
                                unsigned long const options,
                                char * param_name,
                                char * file_name,
                                long const line_num);
# 912 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"


 
 
 
 
 











int   lr_printf (char * fmt, ...);
 
int   lr_set_debug_message (unsigned int msg_class,
                                       unsigned int swtch);
# 934 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
unsigned int   lr_get_debug_message (void);


 
 
 
 
 

void   lr_double_think_time ( double secs);
void   lr_usleep (long);


 
 
 
 
 
 




int *   lr_localtime (long offset);


int   lr_send_port (long port);


# 1010 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"



struct _lr_declare_identifier{
	char signature[24];
	char value[128];
};

int   lr_pt_abort (void);

void vuser_declaration (void);






# 1039 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"


# 1051 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
















 
 
 
 
 







int    _lr_declare_transaction   (char * transaction_name);


 
 
 
 
 







int   _lr_declare_rendezvous  (char * rendezvous_name);

 
 
 
 
 

 
int lr_enable_ip_spoofing();
int lr_disable_ip_spoofing();


 




int lr_convert_string_encoding(char *sourceString, char *fromEncoding, char *toEncoding, char *paramName);


 
int lr_db_connect (char * pFirstArg, ...);
int lr_db_disconnect (char * pFirstArg,	...);
int lr_db_executeSQLStatement (char * pFirstArg, ...);
int lr_db_dataset_action(char * pFirstArg, ...);
int lr_checkpoint(char * pFirstArg,	...);
int lr_db_getvalue(char * pFirstArg, ...);







 
 

















# 1 "c:\\it\\scripts\\tcp_5_ecom_us_return_order\\\\combined_TCP_5_ECOM_US_Return_Order.c" 2

# 1 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/SharedParameter.h" 1



 
 
 
 
# 100 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/SharedParameter.h"





typedef int PVCI2;
typedef int VTCERR2;

 
 
 

 
extern PVCI2    vtc_connect(char *servername, int portnum, int options);
extern VTCERR2  vtc_disconnect(int pvci);
extern VTCERR2  vtc_get_last_error(int pvci);

 
extern VTCERR2  vtc_query_column(int pvci, char *columnName, int columnIndex, char **outvalue);
extern VTCERR2  vtc_query_row(int pvci, int columnIndex, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_send_message(int pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_if_unique(int pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_row1(int pvci, char *columnNames, char *messages, char *delimiter,  unsigned char sendflag, unsigned short *outUpdates);
extern VTCERR2  vtc_update_message(int pvci, char *column, int index , char *message, unsigned short *outRc);
extern VTCERR2  vtc_update_message_ifequals(int pvci, char	*columnName, int index,	char *message, char	*ifmessage,	unsigned short 	*outRc);
extern VTCERR2  vtc_update_row1(int pvci, char *columnNames, int index , char *messages, char *delimiter, unsigned short *outUpdates);
extern VTCERR2  vtc_retrieve_message(int pvci, char *column, char **outvalue);
extern VTCERR2  vtc_retrieve_messages1(int pvci, char *columnNames, char *delimiter, char ***outvalues);
extern VTCERR2  vtc_retrieve_row(int pvci, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_increment(int pvci, char *column, int index , int incrValue, int *outValue);
extern VTCERR2  vtc_clear_message(int pvci, char *column, int index , unsigned short *outRc);
extern VTCERR2  vtc_clear_column(int pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_clear_row(int pvci, int rowIndex, unsigned short *outRc);

extern VTCERR2  vtc_create_column(int pvci, char *column,unsigned short *outRc);
extern VTCERR2  vtc_column_size(int pvci, char *column, int *size);
extern VTCERR2  vtc_ensure_index(int pvci, char *column, unsigned short *outRc);
extern VTCERR2  vtc_drop_index(int pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_noop(int pvci);

 
extern void vtc_free(char *msg);
extern void vtc_free_list(char **msglist);

 


 




 




















 




 
 
 

extern VTCERR2  lrvtc_connect(char *servername, int portnum, int options);
extern VTCERR2  lrvtc_disconnect();
extern VTCERR2  lrvtc_query_column(char *columnName, int columnIndex);
extern VTCERR2  lrvtc_query_row(int columnIndex);
extern VTCERR2  lrvtc_send_message(char *columnName, char *message);
extern VTCERR2  lrvtc_send_if_unique(char *columnName, char *message);
extern VTCERR2  lrvtc_send_row1(char *columnNames, char *messages, char *delimiter,  unsigned char sendflag);
extern VTCERR2  lrvtc_update_message(char *columnName, int index , char *message);
extern VTCERR2  lrvtc_update_message_ifequals(char *columnName, int index, char 	*message, char *ifmessage);
extern VTCERR2  lrvtc_update_row1(char *columnNames, int index , char *messages, char *delimiter);
extern VTCERR2  lrvtc_retrieve_message(char *columnName);
extern VTCERR2  lrvtc_retrieve_messages1(char *columnNames, char *delimiter);
extern VTCERR2  lrvtc_retrieve_row();
extern int     lrvtc_increment(char *columnName, int index , int incrValue);
extern VTCERR2  lrvtc_clear_message(char *columnName, int index);
extern VTCERR2  lrvtc_clear_column(char *columnName);
extern VTCERR2  lrvtc_clear_row(int rowIndex);
extern VTCERR2  lrvtc_create_column(char *columnName);
extern int     lrvtc_column_size(char *columnName);
extern VTCERR2  lrvtc_ensure_index(char *columnName);
extern VTCERR2  lrvtc_drop_index(char *columnName);

extern VTCERR2  lrvtc_noop();

 
 
 

                               


 
 
 





















# 2 "c:\\it\\scripts\\tcp_5_ecom_us_return_order\\\\combined_TCP_5_ECOM_US_Return_Order.c" 2

# 1 "globals.h" 1



 
 

# 1 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/web_api.h" 1







# 1 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h" 1























































 




 








 
 
 

  int
	web_add_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_add_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
	
  int
	web_add_auto_header(
		const char *		mpszHeader,
		const char *		mpszValue);

  int
	web_add_header(
		const char *		mpszHeader,
		const char *		mpszValue);
  int
	web_add_cookie(
		const char *		mpszCookie);
  int
	web_cleanup_auto_headers(void);
  int
	web_cleanup_cookies(void);
  int
	web_concurrent_end(
		const char * const	mpszReserved,
										 
		...								 
	);
  int
	web_concurrent_start(
		const char * const	mpszConcurrentGroupName,
										 
										 
		...								 
										 
	);
  int
	web_create_html_param(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim);
  int
	web_create_html_param_ex(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim,
		const char *		mpszNum);
  int
	web_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_disable_keep_alive(void);
  int
	web_enable_keep_alive(void);
  int
	web_find(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_get_int_property(
		const int			miHttpInfoType);
  int
	web_image(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_image_check(
		const char *		mpszName,
		...);
  int
	web_java_check(
		const char *		mpszName,
		...);
  int
	web_link(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

	
  int
	web_global_verification(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
  int
	web_reg_find(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
				
  int
	web_reg_save_param(
		const char *		mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 

  int
	web_convert_param(
		const char * 		mpszParamName, 
										 
		...);							 
										 
										 


										 

										 
  int
	web_remove_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
				
  int
	web_remove_auto_header(
		const char *		mpszHeaderName,
		...);							 
										 



  int
	web_remove_cookie(
		const char *		mpszCookie);

  int
	web_save_header(
		const char *		mpszType,	 
		const char *		mpszName);	 
  int
	web_set_certificate(
		const char *		mpszIndex);
  int
	web_set_certificate_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_set_connections_limit(
		const char *		mpszLimit);
  int
	web_set_max_html_param_len(
		const char *		mpszLen);
  int
	web_set_max_retries(
		const char *		mpszMaxRetries);
  int
	web_set_proxy(
		const char *		mpszProxyHost);
  int
	web_set_pac(
		const char *		mpszPacUrl);
  int
	web_set_proxy_bypass(
		const char *		mpszBypass);
  int
	web_set_secure_proxy(
		const char *		mpszProxyHost);
  int
	web_set_sockets_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue
	);
  int
	web_set_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue,
		...								 
	);
  int
	web_set_timeout(
		const char *		mpszWhat,
		const char *		mpszTimeout);
  int
	web_set_user(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);

  int
	web_sjis_to_euc_param(
		const char *		mpszParamName,
										 
		const char *		mpszParamValSjis);
										 

  int
	web_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_submit_form(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	spdy_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_set_proxy_bypass_local(
		const char * mpszNoLocal
		);

  int 
	web_cache_cleanup(void);

  int
	web_create_html_query(
		const char* mpszStartQuery,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_create_radio_button_param(
		const char *NameFiled,
		const char *NameAndVal,
		const char *ParamName
		);

  int
	web_convert_from_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										
  int
	web_convert_to_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_ex(
		const char * mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_xpath(
		const char * mpszParamName,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_regexp(
		 const char * mpszParamName,
		 ...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_run(
		const char * mpszCode,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_reset(void);










# 716 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h"


# 729 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h"



























# 767 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h"

 
 
 


  int
	FormSubmit(
		const char *		mpszFormName,
		...);
  int
	InitWebVuser(void);
  int
	SetUser(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);
  int
	TerminateWebVuser(void);
  int
	URL(
		const char *		mpszUrlName);
























# 835 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h"



 
 
 






# 9 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/web_api.h" 2

















 







 














  int
	web_reg_add_cookie(
		const char *		mpszCookie,
		...);							 
										 

  int
	web_report_data_point(
		const char *		mpszEventType,
		const char *		mpszEventName,
		const char *		mpszDataPointName,
		const char *		mpszLAST);	 
										 
										 
										 

  int
	web_text_link(
		const char *		mpszStepName,
		...);

  int
	web_element(
		const char *		mpszStepName,
		...);

  int
	web_image_link(
		const char *		mpszStepName,
		...);

  int
	web_static_image(
		const char *		mpszStepName,
		...);

  int
	web_image_submit(
		const char *		mpszStepName,
		...);

  int
	web_button(
		const char *		mpszStepName,
		...);

  int
	web_edit_field(
		const char *		mpszStepName,
		...);

  int
	web_radio_group(
		const char *		mpszStepName,
		...);

  int
	web_check_box(
		const char *		mpszStepName,
		...);

  int
	web_list(
		const char *		mpszStepName,
		...);

  int
	web_text_area(
		const char *		mpszStepName,
		...);

  int
	web_map_area(
		const char *		mpszStepName,
		...);

  int
	web_eval_java_script(
		const char *		mpszStepName,
		...);

  int
	web_reg_dialog(
		const char *		mpszArg1,
		...);

  int
	web_reg_cross_step_download(
		const char *		mpszArg1,
		...);

  int
	web_browser(
		const char *		mpszStepName,
		...);

  int
	web_control(
		const char *		mpszStepName,
		...);

  int
	web_set_rts_key(
		const char *		mpszArg1,
		...);

  int
	web_save_param_length(
		const char * 		mpszParamName,
		...);

  int
	web_save_timestamp_param(
		const char * 		mpszParamName,
		...);

  int
	web_load_cache(
		const char *		mpszStepName,
		...);							 
										 

  int
	web_dump_cache(
		const char *		mpszStepName,
		...);							 
										 
										 

  int
	web_reg_find_in_log(
		const char *		mpszArg1,
		...);							 
										 
										 

  int
	web_get_sockets_info(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 

  int
	web_add_cookie_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

  int
	web_hook_java_script(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

 
 
 
 
 
 
 
 
 
 
 
 
  int
	web_reg_async_attributes(
		const char *		mpszArg,
		...
	);

 
 
 
 
 
 
  int
	web_sync(
		 const char *		mpszArg1,
		 ...
	);

 
 
 
 
  int
	web_stop_async(
		const char *		mpszArg1,
		...
	);

 
 
 
 
 

 
 
 

typedef enum WEB_ASYNC_CB_RC_ENUM_T
{
	WEB_ASYNC_CB_RC_OK,				 

	WEB_ASYNC_CB_RC_ABORT_ASYNC_NOT_ERROR,
	WEB_ASYNC_CB_RC_ABORT_ASYNC_ERROR,
										 
										 
										 
										 
	WEB_ASYNC_CB_RC_ENUM_COUNT
} WEB_ASYNC_CB_RC_ENUM;

 
 
 

typedef enum WEB_CONVERS_CB_CALL_REASON_ENUM_T
{
	WEB_CONVERS_CB_CALL_REASON_BUFFER_RECEIVED,
	WEB_CONVERS_CB_CALL_REASON_END_OF_TASK,

	WEB_CONVERS_CB_CALL_REASON_ENUM_COUNT
} WEB_CONVERS_CB_CALL_REASON_ENUM;

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

typedef
int														 
	(*RequestCB_t)();

typedef
int														 
	(*ResponseBodyBufferCB_t)(
		  const char *		aLastBufferStr,
		  int				aLastBufferLen,
		  const char *		aAccumulatedStr,
		  int				aAccumulatedLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseCB_t)(
		  const char *		aResponseHeadersStr,
		  int				aResponseHeadersLen,
		  const char *		aResponseBodyStr,
		  int				aResponseBodyLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseHeadersCB_t)(
		  int				aHttpStatusCode,
		  const char *		aAccumulatedHeadersStr,
		  int				aAccumulatedHeadersLen);



 
 
 

typedef enum WEB_CONVERS_UTIL_RC_ENUM_T
{
	WEB_CONVERS_UTIL_RC_OK,
	WEB_CONVERS_UTIL_RC_CONVERS_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_TASK_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_UNAVIALABLE,
	WEB_CONVERS_UTIL_RC_INVALID_ARGUMENT,

	WEB_CONVERS_UTIL_RC_ENUM_COUNT
} WEB_CONVERS_UTIL_RC_ENUM;

 
 
 

  int					 
	web_util_set_request_url(
		  const char *		aUrlStr);

  int					 
	web_util_set_request_body(
		  const char *		aRequestBodyStr);

  int					 
	web_util_set_formatted_request_body(
		  const char *		aRequestBodyStr);


 
 
 
 
 

 
 
 
 
 

 
 
 
 
 
 
 
 

 
 
  int
web_websocket_connect(
		 const char *	mpszArg1,
		 ...
		 );


 
 
 
 
 																						
  int
web_websocket_send(
	   const char *		mpszArg1,
		...
	   );

 
 
 
 
 
 
  int
web_websocket_close(
		const char *	mpszArg1,
		...
		);

 
typedef
void														
(*OnOpen_t)(
			  const char* connectionID,  
			  const char * responseHeader,  
			  int length  
);

typedef
void														
(*OnMessage_t)(
	  const char* connectionID,  
	  int isbinary,  
	  const char * data,  
	  int length  
	);

typedef
void														
(*OnError_t)(
	  const char* connectionID,  
	  const char * message,  
	  int length  
	);

typedef
void														
(*OnClose_t)(
	  const char* connectionID,  
	  int isClosedByClient,  
	  int code,  
	  const char* reason,  
	  int length  
	 );
 
 
 
 
 





# 7 "globals.h" 2

# 1 "lrw_custom_body.h" 1
 




# 8 "globals.h" 2


 
 


# 3 "c:\\it\\scripts\\tcp_5_ecom_us_return_order\\\\combined_TCP_5_ECOM_US_Return_Order.c" 2

# 1 "vuser_init.c" 1
 





 
char *filename = "c:\\Return_Numbers.txt";
long file_stream;

vuser_init()
{
	web_cache_cleanup();
	
	web_cleanup_cookies();
	
	web_set_max_html_param_len("9999");
	
	 web_set_sockets_option("SSL_VERSION", "TLS");
	
	web_set_sockets_option("IGNORE_PREMATURE_SHUTDOWN", "1");
	
	web_set_sockets_option("CLOSE_KEEPALIVE_CONNECTIONS", "1");
	

	 
 






		
		
	web_reg_save_param("c_SAML_Request",
	                   "LB=name=\"SAMLRequest\" value=\"",
	                   "RB=\"/>",
	                   "ORD=1",
	                   "Search=ALL","LAST");
		
	lr_save_string("TCP_05_ECOM_US_Return_Order","sTestCaseName");
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_01_Launch"));
	
	 

	web_url("index.html", 
		"URL={pURL}/manh/index.html", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTML", 
		"EXTRARES", 
		"Url=../favicon.ico", "Referer=", "ENDITEM", 
		"LAST");

	web_set_sockets_option("SSL_VERSION", "TLS");
	
	web_submit_data("SSO",
		"Action={pURL_SSO}/profile/SAML2/POST/SSO",
		"Method=POST",
		"TargetFrame=",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t2.inf",
		"Mode=HTML",
		"ITEMDATA",
		"Name=SAMLRequest", "Value={c_SAML_Request}", "ENDITEM",
		"LAST");
	
	lr_end_transaction(lr_eval_string("{sTestCaseName}_01_Launch"),2);
	
	lr_think_time(1);

	 
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_02_Login"));
	
	 
	 






	 




	
	web_submit_data("j_spring_security_check", 
		"Action={pURL_SSO}/j_spring_security_check", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"Mode=HTML", 
		"EncodeAtSign=YES", 
		"ITEMDATA", 
		"Name=j_username", "Value={pUserName}", "ENDITEM", 
		"Name=j_password", "Value={pPassword}", "ENDITEM",
	 
# 308 "vuser_init.c"
		"LAST");

	web_url("ping.jsp", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466847796870", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t4.inf", 
		"Mode=HTML", 
		"LAST");

	 

	lr_end_transaction(lr_eval_string("{sTestCaseName}_02_Login"),2);

	lr_think_time(1);

	return 0;
}
# 4 "c:\\it\\scripts\\tcp_5_ecom_us_return_order\\\\combined_TCP_5_ECOM_US_Return_Order.c" 2

# 1 "Action.c" 1
Action()
{

	web_url("ping.jsp_2", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466847871165", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t5.inf", 
		"Mode=HTML",
		"LAST");

	web_url("ping.jsp_3", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466847886417", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t6.inf", 
		"Mode=HTML", 
		"LAST");

	web_url("ping.jsp_4", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466847901784", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t7.inf", 
		"Mode=HTML", 
		"EXTRARES", 
		"Url=../manh/olm/resources/icons/trigger/search_trigger.png", "Referer={pURL}/manh/olm/resources/css/dom.css", "ENDITEM", 
		"Url=../manh/olm/ds/model/customertransaction/OrderSearchRequest.js?_dc=1466847915089", "Referer={pURL}/manh/index.html", "ENDITEM", 
		"LAST");

	 
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_03_Goto_Return_Tile_Enter_Order_and_click_on_search"));

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"customerOrderSearchCriteria\">"
						"<pair name=\"entityType\" type=\"str\">All</pair>"
						"<pair name=\"orderNumber\" type=\"str\">{pOrder_No}</pair>"
						"<pair name=\"parentOrderNumber\" type=\"str\"></pair>"
						"<pair name=\"createdFromDate\" type=\"str\"></pair>"
						"<pair name=\"createdToDate\" type=\"str\"></pair>"
						"<pair name=\"noOfRecordsPerPage\" type=\"num\">10</pair>"
						"<pair name=\"customerInfo\" type=\"str\"></pair>"
						"<pair name=\"customerBasicInfo\" type=\"str\"></pair>"
						"<object name=\"sortingCriterion\">"
							"<pair name=\"sortField\" type=\"str\">createdDTTM</pair>"
							"<pair name=\"sortDirection\" type=\"str\">DESC</pair>"
						"</object>"
						"<pair name=\"currentPageNumber\" type=\"num\">0</pair>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY", 
		"LAST");
	
	
	 
	
				web_reg_save_param("c_ExternalCustomerId",
	                   "LB=\"externalCustomerId\":",
	                   "RB=,",
	                   "ORD=1",
	                   "Search=ALL","LAST"); 

	
	 
	
				web_reg_save_param("c_CustomerEmail",
	                   "LB=\"customerEmail\":\"",
	                   "RB=\"",
	                   "ORD=1",
	                   "Search=ALL","LAST");
	
	 
	
				web_reg_save_param("c_CustomerFirstName",
	                   "LB=\"customerFirstName\":\"",
	                   "RB=\",",
	                   "ORD=1",
	                   "Search=ALL","LAST");
	
	 
	
				web_reg_save_param("c_CustomerLastName",
	                   "LB=\"customerLastName\":\"",
	                   "RB=\",",
	                   "ORD=1",
	                   "Search=ALL","LAST");
	
		 
	
		web_reg_save_param("c_CustomerPhone",
	                   "LB=\"customerPhone\":\"",
	                   "RB=\",",
	                   "ORD=1",
	                   "Search=ALL","LAST"); 
		
		 
	
		 			




			
			
		 
	
		web_reg_save_param("c_Amount",
	                   "LB=\"grandTotal\":",
	                   "RB=,",
	                   "ORD=1",
	                   "Search=ALL","LAST");
				
			
		 
	
		web_reg_save_param("c_OrderStatus",
                   "LB=\"orderStatus\":\"",
                   "RB=\",",
                   "ORD=1",
                   "Search=ALL","LAST"); 
	
	web_set_user("{pUserName}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");
		
	web_custom_request("customerOrderAndTransactionList", 
		"URL={pURL}/services/olm/customerorder/customerOrderAndTransactionList?_dc=1466847915521&page=1&start=0&limit=50&sort=%5B%7B%22property%22%3A%22orderCreatedDate%22%2C%22direction%22%3A%22DESC%22%7D%5D", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t8.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY}", 
		"LAST");

	lr_end_transaction(lr_eval_string("{sTestCaseName}_03_Goto_Return_Tile_Enter_Order_and_click_on_search"),2);

	lr_think_time(1);

	web_url("ping.jsp_5", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466847919480", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t9.inf", 
		"Mode=HTML", 
		"LAST");

	 
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_04_Open_Searched_Order"));

	web_url("ping.jsp_6", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466847934753", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t10.inf", 
		"Mode=HTML", 
	 
# 250 "Action.c"
		
		"LAST");
	
	web_custom_request("windows", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14271272&regionId=-1&businessUnitId=-1", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t11.inf", 
		"Mode=HTML", 
		"EncType=", 
	 
# 493 "Action.c"
		
		"LAST"); 
	
	 
	
		web_reg_save_param("c_ItemID",
	                   "LB=\"itemId\":",
	                   "RB=,",
	                   "ORD={pItemID}",
	                   "Search=ALL","LAST");
	
	 
	
		web_reg_save_param("c_CustomerOrderlineID",
	                   "LB=\"customerOrderlineId\":",
	                   "RB=,",
	                   "ORD={pItemID}",
	                   "Search=ALL","LAST");
	

	web_url("ping.jsp_7", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466847990722", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t12.inf", 
		"Mode=HTML", 
		"EXTRARES", 
		"Url=../manh/olm/common/components/StoreLoadMask.js?_dc=1466847997130", "Referer={pURL}/manh/index.html", "ENDITEM", 
		"Url=../services/olm/systemProperties/getKey?_dc=1466847996960&keyName=BING_MAP_KEY", "Referer={pURL}/manh/index.html", "ENDITEM", 
		"Url=../manh/olm/resources/icons/ds/customer/email.png", "Referer={pURL}/manh/index.html", "ENDITEM", 
		"Url=../manh/olm/resources/icons/ds/customer/customer_id.png", "Referer={pURL}/manh/index.html", "ENDITEM", 
		"Url=../services/olm/returns/itemsinorder?_dc=1466847997482&jsonFileName=COItemList&customerOrderNumber={pOrder_No}&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", "ENDITEM", 
		"Url=../manh/olm/resources/icons/ds/search/customer.png", "Referer={pURL}/manh/index.html", "ENDITEM", 
		"Url=../manh/olm/resources/icons/ds/search/order.png", "Referer={pURL}/manh/index.html", "ENDITEM", 
		"Url=../services/olm/customer/customerDetailsByOrder?_dc=1466847997561&orderNumber={pOrder_No}&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", "ENDITEM", 
		"Url=../services/olm/location/countrylist?_dc=1466847997845&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", "ENDITEM",  
		"LAST");

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"companyParameterIdList\">"
						"<array name=\"parameterList\">"
							"<item type=\"str\">CURRENCY_USED</item>"
							"<item type=\"str\">EXT_CUSTOMER_ID_GENERATION</item>"
							"<item type=\"str\">USE_DELIVERY_ZONE</item>"
							"<item type=\"str\">DEFAULT_SHIP_VIA</item>"
							"<item type=\"str\">DEFAULT_ORDER_TYPE</item>"
							"<item type=\"str\">REQUIRES_VALIDATION_FOR_PAYMENT_AUTHORIZATION</item>"
							"<item type=\"str\">REQUIRES_VALIDATION_FOR_PAYMENT_SETTLEMENT</item>"
							"<item type=\"str\">CHARGE_HANDLING_STRATEGY</item>"
							"<item type=\"str\">AUTOMATIC_PROMOTION_VALUE</item>"
							"<item type=\"str\">ITEM_PRICE_EXPIRY_DATE_EXTENSION_DAYS</item>"
							"<item type=\"str\">ITEM_PRICE_SCHEDULER_INTERVAL_IN_DAYS</item>"
							"<item type=\"str\">DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE</item>"
							"<item type=\"str\">VIEW_STORE_LEVEL_PRICES</item>"
							"<item type=\"str\">NETWORK_LEVEL_VIEW</item>"
							"<item type=\"str\">FACILITY_LEVEL_VIEW</item>"
						"</array>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_1",		
		"LAST");
	
	web_custom_request("companyParameterList", 
		"URL={pURL}/services/olm/basedata/companyParameter/companyParameterList?_dc=1466847997847&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t13.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_1}", 
		"LAST");

	lr_end_transaction(lr_eval_string("{sTestCaseName}_04_Open_Searched_Order"),2);

	lr_think_time(1);

	web_url("ping.jsp_8", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466848012527", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t14.inf", 
		"Mode=HTML", 
		"LAST");

	 
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_05_Select_Item_to_be_returned_and_click_on_Done"));

	web_url("ping.jsp_9", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466848027829", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t15.inf", 
		"Mode=HTML", 
		"EXTRARES", 
		"Url=../manh/olm/common/model/EventModel.js?_dc=1466848028635", "Referer={pURL}/manh/index.html", "ENDITEM", 
		"LAST");

	web_convert_from_formatted("FormattedData=<HP_EXTENSION name=\"JsonXml\"><object><object name=\"ReturnOrder\"><pair name=\"orderNumber\" type=\"str\"></pair><pair name=\"orderId\" type=\"str\"></pair><pair name=\"orderType\" type=\"str\"></pair><pair name=\"tcCompanyId\" type=\"str\"></pair><pair name=\"customerOrderId\" type=\"str\"></pair><pair name=\"customerOrderNumber\" type=\"str\">{pOrder_No}</pair><pair name=\"orderStatus\" type=\"str\"></pair><pair name=\"orderStatusId\" type=\"str\"></pair><pair name=\"orderCapturedDate\" type=\"str\"></pair><pair name=\"orderCreatedDate\" type=\"str\"></pair><pair name=\"reponseStatus\" type=\"str\"></pair><pair name=\"orderConfirmed\" type=\"str\"></pair><pair name=\"orderCancelled\" type=\"str\"></pair><pair name=\"noOfPackages\" type=\"num\">0</pair><pair name=\"emailLabelsTo\" type=\"str\"></pair><pair name=\"returnCenter\" type=\"str\"></pair><pair name=\"shipFromAddress\" type=\"str\"></pair><pair name=\"shippingAddress\" type=\"str\"></pair><pair name=\"paymentDetail\" type=\"str\"></pair><pair name=\"orderTotals\" type=\"str\"></pair><pair name=\"isCharge\" type=\"str\"></pair><pair name=\"returnReferenceNumber\" type=\"str\"></pair><pair name=\"onHold\" type=\"str\"></pair><pair name=\"reasonCode\" type=\"str\"></pair><pair name=\"reasonCodeDescription\" type=\"str\"></pair><pair name=\"automatedReturn\" type=\"str\"></pair><object name=\"returnOrderLines\"><array name=\"returnOrderLine\"><object><pair name=\"orderLineId\" type=\"str\"></pair><pair name=\"orderLineNumber\" type=\"str\"></pair><pair name=\"itemId\" type=\"num\">{c_ItemID}</pair>"  
	                           "<pair name=\"itemSize\" type=\"str\"></pair><pair name=\"itemColor\" type=\"str\"></pair><pair name=\"itemStyle\" type=\"str\"></pair><pair name=\"imageURL\" type=\"str\"></pair><pair name=\"webURL\" type=\"str\"></pair><pair name=\"requestedQuantity\" type=\"str\"></pair><pair name=\"orderedQtyUOM\" type=\"str\"></pair><pair name=\"unitPriceAmount\" type=\"str\"></pair><pair name=\"itemDescription\" type=\"str\"></pair><pair name=\"orderLineStatus\" type=\"str\"></pair><pair name=\"receivingCondition\" type=\"str\"></pair><pair name=\"customerOrderlineId\" type=\"num\">{c_CustomerOrderlineID}</pair>"  
	                           "<pair name=\"receiptExpected\" type=\"str\"></pair><pair name=\"reasonCodes\" type=\"str\"></pair><pair name=\"lineTotal\" type=\"str\"></pair><pair name=\"formattedLineTotal\" type=\"str\"></pair><pair name=\"priceAmount\" type=\"str\"></pair><pair name=\"isReturnable\" type=\"str\"></pair><pair name=\"isExchangeable\" type=\"str\"></pair><pair name=\"returnQuantity\" type=\"num\">1</pair><pair name=\"invoicedQuantity\" type=\"str\"></pair><pair name=\"remainingReturnableQuantity\" type=\"str\"></pair><pair name=\"returnReason\" type=\"str\"></pair><pair name=\"returnAction\" type=\"str\">Return</pair><pair name=\"cancelled\" type=\"str\"></pair><pair name=\"priceDetails\" type=\"str\"></pair><pair name=\"formattedTotalLineChargeAmount\" type=\"str\"></pair><pair name=\"chargeDetails\" type=\"str\"></pair><object name=\"notes\"><array name=\"note\"></array></object><pair name=\"receivedQuantity\" type=\"str\"></pair><pair name=\"receiptStatus\" type=\"str\"></pair><pair name=\"onHold\" type=\"str\"></pair><pair name=\"holdReasonCode\" type=\"str\"></pair><pair name=\"customFields\" type=\"str\"></pair><pair name=\"localizedOrderlineStatus\" type=\"str\"></pair></object></array></object><object name=\"paymentDetails\"><array name=\"paymentDetail\"></array></object><object name=\"notes\"><array name=\"note\"></array></object><pair name=\"returnASN\" type=\"str\"></pair><pair name=\"invoiceDetails\" type=\"str\"></pair><pair name=\"processReturn\" type=\"str\"></pair><pair name=\"customFields\" type=\"str\"></pair><pair name=\"customerFullName\" type=\"str\"></pair><pair name=\"customerPhone\" type=\"str\"></pair></object></object></HP_EXTENSION>",
		"TargetParam=DFE_BODY_2", 
		"LAST");
	
		
	 
	
	web_reg_save_param("c_ReturnOrder",
	                   "LB=\"orderNumber\":\"",
	                   "RB=\",",
	                   "ORD=1",
	                   "Search=ALL","LAST");
	
	lr_eval_string("{pTimestamp}");
	
	 
	
	web_reg_save_param("c_OrderId",
	                   "LB=\"orderId\":",
	                   "RB=,",
	                   "ORD=1",
	                   "Search=ALL","LAST");
	
	 
	
	web_reg_save_param("c_CustomerOrderId",
	                   "LB=\"customerOrderId\":",
	                   "RB=,",
	                   "ORD=1",
	                   "Search=ALL","LAST");

	
	 
	
		web_reg_save_param("c_AdressLine1",
	                   "LB=\"addressLine1\":\"",
	                   "RB=\",",
	                   "ORD=1",
	                   "Search=ALL","LAST");
	
	 
	
		web_reg_save_param("c_City",
	                   "LB=\"city\":\"",
	                   "RB=\",",
	                   "ORD=1",
	                   "Search=ALL","LAST");
	
	 
	
		web_reg_save_param("c_State",
	                   "LB=\"stateProv\":\"",
	                   "RB=\",",
	                   "ORD=1",
	                   "Search=ALL","LAST");	
	
	 
	
		web_reg_save_param("c_PostalCode",
	                   "LB=\"postalCode\":",
	                   "RB=,",
	                   "ORD=1",
	                   "Search=ALL","LAST");
	
	 
	
		web_reg_save_param("c_County",
	                   "LB=\"county\":\"",
	                   "RB=\",",
	                   "ORD=1",
	                   "Search=ALL","LAST"); 
	
	 
	
		web_reg_save_param("c_orderLineId",
	                   "LB=\"orderLineId\":",
	                   "RB=,",
	                   "ORD=1",
	                   "Search=ALL","LAST");
	
	web_custom_request("createOrUpdate", 
		"URL={pURL}/services/olm/returns/createOrUpdate?_dc=1466848028628&jsonFileName=createOrUpdate", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t16.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_2}",
		"EXTRARES", 
		"Url=../syscodes/syscodes?_dc=1466848031436&syscode=B032&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", "ENDITEM", 
		"Url=/manh/olm/resources/icons/common/delete_item.png", "Referer={pURL}/manh/olm/resources/css/dom.css", "ENDITEM", 
		"Url=../syscodes/syscodes?_dc=1466848031442&syscode=B028&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", "ENDITEM", 
		"Url=/manh/olm/resources/icons/collapse/expandArrow.png", "Referer={pURL}/manh/olm/resources/css/dom.css", "ENDITEM", 
		"Url=itemsinorder?_dc=1466848031657&jsonFileName=COItemList&customerOrderNumber={pOrder_No}&page=1&start=0&limit=25", "Referer={pURL}/manh/index.html", "ENDITEM",
		"LAST");

	lr_end_transaction(lr_eval_string("{sTestCaseName}_05_Select_Item_to_be_returned_and_click_on_Done"),2);

	lr_think_time(1);
	
	 
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_06_Click_on_button_Proceed_to_review"));

	web_url("ping.jsp_10", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466848044141", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t17.inf", 
		"Mode=HTML", 
		"LAST");

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"events\">"
						"<array name=\"event\">"
							"<object>"
								"<pair name=\"transactionId\" type=\"str\">c98fc47a-3c90-402c-800c-a9d078bbc575</pair>"
								"<pair name=\"eventName\" type=\"str\">DONE</pair>"
								"<pair name=\"screenId\" type=\"num\">280010</pair>"
								"<pair name=\"userName\" type=\"str\">{pUserName}</pair>"
								"<pair name=\"logCategory\" type=\"str\">button</pair>"
								"<pair name=\"logLevel\" type=\"num\">4</pair>"
								"<pair name=\"eventTimeStamp\" type=\"str\">{pCurrentDate}</pair>"
								"<pair name=\"additionalInfo\" type=\"str\">Create a return/exchange</pair>"
							"</object>"
						"</array>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_3", 
		"LAST");

	web_custom_request("saveUserActivity", 
		"URL={pURL}/services/olm/log/saveUserActivity?_dc=1466848046273&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t18.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_3}", 
		"LAST");

	web_url("ping.jsp_11", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466848059469", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t19.inf", 
		"Mode=HTML", 
		"LAST");

	web_convert_from_formatted("FormattedData=" 
	                           "<HP_EXTENSION name=\"JsonXml\">" 
	                           "<object><object name=\"ReturnOrder\">" 
	                           "<pair name=\"orderNumber\" type=\"str\">{c_ReturnOrder}</pair>" 
	                           "<pair name=\"orderId\" type=\"num\">{c_OrderId}</pair>" 
	                           "<pair name=\"orderType\" type=\"str\">RLM</pair>" 
	                           "<pair name=\"tcCompanyId\" type=\"num\">1</pair>" 
	                           "<pair name=\"customerOrderId\" type=\"num\">{c_CustomerOrderId}</pair>" 
	                           "<pair name=\"customerOrderNumber\" type=\"num\">{pOrder_No}</pair>" 
	                           "<pair name=\"orderStatus\" type=\"str\">{c_OrderStatus}</pair>" 
	                           "<pair name=\"orderStatusId\" type=\"num\">100</pair>" 
	                           "<pair name=\"orderCapturedDate\" type=\"str\">10/4/15 14:40 EDT</pair>" 
	                           "<pair name=\"orderCreatedDate\" type=\"str\">10/4/15 14:40 EDT</pair>" 
	                           "<pair name=\"reponseStatus\" type=\"str\"></pair>" 
	                           "<pair name=\"orderConfirmed\" type=\"bool\">false</pair>" 
	                           "<pair name=\"orderCancelled\" type=\"bool\">false</pair>" 
	                           "<pair name=\"noOfPackages\" type=\"num\">1</pair>" 
	                           "<pair name=\"emailLabelsTo\" type=\"str\">{c_CustomerEmail}</pair>" 
	                           "<pair name=\"returnCenter\" type=\"str\">SEDC-ECOM</pair>" 
	                           "<object name=\"shipFromAddress\">" 
	                           "<pair name=\"firstName\" type=\"str\">{c_CustomerFirstName}</pair>" 
	                           "<pair name=\"lastName\" type=\"str\">{c_CustomerLastName}</pair>" 
	                           "<pair name=\"addressLine1\" type=\"str\">{c_AdressLine1}</pair>" 
	                           "<pair name=\"city\" type=\"str\">{c_City}</pair>" 
	                           "<pair name=\"stateProv\" type=\"str\">{c_State}</pair>" 
	                           "<pair name=\"postalCode\" type=\"num\">{c_PostalCode}</pair>" 
	                           "<pair name=\"country\" type=\"str\">US</pair>" 
	                           "<pair name=\"county\" type=\"str\">{c_County}</pair>" 
	                           "<pair name=\"phone\" type=\"num\">{c_CustomerPhone}</pair>"  
	                           "<pair name=\"email\" type=\"str\">{c_CustomerEmail}</pair>" 
	                           "</object>" 
	                           "<object name=\"shippingAddress\">" 
	                           "<pair name=\"firstName\" type=\"str\">SEDC-ECOM</pair>" 
	                           "<pair name=\"addressLine1\" type=\"str\">500 Plaza Drive</pair>" 
	                           "<pair name=\"city\" type=\"str\">Secaucus</pair>" 
	                           "<pair name=\"stateProv\" type=\"str\">NJ</pair>" 
	                           "<pair name=\"postalCode\" type=\"str\">07094</pair>" 
	                           "<pair name=\"country\" type=\"str\">US</pair>" 
	                           "<pair name=\"addressVerified\" type=\"bool\">false</pair>" 
	                           "</object>" 
	                           "<pair name=\"paymentDetail\" type=\"str\"></pair>" 
	                           "<object name=\"orderTotals\">" 
	                           "<pair name=\"totalCharges\" type=\"str\">0.00</pair>" 
	                           "<pair name=\"formattedTotalCharges\" type=\"str\">$0.00</pair>" 
	                           "<pair name=\"totalDiscounts\" type=\"str\">0.00</pair>" 
	                           "<pair name=\"formattedTotalDiscounts\" type=\"str\">$0.00</pair>" 
	                           "<pair name=\"totalTaxes\" type=\"str\">0.00</pair>" 
	                           "<pair name=\"formattedTotalTaxes\" type=\"str\">$0.00</pair>" 
	                           "<pair name=\"itemSubTotal\" type=\"num\">{c_Amount}</pair>" 
	                           "<pair name=\"formattedItemSubTotal\" type=\"str\">${c_Amount}</pair>" 
	                           "<pair name=\"grandTotal\" type=\"num\">{c_Amount}</pair>" 
	                           "<pair name=\"formattedGrandTotal\" type=\"str\">${c_Amount}</pair>" 
	                           "<pair name=\"remainingToPay\" type=\"num\">{c_Amount}</pair>" 
	                           "<pair name=\"formattedRemainingToPay\" type=\"str\">${c_Amount}</pair>" 
	                           "<pair name=\"amountPaid\" type=\"num\">{c_Amount}</pair>" 
	                           "<pair name=\"formattedAmountPaid\" type=\"str\">${c_Amount}</pair>" 
	                           "<object name=\"totalChargeDetails\"><object name=\"totalChargeDetail\"><pair name=\"chargeCategory\" type=\"str\">Shipping</pair><pair name=\"chargeValue\" type=\"str\">Not Available</pair><pair name=\"formattedChargeValue\" type=\"str\">Not Available</pair><pair name=\"showCurrency\" type=\"bool\">false</pair><pair name=\"overriddenSnHCharge\" type=\"bool\">false</pair><pair name=\"chargesNotAvailable\" type=\"bool\">true</pair><pair name=\"eligibleForSnHOverride\" type=\"bool\">false</pair></object></object><object name=\"totalDiscountDetails\"><pair name=\"totalPromotionsApplied\" type=\"num\">0</pair><pair name=\"totalPromotionAmount\" type=\"num\">0</pair><pair name=\"formattedTotalPromotionAmount\" type=\"str\">$0.00</pair><pair name=\"totalCouponsApplied\" type=\"num\">0</pair><pair name=\"totalCouponAmount\" type=\"num\">0</pair><pair name=\"formattedTotalCouponAmount\" type=\"str\">$0.00</pair><pair name=\"totalAppeasementsApplied\" type=\"num\">0</pair><pair name=\"totalAppeasementAmount\" type=\"num\">0</pair><pair name=\"formattedTotalAppeasementAmount\" type=\"str\">$0.00</pair></object></object><pair name=\"isCharge\" type=\"bool\">false</pair><pair name=\"returnReferenceNumber\" type=\"str\"></pair><pair name=\"onHold\" type=\"bool\">false</pair><pair name=\"reasonCode\" type=\"str\"></pair><pair name=\"reasonCodeDescription\" type=\"str\"></pair><pair name=\"automatedReturn\" type=\"bool\">false</pair><object name=\"chargeGroupDetails\"><array name=\"chargeGroupDetail\"></array></object><object name=\"returnOrderLines\"><array name=\"returnOrderLine\"><object><pair name=\"orderLineId\" type=\"num\">{c_orderLineId}</pair><pair name=\"orderLineNumber\" type=\"num\">1</pair><pair name=\"itemId\" type=\"num\">{c_ItemID}</pair>"  
	                           "<pair name=\"itemSize\" type=\"str\"></pair><pair name=\"itemColor\" type=\"str\"></pair><pair name=\"itemStyle\" type=\"str\"></pair><pair name=\"imageURL\" type=\"str\"></pair><pair name=\"webURL\" type=\"str\"></pair><pair name=\"requestedQuantity\" type=\"str\"></pair><pair name=\"orderedQtyUOM\" type=\"str\"></pair><pair name=\"unitPriceAmount\" type=\"str\"></pair><pair name=\"itemDescription\" type=\"str\"></pair><pair name=\"orderLineStatus\" type=\"str\"></pair><pair name=\"receivingCondition\" type=\"str\">USER Defined - B028</pair><pair name=\"customerOrderlineId\" type=\"num\">{c_CustomerOrderlineID}</pair>"  
	                           "<pair name=\"receiptExpected\" type=\"bool\">true</pair><pair name=\"reasonCodes\" type=\"str\"></pair><pair name=\"lineTotal\" type=\"str\"></pair><pair name=\"formattedLineTotal\" type=\"str\"></pair><pair name=\"priceAmount\" type=\"str\"></pair><pair name=\"isReturnable\" type=\"str\"></pair><pair name=\"isExchangeable\" type=\"str\"></pair><pair name=\"returnQuantity\" type=\"str\">1</pair><pair name=\"invoicedQuantity\" type=\"str\"></pair><pair name=\"remainingReturnableQuantity\" type=\"str\"></pair><pair name=\"returnReason\" type=\"str\">Customer Preference</pair><pair name=\"returnAction\" type=\"str\">Return</pair><pair name=\"cancelled\" type=\"str\"></pair><pair name=\"priceDetails\" type=\"str\"></pair><pair name=\"formattedTotalLineChargeAmount\" type=\"str\"></pair><pair name=\"chargeDetails\" type=\"str\"></pair><object name=\"notes\"><array name=\"note\"></array></object><pair name=\"receivedQuantity\" type=\"str\"></pair><pair name=\"receiptStatus\" type=\"str\"></pair><pair name=\"onHold\" type=\"str\"></pair><pair name=\"holdReasonCode\" type=\"str\"></pair><pair name=\"customFields\" type=\"str\"></pair><pair name=\"localizedOrderlineStatus\" type=\"str\"></pair></object></array></object><object name=\"paymentDetails\"><array name=\"paymentDetail\"></array></object><object name=\"notes\"><array name=\"note\"></array></object><pair name=\"returnASN\" type=\"str\"></pair><pair name=\"invoiceDetails\" type=\"str\"></pair><pair name=\"processReturn\" type=\"str\"></pair><pair name=\"customFields\" type=\"str\"></pair><pair name=\"customerFullName\" type=\"str\">{c_CustomerFirstName} {c_CustomerLastName}</pair><pair name=\"customerPhone\" type=\"num\">{c_CustomerPhone}</pair></object></object></HP_EXTENSION>",
		"TargetParam=DFE_BODY_4", 
		"LAST");

	web_custom_request("createOrUpdate_2", 
		"URL={pURL}/services/olm/returns/createOrUpdate?_dc=1466848063223&jsonFileName=createOrUpdate", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t20.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_4}", 
		"LAST");

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"events\">"
						"<array name=\"event\">"
							"<object>"
								"<pair name=\"transactionId\" type=\"str\">c98fc47a-3c90-402c-800c-a9d078bbc575</pair>"
								"<pair name=\"eventName\" type=\"str\">PROCEED TO REVIEW</pair>"
								"<pair name=\"screenId\" type=\"num\">280010</pair>"
								"<pair name=\"userName\" type=\"str\">{pUserName}</pair>"
								"<pair name=\"logCategory\" type=\"str\">button</pair>"
								"<pair name=\"logLevel\" type=\"num\">4</pair>"
								"<pair name=\"eventTimeStamp\" type=\"str\">{pCurrentDate}</pair>"
								"<pair name=\"additionalInfo\" type=\"str\">Proceed to review for RMA: {c_ReturnOrder}</pair>"
							"</object>"
						"</array>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_5", 
		"LAST");

	web_custom_request("saveUserActivity_2", 
		"URL={pURL}/services/olm/log/saveUserActivity?_dc=1466848066277&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t21.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_5}", 
		"LAST");

	lr_end_transaction(lr_eval_string("{sTestCaseName}_06_Click_on_button_Proceed_to_review"),2);

	lr_think_time(1);

	web_url("ping.jsp_12", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466848076505", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t22.inf", 
		"Mode=HTML", 
		"LAST");

	 
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_07_Click_On_button_Confirm_Return"));

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"events\">"
						"<array name=\"event\">"
							"<object>"
								"<pair name=\"transactionId\" type=\"str\">c98fc47a-3c90-402c-800c-a9d078bbc575</pair>"
								"<pair name=\"eventName\" type=\"str\">CONFIRM RETURN</pair>"
								"<pair name=\"screenId\" type=\"num\">280010</pair>"
								"<pair name=\"userName\" type=\"str\">{pUserName}</pair>"
								"<pair name=\"logCategory\" type=\"str\">button</pair>"
								"<pair name=\"logLevel\" type=\"num\">4</pair>"
								"<pair name=\"eventTimeStamp\" type=\"str\">{pCurrentDate}</pair>"
								"<pair name=\"additionalInfo\" type=\"str\">confirming return order</pair>"
							"</object>"
						"</array>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_6", 
		"LAST");

	web_custom_request("saveUserActivity_3", 
		"URL={pURL}/services/olm/log/saveUserActivity?_dc=1466848087860&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t23.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_6}", 
		"LAST");

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"ReturnOrder\">"
						"<pair name=\"orderNumber\" type=\"str\">{c_ReturnOrder}</pair>"
						"<pair name=\"orderId\" type=\"num\">{c_OrderId}</pair>"
						"<pair name=\"orderType\" type=\"str\">RLM</pair>"
						"<pair name=\"tcCompanyId\" type=\"num\">1</pair>"
						"<pair name=\"customerOrderId\" type=\"num\">{c_CustomerOrderId}</pair>"
						"<pair name=\"customerOrderNumber\" type=\"num\">{pOrder_No}</pair>"
						"<pair name=\"orderStatus\" type=\"str\">{c_OrderStatus}</pair>"
						"<pair name=\"orderStatusId\" type=\"num\">100</pair>"
						"<pair name=\"orderCapturedDate\" type=\"str\">10/4/15 14:40 EDT</pair>"
						"<pair name=\"orderCreatedDate\" type=\"str\">10/4/15 14:40 EDT</pair>"
						"<pair name=\"reponseStatus\" type=\"str\"></pair>"
						"<pair name=\"orderConfirmed\" type=\"bool\">true</pair>"
						"<pair name=\"orderCancelled\" type=\"bool\">false</pair>"
						"<pair name=\"noOfPackages\" type=\"num\">1</pair>"
						"<pair name=\"emailLabelsTo\" type=\"str\">{c_CustomerEmail}</pair>"
						"<pair name=\"returnCenter\" type=\"str\">SEDC-ECOM</pair>"
						"<object name=\"shipFromAddress\">"
							"<pair name=\"firstName\" type=\"str\">{c_CustomerFirstName}</pair>"
							"<pair name=\"lastName\" type=\"str\">{c_CustomerLastName}</pair>"
							"<pair name=\"addressLine1\" type=\"str\">{c_AdressLine1}</pair>"
							"<pair name=\"city\" type=\"str\">{c_City}</pair>"
							"<pair name=\"stateProv\" type=\"str\">{c_State}</pair>"
							"<pair name=\"postalCode\" type=\"num\">{c_PostalCode}</pair>"
							"<pair name=\"country\" type=\"str\">US</pair>"
							"<pair name=\"county\" type=\"str\">{c_County}</pair>"
							"<pair name=\"phone\" type=\"num\">{c_CustomerPhone}</pair>"
							"<pair name=\"email\" type=\"str\">{c_CustomerEmail}</pair>"
						"</object>"
						"<object name=\"shippingAddress\">"
							"<pair name=\"firstName\" type=\"str\">SEDC-ECOM</pair>"
							"<pair name=\"addressLine1\" type=\"str\">500 Plaza Drive</pair>"
							"<pair name=\"city\" type=\"str\">Secaucus</pair>"
							"<pair name=\"stateProv\" type=\"str\">NJ</pair>"
							"<pair name=\"postalCode\" type=\"str\">07094</pair>"
							"<pair name=\"country\" type=\"str\">US</pair>"
							"<pair name=\"addressVerified\" type=\"bool\">true</pair>"
						"</object>"
						"<pair name=\"paymentDetail\" type=\"str\"></pair>"
						"<object name=\"orderTotals\">"
							"<pair name=\"totalCharges\" type=\"str\">0.00</pair>"
							"<pair name=\"formattedTotalCharges\" type=\"str\">$0.00</pair>"
							"<pair name=\"totalDiscounts\" type=\"str\">0.00</pair>"
							"<pair name=\"formattedTotalDiscounts\" type=\"str\">$0.00</pair>"
							"<pair name=\"totalTaxes\" type=\"str\">0.00</pair>"
							"<pair name=\"formattedTotalTaxes\" type=\"str\">$0.00</pair>"
							"<pair name=\"itemSubTotal\" type=\"num\">{c_Amount}</pair>"
							"<pair name=\"formattedItemSubTotal\" type=\"str\">${c_Amount}</pair>"
							"<pair name=\"grandTotal\" type=\"num\">{c_Amount}</pair>"
							"<pair name=\"formattedGrandTotal\" type=\"str\">${c_Amount}</pair>"
							"<pair name=\"remainingToPay\" type=\"num\">{c_Amount}</pair>"
							"<pair name=\"formattedRemainingToPay\" type=\"str\">${c_Amount}</pair>"
							"<pair name=\"amountPaid\" type=\"num\">{c_Amount}</pair>"
							"<pair name=\"formattedAmountPaid\" type=\"str\">${c_Amount}</pair>"
							"<object name=\"totalChargeDetails\">"
								"<object name=\"totalChargeDetail\">"
									"<pair name=\"chargeCategory\" type=\"str\">Shipping</pair>"
									"<pair name=\"chargeValue\" type=\"str\">Not Available</pair>"
									"<pair name=\"formattedChargeValue\" type=\"str\">Not Available</pair>"
									"<pair name=\"showCurrency\" type=\"bool\">false</pair>"
									"<pair name=\"overriddenSnHCharge\" type=\"bool\">false</pair>"
									"<pair name=\"chargesNotAvailable\" type=\"bool\">true</pair>"
									"<pair name=\"eligibleForSnHOverride\" type=\"bool\">false</pair>"
								"</object>"
							"</object>"
							"<object name=\"totalDiscountDetails\">"
								"<pair name=\"totalPromotionsApplied\" type=\"num\">0</pair>"
								"<pair name=\"totalPromotionAmount\" type=\"num\">0</pair>"
								"<pair name=\"formattedTotalPromotionAmount\" type=\"str\">$0.00</pair>"
								"<pair name=\"totalCouponsApplied\" type=\"num\">0</pair>"
								"<pair name=\"totalCouponAmount\" type=\"num\">0</pair>"
								"<pair name=\"formattedTotalCouponAmount\" type=\"str\">$0.00</pair>"
								"<pair name=\"totalAppeasementsApplied\" type=\"num\">0</pair>"
								"<pair name=\"totalAppeasementAmount\" type=\"num\">0</pair>"
								"<pair name=\"formattedTotalAppeasementAmount\" type=\"str\">$0.00</pair>"
							"</object>"
						"</object>"
						"<pair name=\"isCharge\" type=\"bool\">false</pair>"
						"<pair name=\"returnReferenceNumber\" type=\"str\"></pair>"
						"<pair name=\"onHold\" type=\"bool\">false</pair>"
						"<pair name=\"reasonCode\" type=\"str\"></pair>"
						"<pair name=\"reasonCodeDescription\" type=\"str\"></pair>"
						"<pair name=\"automatedReturn\" type=\"bool\">false</pair>"
						"<object name=\"chargeGroupDetails\">"
							"<array name=\"chargeGroupDetail\"></array>"
						"</object>"
						"<object name=\"returnOrderLines\">"
							"<array name=\"returnOrderLine\"></array>"
						"</object>"
						"<object name=\"paymentDetails\">"
							"<array name=\"paymentDetail\"></array>"
						"</object>"
						"<object name=\"notes\">"
							"<array name=\"note\"></array>"
						"</object>"
						"<pair name=\"returnASN\" type=\"str\"></pair>"
						"<pair name=\"invoiceDetails\" type=\"str\"></pair>"
						"<pair name=\"processReturn\" type=\"str\"></pair>"
						"<pair name=\"customFields\" type=\"str\"></pair>"
						"<pair name=\"customerFullName\" type=\"str\">{c_CustomerFirstName} {c_CustomerLastName}</pair>"
						"<pair name=\"customerPhone\" type=\"num\">{c_CustomerPhone}</pair>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_7", 
		"LAST");

	web_custom_request("createOrUpdate_3", 
		"URL={pURL}/services/olm/returns/createOrUpdate?_dc=1466848087753&jsonFileName=createOrUpdate", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t24.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_7}", 
		"LAST");

	web_url("ping.jsp_13", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466848092048", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t25.inf", 
		"Mode=HTML", 
		"LAST");

	lr_end_transaction(lr_eval_string("{sTestCaseName}_07_Click_On_button_Confirm_Return"),2);

	lr_think_time(1);

	 
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_08_Capture_Order_Number_and_close_the_popup_message"));

	web_url("ping.jsp_14", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466848109088", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t26.inf", 
		"Mode=HTML", 
		"LAST");

	web_custom_request("windows_2", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14271272", 
		"Method=DELETE", 
		"TargetFrame=", 
		"Resource=0", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t27.inf", 
		"Mode=HTML", 
		"LAST");

	web_custom_request("windows_3", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14271272", 
		"Method=DELETE", 
		"TargetFrame=", 
		"Resource=0", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t28.inf", 
		"Mode=HTML", 
		"LAST");

	web_custom_request("windows_4", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-14272587&regionId=-1&businessUnitId=-1", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t29.inf", 
		"Mode=HTML", 
		"EncType=", 
		"LAST");

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"returnOrderSearchCriteria\">"
						"<pair name=\"entityType\" type=\"str\"></pair>"
						"<pair name=\"orderNumber\" type=\"str\"></pair>"
						"<pair name=\"parentOrderNumber\" type=\"num\">{pOrder_No}</pair>"
						"<pair name=\"createdFromDate\" type=\"str\"></pair>"
						"<pair name=\"createdToDate\" type=\"str\"></pair>"
						"<pair name=\"noOfRecordsPerPage\" type=\"num\">1000</pair>"
						"<object name=\"customerInfo\">"
							"<pair name=\"customerFirstName\" type=\"str\">{c_CustomerFirstName}</pair>"
							"<pair name=\"customerLastName\" type=\"str\">{c_CustomerLastName}</pair>"
							"<pair name=\"customerFullName\" type=\"str\">{c_CustomerFirstName} {c_CustomerLastName}</pair>"
							"<pair name=\"customerEmail\" type=\"str\">{c_CustomerEmail}</pair>"
							"<pair name=\"customerPhone\" type=\"str\">{c_CustomerPhone}</pair>"
							"<pair name=\"customerId\" type=\"str\"></pair>"
							"<pair name=\"externalCustomerId\" type=\"str\">{c_ExternalCustomerId}</pair>" 
							"<pair name=\"customField1\" type=\"str\"></pair>"
							"<pair name=\"customField2\" type=\"str\"></pair>"
							"<pair name=\"customField3\" type=\"str\"></pair>"
							"<pair name=\"customField4\" type=\"str\"></pair>"
							"<pair name=\"customField5\" type=\"str\"></pair>"
							"<pair name=\"customField6\" type=\"str\"></pair>"
							"<pair name=\"customField7\" type=\"str\"></pair>"
							"<pair name=\"customField8\" type=\"str\"></pair>"
							"<pair name=\"customField9\" type=\"str\"></pair>"
							"<pair name=\"customField10\" type=\"str\"></pair>"
						"</object>"
						"<pair name=\"customerBasicInfo\" type=\"str\"></pair>"
						"<object name=\"sortingCriterion\">"
							"<pair name=\"sortField\" type=\"str\">createdDTTM</pair>"
							"<pair name=\"sortDirection\" type=\"str\">DESC</pair>"
						"</object>"
						"<pair name=\"currentPageNumber\" type=\"num\">0</pair>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_8", 
		"LAST");

	web_custom_request("returnList", 
		"URL={pURL}/services/olm/returns/returnList?_dc=1466848176607&page=1&start=0&limit=50&sort=%5B%7B%22property%22%3A%22orderCreatedDate%22%2C%22direction%22%3A%22DESC%22%7D%5D", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t30.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_8}", 
		"LAST");

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<object name=\"customerOrderSearchCriteria\">"
						"<pair name=\"entityType\" type=\"str\">All</pair>"
						"<pair name=\"orderNumber\" type=\"num\">{pOrder_No}</pair>"
						"<pair name=\"parentOrderNumber\" type=\"str\"></pair>"
						"<pair name=\"createdFromDate\" type=\"str\"></pair>"
						"<pair name=\"createdToDate\" type=\"str\"></pair>"
						"<pair name=\"noOfRecordsPerPage\" type=\"num\">1000</pair>"
						"<object name=\"customerInfo\">"
							"<pair name=\"customerFirstName\" type=\"str\">{c_CustomerFirstName}</pair>"
							"<pair name=\"customerLastName\" type=\"str\">{c_CustomerLastName}</pair>"
							"<pair name=\"customerFullName\" type=\"str\">{c_CustomerFirstName} {c_CustomerLastName}</pair>"
							"<pair name=\"customerEmail\" type=\"str\">{c_CustomerEmail}</pair>"
							"<pair name=\"customerPhone\" type=\"str\">{c_CustomerPhone}</pair>"
							"<pair name=\"customerId\" type=\"str\"></pair>"
							"<pair name=\"externalCustomerId\" type=\"str\">{c_ExternalCustomerId}</pair>"
							"<pair name=\"customField1\" type=\"str\"></pair>"
							"<pair name=\"customField2\" type=\"str\"></pair>"
							"<pair name=\"customField3\" type=\"str\"></pair>"
							"<pair name=\"customField4\" type=\"str\"></pair>"
							"<pair name=\"customField5\" type=\"str\"></pair>"
							"<pair name=\"customField6\" type=\"str\"></pair>"
							"<pair name=\"customField7\" type=\"str\"></pair>"
							"<pair name=\"customField8\" type=\"str\"></pair>"
							"<pair name=\"customField9\" type=\"str\"></pair>"
							"<pair name=\"customField10\" type=\"str\"></pair>"
						"</object>"
						"<pair name=\"customerBasicInfo\" type=\"str\"></pair>"
						"<object name=\"sortingCriterion\">"
							"<pair name=\"sortField\" type=\"str\">createdDTTM</pair>"
							"<pair name=\"sortDirection\" type=\"str\">DESC</pair>"
						"</object>"
						"<pair name=\"currentPageNumber\" type=\"num\">0</pair>"
					"</object>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_9", 
		"LAST");

	web_custom_request("customerOrderAndTransactionList_2", 
		"URL={pURL}/services/olm/customerorder/customerOrderAndTransactionList?_dc=1466848176577&page=1&start=0&limit=50&sort=%5B%7B%22property%22%3A%22orderCreatedDate%22%2C%22direction%22%3A%22DESC%22%7D%5D", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t31.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_9}", 
		"LAST");

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<pair name=\"orderId\" type=\"num\">{c_CustomerOrderId}</pair>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_10", 
		"LAST");

	web_custom_request("loadIsReturnableOrder", 
		"URL={pURL}/services/olm/isReturnableOrderUpdateService/loadIsReturnableOrder?_dc=1466848180140&orderId={c_CustomerOrderId}&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t32.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_10}", 
		"EXTRARES", 
		"Url=/manh/olm/resources/icons/transaction/CO_icon.png", "Referer={pURL}/manh/olm/resources/css/dom.css", "ENDITEM", 
		"LAST");

	web_convert_from_formatted("FormattedData="
			"<HP_EXTENSION name=\"JsonXml\">"
				"<object>"
					"<pair name=\"orderId\" type=\"num\">{c_CustomerOrderId}</pair>"
				"</object>"
			"</HP_EXTENSION>", 
		"TargetParam=DFE_BODY_11", 
		"LAST");

	web_custom_request("loadIsReturnableOrder_2", 
		"URL={pURL}/services/olm/isReturnableOrderUpdateService/loadIsReturnableOrder?_dc=1466848181270&orderId={c_CustomerOrderId}&page=1&start=0&limit=25", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t33.inf", 
		"Mode=HTML", 
		"EncType=application/json", 
		"Body={DFE_BODY_11}", 
		"EXTRARES", 
		"Url=/manh/olm/resources/icons/collapse/expandArrowWhite.png", "Referer={pURL}/manh/olm/resources/css/dom.css", "ENDITEM", 
		"LAST");

	lr_end_transaction(lr_eval_string("{sTestCaseName}_08_Capture_Order_Number_and_close_the_popup_message"),2);

	lr_think_time(1);

	web_url("ping.jsp_15", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1466848189906", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t34.inf", 
		"Mode=HTML", 
		"LAST");

	if ((file_stream = fopen(filename, "a+")) == 0)  
	{ 
	lr_error_message ("Cannot open %s", filename); 
	return -1; 
	}


	fprintf (file_stream, "%s\n", lr_eval_string("THE PARAMETER IS :{c_ReturnOrder},{pOrder_No},{pTimestamp}"));  

	fclose(file_stream);
	
	return 0;
}
# 5 "c:\\it\\scripts\\tcp_5_ecom_us_return_order\\\\combined_TCP_5_ECOM_US_Return_Order.c" 2

# 1 "vuser_end.c" 1
vuser_end()
{

	 
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_09_Logout"));

	web_url("user.png", 
		"URL={pURL}/manh/mps/resources/icons/64/user.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t35.inf", 
		"LAST");

	web_url("default-medium-arrow.png", 
		"URL={pURL}/manh/resources/css/gray/images/button/default-medium-arrow.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", 
		"Snapshot=t36.inf", 
		"LAST");

	web_url("mouse.png", 
		"URL={pURL}/manh/mps/resources/icons/mouse.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer={pURL}/manh/resources/css/gray/mps-gray.css", 
		"Snapshot=t37.inf", 
		"LAST");

	web_url("checkbox.png", 
		"URL={pURL}/manh/resources/css/gray/images/form/checkbox.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer={pURL}/manh/resources/css/gray/ext-theme-gray-all.css", 
		"Snapshot=t38.inf", 
		"LAST");

	web_add_cookie("ca_JSESSIONID=y77zu0NruyTpYAJKSB6Ww4Dc; DOMAIN=njisldombat03.tcphq.tcpcorp.local.com"); 

	web_url("logout", 
		"URL={pURL}/saml/logout", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t39.inf", 
		"Mode=HTML", 
		"LAST");

	lr_end_transaction(lr_eval_string("{sTestCaseName}_09_Logout"),2); 
	

	return 0;
}
# 6 "c:\\it\\scripts\\tcp_5_ecom_us_return_order\\\\combined_TCP_5_ECOM_US_Return_Order.c" 2

