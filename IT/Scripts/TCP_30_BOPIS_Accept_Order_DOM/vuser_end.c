vuser_end()
{

	web_url("ping.jsp_11",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1464774271555",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t19.inf",
		"Mode=HTML",
		LAST);

	lr_start_transaction(lr_eval_string("{sTestCaseName}_07_Logout"));

	web_url("user.png", 
		"URL={pURL}/manh/mps/resources/icons/64/user.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t20.inf", 
		LAST);

	web_url("default-medium-arrow.png", 
		"URL={pURL}/manh/resources/css/elemental/images/button/default-medium-arrow.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer={pURL}/manh/resources/css/elemental/ext-theme-elemental-all.css", 
		"Snapshot=t21.inf", 
		LAST);

	web_url("mouse.png", 
		"URL={pURL}/manh/mps/resources/icons/mouse.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer={pURL}/manh/resources/css/elemental/mps-elemental.css", 
		"Snapshot=t22.inf", 
		LAST);

	web_url("checkbox.png", 
		"URL={pURL}/manh/resources/css/elemental/images/form/checkbox.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer={pURL}/manh/resources/css/elemental/ext-theme-elemental-all.css", 
		"Snapshot=t23.inf", 
		LAST);

	web_url("logout", 
		"URL={pURL}/saml/logout", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t24.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url={pURL_SSO}/images/loading.gif", "Referer={pURL_SSO}/manh/resources/css/mip.css", ENDITEM, 
		LAST);

	web_reg_find("Text=Sign Out | Manhattan Associates Inc.", 
		LAST);

	web_url("miplogout", 
		"URL={pURL_SSO}/miplogout", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t25.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_07_Logout"),LR_AUTO);

	return 0;
}