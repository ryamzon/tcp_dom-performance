Action()
{

	/* Navigate to view Store orders */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_04_View_Store_orders"));
	
	
	web_url("ping.jsp_3",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1464774146341",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t7.inf",
		"Mode=HTML",
		LAST); 

	web_url("ping.jsp_4",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1464774161707",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t8.inf",
		"Mode=HTML",
		LAST); 
	
	web_set_user("{pUsername}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");

	web_custom_request("windows", 
		"URL={pURL}/services/rest/lps/WindowLifecycleService/windows?windowId=screen-7761257&regionId=-1&businessUnitId=-1", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t9.inf", 
		"Mode=HTML", 
		"EncType=", 
		LAST);
	
	/*Extract MANH-CSRFToken value from response. Original Value: "K0guUJ4kb1rCvkUzIZuf0/EebXD/luvbBZZpVIr7OeE="*/
		
	web_reg_save_param("cMANH_CSRFToken",
	                   "LB=<input id=\"MANH-CSRFToken\" type=\"hidden\" name=\"MANH-CSRFToken\" value=\"",
	                   "RB=\"/><script",
	                   "ORD=1",
	                   LAST);
	
	/*Extract View State value from response. Orginial Value: 4712175963253169600:-3939968574676408262*/
	web_reg_save_param("cViewState_01",
	                   "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
	                   "RB=\" autocomplete=\"",
	                   "ORD=1",
	                   LAST);
	
	/*Extract DataTable value from response. Original Value: ${cDataTable}*/
	web_reg_save_param("cDataTable",
	                   "LB=dataTable$:$",
	                   "RB=\" />",
	                   "ORD=1",
	                   LAST);
	
	/*Extract Filter Name value from response. Original Value: FL_bvinod*/
	web_reg_save_param("cFilterName",
	                   "LB=name=\"dataForm:fltrListFltrId:filterName\" value=\"",
	                   "RB=\" />",
	                   "ORD=1",
	                   LAST);
	
	/*Extract Filter Owner value from response. Original Value: bvinod*/
	web_reg_save_param("cFilterOwner",
	                   "LB=name=\"dataForm:fltrListFltrId:owner\" value=\"",
	                   "RB=\" />",
	                   "ORD=1",
	                   LAST);
	
	/*Extract StoreAliasID value from response. Original Value: 0001*/
/*	web_reg_save_param("cStoreAliasID",
	                   "LB=document.forms[this.form.id].addParam('storeAliasIdFromBOPIS','",
	                   "RB=');",
	                   "ORD=1",
	                   LAST); */
	
	/*Extract IsCancel value from response. Original Value: true*/
	web_reg_save_param("cIsCancel",
	                   "LB=hidden\" name=\"isCancel\" value=\"",
	                   "RB=\"",
	                   "ORD=ALL",
	                   LAST);
	
		/*Extract LandingPage_DistributionID value from response*/
	web_reg_save_param("cLandingPage_DistributionID",
                   "LB=tcId8\">",
                   "RB=<",
                   "ORD=ALL",
                   LAST);
	
	
	/*Extract OrderStatusCode value from response*/
	web_reg_save_param("cOrderStatusCode",
	                   "LB=orderStatusCode\" value=\"",
	                   "RB=\"",
	                   "ORD=ALL",
	                   LAST);
	
	
	/*Extract Picklist value from response*/
	web_reg_save_param("cPK",
                   "LB=/><input type=\"hidden\" value=\"",
                   "RB=\"",
                   "ORD=ALL",
                   LAST);
	
	web_url("InStorePickupSolution.xhtml", 
		"URL={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7761257", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t10.inf", 
		"Mode=HTML", 
		LAST);

	web_url("ping.jsp_5", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1464774179352", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t11.inf", 
		"Mode=HTML", 
		LAST);
	
	lr_end_transaction(lr_eval_string("{sTestCaseName}_04_View_Store_orders"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_6",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1464774194731",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t12.inf",
		"Mode=HTML",
		LAST); 
	
	/* Search Sales Order */

	lr_start_transaction(lr_eval_string("{sTestCaseName}_05_Search_Sales_Order"));

	web_url("ping.jsp_7",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1464774210083",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t13.inf",
		"Mode=HTML",
		LAST); 
	
	/*Extract View State value from response. Orginial Value: 4712175963253169600:-3939968574676408262*/
	web_reg_save_param("cViewState_02",
	                   "LB=name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"",
	                   "RB=\" autocomplete=\"",
	                   "ORD=1",
	                   LAST);
	
	/*Extract cSelectedRow value from response. Orginial Value: 205261*/
	web_reg_save_param("cSelectedRow",
	                   "LB=checkAll_c0_dataForm:OrderListPage_entityListView:releaseDataTable\" value=\"0\" /><input type=\"hidden\" value=\"",
	                   "RB=\" id=",
	                   "ORD=1",
	                   LAST);
	
	/*Extract Dtributor ID value from response. Orginial Value: 00204703 */
	web_reg_save_param("cDistributorID",
	                   "LB=\"dataForm:OrderListPage_entityListView:releaseDataTable:0:tcId8\">",
	                   "RB=</span>",
	                   "ORD=1",
	                   LAST);
	
		/*Extract Pick List ID value from response. Original Value:000000134*/
	web_reg_save_param("cPickListID",
	                   "LB=\"dataForm:OrderListPage_entityListView:releaseDataTable:0:pickListId\">",
	                   "RB=</span>",
	                   "ORD=1",
	                   LAST);
	
	web_set_user("{pUsername}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");
	
	web_custom_request("InStorePickupSolution.xhtml_2", 
		"URL={pURL}/eem/instorepickup/InStorePickupSolution.xhtml", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/xml", 
		"Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7761257", 
		"Snapshot=t14.inf", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded; charset=UTF-8", 
		"Body=AJAXREQUEST=_viewRoot&dataForm=dataForm&uniqueToken=1&MANH-CSRFToken={cMANH_CSRFToken}&helpurlEle=%2Flcom%2Fcommon%2Fjsp%2Fhelphelper.jsp%3Fserver%3D58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B%26uri%3D%252Feem%252Finstorepickup%252FInStorePickupSolution.jsflps&moreActionTargetLinkconfirmPickupMessage_ActionPanel=&moreActionButtonPressedconfirmPickupMessage_ActionPanel=&dataForm%3ApufOrderId=&dataForm%3ApufOrderNbr=&dataForm%3ApufOrderType="
		"&dataForm%3ApufDeliveryOptions=&moreActionTargetLinkpuFooter_p1=&moreActionButtonPressedpuFooter_p1=&moreActionTargetLinkpuFooter_p2=&moreActionButtonPressedpuFooter_p2=&dataForm%3AFacility_popup_Facility_Contact_dataTable_deleteHidden=&dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedRows=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AsortDir=desc&"
		"dataForm%3AFacility_popup_Facility_Contact_dataTable%3AcolCount=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableClicked=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableResized=false&Facility_popup_Facility_Contact_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs_pageallrowskey=&dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedIdList=&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs_allselectedrowskey="
		"Facility_popup_Facility_Contact_dataTable%24%3A%24{cDataTable}&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_deleteHidden=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedRows=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AsortDir=desc&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AcolCount=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AtableClicked=&"
		"dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AtableResized=false&Facility_popup_Credit_Limit_List_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_pageallrowskey=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedIdList=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_allselectedrowskey=Facility_popup_Credit_Limit_List_dataTable%24%3A%24{cDataTable}&dataForm%3ACancelReasonCode_DropDown=EX&moreActionTargetLinkRCActionPanel=&"
		"moreActionButtonPressedRCActionPanel=&dataForm%3AauditLPNIds=&moreActionTargetLinkfpp_rapanel=&moreActionButtonPressedfpp_rapanel=&moreActionTargetLinkaudit_lapanel=&moreActionButtonPressedaudit_lapanel=&dataForm%3AshipLPNIds=&moreActionTargetLinkfpp_ship_rapanel=&moreActionButtonPressedfpp_ship_rapanel=&moreActionTargetLinkship_lapanel=&moreActionButtonPressedship_lapanel=&dataForm%3Ato_StoreType=CS&dataForm%3ACreateTransferOrder_facility_InText=&dataForm%3ACreateTransferOrder_City_InText=&"
		"dataForm%3ACreateTransferOrder_ZipCode_InText=&dataForm%3ACreateTransferOrder_State_InText=&dataForm%3ApickListIdecId=&dataForm%3ApickListId=&dataForm%3AscanPickListPromptText=(scan%20pick%20list%20ID)&dataForm%3ASelectPickList_dropdown=&moreActionTargetLinkAcceptCreatePickList_btnPnl1=&moreActionButtonPressedAcceptCreatePickList_btnPnl1=&moreActionTargetLinkAcceptCreatePickList_btnPnl2=&moreActionButtonPressedAcceptCreatePickList_btnPnl2=&moreActionTargetLinkshipaddinfo_footer_panel=&"
		"moreActionButtonPressedshipaddinfo_footer_panel=&moreActionTargetLinkcolp_lapanel=&moreActionButtonPressedcolp_lapanel=&dataForm%3AfltrListFltrId%3AfieldName=&dataForm%3AfltrListFltrId%3AfilterName={cFilterName}&dataForm%3AfltrListFltrId%3Aowner={cFilterOwner}&dataForm%3AfltrListFltrId%3AobjectType=FL_FILTER&dataForm%3AfltrListFltrId%3AfilterObjectType=&dataForm%3AfltrListFltrId%3Afield0value1=&dataForm%3AfltrListFltrId%3Afield0=FILTER.FILTER_NAME&dataForm%3AfltrListFltrId%3Afield0operator=&"
		"dataForm%3AfltrListFltrId%3Afield1value1=&dataForm%3AfltrListFltrId%3Afield1=FILTER.IS_DEFAULT&dataForm%3AfltrListFltrId%3Afield1operator=&dataForm%3AfltrListFltrId%3Afield2value1=&dataForm%3AfltrListFltrId%3Afield2=FILTER.IS_PRIVATE&dataForm%3AfltrListFltrId%3Afield2operator=&dataForm%3AfltrListFltrId%3Afield3value1=&dataForm%3AfltrListFltrId%3Afield3=FILTER.OWNER&dataForm%3AfltrListFltrId%3Afield3operator=&dataForm%3AfltrListFltrId%3Afield4value1=&dataForm%3AfltrListFltrId%3Afield4="
		"FILTER.IS_DELETED&dataForm%3AfltrListFltrId%3Afield4operator=&dataForm%3AfltrListFltrId%3AfltrCondition=&dataForm%3AfltrListFltrId%3AfltrCrtSel=&windowId=screen-7761257&windowId=screen-7761257&defaultStoreAliasId={cStoreAliasID}&notesSaveAction=notesSaveAction&ajaxTabClicked=&inStorePickUpSolutionTabPanel_SubmitOnTabClick=true&inStorePickUpSolutionTabPanel_selectedTab=TAB_OrderList&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AradioSelect=quick&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_fltrExpColTxt=DONE&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrExpColState=collapsed&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrExpIconSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2Farrow_expand.gif&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrColIconSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2Farrow_collapse.gif&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrdropDownSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2FarrowDown.gif&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10=Order%20nbr&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject10=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10value1ecId=&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10value1=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20=Reference%20order&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject20=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20value1ecId=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20value1={pOrderNumber}&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield28=First%20name&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield28operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject28=INSTORE_ORDER_PO&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield28value1=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield29=Last%20name&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield29operator=%3D&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject29=INSTORE_ORDER_PO&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield29value1=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield30=Status&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield30operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject30=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield30value1=%5B%5D&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield40=Delivery%20type&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield40operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject40=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield40value1=%5B%5D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield50=Order%20Type&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield50operator=%3D&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject50=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield50value1=%5B%5D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield290=Pick%20list%20ID&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield290operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject290=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield290value1=&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3AcurrentAppliedFilterId=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_quickFilterGroupButton_mainButtonCategory=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_quickFilterGroupButton_mainButtonIndex=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_quickFilterGroupButton_changeDefault=false&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_savedFilterGroupButton_mainButtonCategory=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_savedFilterGroupButton_mainButtonIndex=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_savedFilterGroupButton_changeDefault=false&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AdummyToGetPrefix=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AfilterId=&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Aowner=&customParams%20=windowId%3Dscreen-7761257&queryPersistParameter=%26windowId%3Dscreen-7761257&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AobjectType=INSTORE_ORDER&isJSF=true&filterScreenType=ON_SCREEN&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3Apager%3ApageInput=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3ApagerBoxValue=&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AisPaginationEvent=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3ApagerAction=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_deleteHidden=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_selectedRows=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AisSortButtonClick=InStorePickupMiniDO.effectiveRankString&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AsortDir=asc&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AcolCount=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AtableClicked=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AtableResized=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3APK_0={cPK_1}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AtcDistributionOrderId={cLandingPage_DistributionID_2}&isCancel="
		"{cIsCancel_1}&isCancel={cIsCancel_2}&isCancel={cIsCancel_3}&isCancel={cIsCancel_4}&isCancel={cIsCancel_5}&isCancel={cIsCancel_6}&isCancel={cIsCancel_7}&isCancel={cIsCancel_8}&isCancel={cIsCancel_9}&isCancel={cIsCancel_10}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AorderStatusCode={cOrderStatusCode_1}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AdestinationActionCode=02&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AshipByParcel=true&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3APK_1={cPK_2}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3AtcDistributionOrderId={cLandingPage_DistributionID_3}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3AorderStatusCode"
		"={cOrderStatusCode_2}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3AshipByParcel=true&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A1%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3APK_2="
		"{cPK_3}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3AtcDistributionOrderId={cLandingPage_DistributionID_4}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3AorderStatusCode={cOrderStatusCode_3}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3AdeliveryOptionCode=01&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3AshipByParcel=true&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A2%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3APK_3={cPK_4}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3AtcDistributionOrderId"
		"={cLandingPage_DistributionID_5}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3AorderStatusCode={cOrderStatusCode_4}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3AshipByParcel=true&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A3%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3APK_4={cPK_5}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3AtcDistributionOrderId={cLandingPage_DistributionID_6}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3AorderStatusCode={cOrderStatusCode_5}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3AdoType=20&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3AshipByParcel=true&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A4%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3APK_5={cPK_6}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3AOrderList_scorInd_11=false"
		"&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3AtcDistributionOrderId={cLandingPage_DistributionID_7}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3AorderStatusCode={cOrderStatusCode_6}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3AdestinationActionCode=02&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3AshipByParcel=true&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A5%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3APK_6=2{cPK_7}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3AtcDistributionOrderId={cLandingPage_DistributionID_8}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3AorderStatusCode"
		"={cOrderStatusCode_7}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3AshipByParcel=true&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A6%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3APK_7={cPK_8}&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3AtcDistributionOrderId={cLandingPage_DistributionID_9}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3AorderStatusCode={cOrderStatusCode_8}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3AdeliveryOptionCode=01&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3AshipByParcel=true&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A7%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3APK_8={cPK_9}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3AtcDistributionOrderId"
		"={cLandingPage_DistributionID_10}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3AorderStatusCode={cOrderStatusCode_9}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3AshipByParcel=true&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A8%3ApnhFlag=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3APK_9={cPK_10}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3AtcDistributionOrderId={cLandingPage_DistributionID_11}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3AorderStatusCode={cOrderStatusCode_10}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3AdoType="
		"20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3AshipByParcel=true&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A9%3ApnhFlag=&releaseDataTable_hdnMaxIndexHldr=9&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_trs_pageallrowskey="
		"205143%23%3A%23205145%23%3A%23205153%23%3A%23205156%23%3A%23205367%23%3A%23205204%23%3A%23205206%23%3A%23205348%23%3A%23205365%23%3A%23205158%23%3A%23&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_selectedIdList=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_trs_allselectedrowskey=releaseDataTable%24%3A%24{cDataTable}&targetLink=&dataForm%3ATransferOrder_GrpBtnCnt_mainButtonCategory=-1&dataForm%3ATransferOrder_GrpBtnCnt_mainButtonIndex=-1&"
		"dataForm%3ATransferOrder_GrpBtnCnt_changeDefault=false&moreActionTargetLinkbuttonsInList_1=&moreActionButtonPressedbuttonsInList_1=&backingBeanName=&javax.faces.ViewState={cViewState_01}&fltrApplyFromQF=true&reRenderParent=AJAXOrderListPanel%2CAJAXOrderSummaryPanel&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_orderapply=dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_orderapply&fltrClientId="
		"dataForm%3AOrderListPage_entityListView%3Afilter_order&", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_05_Search_Sales_Order"),LR_AUTO);

	lr_think_time(1);

	web_url("ping.jsp_8",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1464774225447",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t15.inf",
		"Mode=HTML",
		LAST);

	web_url("ping.jsp_9",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1464774240822",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t16.inf",
		"Mode=HTML",
		LAST); 
	
		web_reg_find("Text={pTextCheck}",LAST);
	
	/* Select Sales Order and click on Accept button */

	lr_start_transaction(lr_eval_string("{sTestCaseName}_06_Select_Sales_Order_and_click_on_Accept_button"));
	
	web_set_user("{pUsername}","{pPassword}","eom-ca-perf.tcphq.tcpcorp.local.com:30000");

	web_custom_request("InStorePickupSolution.xhtml_3",
		"URL={pURL}/eem/instorepickup/InStorePickupSolution.xhtml", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/xml", 
		"Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7761257", 
		"Snapshot=t17.inf", 
		"Mode=HTML", 
		"EncType=application/x-www-form-urlencoded; charset=UTF-8", 
		"Body=AJAXREQUEST=_viewRoot&dataForm=dataForm&uniqueToken=1&MANH-CSRFToken={cMANH_CSRFToken}&helpurlEle=%2Flcom%2Fcommon%2Fjsp%2Fhelphelper.jsp%3Fserver%3D58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B%26uri%3D%252Feem%252Finstorepickup%252FInStorePickupSolution.jsflps&moreActionTargetLinkconfirmPickupMessage_ActionPanel=&moreActionButtonPressedconfirmPickupMessage_ActionPanel=&dataForm%3ApufOrderId=&dataForm%3ApufOrderNbr=&dataForm%3ApufOrderType="
		"&dataForm%3ApufDeliveryOptions=&moreActionTargetLinkpuFooter_p1=&moreActionButtonPressedpuFooter_p1=&moreActionTargetLinkpuFooter_p2=&moreActionButtonPressedpuFooter_p2=&dataForm%3AFacility_popup_Facility_Contact_dataTable_deleteHidden=&dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedRows=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AsortDir=desc&"
		"dataForm%3AFacility_popup_Facility_Contact_dataTable%3AcolCount=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableClicked=&dataForm%3AFacility_popup_Facility_Contact_dataTable%3AtableResized=false&Facility_popup_Facility_Contact_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs_pageallrowskey=&dataForm%3AFacility_popup_Facility_Contact_dataTable_selectedIdList=&dataForm%3AFacility_popup_Facility_Contact_dataTable_trs_allselectedrowskey="
		"Facility_popup_Facility_Contact_dataTable%24%3A%24{cDataTable}&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_deleteHidden=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedRows=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AisSortButtonClick=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AsortDir=desc&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AcolCount=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AtableClicked=&"
		"dataForm%3AFacility_popup_Credit_Limit_List_dataTable%3AtableResized=false&Facility_popup_Credit_Limit_List_dataTable_hdnMaxIndexHldr=0&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_pageallrowskey=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_selectedIdList=&dataForm%3AFacility_popup_Credit_Limit_List_dataTable_trs_allselectedrowskey=Facility_popup_Credit_Limit_List_dataTable%24%3A%24{cDataTable}&dataForm%3ACancelReasonCode_DropDown=EX&moreActionTargetLinkRCActionPanel=&"
		"moreActionButtonPressedRCActionPanel=&dataForm%3AauditLPNIds=&moreActionTargetLinkfpp_rapanel=&moreActionButtonPressedfpp_rapanel=&moreActionTargetLinkaudit_lapanel=&moreActionButtonPressedaudit_lapanel=&dataForm%3AshipLPNIds=&moreActionTargetLinkfpp_ship_rapanel=&moreActionButtonPressedfpp_ship_rapanel=&moreActionTargetLinkship_lapanel=&moreActionButtonPressedship_lapanel=&dataForm%3Ato_StoreType=CS&dataForm%3ACreateTransferOrder_facility_InText=&dataForm%3ACreateTransferOrder_City_InText=&"
		"dataForm%3ACreateTransferOrder_ZipCode_InText=&dataForm%3ACreateTransferOrder_State_InText=&dataForm%3ApickListIdecId=&dataForm%3ApickListId=&dataForm%3AscanPickListPromptText=(scan%20pick%20list%20ID)&dataForm%3ASelectPickList_dropdown=&moreActionTargetLinkAcceptCreatePickList_btnPnl1=&moreActionButtonPressedAcceptCreatePickList_btnPnl1=&moreActionTargetLinkAcceptCreatePickList_btnPnl2=&moreActionButtonPressedAcceptCreatePickList_btnPnl2=&moreActionTargetLinkshipaddinfo_footer_panel=&"
		"moreActionButtonPressedshipaddinfo_footer_panel=&moreActionTargetLinkcolp_lapanel=&moreActionButtonPressedcolp_lapanel=&dataForm%3AfltrListFltrId%3AfieldName=&dataForm%3AfltrListFltrId%3AfilterName={cFilterName}&dataForm%3AfltrListFltrId%3Aowner={cFilterOwner}&dataForm%3AfltrListFltrId%3AobjectType=FL_FILTER&dataForm%3AfltrListFltrId%3AfilterObjectType=&dataForm%3AfltrListFltrId%3Afield0value1=&dataForm%3AfltrListFltrId%3Afield0=FILTER.FILTER_NAME&dataForm%3AfltrListFltrId%3Afield0operator=&"
		"dataForm%3AfltrListFltrId%3Afield1value1=&dataForm%3AfltrListFltrId%3Afield1=FILTER.IS_DEFAULT&dataForm%3AfltrListFltrId%3Afield1operator=&dataForm%3AfltrListFltrId%3Afield2value1=&dataForm%3AfltrListFltrId%3Afield2=FILTER.IS_PRIVATE&dataForm%3AfltrListFltrId%3Afield2operator=&dataForm%3AfltrListFltrId%3Afield3value1=&dataForm%3AfltrListFltrId%3Afield3=FILTER.OWNER&dataForm%3AfltrListFltrId%3Afield3operator=&dataForm%3AfltrListFltrId%3Afield4value1=&dataForm%3AfltrListFltrId%3Afield4="
		"FILTER.IS_DELETED&dataForm%3AfltrListFltrId%3Afield4operator=&dataForm%3AfltrListFltrId%3AfltrCondition=&dataForm%3AfltrListFltrId%3AfltrCrtSel=&windowId=screen-7761257&defaultStoreAliasId={cStoreAliasID}&notesSaveAction=notesSaveAction&ajaxTabClicked=&inStorePickUpSolutionTabPanel_SubmitOnTabClick=true&inStorePickUpSolutionTabPanel_selectedTab=TAB_OrderList&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AradioSelect=quick&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_fltrExpColTxt=DONE&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrExpColState=collapsed&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrExpIconSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2Farrow_expand.gif&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrColIconSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2Farrow_collapse.gif&dataForm%3AOrderListPage_entityListView%3Afilter_order%3A_filtrdropDownSrc=%2Flps%2Fresources%2Fthemes%2Ficons%2Fmablue%2FarrowDown.gif&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10=Order%20nbr&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject10=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10value1ecId=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield10value1=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20=Reference%20order&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject20=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20value1ecId=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield20value1={pOrderNumber}&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield28=First%20name&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield28operator=%3D&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject28=INSTORE_ORDER_PO&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield28value1=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield29=Last%20name&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield29operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject29=INSTORE_ORDER_PO&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield29value1=&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield30=Status&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield30operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject30=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield30value1=%5B%5D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield40=Delivery%20type&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield40operator=%3D&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject40=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield40value1=%5B%5D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield50=Order%20Type&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield50operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject50=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield50value1=%5B%5D&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield290=Pick%20list%20ID&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield290operator=%3D&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AsubObject290=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afield290value1=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AcurrentAppliedFilterId=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_quickFilterGroupButton_mainButtonCategory=-1"
		"&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_quickFilterGroupButton_mainButtonIndex=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_quickFilterGroupButton_changeDefault=false&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_savedFilterGroupButton_mainButtonCategory=-1&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_savedFilterGroupButton_mainButtonIndex=-1&"
		"dataForm%3AOrderListPage_entityListView%3Afilter_order%3Afilter_order_savedFilterGroupButton_changeDefault=false&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AdummyToGetPrefix=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AfilterId=2147483647&dataForm%3AOrderListPage_entityListView%3Afilter_order%3Aowner=&customParams%20=%26%26%26&queryPersistParameter=&dataForm%3AOrderListPage_entityListView%3Afilter_order%3AobjectType=INSTORE_ORDER&isJSF=true&filterScreenType=ON_SCREEN&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3Apager%3ApageInput=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3ApagerBoxValue=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AisPaginationEvent=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3ApagerAction=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_deleteHidden=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_selectedRows={cSelectedRow}%23%3A%23&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_selectedRows=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AisSortButtonClick=InStorePickupMiniDO.effectiveRankString&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AsortDir=asc&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AcolCount=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AtableClicked=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3AtableResized=false&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_checkAll=on&releaseDataTable_hdnMaxIndexHldr=1&checkAll_c0_dataForm%3AOrderListPage_entityListView%3AreleaseDataTable=0&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3APK_0={cSelectedRow}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AOrderList_scorInd_11=false&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AtcDistributionOrderId={cDistributorID}&isCancel=false&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AorderStatusCode={cOrderStatusCode_1}&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AdoType=20&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AdeliveryOptionCode=01&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AdestinationActionCode=02&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3AshipByParcel=true&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable%3A0%3ApnhFlag=&"
		"dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_trs_pageallrowskey={cSelectedRow}%23%3A%23&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_selectedIdList=&dataForm%3AOrderListPage_entityListView%3AreleaseDataTable_trs_allselectedrowskey=releaseDataTable%24%3A%24{cDataTable}&targetLink=&dataForm%3ATransferOrder_GrpBtnCnt_mainButtonCategory=-1&dataForm%3ATransferOrder_GrpBtnCnt_mainButtonIndex=-1&dataForm%3ATransferOrder_GrpBtnCnt_changeDefault=false&"
		"moreActionTargetLinkbuttonsInList_1=&moreActionButtonPressedbuttonsInList_1=&backingBeanName=&javax.faces.ViewState={cViewState_02}&dataForm%3AacceptOrderBtn=dataForm%3AacceptOrderBtn&permissionsEL=ACCEPTSTOREORDER&", 
		LAST);

/*web_submit_data("InStorePickupSolution.xhtml_3", 
		"Action={pURL}/eem/instorepickup/InStorePickupSolution.xhtml", 
		"Method=POST", 
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/eem/instorepickup/InStorePickupSolution.xhtml?windowId=screen-7761257",
		"Snapshot=t17.inf",
		"Mode=HTML",
		ITEMDATA
		"Name=dataForm","Value=dataForm",ENDITEM,
		"Name=uniqueToken","Value=1",ENDITEM,
		"Name=MANH-CSRFToken","Value={cMANH_CSRFToken}",ENDITEM,
		"Name=helpurlEle","Value=/lcom/common/jsp/helphelper.jsp?server=58058EBCE760C1E6074D7DBF49189C94AF47D196C5FFB7A4CC672A6F120C5A5B&uri=%2Feem%2Finstorepickup%2FInStorePickupSolution.jsflps",ENDITEM,
		"Name=moreActionTargetLinkconfirmPickupMessage_ActionPanel","Value=",ENDITEM,
		"Name=moreActionButtonPressedconfirmPickupMessage_ActionPanel","Value=",ENDITEM,
		"Name=dataForm:pufOrderId","Value=",ENDITEM,
		"Name=dataForm:pufOrderNbr","Value=",ENDITEM,
		"Name=dataForm:pufOrderType","Value=",ENDITEM,
		"Name=dataForm:pufDeliveryOptions","Value=",ENDITEM,
		"Name=moreActionTargetLinkpuFooter_p1","Value=",ENDITEM,
		"Name=moreActionButtonPressedpuFooter_p1","Value=",ENDITEM,
		"Name=moreActionTargetLinkpuFooter_p2","Value=",ENDITEM,
		"Name=moreActionButtonPressedpuFooter_p2","Value=",ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_deleteHidden","Value=",ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_selectedRows","Value=",ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:isSortButtonClick","Value=",ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:sortDir","Value=desc",ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:colCount","Value=",ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:tableClicked","Value=",ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable:tableResized","Value=false",ENDITEM,
		"Name=Facility_popup_Facility_Contact_dataTable_hdnMaxIndexHldr","Value=0",ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_trs_pageallrowskey","Value=",ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_selectedIdList","Value=",ENDITEM,
		"Name=dataForm:Facility_popup_Facility_Contact_dataTable_trs_allselectedrowskey","Value=Facility_popup_Facility_Contact_dataTable$:${cDataTable}",ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_deleteHidden","Value=",ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_selectedRows","Value=",ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:isSortButtonClick","Value=",ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:sortDir","Value=desc",ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:colCount","Value=",ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:tableClicked","Value=",ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable:tableResized","Value=false",ENDITEM,
		"Name=Facility_popup_Credit_Limit_List_dataTable_hdnMaxIndexHldr","Value=0",ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_trs_pageallrowskey","Value=",ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_selectedIdList","Value=",ENDITEM,
		"Name=dataForm:Facility_popup_Credit_Limit_List_dataTable_trs_allselectedrowskey","Value=Facility_popup_Credit_Limit_List_dataTable$:${cDataTable}",ENDITEM,
		"Name=dataForm:CancelReasonCode_DropDown","Value=EX",ENDITEM,
		"Name=moreActionTargetLinkRCActionPanel","Value=",ENDITEM,
		"Name=moreActionButtonPressedRCActionPanel","Value=",ENDITEM,
		"Name=dataForm:auditLPNIds","Value=",ENDITEM,
		"Name=moreActionTargetLinkfpp_rapanel","Value=",ENDITEM,
		"Name=moreActionButtonPressedfpp_rapanel","Value=",ENDITEM,
		"Name=moreActionTargetLinkaudit_lapanel","Value=",ENDITEM,
		"Name=moreActionButtonPressedaudit_lapanel","Value=",ENDITEM,
		"Name=dataForm:shipLPNIds","Value=",ENDITEM,
		"Name=moreActionTargetLinkfpp_ship_rapanel","Value=",ENDITEM,
		"Name=moreActionButtonPressedfpp_ship_rapanel","Value=",ENDITEM,
		"Name=moreActionTargetLinkship_lapanel","Value=",ENDITEM,
		"Name=moreActionButtonPressedship_lapanel","Value=",ENDITEM,
		"Name=dataForm:to_StoreType","Value=CS",ENDITEM,
		"Name=dataForm:CreateTransferOrder_facility_InText","Value=",ENDITEM,
		"Name=dataForm:CreateTransferOrder_City_InText","Value=",ENDITEM,
		"Name=dataForm:CreateTransferOrder_ZipCode_InText","Value=",ENDITEM,
		"Name=dataForm:CreateTransferOrder_State_InText","Value=",ENDITEM,
		"Name=dataForm:pickListIdecId","Value=",ENDITEM,
		"Name=dataForm:pickListId","Value=",ENDITEM,
		"Name=dataForm:scanPickListPromptText","Value=(scan pick list ID)",ENDITEM,
		"Name=dataForm:SelectPickList_dropdown","Value=",ENDITEM,
		"Name=moreActionTargetLinkAcceptCreatePickList_btnPnl1","Value=",ENDITEM,
		"Name=moreActionButtonPressedAcceptCreatePickList_btnPnl1","Value=",ENDITEM,
		"Name=moreActionTargetLinkAcceptCreatePickList_btnPnl2","Value=",ENDITEM,
		"Name=moreActionButtonPressedAcceptCreatePickList_btnPnl2","Value=",ENDITEM,
		"Name=moreActionTargetLinkshipaddinfo_footer_panel","Value=",ENDITEM,
		"Name=moreActionButtonPressedshipaddinfo_footer_panel","Value=",ENDITEM,
		"Name=moreActionTargetLinkcolp_lapanel","Value=",ENDITEM,
		"Name=moreActionButtonPressedcolp_lapanel","Value=",ENDITEM,
		"Name=dataForm:fltrListFltrId:fieldName","Value=",ENDITEM,
		"Name=dataForm:fltrListFltrId:filterName","Value={cFilterName}",ENDITEM,
		"Name=dataForm:fltrListFltrId:owner","Value={cFilterOwner}",ENDITEM,
		"Name=dataForm:fltrListFltrId:objectType","Value=FL_FILTER",ENDITEM,
		"Name=dataForm:fltrListFltrId:filterObjectType","Value=",ENDITEM,
		"Name=dataForm:fltrListFltrId:field0value1","Value=",ENDITEM,
		"Name=dataForm:fltrListFltrId:field0","Value=FILTER.FILTER_NAME",ENDITEM,
		"Name=dataForm:fltrListFltrId:field0operator","Value=",ENDITEM,
		"Name=dataForm:fltrListFltrId:field1value1","Value=",ENDITEM,
		"Name=dataForm:fltrListFltrId:field1","Value=FILTER.IS_DEFAULT",ENDITEM,
		"Name=dataForm:fltrListFltrId:field1operator","Value=",ENDITEM,
		"Name=dataForm:fltrListFltrId:field2value1","Value=",ENDITEM,
		"Name=dataForm:fltrListFltrId:field2","Value=FILTER.IS_PRIVATE",ENDITEM,
		"Name=dataForm:fltrListFltrId:field2operator","Value=",ENDITEM,
		"Name=dataForm:fltrListFltrId:field3value1","Value=",ENDITEM,
		"Name=dataForm:fltrListFltrId:field3","Value=FILTER.OWNER",ENDITEM,
		"Name=dataForm:fltrListFltrId:field3operator","Value=",ENDITEM,
		"Name=dataForm:fltrListFltrId:field4value1","Value=",ENDITEM,
		"Name=dataForm:fltrListFltrId:field4","Value=FILTER.IS_DELETED",ENDITEM,
		"Name=dataForm:fltrListFltrId:field4operator","Value=",ENDITEM,
		"Name=dataForm:fltrListFltrId:fltrCondition","Value=",ENDITEM,
		"Name=dataForm:fltrListFltrId:fltrCrtSel","Value=",ENDITEM,
		"Name=windowId","Value=screen-7761257",ENDITEM,
		"Name=defaultStoreAliasId","Value=0001ČesSaveAction","Value=notesSaveAction",ENDITEM,
		"Name=ajaxTabClicked","Value=",ENDITEM,
		"Name=inStorePickUpSolutionTabPanel_SubmitOnTabClick","Value=true",ENDITEM,
		"Name=inStorePickUpSolutionTabPanel_selectedTab","Value=TAB_OrderList",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:radioSelect","Value=quick",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:_fltrExpColTxt","Value=DONE",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:_filtrExpColState","Value=collapsed",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:_filtrExpIconSrc","Value=/lps/resources/themes/icons/mablue/arrow_expand.gif",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:_filtrColIconSrc","Value=/lps/resources/themes/icons/mablue/arrow_collapse.gif",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:_filtrdropDownSrc","Value=/lps/resources/themes/icons/mablue/arrowDown.gif",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field10","Value=Order nbr",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field10operator","Value=","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:subObject10","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field10value1ecId","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field10value1","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field20","Value=Reference order",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field20operator","Value=","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:subObject20","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field20value1ecId","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field20value1","Value={pOrderNumber}",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field28","Value=First name",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field28operator","Value=","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:subObject28","Value=INSTORE_ORDER_PO",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field28value1","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field29","Value=Last name",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field29operator","Value=","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:subObject29","Value=INSTORE_ORDER_PO",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field29value1","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field30","Value=Status",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field30operator","Value=","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:subObject30","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field30value1","Value=[]",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field40","Value=Delivery type",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field40operator","Value=","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:subObject40","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field40value1","Value=[]",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field50","Value=Order Type",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field50operator","Value=","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:subObject50","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field50value1","Value=[]",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field290","Value=Pick list ID",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field290operator","Value=","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:subObject290","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:field290value1","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:currentAppliedFilterId","Value=-1",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:filter_order_quickFilterGroupButton_mainButtonCategory","Value=-1",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:filter_order_quickFilterGroupButton_mainButtonIndex","Value=-1",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:filter_order_quickFilterGroupButton_changeDefault","Value=false",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:filter_order_savedFilterGroupButton_mainButtonCategory","Value=-1",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:filter_order_savedFilterGroupButton_mainButtonIndex","Value=-1",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:filter_order_savedFilterGroupButton_changeDefault","Value=false",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:dummyToGetPrefix","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:filterId","Value=2147483647",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:owner","Value=",ENDITEM,
		"Name=customParams ","Value=&&&",ENDITEM,
		"Name=queryPersistParameter","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:filter_order:objectType","Value=INSTORE_ORDER",ENDITEM,
		"Name=isJSF","Value=true",ENDITEM,
		"Name=filterScreenType","Value=ON_SCREEN",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:pager:pageInput","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:pagerBoxValue","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:isPaginationEvent","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:pagerAction","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable_deleteHidden","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable_selectedRows","Value={cSelectedRow}#:#",ENDITEM,
		//"Name=dataForm:OrderListPage_entityListView:releaseDataTable_selectedRows","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:isSortButtonClick","Value=InStorePickupMiniDO.effectiveRankString",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:sortDir","Value=asc",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:colCount","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:tableClicked","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:tableResized","Value=false",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable_checkAll","Value=on",ENDITEM,
		"Name=releaseDataTable_hdnMaxIndexHldr","Value=1",ENDITEM,
		"Name=checkAll_c0_dataForm:OrderListPage_entityListView:releaseDataTable","Value=0",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:0:PK_0","Value={cSelectedRow}",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:0:OrderList_scorInd_11","Value=false",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:0:tcDistributionOrderId","Value={cDistributorID}",ENDITEM,
		"Name=isCancel","Value=false",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:0:orderStatusCode","Value=Open",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:0:doType","Value=20",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:0:deliveryOptionCode","Value=01",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:0:destinationActionCode","Value=02",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:0:shipByParcel","Value=true",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable:0:pnhFlag","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable_trs_pageallrowskey","Value={cSelectedRow}#:#",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable_selectedIdList","Value=",ENDITEM,
		"Name=dataForm:OrderListPage_entityListView:releaseDataTable_trs_allselectedrowskey","Value=releaseDataTable$:${cDataTable}",ENDITEM,
		"Name=targetLink","Value=",ENDITEM,
		"Name=dataForm:TransferOrder_GrpBtnCnt_mainButtonCategory","Value=-1",ENDITEM,
		"Name=dataForm:TransferOrder_GrpBtnCnt_mainButtonIndex","Value=-1",ENDITEM,
		"Name=dataForm:TransferOrder_GrpBtnCnt_changeDefault","Value=false",ENDITEM,
		"Name=moreActionTargetLinkbuttonsInList_1","Value=",ENDITEM,
		"Name=moreActionButtonPressedbuttonsInList_1","Value=",ENDITEM,
		"Name=backingBeanName","Value=",ENDITEM,
		"Name=javax.faces.ViewState","Value={cViewState_02}",ENDITEM,
		"Name=dataForm:acceptOrderBtn","Value=dataForm:acceptOrderBtn",ENDITEM,
		"Name=permissionsEL","Value=ACCEPTSTOREORDER",ENDITEM,
		LAST);*/

	web_url("ping.jsp_10",
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1464774256180",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Referer={pURL}/manh/index.html",
		"Snapshot=t18.inf",
		"Mode=HTML",
		LAST); 

	lr_end_transaction(lr_eval_string("{sTestCaseName}_06_Select_Sales_Order_and_click_on_Accept_button"),LR_AUTO);

	return 0;
}