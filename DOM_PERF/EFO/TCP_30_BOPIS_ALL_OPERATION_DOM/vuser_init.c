/* -------------------------------------------------------------------------------
	Script Title       : 
	Script Description : 
                        
                        
	Recorder Version   : 2739
   ------------------------------------------------------------------------------- */

vuser_init()
{

	web_cache_cleanup();
	
	web_cleanup_cookies();
	
	web_set_max_html_param_len("9999");
	
	 web_set_sockets_option("SSL_VERSION", "TLS");
	
	web_set_sockets_option("IGNORE_PREMATURE_SHUTDOWN", "1");
	
	web_set_sockets_option("CLOSE_KEEPALIVE_CONNECTIONS", "1");
	

	/* Extract SAML Request value from response. */	
	web_reg_save_param("c_SAML_Request",
	                   "LB=Request\" value=\"",
	                   "RB=\"/>",
	                   LAST);
	
	lr_save_string("TCP_30_BOPIS_ALL_OPERATIONS_DOM","sTestCaseName");
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_01_Launch"));
	
	/* Launch */
	
	web_url("index.html", 
		"URL={pURL}/manh/index.html", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTML", 
		LAST);

	web_set_sockets_option("SSL_VERSION", "TLS");

	web_submit_data("SSO", 
		"Action={pURL_SSO}/profile/SAML2/POST/SSO", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t2.inf", 
		"Mode=HTML", 
		ITEMDATA, 
		"Name=SAMLRequest", "Value={c_SAML_Request}", ENDITEM, 
		LAST);
	
	lr_end_transaction(lr_eval_string("{sTestCaseName}_01_Launch"),LR_AUTO);
	
	lr_think_time(1);

	/* Login */
	
	lr_start_transaction(lr_eval_string("{sTestCaseName}_02_Login"));

	web_reg_find("Text=Manhattan Associates", 
		LAST);

	web_submit_data("j_spring_security_check", 
		"Action={pURL_SSO}/j_spring_security_check", 
		"Method=POST", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t3.inf", 
		"Mode=HTML", 
		ITEMDATA, 
		"Name=j_username", "Value={pUsername}", ENDITEM, 
		"Name=j_password", "Value={pPassword}", ENDITEM, 
		LAST);

	web_url("ping.jsp", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1465880973543", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t4.inf", 
		"Mode=HTML", 
		LAST);

	/*Possible OAUTH authorization was detected. It is recommended to correlate the authorization parameters.*/

	web_url("ping.jsp_2", 
		"URL={pURL}/sessiontracking/ping.jsp?_dc=1465881036825", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t5.inf", 
		"Mode=HTML", 
		LAST);
	
	lr_end_transaction(lr_eval_string("{sTestCaseName}_02_Login"),LR_AUTO);
	
	lr_think_time(1);
	
		/* Select Store location */

	lr_start_transaction(lr_eval_string("{sTestCaseName}_03_Select_Store"));

	web_custom_request("submit", 
		"URL={pURL}/services/rest/eem/PostLoginService/submit?selectedValue={pStoreID}", 
		"Method=POST", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={pURL}/manh/index.html", 
		"Snapshot=t6.inf", 
		"Mode=HTML", 
		"EncType=", 
		LAST);

	lr_end_transaction(lr_eval_string("{sTestCaseName}_03_Select_Store"),LR_AUTO);
	
	return 0;
}
