vuser_end()
{

	lr_start_transaction("Logout");

	web_add_cookie("filterExpandState=true; DOMAIN=njiqlropapp01.tcphq.tcpcorp.local.com");

	web_url("user.png", 
		"URL=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/mps/resources/icons/64/user.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/index.html", 
		"Snapshot=t56.inf", 
		LAST);

	web_url("default-medium-arrow.png", 
		"URL=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/resources/css/elemental/images/button/default-medium-arrow.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/resources/css/elemental/ext-theme-elemental-all.css", 
		"Snapshot=t57.inf", 
		LAST);

	web_url("checkbox.png", 
		"URL=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/resources/css/elemental/images/form/checkbox.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/resources/css/elemental/ext-theme-elemental-all.css", 
		"Snapshot=t58.inf", 
		LAST);

	web_url("mouse.png", 
		"URL=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/mps/resources/icons/mouse.png", 
		"TargetFrame=", 
		"Resource=1", 
		"RecContentType=image/png", 
		"Referer=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/resources/css/elemental/mps-elemental.css", 
		"Snapshot=t59.inf", 
		LAST);

	web_url("logout", 
		"URL=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/saml/logout", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=http://njiqlropapp01.tcphq.tcpcorp.local.com:30000/manh/index.html", 
		"Snapshot=t60.inf", 
		"Mode=HTML", 
		EXTRARES, 
		"Url=https://njiqlropapp01.tcphq.tcpcorp.local.com:15001/images/loading.gif", "Referer=https://njiqlropapp01.tcphq.tcpcorp.local.com:15001/manh/resources/css/mip.css", ENDITEM, 
		LAST);

	web_reg_find("Text=Sign Out | Manhattan Associates Inc.", 
		LAST);

	web_url("miplogout", 
		"URL=https://njiqlropapp01.tcphq.tcpcorp.local.com:15001/miplogout", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t61.inf", 
		"Mode=HTML", 
		LAST);

	lr_end_transaction("Logout",LR_AUTO);

	lr_think_time(3);

	return 0;
}